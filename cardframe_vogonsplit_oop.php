<?php

// Vogon split frames

class CardFrame_Vogon_Split {

	////// CONSTANTS //////
	const WIDTH = 1046;
	const WIDTHHALF = 488; // 976;
	const HEIGHT = 750;

	////// PROPERTIES //////

	public $dirframe, $dirmana, $dirfont; // directories to resources
	public $q; // array holding query variables
	public $cardnamefont, $typefont, $manafont, $ptfont, $textfont, $italicfont;
	public $im, $mcircle, $textdivider, $extrabar; // images
	public $black, $white, $clear; // ImagickPixel constants
	public $typetext; // final type line

	////// METHODS //////

	function __construct($qarray) {
		// data from query
		$this->q = $qarray;

		$this->typetext = cardimage_gettypeline($this->q['supertype'], $this->q['cardtype'], $this->q['subtype'], $this->q['genre'], "— ");

		if (preg_match('/artifact/i', $this->typetext) && preg_match('/^c/', $this->q['cardback'])) {
			$this->q['cardback'] = "acard.jpg";
		}

		$this->manacosts = explode(";;", $this->q['manacost'], 2);
		$this->rulestexts = explode(";;", $this->q['rulestext']);
		$this->flavortexts = explode(";;", $this->q['flavortext']);
		$this->extratexts = explode(";;", $this->q['extra']);
		$this->cardnames = explode(";;", $this->q['cardname']);

		// file resources
		$this->dirframe = "frames/frame_" . $this->q['frame'];
		$this->textdivider = new Imagick( "$this->dirframe/horiz-divider.png" );
		$this->extrabar = new Imagick( "$this->dirframe/extrabar.png" );
		$this->dirmana = "symbols/" . ((empty($this->q['symbol'])) ? "modern" : $this->q['symbol']);
		$this->mcircle = new Imagick( "$this->dirmana/mana_circle.png");
		$this->seticon = "$this->dirframe/seticon.png";
		$this->dirfont = $_SERVER['DOCUMENT_ROOT'] . "/fonts";
		$this->cardnamefont = "$this->dirfont/kelvinch-bold.ttf";
		$this->typefont = "$this->dirfont/kelvinch-bold.ttf";
		$this->manafont = "$this->dirfont/ubuntumono-regular.ttf";
		$this->ptfont = "$this->dirfont/crimson-regular.ttf";
		$this->boldfont = "$this->dirfont/crimson-bold.ttf";
		$this->textfont = "$this->dirfont/crimson-regular.ttf";
		$this->italicfont = "$this->dirfont/crimson-italic.ttf";
		$this->bolditalicfont = "$this->dirfont/crimson-bolditalic.ttf";
		$this->dyratio = 1.0;

		// drawing resources
		$this->black = new ImagickPixel("black");
		$this->white = new ImagickPixel("white");
		$this->clear = new ImagickPixel("transparent");
		$this->draw = new ImagickDraw();
		$this->manadraw = new ImagickDraw();
		$this->im = new Imagick();
		$this->im->newImage($this::WIDTH, $this::HEIGHT, $this->black, "png");
		$this->extrabars = [];
	}

	function drawBackground() {
		// background base

		$cardback = "$this->dirframe/" . $this->q['cardback'];
		if (file_exists($cardback)) {
			$bg = new Imagick($cardback);
			$this->im->compositeImage($bg, imagick::COMPOSITE_OVER, 0, 0);
		}
	}

	function drawBlend() {
		// find color of each side from mana costs
		// then print card frames and mana symbols

		$x = 70;
		$y = 0;
		$cardback = "$this->dirframe/" . $this->q['cardback'];
		for($i=0; $i<2; $i++) {
			if (in_array($this->q['genre'], array('magic','space','earth') )) {
				$this->blendoptions = cardfuncs_getblendoptions($this->manacosts[$i] ?? '', ($this->rulestexts[$i] ?? '') . ($this->extratexts[$i+1] ?? ''));

				$options = [
					'width' => $this::WIDTHHALF
					,'height' => $this::HEIGHT
					,'black' => $this->black
					,'blendstartrate' => 0.1
					,'blendendrate' => 0.9
					,'contrast' => 2.0
					,'midtone' => 0.0
				];

				if ($this->q['color']=='q') {
					$options['blendframemask'] = "blend_frame_mask.png";
				}
				$oldim = $this->im;
				$this->im = new Imagick($cardback);
				$this->im = cardfuncs_renderblend($options, $this);

				$oldim->compositeImage($this->im, imagick::COMPOSITE_OVER, $x, $y);
				$this->im = $oldim;
			}

			// save the extra bar for later pasting
			$this->extrabars[$i] = $this->im->getImageRegion(408, 42, 40 + $x, 389);

			$x += $this::WIDTHHALF;
		}
	}

	function drawArt() {
		// art

		if ($this->q['arturl']!="") {
			// split art URLs
			$arts = explode(";;", $this->q['arturl'], 2);

			$x = 112;
			$y = 57;
			for($i=0, $ct=count($arts); $i<$ct; $i++) {
				$this->q['arturl'] = $arts[$i];

				$options = [
					'artx' => $x
					,'arty' => $y
					,'artwidth' => 404
					,'artheight' => 373
				];
				cardfuncs_makeArtImage($options, $this);

				// extra bar over art
				if (isset($this->extratexts[$i+1]) && trim($this->extratexts[$i+1]) != '') {
					$this->extrabars[$i]->setImageAlpha(0.6);
					$this->im->compositeImage($this->extrabars[$i], imagick::COMPOSITE_OVER, $x - 2, 389);
				}

				$x += $this::WIDTHHALF;
			}
		}
	}

	function drawArtistCreator() {
		// artist and creator

		$options = [
			'font' => $this->typefont
			,'fontsize' => 21.0
			,'textx' => 48
			,'texty' => 375
			,'wmax' => $this::HEIGHT
			,'text' => "Illus. by " . $this->q['artist']
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
			,'textalign' => imagick::ALIGN_CENTER
			,'angle' => 90.0
		];

		cardfuncs_makeLinearText($options, $this);

		$options = [
			'font' => $this->typefont
			,'fontsize' => 21.0
			,'textx' => 20
			,'texty' => 375
			,'wmax' => $this::HEIGHT
			,'text' => "Created by " . $this->q['creator']
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
			,'textalign' => imagick::ALIGN_CENTER
			,'angle' => 90.0
		];

		cardfuncs_makeLinearText($options, $this);

	}

	function drawWatermark() {
		// watermark

		$wms = preg_split('/;;/', $this->q['wmurl'] . ';;');

		$x = 314;
		$y = 590;

		for ($i=0; $i<2; $i++) {
			$options = [
				'wmx' => $x
				,'wmy' => $y
				,'wmw' => 404
				,'wmh' => 311
				,'wmurl' => $wms[$i]
			];

			cardfuncs_makeWatermark($options, $this);

			$x += $this::WIDTHHALF;
		}
	}

	function drawManaCost() {
		// write mana cost, simplified, align right

		$x = 543;
		$i = 0;
		$this->mcbox = [];
		foreach ($this->manacosts as $manacost) {
			$this->q['manacost'] = $manacost;
			$options = [
				'font' => $this->manafont
				,'fontsize' => 28.0
				,'fontcolor' => $this->black
				,'manacostx' => $x
				,'manacosty' => 14
				,'manaiconwidth' => 28
				,'manaiconheight' => 28
				,'width' => $this::WIDTHHALF
				,'height' => 28
			];
			$this->mcbox[++$i] = cardfuncs_makeManaCost($options, $this);

			$x += $this::WIDTHHALF;
		}
	}

	function drawCardName() {
		// write card name

		$this->cardnames = preg_split('/;;/', $this->q['cardname'], 2);


		$x = 80;
		$i = 0;

		foreach ($this->cardnames as $cardname) {
			$iconx = 38;
			try {
				if (isset($this->typeicons[$i])) {
					$iconx = $this->typeicons[$i]->getimagewidth() + 5;
				}
			}
			catch (Exception $e) {
				cclog( "\n" . '$icon error : ' . $e);
			}
			$options = [
				'font' => $this->cardnamefont
				,'fontsize' => 32.0
				,'textx' => $x + $iconx
				,'texty' => 29
				,'wmax' => 470 - $this->mcbox[++$i]['width'] - $iconx
				,'text' => $cardname
				,'textcolor' => $this->black
			];
			cardfuncs_makeLinearText($options, $this);

			$x += $this::WIDTHHALF;
		}
	}

	function drawType() {
		// write card type

		// split card type
		$typetexts = explode(";;", $this->typetext, 2);

		$x = 528;
      $y = 67;

		for ($i=0, $ct=count($typetexts); $i<$ct; $i++) {
			// trim extra spaces and dashes
			$typetexts[$i] = preg_replace("/[-—][-—]+/", "—", $typetexts[$i]);
			$typetexts[$i] = trim($typetexts[$i]);
			$typetexts[$i] = trim($typetexts[$i], "—");
			$typetexts[$i] = trim($typetexts[$i]);

			$options = [
				'font' => $this->typefont
				,'fontsize' => 18.0
				,'textx' => $x
				,'texty' => $y
				,'wmax' => 670
				,'text' => $typetexts[$i]
				,'textcolor' => $this->black
            ,'textalign' => imagick::ALIGN_LEFT
            ,'angle' => 90.0
			];
			cardfuncs_makeLinearText($options, $this);

			$x += $this::WIDTHHALF;
		}

	}

	function drawPT() {
		// write power toughness

		$pts = explode(";;", $this->q['powertoughness'], 2);
		for ($i=0, $ct=count($pts); $i<$ct; $i++) {
			$pts[$i] = trim($pts[$i]);
		}

		$x = 84;
		$y = 67;

		for ($i=0, $ct=count($pts); $i<$ct; $i++) {
			if ($pts[$i] != '') {
				$options = [
					'font' => $this->ptfont
					,'fontsize' => 22.0
					,'textx' => $x
					,'texty' => $y
					,'wmax' => 576
					,'text' => $pts[$i]
					,'textcolor' => $this->black
					,'angle' => 90.0
				];
				cardfuncs_makeLinearText($options, $this);
			}

			$x += $this::WIDTHHALF;
		}
	}

	function drawRulesText() {
		// write rules and flavor texts

		cardfuncs_conjoined_cardframe_op($this->q, ['extra', 'rulestext', 'flavortext'], ["cardfuncs_replaceMemtext"]);

		$x = 112;
		$y = 435;
		$textboxwidth = 404;
		$textboxheight = 311;

		// extra text; will affect remaining rules box
		if ($this->extratexts[0] != '') {
			$this->extratexts[0] = cardfuncs_replaceCardTraits($this->extratexts[0], $this->q);
			$this->im->compositeImage($this->extrabar, imagick::COMPOSITE_OVER, 111, 708);
			$extraheight = 40;
			$options = [
				'textfont' => $this->textfont
				,'italicfont' => $this->italicfont
				,'fontsize' => 34.0
				,'minfontsize' => 22.0
				,'textletterx' => 0
				,'textlettery' => 0
				,'textboxx' => 114
				,'textboxy' => 708
				,'textboxwidth' => 893
				,'textboxheight' => $extraheight
				,'rulestext' => $this->extratexts[0]
			];
			cardfuncs_makeRulesbox($options, $this);
			$textboxheight -= $extraheight;
		}

		// actual rules text
		for ($i=0, $ct=min(count($this->rulestexts), 2); $i<$ct; $i++) {
			if ($this->rulestexts[$i] != '') {
				$q = $this->q;
				$q['cardname'] = $this->cardnames[$i];
				$this->rulestexts[$i] = trim(cardfuncs_replaceCardTraits($this->rulestexts[$i], $q));

				$options = [
					'textfont' => $this->textfont
					,'italicfont' => $this->italicfont
					,'fontsize' => 40.0
					,'minfontsize' => 12.0
					,'textletterx' => 2
					,'textlettery' => 4
					,'textboxx' => $x
					,'textboxy' => $y
					,'textboxwidth' => $textboxwidth
					,'textboxheight' => $textboxheight
					,'rulestext' => $this->rulestexts[$i]
					,'flavortext' => $this->flavortexts[$i] ?? ''
				];
				cardfuncs_makeRulesbox($options, $this);
			}
			$x += $this::WIDTHHALF;
		}

		// extra text per half
		$x = 117;
		$y = 395;
		for ($i=1, $ct=count($this->extratexts); $i<$ct; $i++) {
			if (isset($this->extratexts[$i]) && $this->extratexts[$i] != '') {
				$q = $this->q;
				$q['cardname'] = $this->cardnames[$i-1];
				$this->extratexts[$i] = trim(cardfuncs_replaceCardTraits($this->extratexts[$i], $q));

				$options = [
					'textfont' => $this->textfont
					,'italicfont' => $this->italicfont
					,'fontsize' => 40.0
					,'minfontsize' => 12.0
					,'textletterx' => 0
					,'textlettery' => 0
					,'textboxx' => $x
					,'textboxy' => $y
					,'textboxwidth' => 394
					,'textboxheight' => 30
					,'rulestext' => $this->extratexts[$i]
					,'flavortext' => ''
				];
				cardfuncs_makeRulesbox($options, $this);
			}
			$x += $this::WIDTHHALF;
		}
	}

	function drawSetIcon() {
		// draw set icon, with rarity colors filled

		$options = [
			'w' => 54
			,'h' => 54
			,'x' => 742
			,'y' => 8
			,'angle' => 90.0
		];

		cardfuncs_makeSetIcon($options, $this);
	}

	function drawTypeIcon() {
		// write card type icon

		$icons = preg_split('/;;/', $this->q['typeiconurl'] . ';;');

		$x = 78;
		$y = 12;

		$this->typeicons = [];

		for ($i=0; $i<2; $i++) {
			$options = [
				'x' => $x
				,'y' => $y
				,'w' => 32
				,'h' => 32
				,'typeiconurl' => $icons[$i]
			];

			$this->typeicons[$i] = cardfuncs_makeTypeIcon($options, $this);

			$x += $this::WIDTHHALF;
		}
	}

	function drawOverlay() {
		// overlays

		$ovs = preg_split('/;;/', $this->q['overlayurl'] . ';;');

		$x = 70;
		$y = 0;

		for ($i=0; $i<2; $i++) {
        $options = [
            'ovx' => $x
            ,'ovy' => $y
            ,'ovw' => $this::WIDTHHALF
            ,'ovh' => $this::HEIGHT
				,'overlayurl' => $ovs[$i]
        ];

			cardfuncs_makeOverlay($options, $this);

			$x += $this::WIDTHHALF;
		}
	}

	function createFinalImage() {
		// start creation of image
		//$this->drawBackground();
		$this->drawBlend();
		$this->drawArt();
		$this->drawWatermark();
		$this->drawSetIcon();
		$this->drawManaCost();
		$this->drawTypeIcon();
		$this->drawCardName();
		$this->drawType();
		$this->drawPT();
		$this->drawRulesText();
		$this->drawArtistCreator();
		$this->drawOverlay();

		/* Output the image*/
		return $this->im;
	}
} // end class def

$cardframe = new CardFrame_Vogon_Split($qarray);
?>
