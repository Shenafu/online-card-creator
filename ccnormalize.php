<?php
error_reporting(E_ALL); ini_set('display_errors', 1);
ini_set('memory_limit','256M');

function build_query($qarray, $table, $mysqli) {
	$query = "REPLACE INTO $table (";

	$vars = [
		'cardname',
		'creator',
		'manacost',
		'powertoughness',
		'extra',
		'rulestext',
		'flavortext',
		'rarity',
		'seticonurl',
		'wmurl',
		'arturl',
		'artist',
		'color',
		'border',
		'frame',
		'symbol',
		'genre',
		'supertype',
		'cardtype',
		'subtype',
	];
	foreach ($vars as $key) {
		$query .= "\n";
		$query .= $key;
		$query .= ",";
	}

	$query = rtrim($query, ',');
	$query .= ") VALUES (";

	foreach ($vars as $key) {
		if (isset($qarray[$key])) {
		$query .= "\n'";
		$query .= $mysqli->real_escape_string($qarray[$key]);
		$query .= "',";
		}
		else {
			$query .= "\n'',";
		}
	}

	$query = rtrim($query, ',');
	$query .= ");";
	return $query;
}


include_once "../magic/cccreds.php"; // holds $sqlhost, $sqluser, $sqlpass, $sqldb, $sqltbl, $tblnormal

function connect_db() {
	global $sqlhost, $sqluser, $sqlpass, $sqldb;
	$mysqli = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
	if ($mysqli->connect_errno) {
		printf("Connect failed: %s\n", $mysqli->connect_error);
		exit();
	}
	//echo 'Connected successfully';
	$mysqli->set_charset("utf8_unicode_ci");

	return $mysqli;
}

$mysqli = connect_db();
$oldt = $sqltbl;
$newt = $tblnormal;

function import_data($mysqli, $oldt, $newt) {
	$query = "SELECT * FROM " . $oldt;

	/*
	// debug
	$limit = " LIMIT 12020, 20;";
	$query .= $limit;
	//*/

	$result = $mysqli->query($query);

	// Check result
	// This shows the actual query sent to MySQL, and the error. Useful for debugging.
	if (!$result) {
		$message  = 'Invalid query: ' . $mysqli->error() . "\n";
		$message .= 'Whole query: ' . $query;
		die($message);
	}

	echo "<p>REPLACING ROWS.</p>";

	while ($row = $result->fetch_assoc()) {
		$urlquery = $row['c2i_string'];
		parse_str($urlquery, $qarray);
		$urlquery = null;
		unset($urlquery);

		$newquery = build_query($qarray, $newt, $mysqli);
		$qarray = null;
		unset($qarray);
		$sqlerr = $mysqli->query($newquery);

		if (!$sqlerr) {
			$message  = '<pre>Invalid query: ' . $mysqli->error . '</pre>';
			$message .= '<pre>Whole query: ' . $newquery . '</pre>';
			//die($message);
		}
		$newquery = null;
		unset($newquery);
		$row = null;
		unset($row);
	}
	$result->free();
	$result = null;
	unset($result);
}

import_data($mysqli, $oldt, $newt);

function update_fields($mysqli, $table) {

	echo "<p>UPDATING FIELDS.</p>";
	$queries = [
"UPDATE $table SET creator=' ' WHERE creator='';",
"UPDATE $table SET rarity='1' WHERE rarity='undefined';",
"UPDATE $table SET rarity='1' WHERE rarity='';",
"UPDATE $table SET color='c' WHERE color='undefined';",
"UPDATE $table SET color='c' WHERE color='';",
"UPDATE $table SET border='' WHERE border='undefined';",
"UPDATE $table SET frame='classicshift' WHERE frame='undefined';",
"UPDATE $table SET frame='classicshift' WHERE frame='';",
"UPDATE $table SET genre='magic' WHERE genre='undefined';",
"UPDATE $table SET genre='magic' WHERE genre='';",
	];
	foreach ($queries as $query) {
		$sqlerr = $mysqli->query($query);
		if (!$sqlerr) {
			$message  = 'Invalid query: ' . $mysqli->error . "\n";
			$message .= 'Whole query: ' . $query;
			die($message);
		}
	}
}

update_fields($mysqli, $newt);

echo "<p>NORMALIZATION COMPLETE.</p>";
?>