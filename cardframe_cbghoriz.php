<?php

// CBG Horizontal frames

class CardFrame_CBGHoriz {
	
	////// CONSTANTS //////
	const WIDTH = 523;
	const HEIGHT = 375;
	
	////// PROPERTIES //////
	
	public $dirframe, $dirmana, $dirfont; // directories to resources
	public $q; // array holding query variables
	public $cardnamefont, $typefont, $manafont, $ptfont, $textfont, $italicfont;
	public $im, $mcircle, $textdivider; // images
	public $black, $white, $clear; // ImagickPixel constants
	public $typetext; // final type line

	////// METHODS //////
	
	function __construct($qarray) {
		// data from query
		$this->q = $qarray;
		
		$this->typetext = cardimage_gettypeline($this->q['supertype'], $this->q['cardtype'], $this->q['subtype'], $this->q['genre'], "— ");

		$this->q['cardback'] = str_replace("lcard", 'card', $this->q['cardback']);		
		if (preg_match('/artifact/i', $this->typetext) && preg_match('/^c/', $this->q['cardback'])) {
			$this->q['cardback'] = "acard.jpg";
		}
		
		$this->q['rulestext'] = $this->q['extra'] . "\n". $this->q['rulestext'];

		// file resources
		$this->dirframe = "frames/frame_" . $this->q['frame'];
		$this->textdivider = new Imagick( "$this->dirframe/horiz-divider.png" );
		$this->dirmana = "symbols/" . ((empty($this->q['symbol'])) ? "modern" : $this->q['symbol']);
		$this->mcircle = new Imagick( "$this->dirmana/mana_circle.png");
		$this->seticon = "$this->dirframe/seticon.png";
		$this->dirfont = $_SERVER['DOCUMENT_ROOT'] . "/fonts";
		$this->cardnamefont = "$this->dirfont/kelvinch-bold.ttf";
		$this->typefont = "$this->dirfont/kelvinch-bold.ttf";
		$this->manafont = "$this->dirfont/ubuntumono-regular.ttf";
		$this->ptfont = "$this->dirfont/crimson-regular.ttf";
		$this->boldfont = "$this->dirfont/crimson-bold.ttf";
		$this->textfont = "$this->dirfont/crimson-regular.ttf";
		$this->italicfont = "$this->dirfont/crimson-italic.ttf";
		$this->bolditalicfont = "$this->dirfont/crimson-bolditalic.ttf";
		$this->dyratio = 1.0;
	
		// drawing resources
		$this->black = new ImagickPixel("black");
		$this->white = new ImagickPixel("white");
		$this->clear = new ImagickPixel("transparent");	
		$this->draw = new ImagickDraw();
		$this->manadraw = new ImagickDraw();
		$this->im = new Imagick();
		$this->im->newImage($this::WIDTH, $this::HEIGHT, $this->white, "png");
	}		

	function drawBackground() {
		// background base

        $cardback = "$this->dirframe/" . $this->q['cardback'];
		if (file_exists($cardback)) {
			$bg = new Imagick($cardback);
            $this->im->compositeImage($bg, imagick::COMPOSITE_OVER, 0, 0);
        }
	}
	
	function drawBlend() {
		// blending for multicolor and hybrid

		$this->blendoptions = cardfuncs_getblendoptions($this->q['manacost'], $this->q['rulestext']);
		$options = [
			'width' => $this::WIDTH
			,'height' => $this::HEIGHT
			,'black' => $this->black
			,'blendstartrate' => 0.4
			,'blendendrate' => 0.6
			,'contrast' => 20
			,'midtone' => 0.166
		];
		$this->im = cardfuncs_renderblend($options, $this);
	}
	
	function drawArt() {
		// art
		
		$options = [
			'artx' => 33
			,'arty' => 64
			,'artwidth' => 197
			,'artheight' => 279
		];
		
		$boxx = 23;
		$boxy = 55;

		cardfuncs_makeArtImage($options, $this);
		$artbox = new Imagick($this->dirframe . "/image_box.png");
		$this->im->compositeImage($artbox, imagick::COMPOSITE_OVER, $boxx, $boxy);
	}
	
	function drawArtistCreator() {
		// artist and creator

		$options = [
			'font' => $this->typefont
			,'fontsize' => 10.0
			,'textx' => 253
			,'texty' => 332
			,'wmax' => $this::WIDTH
			,'text' => "Illus. by " . $this->q['artist']
			,'textcolor' => $this->white
		];

		cardfuncs_makeLinearText($options, $this);
		
		$options = [
			'font' => $this->typefont
			,'fontsize' => 10.0
			,'textx' => 253
			,'texty' => 343
			,'wmax' => $this::WIDTH
			,'text' => "Created by " . $this->q['creator']
			,'textcolor' => $this->white
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawWatermark() {
		// watermark
		
		$options = [
			'wmx' => 374
			,'wmy' => 198
			,'wmw' => 227
			,'wmh' => 251
		];
		
		cardfuncs_makeWatermark($options, $this);
	}
	
	function drawManaCost() {
		// write mana cost, simplified, align right

		$options = [
			'font' => $this->manafont
			,'fontsize' => 19.0
			,'fontcolor' => $this->black
			,'manacostx' => 492
			,'manacosty' => 28
			,'manaiconwidth' => 18
			,'manaiconheight' => 18
			,'width' => 375
			,'height' => 18
		];
		
		$this->mcbox = cardfuncs_makeManaCost($options, $this);
	}

	function drawCardName() {
		// write card name

		$options = [
			'font' => $this->cardnamefont
			,'fontsize' => 19.0
			,'textx' => 80
			,'texty' => 36
			,'wmax' => 410 - $this->mcbox['width']
			,'text' => $this->q['cardname']
			,'textcolor' => $this->white
		];
		
		cardfuncs_makeLinearText($options, $this);
	}

	function drawType() {
		// write card type

		$options = [
			'font' => $this->typefont
			,'fontsize' => 13.0
			,'textx' => 252
			,'texty' => 58
			,'wmax' => 218
			,'text' => $this->typetext
			,'textcolor' => $this->white
		];
		
		cardfuncs_makeLinearText($options, $this);		
	}

	function drawPT() {
		// write power toughness

		// write power / toughness, centered
		$options = [
			'font' => $this->ptfont
			,'fontsize' => 17.0
			,'textx' => 49
			,'texty' => 38
			,'wmax' => 40
			,'text' => $this->q['powertoughness']
			,'textcolor' => $this->white
			,'textalign' => imagick::ALIGN_CENTER
		];
		
		cardfuncs_makeLinearText($options, $this);
	}
	
	function drawRulesText() {	
		// write rules and flavor texts
		
		cardfuncs_conjoined_cardframe_op($this->q, ['rulestext', 'flavortext'], ["cardfuncs_replaceMemtext", "cardfuncs_replaceCardTraits"]);

		$options = [
			'textfont' => $this->textfont
			,'boldfont' => $this->boldfont
			,'italicfont' => $this->italicfont
			,'bolditalicfont' => $this->bolditalicfont
			,'fontsize' => 25.0
			,'minfontsize' => 6.0
			,'textletterx' => 0
			,'textlettery' => 0
			,'textboxx' => 254
			,'textboxy' => 73
			,'textboxwidth' => 225
			,'textboxheight' => 251
			,'rulestext' => $this->q['rulestext']
			,'flavortext' => $this->q['flavortext']
		];
	
		cardfuncs_makeRulesbox($options, $this);
	}
	
	function drawSetIcon() {
		// draw set icon, with rarity colors filled
		
		$options = [
			'w' => 21
			,'h' => 21
			,'x' => 473
			,'y' => 52
			,'align' => Imagick::ALIGN_LEFT
			,'keepratio' => false
		];

		cardfuncs_makeSetIcon($options, $this);
	}
	
	function drawOverlay() {
        $options = [
            'ovx' => 18
            ,'ovy' => 18
            ,'ovw' => 488
            ,'ovh' => 340
        ];

		cardfuncs_makeOverlay($options, $this);
	}

	function createFinalImage() {
		// start creation of image
		$this->drawBackground();
		$this->drawBlend();
		$this->drawArt();
		$this->drawWatermark();
		$this->drawSetIcon();
		$this->drawManaCost();
		$this->drawCardName();
		$this->drawType();
		$this->drawPT();
		$this->drawRulesText();
		$this->drawArtistCreator();
		$this->drawOverlay();
		
		/* Output the image*/
		return $this->im;
	}
} // end class def

$cardframe = new CardFrame_CBGHoriz($qarray);
?>
