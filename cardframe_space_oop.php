<?php

// space frames

class CardFrame_Space {

	////// CONSTANTS //////
	const WIDTH = 375;
	const HEIGHT = 523;

	////// PROPERTIES //////

	public $dirframe, $dirmana, $dirfont; // directories to resources
	public $q; // array holding query variables
	public $cardnamefont, $typefont, $manafont, $ptfont, $textfont, $italicfont;
	public $im, $mcircle, $textdivider; // images
	public $black, $white, $clear; // ImagickPixel constants
	public $typetext; // final type line

	////// METHODS //////

	function __construct($qarray) {
		// data from query
		$this->q = $qarray;

		$this->typetext = cardimage_gettypeline($this->q['supertype'], $this->q['cardtype'], $this->q['subtype'], $this->q['genre'], "— ");

		if (preg_match('/artifact/i', $this->typetext) && preg_match('/^c/', $this->q['cardback'])) {
			$this->q['cardback'] = "acard.jpg";
		}

		$this->q['rulestext'] = $this->q['extra'] . "\n". $this->q['rulestext'];

		// file resources
		$this->dirframe = "frames/frame_" . $this->q['frame'];
		$this->textdivider = new Imagick( "$this->dirframe/horiz-divider.png" );
		$this->dirmana = "symbols/" . ((empty($this->q['symbol'])) ? "space" : $this->q['symbol']);
		$this->mcircle = new Imagick( "$this->dirmana/mana_circle.png");
		$this->seticon = "$this->dirframe/seticon.png";
		$this->dirfont = $_SERVER['DOCUMENT_ROOT'] . "/fonts";
		$this->cardnamefont = "$this->dirfont/kelvinch-bold.ttf";
		$this->typefont = "$this->dirfont/kelvinch-bold.ttf";
		$this->manafont = "$this->dirfont/ubuntumono-regular.ttf";
		$this->ptfont = "$this->dirfont/crimson-regular.ttf";
		$this->boldfont = "$this->dirfont/crimson-bold.ttf";
		$this->textfont = "$this->dirfont/crimson-regular.ttf";
		$this->italicfont = "$this->dirfont/crimson-italic.ttf";
		$this->bolditalicfont = "$this->dirfont/crimson-bolditalic.ttf";
		$this->dyratio = 1.0;

		// drawing resources
		$this->black = new ImagickPixel("black");
		$this->white = new ImagickPixel("white");
		$this->clear = new ImagickPixel("transparent");
		$this->draw = new ImagickDraw();
		$this->manadraw = new ImagickDraw();
		$this->im = new Imagick();
		$this->im->newImage($this::WIDTH, $this::HEIGHT, $this->white, "png");
	}

	function drawBackground() {
		// background base

        $cardback = "$this->dirframe/" . $this->q['cardback'];
		if (file_exists($cardback)) {
			$bg = new Imagick($cardback);
            $this->im->compositeImage($bg, imagick::COMPOSITE_OVER, 0, 0);
        }
	}

	function drawBlend() {
		// blending for multicolor and hybrid

		$this->blendoptions = cardfuncs_getblendoptions($this->q['manacost'], $this->q['rulestext']);
		$options = [
			'width' => $this::WIDTH
			,'height' => $this::HEIGHT
			,'black' => $this->black
			,'blendstartrate' => 0.4
			,'blendendrate' => 0.6
			,'contrast' => 20
			,'midtone' => 0.166
		];
		$this->im = cardfuncs_renderblend($options, $this);
	}

	function drawArt() {
		// art

		$options = [
			'artx' => 29
			,'arty' => 42
			,'artwidth' => 320
			,'artheight' => 231
			,'mask' => $this->dirframe . '/mask_image.png'
		];

		cardfuncs_makeArtImage($options, $this);
	}

	function drawArtistCreator() {
		// artist and creator

		$options = [
			'font' => $this->typefont
			,'fontsize' => 7.0
			,'angle' => 270
			,'textx' => 18
			,'texty' => 502
			,'wmax' => $this::WIDTH
			,'text' => "Illus. by " . $this->q['artist']
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
		];

		cardfuncs_makeLinearText($options, $this);

		$options = [
			'font' => $this->typefont
			,'fontsize' => 7.0
			,'angle' => 270
			,'textx' => 351
			,'texty' => 502
			,'wmax' => $this::WIDTH
			,'text' => "Created by " . $this->q['creator']
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawWatermark() {
		// watermark

		$options = [
			'wmx' => 187
			,'wmy' => 379
			,'wmw' => 310
			,'wmh' => 142
		];

		cardfuncs_makeWatermark($options, $this);
	}

	function drawManaCost() {
		// write mana cost, simplified, align right

		$manapos = [
			[256, 40]
			,[274, 58]
			,[292, 76]
			,[310, 94]
			,[328, 112]
		];

		$options = [
			'font' => $this->manafont
			,'fontsize' => 24.0
			,'fontcolor' => $this->black
			,'manaiconwidth' => 24
			,'manaiconheight' => 24
			,'manapos' => $manapos
		];

		cardfuncs_makeManaCost($options, $this);
	}

	function drawCardName() {
		// write card name
		$x = 28;
		try {
			if (isset($this->typeicon)) {
				$x += $this->typeicon->getimagewidth() + 5;
			}
		}
		catch (Exception $e) {
			cclog( "\n" . '$icon error : ' . $e);
		}

		$options = [
			'font' => $this->cardnamefont
			,'fontsize' => 16.0
			,'textx' => $x
			,'texty' => 30
			,'wmax' => 338 - $x
			,'text' => mb_strtoupper($this->q['cardname'])
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawType() {
		// write card type

		$options = [
			'font' => $this->typefont
			,'fontsize' => 16.0
			,'textx' => 26
			,'texty' => 285
			,'wmax' => 293
			,'text' => mb_strtoupper($this->typetext)
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawPT() {
		// write power toughness

		$pts = explode('/', $this->q['powertoughness'], 2);
		for ($i=0, $ct=count($pts); $i<$ct; $i++) {
			$pts[$i] = trim($pts[$i]);
		}

		if ($pts[0] != '') {
			$options = [
				'font' => $this->ptfont
				,'fontsize' => 22.0
				,'textx' => 166
				,'texty' => 488
				,'wmax' => 305
				,'text' => $pts[0]
				,'textcolor' => $this->white
				,'shadowcolor' => $this->black
				,'textalign' => imagick::ALIGN_RIGHT
			];

			cardfuncs_makeLinearText($options, $this);
		}
		if (isset($pts[1]) && $pts[1] != '') {
			$options = [
				'font' => $this->ptfont
				,'fontsize' => 22.0
				,'textx' => 222
				,'texty' => 488
				,'wmax' => 305
				,'text' => $pts[1]
				,'textcolor' => $this->white
				,'shadowcolor' => $this->black
				,'textalign' => imagick::ALIGN_LEFT
			];

			cardfuncs_makeLinearText($options, $this);
		}
	}

	function drawRulesText() {
		// write rules and flavor texts

		cardfuncs_conjoined_cardframe_op($this->q, ['rulestext', 'flavortext'], ["cardfuncs_replaceMemtext", "cardfuncs_replaceCardTraits"]);

		$options = [
			'textfont' => $this->textfont
			,'italicfont' => $this->italicfont
			,'fontsize' => 26.0
			,'minfontsize' => 6.0
			,'textletterx' => 0
			,'textlettery' => 0
			,'textboxx' => 37
			,'textboxy' => 304
			,'textboxwidth' => 302
			,'textboxheight' => 156
			,'rulestext' => $this->q['rulestext']
			,'flavortext' => $this->q['flavortext']
		];

		cardfuncs_makeRulesbox($options, $this);
	}

	function drawSetIcon() {
		// draw set icon, with rarity colors filled

		$options = [
			'w' => 18
			,'h' => 18
			,'x' => 351
			,'y' => 274
		];

		cardfuncs_makeSetIcon($options, $this);
	}

	function drawTypeIcon() {
		// write card type icon
		$options = [
			'x' => 18
			,'y' => 18
			,'w' => 20
			,'h' => 20
		];

		$this->typeicon = cardfuncs_makeTypeIcon($options, $this);
	}

	function drawOverlay() {
        $options = [
            'ovx' => 15
            ,'ovy' => 13
            ,'ovw' => 347
            ,'ovh' => 495
        ];

		cardfuncs_makeOverlay($options, $this);
	}

	function createFinalImage() {
		// start creation of image
		$this->drawBackground();
		//$this->drawBlend();
		$this->drawArt();
		$this->drawWatermark();
		$this->drawSetIcon();
		$this->drawTypeIcon();
		$this->drawCardName();
		$this->drawManaCost();
		$this->drawType();
		$this->drawPT();
		$this->drawRulesText();
		$this->drawArtistCreator();
		$this->drawOverlay();

		/* Output the image*/
		return $this->im;
	}
} // end class def

$cardframe = new CardFrame_Space($qarray);
?>
