<?php

function fix_creator($creator) {
	if (!isset($creator) || $creator=='') {
		return ' ';
	}
	return $creator;
}

function retrieve($cardname, $creator) {
    include_once "cccreds.php"; // holds $sqlhost, $sqluser, $sqlpass, $sqldb, $sqltbl

	$mysqli = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
	if ($mysqli->connect_errno) {
		printf("Connect failed: %s\n", $mysqli->connect_error);
		exit();
	}
	//echo 'Connected successfully';

	$cn = $mysqli->real_escape_string($cardname);
	$cr = $mysqli->real_escape_string($creator);
	$query = "call get_card('$cn', '$cr');";
	$result = $mysqli->query($query);

	// Check result
	// This shows the actual query sent to MySQL, and the error. Useful for debugging.
	if (!$result) {
		$message  = 'Invalid query: ' . $mysqli->error . "\n";
		$message .= 'Whole query: ' . $query;
		die($message);
	}

	$row = $result->fetch_assoc();
	$urlquery = '';
	if ($row) {
		foreach ($row as $key=>$value) {
			$urlquery .= "$key=" . rawurlencode($value) . "&";
		}
		unset($row);
	}
	return $urlquery;
}

?>