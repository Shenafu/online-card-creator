<?php
$CC_DEBUG = false; // true | false;

function cclog( $message ) {
	global $CC_DEBUG;
	if ( $CC_DEBUG ) {
		error_log( $message, 3, "cc.log" );
	}
}

?>