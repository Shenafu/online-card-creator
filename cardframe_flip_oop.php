<?php

// flip frames

class CardFrame_Flip {

	////// CONSTANTS //////
	const WIDTH = 375;
	const HEIGHT = 523;

	////// PROPERTIES //////

	public $dirframe, $dirmana, $dirfont; // directories to resources
	public $q; // array holding query variables
	public $cardnamefont, $typefont, $manafont, $ptfont, $textfont, $italicfont;
	public $im, $mcircle; // images
	public $black, $white, $clear; // ImagickPixel constants
	public $typetext; // final type line

	////// METHODS //////

	function __construct($qarray) {
		// data from query
		$this->q = $qarray;

		$this->typetext = cardimage_gettypeline($this->q['supertype'], $this->q['cardtype'], $this->q['subtype'], $this->q['genre'], "— ");

		if (preg_match('/artifact/i', $this->typetext) && preg_match('/^c/', $this->q['cardback'])) {
			$this->q['cardback'] = "acard.jpg";
		}

		// file resources
		$this->dirframe = "frames/frame_" . $this->q['frame'];
		$this->dirmana = "symbols/" . ((empty($this->q['symbol'])) ? "modern" : $this->q['symbol']);
		$this->mcircle = new Imagick( "$this->dirmana/mana_circle.png");
		$this->seticon = "$this->dirframe/seticon.png";
		$this->dirfont = $_SERVER['DOCUMENT_ROOT'] . "/fonts";
		$this->cardnamefont = "$this->dirfont/kelvinch-bold.ttf";
		$this->typefont = "$this->dirfont/kelvinch-regular.ttf";
		$this->manafont = "$this->dirfont/ubuntumono-regular.ttf";
		$this->ptfont = "$this->dirfont/crimson-regular.ttf";
		$this->boldfont = "$this->dirfont/crimson-bold.ttf";
		$this->textfont = "$this->dirfont/crimson-regular.ttf";
		$this->italicfont = "$this->dirfont/crimson-italic.ttf";
		$this->bolditalicfont = "$this->dirfont/crimson-bolditalic.ttf";
		$this->dyratio = 1.0;

		// drawing resources
		$this->black = new ImagickPixel("black");
		$this->white = new ImagickPixel("white");
		$this->clear = new ImagickPixel("transparent");
		$this->draw = new ImagickDraw();
		$this->manadraw = new ImagickDraw();
		$this->im = new Imagick();
		$this->im->newImage($this::WIDTH, $this::HEIGHT, $this->white, "png");
	}

	function drawBackground() {
		// background base

        $cardback = "$this->dirframe/" . $this->q['cardback'];
		if (file_exists($cardback)) {
			$bg = new Imagick($cardback);
            $this->im->compositeImage($bg, imagick::COMPOSITE_OVER, 0, 0);
        }
	}

	function drawBlend() {
		// blending for multicolor and hybrid

		$this->blendoptions = cardfuncs_getblendoptions($this->q['manacost'], $this->q['rulestext']);
		$options = [
			'width' => $this::WIDTH
			,'height' => $this::HEIGHT
			,'black' => $this->black
			,'blendstartrate' => 0.4
			,'blendendrate' => 0.6
			,'contrast' => 20
			,'midtone' => 0.166
		];
		$this->im = cardfuncs_renderblend($options, $this);
	}

	function drawArt() {
		// art

		$options = [
			'artx' => 32
			,'arty' => 162
			,'artwidth' => 311
			,'artheight' => 182
		];

		cardfuncs_makeArtImage($options, $this);
	}

	function drawArtistCreator() {
		// artist and creator

		$options = [
			'font' => $this->typefont
			,'fontsize' => 12.0
			,'textx' => 60
			,'texty' => 494
			,'wmax' => 289
			,'text' => "Illus. by " . $this->q['artist']
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
		];

		cardfuncs_makeLinearText($options, $this);

		$options = [
			'font' => $this->typefont
			,'fontsize' => 12.0
			,'textx' => 60
			,'texty' => 515
			,'wmax' => 289
			,'text' => "Created by " . $this->q['creator']
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawWatermark() {
		// watermark

		$options = [
			'wmx' => 187
			,'wmy' => 93
			,'wmw' => 300
			,'wmh' => 58
		];

		cardfuncs_makeWatermark($options, $this);
	}

	function drawManaCost() {
		// write mana cost, simplified, align right

		$options = [
			'font' => $this->manafont
			,'fontsize' => 18.0
			,'fontcolor' => $this->black
			,'manacostx' => 343
			,'manacosty' => 33
			,'manaiconwidth' => 18
			,'manaiconheight' => 18
			,'width' => 375
			,'height' => 18
		];

		$this->mcbox = cardfuncs_makeManaCost($options, $this);
	}

	function drawCardName() {
		// write card name

		$this->cardnames = preg_split('/;;/', $this->q['cardname'], 2);

		$x = 34;
		try {
			if (isset($this->typeicon)) {
				$x += $this->typeicon->getimagewidth();
			}
		}
		catch (Exception $e) {
			//cclog( "\n" . '$icon error : ' . $e);
		}

		$options = [
			'font' => $this->cardnamefont
			,'fontsize' => 20.0
			,'textx' => $x
			,'texty' => 41
			,'wmax' => 339 - $this->mcbox['width'] - $x
			,'text' => $this->cardnames[0]
			,'textcolor' => $this->black
		];

		cardfuncs_makeLinearText($options, $this);

		$options = [
			'font' => $this->cardnamefont
			,'fontsize' => 20.0
			,'angle' => 180
			,'textx' => 341
			,'texty' => 463
			,'wmax' => 305
			,'text' => $this->cardnames[1]
			,'textcolor' => $this->black
            ,'textalign' => imagick::ALIGN_RIGHT
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawType() {
		// write card type

		if ($this->typetext != "") {
			// split card type
			$typetexts = explode(";;", $this->typetext, 2);

			// trim extra spaces and dashes
			for ($i=0, $ct=count($typetexts); $i<$ct; $i++) {
				$typetexts[$i] = preg_replace("/[-—][-—]+/", "—", $typetexts[$i]);
				$typetexts[$i] = trim($typetexts[$i]);
				$typetexts[$i] = trim($typetexts[$i], "—");
				$typetexts[$i] = trim($typetexts[$i]);
			}

			$options = [
				'font' => $this->typefont
				,'fontsize' => 16.0
				,'textx' => 35
				,'texty' => 143
				,'wmax' => 250
				,'text' => $typetexts[0]
				,'textcolor' => $this->black
			];

			cardfuncs_makeLinearText($options, $this);

			if (isset($typetexts[1])) {

				$options = [
					'font' => $this->typefont
					,'fontsize' => 18.0
					,'angle' => 180
					,'textx' => 341
					,'texty' => 363
					,'wmax' => 250
					,'text' => $typetexts[1]
					,'textcolor' => $this->black
					,'textalign' => imagick::ALIGN_RIGHT
				];

				cardfuncs_makeLinearText($options, $this);
			}
		}
	}

	function drawPT() {
		// write power toughness

		$pts = explode(";;", $this->q['powertoughness']);
		for ($i=0, $ct=count($pts); $i<$ct; $i++) {
			$pts[$i] = trim($pts[$i]);
		}

		if (isset($pts[0]) && $pts[0] != "") {
			$ptfile = preg_replace('/^(.*)card.jpg/', '/$1pt.jpg', $this->q['cardback']);
			$ptfile = "$this->dirframe/$ptfile";
			if (file_exists($ptfile)) {
				 $ptimg = new Imagick($ptfile);
				 $this->im->compositeImage($ptimg, imagick::COMPOSITE_OVER, 286, 123);
			}

			$options = [
				'font' => $this->ptfont
				,'fontsize' => 22.0
				,'textx' => 319
				,'texty' => 142
				,'wmax' => 45
				,'text' => $pts[0]
				,'textcolor' => $this->black
				,'textalign' => imagick::ALIGN_CENTER
			];

			cardfuncs_makeLinearText($options, $this);
		}

		if (isset($pts[1]) && $pts[1] != "") {
			$ptfile = preg_replace('/^(.*)card.jpg/', '/$1pt2.jpg', $this->q['cardback']);
			$ptfile = "$this->dirframe/$ptfile";
			if (file_exists($ptfile)) {
				 $ptimg = new Imagick($ptfile);
				 $this->im->compositeImage($ptimg, imagick::COMPOSITE_OVER, 17, 342);
			}

			$options = [
				'font' => $this->ptfont
				,'fontsize' => 22.0
				,'angle' => 180
				,'textx' => 52
				,'texty' => 362
				,'wmax' => 45
				,'text' => $pts[1]
				,'textcolor' => $this->black
				,'textalign' => imagick::ALIGN_CENTER
			];

			cardfuncs_makeLinearText($options, $this);
		}
	}

	function drawRulesText() {
		// write rules and flavor texts

        $this->q['rulestext'] = $this->q['extra'] . "\n". $this->q['rulestext'];

		cardfuncs_conjoined_cardframe_op($this->q, ['rulestext', 'flavortext'], ["cardfuncs_replaceMemtext"]);

		$rulestexts = explode(";;", $this->q['rulestext'], 2);
		$cardnames = explode(";;", $this->q['cardname']);
		$pts = explode(";;", $this->q['powertoughness']);
		for ($i=0, $ct=count($rulestexts); $i<$ct; $i++) {
			$q = $this->q;
			$q['cardname'] = $cardnames[$i];
			$q['powertoughness'] = isset($pts[$i]) ? $pts[$i] : "";
			$rulestexts[$i] = trim(cardfuncs_replaceCardTraits($rulestexts[$i], $q));

			if ($rulestexts[$i] != '') {
				$options = [
					'textfont' => $this->textfont
					,'boldfont' => $this->boldfont
					,'italicfont' => $this->italicfont
					,'bolditalicfont' => $this->bolditalicfont
					,'fontsize' => 20.0
					,'minfontsize' => 6.0
					,'angle' => $i==0 ? 0 : 180
					,'textletterx' => 2
					,'textlettery' => 4
					,'textboxx' => $i==0 ? 32 : 341
					,'textboxy' => $i==0 ? 61 : 445
					,'textboxwidth' => 307
					,'textboxheight' => 67
					,'rulestext' => $rulestexts[$i]
					,'flavortext' => ''
				];

				cardfuncs_makeRulesbox($options, $this);
			}
		}
	}

	function drawSetIcon() {
		// draw set icon, with rarity colors filled

		$options = [
			'w' => 18
			,'h' => 18
			,'x' => 353
			,'y' => 485
		];

		cardfuncs_makeSetIcon($options, $this);
	}

	function drawTypeIcon() {
		// write card type icon
		$options = [
			'x' => 30
			,'y' => 30
			,'w' => 20
			,'h' => 20
		];


		$this->typeicon = cardfuncs_makeTypeIcon($options, $this);
	}

	function drawOverlay() {
        $options = [
            'ovx' => 17
            ,'ovy' => 17
            ,'ovw' => 340
            ,'ovh' => 488
        ];

		cardfuncs_makeOverlay($options, $this);
	}

	function createFinalImage() {
		// start creation of image
		$this->drawBackground();
		//$this->drawBlend();
		$this->drawArt();
		$this->drawWatermark();
		$this->drawSetIcon();
		$this->drawManaCost();
		$this->drawTypeIcon();
		$this->drawCardName();
		$this->drawType();
		$this->drawPT();
		$this->drawRulesText();
		$this->drawArtistCreator();
		$this->drawOverlay();

		/* Output the image*/
		return $this->im;
	}
} // end class def

$cardframe = new CardFrame_Flip($qarray);
?>
