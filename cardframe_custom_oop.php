<?php

// custom frames

class CardFrame_Custom {
	////// PROPERTIES //////

	public $cardw, $cardh, $border, $bottombar; // card dimensions
	public $tint, $tints; // color definitions

	public $q; // array holding query variables
	public $von, $section; // VSV holding custom frame data
	public $dirframe, $dirmana, $dirfont; // directories to resources
	public $cardnamefont, $typefont, $manafont, $ptfont, $textfont, $italicfont;
	public $im, $mcircle, $textdivider; // images
	public $black, $white, $legendcolor, $clear; // ImagickPixel constants
	public $typetext; // final type line
	public $saveddir, $savedfile, $savedurl; // save image for backup
	public $artbevel, $artbevelpw; // bevel around art
	public $split_saved_fields = [
		'art' => 'arturl',
		'watermark' => 'wmurl',
		'seticon' => 'seticonurl',
		'typeicon' => 'typeiconurl',
		'overlay' => 'overlayurl',
		'text' => 'text',
		'rules' => ['rulestext', 'flavortext'],
		'cost' => 'manacost',
	];

	public $default_fields = [
		'art' => ['arturl' => '<-arturl->'],
		'watermark' => ['wmurl' => '<-wmurl->'],
		'seticon' => ['seticonurl' => '<-seticonurl->'],
		'typeicon' => ['typeiconurl' => '<-typeiconurl->'],
		'overlay' => ['overlayurl' => '<-overlayurl->'],
		'rules' => ['rulestext' => '<-rulestext->', 'flavortext' => '<-flavortext->'],
		'rulespw' => ['rulestext' => '<-rulestext->', 'flavortext' => '<-flavortext->'],
		'ruleslevel' => ['rulestext' => '<-rulestext->', 'flavortext' => '<-flavortext->'],
		'cost' => ['manacost' => '<-manacost->'],
	];

	////// METHODS //////

	function __construct($qarray) {
		// define basic card dimensions
		$this->cardw = 750;
		$this->cardh = 1046;

		// data from query
		$this->q = $qarray;

		// file resources
		$this->dirframe = "frames/frame_" . $this->q['frame'];
		$this->textdivider = new Imagick( "$this->dirframe/horiz-divider.png" );
		$this->dirmana = "symbols/" . ((empty($this->q['symbol'])) ? "modern" : $this->q['symbol']);
		$this->mcircle = new Imagick( "$this->dirmana/mana_circle.png");
		$this->seticon = "$this->dirframe/seticon.png";
		$this->dirfont = $_SERVER['DOCUMENT_ROOT'] . "/fonts";
		$this->cardnamefont = "$this->dirfont/kelvinch-bold.ttf";
		$this->typefont = "$this->dirfont/kelvinch-bold.ttf";
		$this->manafont = "$this->dirfont/ubuntumono-regular.ttf";
		$this->ptfont = "$this->dirfont/crimson-regular.ttf";
		$this->boldfont = "$this->dirfont/crimson-bold.ttf";
		$this->textfont = "$this->dirfont/crimson-regular.ttf";
		$this->italicfont = "$this->dirfont/crimson-italic.ttf";
		$this->bolditalicfont = "$this->dirfont/crimson-bolditalic.ttf";
		$this->dyratio = 1.0;

		// drawing resources
		$this->black = new ImagickPixel("black");
		$this->white = new ImagickPixel("white");
		$this->legendcolor = new ImagickPixel("#9400D3"); // crimson #DC143C ; darkviolet #9400D3 ;
		$this->clear = new ImagickPixel("transparent");
		$this->halfclear = new ImagickPixel("#ffffff00");

		$this->tints = array(
			"c" => "#C0C0C0",
			"w" => "#FFFFFF",
			"u" => "#99CCFF",
			"b" => "#000000",
			"r" => "#DC143C",
			"g" => "#6B8E23",
			"m" => "#D4AF37",
			"q" => "#FFC0CB",
			"a" => "#F4DAC9",
			"o" => "#FF7F00",
			"y" => "#FFFF00",
			"k" => "#45E0DA",
			"p" => "#932AA1",
			"t" => "#70341A",
		);

		$this->draw = new ImagickDraw();
		$this->manadraw = new ImagickDraw();
		$this->artbevel = new Imagick( "$this->dirframe/art_bevel.png");
		$this->artbevelpw = new Imagick( "$this->dirframe/art_bevel_pw.png");

		$this->saveddir = "saved/";
		$this->savedfile = "";
		$this->savedurl = "https://ieants.cc/magic/saved/";

		$this->draw_baseimage();
		// data from VSV file
		$this->section = "meta";
		$this->von = $this->retrieve_custom_data($this->q['customframe']);
	}

	function retrieve_custom_data($url) {
		// retrieve data from VON file

		$isFile = stripos(trim($url), 'http');

		if ($isFile !== false && $isFile == 0) {
			// check URL with cURL

			$url = trim($url);

			$ch = @curl_init($url);
			if($ch === false){
				return false;
			}

			$timeout = 60; // seconds
			@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	// catch output (do NOT print!)
			@curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);	// prevent waiting forever to get a result

			$content = @curl_exec($ch);
			$header = curl_getinfo($ch);
			curl_close($ch);

/*
			cclog("\n\n");
			cclog('$content = ');
			cclog(print_r($content, true));
			//*/

			// VON or JSON
			if (stristr($url, '.von')) {
				spl_autoload("vsv_oop");
				$von = new vsv($content);
				$von = $von->mapFromVon();
				return $von;
			}
			elseif ($header['content_type'] == 'application/json') {
				$von = json_decode($content, false);
				return $von;
			}
		}

		return;
	}

	function parseCustomData() {
		// execute each line in VSV
		$von = $this->von;
		foreach ($von as $section) {
			/*
			cclog("\n\n");
			cclog('$section = ');
			cclog(print_r($section, true));
			//*/

			if (isset($section->section)) {
				$this->section = $section->section;
			}
			$bRun = true;
			if (property_exists($section, 'if')) {
				$bRun = $this->if_and($section->if);
			}
			if ($bRun) {
				$func = 'draw_' . $this->section;
				if (method_exists($this, $func)) {
					$this->split_options($section, $func);
				}
			}
		}
	}

	function split_options($section, $func) {
		// split the text or field into parts
		// delimiter is double semicolon ;;
		// each part can be drawn in different location on card
		$sectiontype = $section->section;
		$split = $section->split ?? [];
		if (property_exists($section, 'options')) {
			array_unshift($split, $section->options);
		}
		if (!count($split)) {
			// if missing "options", insert empty object
			// to allow later foreach to call the $func
			$split[] = (object)[];
		}

		if (!isset($this->split_saved_fields[$sectiontype])) {
			// if missing fields, means section can't be split
			// so just call the func and exit
			$this->$func($split[0]);
			return;
		}

		$this->fill_default_fields($split[0], $sectiontype);

		$fields = $this->split_saved_fields[$sectiontype];
		if (!is_array($fields)) {
			$fields = [$fields];
		}

		$parts = (object)[];

		foreach ($fields as $f) {
			if (!empty($split[0])) {
				$parts->$f = explode(';;',
				property_exists($split[0], $f) ? $this->replace_prop($split[0], $f) : '',
				count($split));
			}
		}
		$options = (object)[];

		for ($i=0; $i<count($split); $i++) {
			$opt = $split[$i];
			$options = (object)array_merge((array)$options, (array)$opt);
			foreach ($fields as $f) {
				$options->$f = $parts->$f[$i] ?? '';
			}
			$this->$func($options);
		}
	}

	function fill_default_fields(&$options, $sectiontype) {
		if (isset($this->default_fields[$sectiontype])) {
			foreach ($this->default_fields[$sectiontype] as $field => $value) {
				if (!property_exists($options, $field)) {
					$options->$field = $value;
				}
			}
		}
	}

	function draw_baseimage() {
		// image base
		$this->im = new Imagick();
		$this->tint = $this->tints[$this->q['color']];
		$this->im->newImage($this->cardw, $this->cardh, $this->tint, "png");
	}

	function draw_meta($options) {
		foreach ($options as $key => $value) {
			switch ($key) {
				case 'tints':
					$this->tints = array_merge($this->tints, (array)$value);
					break;

				case 'custommana':
					storeCustomMana($value);
					storeCustomMana($this->q['custommana']);
					break;

				default:
					$this->$key = $value;
			}
		}
		$this->draw_baseimage();
	}

	function draw_background($options) {
		// background base
		$color = $this->q['color'];
		if ($this->q['border'] != '') {
			$color .= $this->q['border'];
		}

		if (property_exists($options->cardbacks, $color)) {
			$url = $options->cardbacks->$color;
			// add http://
			if (0==preg_match("/^http(s)?:\/\//", $url)) {
				$url = "http://$url";
			}
			if (isValidUrl($url)) {
				$art = new Imagick($url);
				$art->thumbnailImage($this->cardw, $this->cardh);
				$this->im->compositeImage($art, imagick::COMPOSITE_OVER, 0, 0);
			}
		}
	}

	function draw_blend($options) {
		// blending for multicolor and hybrid
		// only for Magic/Space genres

	}

	function draw_art($options) {
		// art
		$this->replace_prop($options, 'arturl');
		cardfuncs_makeArtImage($options, $this);
	}

	function draw_watermark($options) {
		// watermark
			$this->replace_prop($options, 'wmurl');
			cardfuncs_makeWatermark($options, $this);
	}

	function draw_seticon($options) {
		// draw set icon, with rarity colors filled
		$this->set_align($options, 'align');
		$this->replace_prop($options, 'seticonurl');
		cardfuncs_makeSetIcon($options, $this);
	}

	function draw_typeicon($options) {
		// draw type icon

		global $GENRES;

		$this->set_align($options, 'align');
		if (property_exists($options, 'typeiconurl')) {
			$this->replace_prop($options, 'typeiconurl');
		}
		else if ($this->q['typeiconurl'] == '') {
			if (property_exists($options, 'genres')) {
				$genre = $this->q['genre'];
				if (property_exists($options->genres, $genre)) {
					$cardtype = $this->q['cardtype'];
					$type2icon = $GENRES[$genre]['types'];
					// negate useless type = tribal
					$cardtype = '0' . substr($cardtype, 1, count($type2icon));

					if (intval($cardtype) == 0) {
						// no card type assigned
						return;
					}

					// try default type icon
					if (substr_count($cardtype, '1') > 1) {
						// multiple types
						$icontype = 'multiple';
					}
					else {
						// single type
						$icontype = strtolower($type2icon[ strpos($cardtype, '1') ]);
					}
					if (property_exists($options->genres->$genre, $icontype)) {
						$this->q['typeiconurl'] = $options->genres->$genre->$icontype;
					}
				}
			}
		}
		cardfuncs_makeTypeIcon($options, $this);
	}

	function draw_overlay($options) {
		// overlay
		$this->replace_prop($options, 'overlayurl');
		cardfuncs_makeOverlay($options, $this);
	}

	function draw_text($options) {
		// write linear text
		if (property_exists($options, 'text') && !empty(trim($options->text))) {
			$this->replace_prop($options, 'text');
			$this->set_align($options, 'textalign');
			$this->set_font($options, 'font');

			cardfuncs_makeLinearText($options, $this);
		}
	}

	function draw_textlevel($options) {
		// write text on levels, divided by double semicolons ;;
		// runs through a loop per partition

		$origtext = $this->replace_prop($options, 'text');
		$matches = preg_split("/;;/", $origtext);
		$numtotal = count($matches);
		if ($options->addflavor && !empty($this->q['flavortext'])) {
			$numtotal++;
		}
		$wmax = $options->wmax ?? 400;
		$strokeWidth = $options->strokew ?? 5;
		$abilh = $numtotal ? ($wmax - ($numtotal - 1) * $strokeWidth) / $numtotal : 0;

		$this->set_font($options, 'font');
		$this->set_align($options, 'textalign');
		switch ($options->textalign ?? imagick::ALIGN_LEFT) {
			case imagick::ALIGN_LEFT:
				$dalign = 0.0;
				break;

			case imagick::ALIGN_CENTER:
				$dalign = 0.5;
				break;

			case imagick::ALIGN_RIGHT:
				$dalign = 1.0;
				break;
		}

		$angle = $options->angle ?? 0;
		switch ($angle) {
			case 0:
				$dx = $abilh + $strokeWidth;
				$dy = 0;
				$options->textx = ($options->textx ?? 0) + $dx * $dalign;
				$options->texty = $options->texty ?? 0;
				break;

			case 180:
				$dx = -($abilh + $strokeWidth);
				$dy = 0;
				$options->textx = ($options->textx ?? 0) + $dx * $dalign;
				$options->texty = $options->texty ?? 0;
				break;

			case 90:
				$dx = 0;
				$dy = $abilh + $strokeWidth;
				$options->textx = $options->textx ?? 0;
				$options->texty = ($options->texty ?? 0) + $dy * $dalign;
				break;

			case 270:
				$dx = 0;
				$dy = $abilh + $strokeWidth;
				$options->textx = $options->textx ?? 0;
				$options->texty = ($options->texty ?? 0) + $dy * $dalign;
				break;

		}

		foreach ($matches as $i => $ti) {
			$options->text = $ti;
			cardfuncs_makeLinearText($options, $this);

			$options->textx += $dx;
			$options->texty += $dy;
		}
	}

	function draw_cost($options) {
		// cost (mana)
		$this->set_align($options, 'manaalign');
		$this->set_font($options, 'font');
		cardfuncs_makeManaCost($options, $this);
	}

	function draw_rules($options) {
		// write rules for regular card
		cardfuncs_conjoined_cardframe_op($this->q, ['extra', 'rulestext', 'flavortext'], ["cardfuncs_replaceMemtext", "cardfuncs_replaceCardTraits"]);
		$this->replace_prop($options, 'rulestext');
		$this->replace_prop($options, 'flavortext');
		$this->set_font($options, 'textfont');
		$this->set_font($options, 'boldfont');
		$this->set_font($options, 'italicfont');
		$this->set_font($options, 'bolditalicfont');
		cardfuncs_makeRulesbox($options, $this);
	}

	function draw_rulespw($options) {
		// write rules for planeswalker
		cardfuncs_conjoined_cardframe_op($this->q, ['extra', 'rulestext', 'flavortext'], ["cardfuncs_replaceMemtext", "cardfuncs_replaceCardTraits"]);
		$this->replace_prop($options, 'rulestext');
		$this->replace_prop($options, 'flavortext');
		$this->set_font($options, 'textfont');
		$this->set_font($options, 'boldfont');
		$this->set_font($options, 'italicfont');
		$this->set_font($options, 'bolditalicfont');
		cardfuncs_makeRulesboxPW($options, $this);
	}

	function draw_ruleslevel($options) {
		// write rules for levelers, sagas, partitioned rules
		cardfuncs_conjoined_cardframe_op($this->q, ['extra', 'rulestext', 'flavortext'], ["cardfuncs_replaceMemtext", "cardfuncs_replaceCardTraits"]);
		$this->replace_prop($options, 'rulestext');
		$this->replace_prop($options, 'flavortext');
		$this->set_font($options, 'textfont');
		$this->set_font($options, 'boldfont');
		$this->set_font($options, 'italicfont');
		$this->set_font($options, 'bolditalicfont');
		cardfuncs_makeRulesboxLeveler($options, $this);
	}

	function draw_saveimage($options) {
		$this->delete_temp();

		// temporarily save current image
		$filename = $this->randhash() . ".png";
		$this->savedfile = $this->saveddir . $filename;
		$this->savedurl .= $filename;
		saveFinalImage($this->im, $this->savedfile);
	}

	function get_prop($prop) {
		switch ($prop) {
			// fix common prop errors
			case 'pt':
			case 'loyalty':
			case 'stat':
				$prop = 'powertoughness';
				break;
			case 'name':
				$prop = 'cardname';
				break;
			case 'cost':
			case 'mana':
				$prop = 'manacost';
				break;
			case 'rules':
				$prop = 'rulestext';
				break;
			case 'flavor':
				$prop = 'flavortext';
				break;
			default:
		}
		switch ($prop) {
			case 'supertype':
				$prop = cardfuncs_getsupertype($this->q['supertype'], $this->q['genre']);
				break;
			case 'cardtype':
				$prop = cardfuncs_getcardtype($this->q['cardtype'], $this->q['genre']);
				break;
			default:
				$prop = isset($this->q[$prop]) ? $this->q[$prop] : NULL;
		}
		return $prop;
	}

	function replace_prop(&$options, $prop) {
		if (property_exists($options, $prop)) {
			$text = $options->$prop;
			$text = preg_replace_callback('/\<-(.*?)-\>/',
				function($matches) {
					$m = $matches[1];
					switch ($m) {
						case 'type':
							$s = cardimage_gettypeline($this->q['supertype'], $this->q['cardtype'], $this->q['subtype'], $this->q['genre'], "— ");
							break;
						case 'savedimage':
							$s = $this->savedurl;
							break;
						default:
							$s = $this->get_prop($m);
					}
					return isset($s) ? $s : '';
				}
			, $text);
			$options->$prop = $text;
			return $text;
		}
	}

	function set_align(&$options, $prop) {
		if (property_exists($options, $prop)) {
			switch ($options->$prop) {
				case Imagick::ALIGN_RIGHT:
				case 'right':
					$options->$prop = Imagick::ALIGN_RIGHT;
					break;
				case Imagick::ALIGN_LEFT:
				case 'left':
					$options->$prop = Imagick::ALIGN_LEFT;
					break;
				case Imagick::ALIGN_CENTER:
				case 'center':
					$options->$prop = Imagick::ALIGN_CENTER;
					break;
				default:
					$options->$prop = '';
					break;
			}
		}
	}

	function set_font(&$options, $prop) {
		// find font file in font dir
		if (property_exists($options, $prop)) {
			if (file_exists($options->$prop)) {
				return;
			}
			$fontfile = $this->dirfont . "/" . $options->$prop;
			if (file_exists($fontfile)) {
				$options->$prop = $fontfile;
			}
			else {
				unset($options->$prop);
			}
		}
	}

	function if_and($args) {
		$bRes = true;
		for ($i=0; $bRes && $i<count($args); $i++) {
			$arg = $args[$i];
			$func = 'if_' . array_shift($arg);
			$bRes = method_exists($this, $func) && $this->$func($arg);
		}
		return $bRes;
	}

	function if_or($args) {
		$bRes = false;
		for ($i=0; !$bRes && $i<count($args); $i++) {
			$arg = $args[$i];
			$func = 'if_' . array_shift($arg);
			$bRes = method_exists($this, $func) && $this->$func($arg);
		}
		return $bRes;
	}

	function if_not($args) {
		$arg = $args[0];
		$func = 'if_' . array_shift($arg);
		$bRes = method_exists($this, $func) && $this->$func($arg);
		return !$bRes;
	}

	function if_iscardtype($args) {
		$want = filter_var($args[0], FILTER_VALIDATE_BOOLEAN);
		$type = $args[1];
		$res = substr($this->q['cardtype'], $type, 1) == '1';
		return $want == $res;
	}

	function if_issupertype($args) {
		$want = filter_var($args[0], FILTER_VALIDATE_BOOLEAN);
		$type = $args[1];
		$res = substr($this->q['supertype'], $type, 1) == '1';
		return $want == $res;
	}

	function if_isintext($args) {
		$want = filter_var($args[0], FILTER_VALIDATE_BOOLEAN);
		$prop = $this->get_prop($args[1]);
		try {
			$pattern = $args[2];
			$res = preg_match($pattern, $prop);
		}
		catch (Exception $e) {
			// invalid prop or pattern
			return;
		}
		return $want == $res;
	}

	function randhash() {
		return strtr(base64_encode(random_bytes(9)), '+/', '-_');
	}

	function delete_temp() {
		// delete temporary files
		if (file_exists($this->savedfile)) {
			unlink($this->savedfile);
		}
	}

	function createFinalImage() {
		// start creation of image
		$this->draw_baseimage();
		$this->parseCustomData();

		$this->delete_temp();

		/* Output the image*/
		return $this->im;
	}
} // end class def

$cardframe = new CardFrame_Custom($qarray);
?>
