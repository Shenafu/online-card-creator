<!doctype html>
<html class="shenafu" id="cardcreatormanual">
<head>
	<title>Shenafu's Card Creator Instructions</title>
	<meta  charset="utf-8" />
	<link rel="icon" type="image/jpg" href="/magic/favicon.ico"/>
<style type="text/css">
@import url('https://fonts.googleapis.com/css?family=Fondamento|Itim');

html { background-color: #f0e68c; /* khaki */}
body * { font: 14pt 'Itim', sans-serif; margin-left: auto; margin-right: auto; max-width: 60em;}
h1, h1 * {font: 21pt 'Fondamento', cursive; }
h2 { font: small-caps 21pt 'Fondamento', cursive; margin-top: 0px; margin-bottom: 0px; color: yellow; background: green; }
h3 { text-align: center; color: black; background: orange; margin: 2px; font: 16pt 'Fondamento', cursive; text-decoration: underline; }
pre { white-space: pre-wrap; }
pre > ul { margin-top: -2em; }
p { margin-top: 0px; }

</style>
<script src="/lib/markdown.js"></script>
<script>

function renderMarkdown(element) {
	var text = element.innerHTML;
	element.innerHTML = markdown.toHTML(text);
}

function CCOnload() {
	renderMarkdown(hintstext);
}
</script>
</head>

<body onload="CCOnload();" >
<h1><a href="https://ieants.cc/magic/cc/" target="cc">Shena'Fu's Online Card Creator</a></h1>
<pre id="hintstext" class="markdown">
<?php readfile('cchints.txt'); ?>
</pre>


</body>
</html>