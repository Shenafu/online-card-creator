<?php

// vogon deco frames

class CardFrame_VogonDeco {

	////// CONSTANTS //////
	const WIDTH = 750;
	const HEIGHT = 1046;
	const BORDER = 32;
	const BOTTOMBAR = 66;
	const TINT = [
		"c" => "#DEB887"
		,"w" => "#F5F5DC"
		,"u" => "#99CCFF"
		,"b" => "#A0A0A0"
		,"r" => "#DC143C"
		,"g" => "#6B8E23"
		,"m" => "#FFD700"
	];

	////// PROPERTIES //////

	public $q; // array holding query variables
	public $dirframe, $dirmana, $dirfont; // directories to resources
	public $cardnamefont, $typefont, $manafont, $ptfont, $textfont, $italicfont;
	public $im, $mcircle, $textdivider; // images
	public $black, $white, $legendcolor, $clear; // ImagickPixel constants
	public $typetext; // final type line
	public $tints; // tint the entire card based on color provided
	public $extrabar; // save the extra bar for later pasting
	public $artbevel, $artbevelpw; // bevel around art

	////// METHODS //////

	function __construct($qarray) {
		// data from query
		$this->q = $qarray;

		// file resources
		$this->dirframe = "frames/frame_" . $this->q['frame'];
		$this->textdivider = new Imagick( "$this->dirframe/horiz-divider.png" );
		$this->dirmana = "symbols/" . ((empty($this->q['symbol'])) ? "modern" : $this->q['symbol']);
		$this->mcircle = new Imagick( "$this->dirmana/mana_circle.png");
		$this->seticon = "$this->dirframe/seticon.png";
		$this->dirfont = $_SERVER['DOCUMENT_ROOT'] . "/fonts";
		$this->cardnamefont = "$this->dirfont/FhaNicholson-Regular.ttf";
		$this->typefont = "$this->dirfont/FhaNicholson-Regular.ttf";
		$this->manafont = "$this->dirfont/CaviarDreams.ttf";
		$this->ptfont = "$this->dirfont/CaviarDreams.ttf";
		$this->boldfont = "$this->dirfont/CaviarDreams_Bold.ttf";
		$this->textfont = "$this->dirfont/CaviarDreams.ttf";
		$this->italicfont = "$this->dirfont/CaviarDreams_Italic.ttf";
		$this->bolditalicfont = "$this->dirfont/CaviarDreams_BoldItalic.ttf";
		$this->dyratio = 1.0;

		// drawing resources
		$this->black = new ImagickPixel("black");
		$this->white = new ImagickPixel("white");
		$this->legendcolor = new ImagickPixel("#9400D3"); // crimson #DC143C ; darkviolet #9400D3 ;
		$this->clear = new ImagickPixel("transparent");
		$this->halfclear = new ImagickPixel("#ffffff00");
		$this->draw = new ImagickDraw();
		$this->manadraw = new ImagickDraw();
		$this->artbevel = new Imagick( "$this->dirframe/art_bevel.png");
		$this->artbevelpw = new Imagick( "$this->dirframe/art_bevel_pw.png");
		$this->im = new Imagick();
		$this->im->newImage($this::WIDTH, $this::HEIGHT, $this->white, "png");

	}

	function drawBackground() {
		// background base
		$this->q['cardback'] = str_replace('.jpg', '.png', $this->q['cardback']);
		$cardback = "$this->dirframe/" . $this->q['cardback'];
		if (file_exists($cardback)) {
            $bg = new Imagick($cardback);
            $bg->setimageformat('png');
            $bgmask = "/mask" . (''!=trim($this->q['extra']) ? "_extra" : '') . ".png";
				$bgmask = new Imagick($this->dirframe . $bgmask);
            $bgmask->setimageformat('png');
            $bg->compositeImage($bgmask, imagick::COMPOSITE_COPYOPACITY, 0, 0);
            $this->im->compositeImage($bg, imagick::COMPOSITE_OVER, 0, 0);
        }
	}

	function drawBlend() {
		// blending for multicolor and hybrid
		// only for Magic/Space genres

		if (in_array($this->q['genre'], array('magic','space','earth') )) {

			$this->blendoptions = cardfuncs_getblendoptions($this->q['manacost'] . $this->q['powertoughness'], $this->q['extra'] . $this->q['rulestext']);
			$options = [
				'width' => $this::WIDTH
				,'height' => $this::HEIGHT
				,'black' => $this->black
				,'blendstartrate' => 0.1
				,'blendendrate' => 0.9
				,'contrast' => 2.0
				,'midtone' => 0.0
				,'landscape' => true
				,'extension' => 'png'
			];
			if (3==count($this->blendoptions['colors'])) {
				$options['blendstartrate'] = 0.56;
				$options['blendendrate'] = 0.76;
				$options['contrast'] = 2.0;
				$options['midtone'] = -1.0;
			}

			$options['blendframemask'] = ($this->q['color']=='q') ? "mask" : "mask_seams";
			$options['blendframemask'] .= (isset($this->q['extra']) && ''!=$this->q['extra']) ? "_extra" : "";
			$options['blendframemask'] .= ".png";
			$this->im = cardfuncs_renderblend($options, $this);
		}
	}

	function drawArt() {
		// art
		$options = [
			'artx' => 0
			,'arty' => 0
			,'artwidth' => 750
			,'artheight' => 660
		];
		cardfuncs_makeArtImage($options, $this);
	}

	function drawArtistCreator() {
		// artist and creator

		$options = [
			'font' => $this->typefont
			,'fontsize' => 18.0
			,'textx' => 375
			,'texty' => 1005
			,'wmax' => $this::WIDTH
			,'text' => "Illus. by " . $this->q['artist']
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
			,'textalign' => imagick::ALIGN_CENTER
		];

		cardfuncs_makeLinearText($options, $this);

		$options = [
			'font' => $this->typefont
			,'fontsize' => 18.0
			,'textx' => 375
			,'texty' => 1028
			,'wmax' => $this::WIDTH
			,'text' => "Created by " . $this->q['creator']
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
			,'textalign' => imagick::ALIGN_CENTER
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawManaCost() {
		// write mana cost, simplified, align right

		$manapos = [
			[16, 50]
			,[16, 188]
			,[16, 326]
			,[608, 50]
			,[608, 188]
			,[608, 326]
		];

		$options = [
			'font' => $this->manafont
			,'fontsize' => 116.0
			,'fontcolor' => $this->black
			,'manaiconwidth' => 128
			,'manaiconheight' => 128
			,'manapos' => $manapos
		];

		cardfuncs_makeManaCost($options, $this);
	}

	function drawCardName() {
		// write card name

		$options = [
			'font' => $this->cardnamefont
			,'fontsize' => 36.0
			,'textx' => 375
			,'texty' => 23
			,'wmax' => 468
			,'text' => $this->q['cardname']
			,'textcolor' => substr($this->q['supertype'], 0, 1)=='1' ? $this->legendcolor : $this->black
			,'textalign' => imagick::ALIGN_CENTER
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawType() {
		// write card type

		$this->typetext = cardimage_gettypeline($this->q['supertype'], $this->q['cardtype'], $this->q['subtype'], $this->q['genre'], "| ");

		$options = [
			'font' => $this->typefont
			,'fontsize' => 30.0
			,'textx' => 375
			,'texty' => 961
			,'wmax' => 695
			,'text' => $this->typetext
			,'textcolor' => $this->black
			,'textalign' => imagick::ALIGN_CENTER
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawPT() {
		// write power toughness
		if (''!=trim($this->q['powertoughness'])) {

			$manapos = [
				[6, 627]
				,[6, 727]
				,[6, 827]
				,[648, 627]
				,[648, 727]
				,[648, 827]
			];

			$options = [
				'font' => $this->manafont
				,'fontsize' => 90.0
				,'fontcolor' => $this->black
				,'manaiconwidth' => 96
				,'manaiconheight' => 96
				,'manapos' => $manapos
				,'altmana' => true
				,'manacost' => $this->q['powertoughness']
			];

			cardfuncs_makeManaCost($options, $this);
		}
	}

	function drawWatermark() {
		// watermark
        $options = [
            'wmx' => 375
            ,'wmy' => 755
            ,'wmw' => 492
            ,'wmh' => 332
        ];

		cardfuncs_makeWatermark($options, $this);
	}

	function drawRulesText() {
		// write rules, flavor, extra texts

		cardfuncs_conjoined_cardframe_op($this->q, ['extra', 'rulestext', 'flavortext'], ["cardfuncs_replaceMemtext", "cardfuncs_replaceCardTraits"]);


		// extra text

		$options = [
			'textfont' => $this->textfont
			,'boldfont' => $this->boldfont
			,'italicfont' => $this->italicfont
			,'bolditalicfont' => $this->bolditalicfont
			,'fontsize' => 50.0
			,'minfontsize' => 18.0
			,'textletterx' => 0
			,'textlettery' => 0
			,'textboxx' => 237
			,'textboxy' => 516
			,'textboxwidth' => 274
			,'textboxheight' => 50
			,'rulestext' => $this->q['extra']
			,'dyratio' => $this->dyratio
			,'manafontratio' => 0.85
		];

		cardfuncs_makeRulesbox($options, $this);

		// actual rules and flavor text

		$options = [
			'textfont' => $this->textfont
			,'boldfont' => $this->boldfont
			,'italicfont' => $this->italicfont
			,'bolditalicfont' => $this->bolditalicfont
			,'fontsize' => 50.0
			,'minfontsize' => 12.0
			,'textletterx' => 0
			,'textlettery' => 0
			,'textboxx' => 127
			,'textboxy' => 589
			,'textboxwidth' => 496
			,'textboxheight' => 332
			,'rulestext' => $this->q['rulestext']
			,'flavortext' => $this->q['flavortext']
			,'dyratio' => $this->dyratio
			,'manafontratio' => 0.9
		];

		cardfuncs_makeRulesbox($options, $this);
	}

	function drawSetIcon() {
		// draw set icon, with rarity colors filled

		$options = [
			'w' => 48
			,'h' => 48
			,'x' => 741
			,'y' => 992
		];

		cardfuncs_makeSetIcon($options, $this);
	}

	function drawTypeIcon() {
		// write card type icon
		$options = [
			'w' => 48
			,'h' => 48
			,'x' => 9
			,'y' => 992
			,'invert' => true
			,'genre' => $this->q['genre']
			,'cardtype' => $this->q['cardtype']
		];

		cardfuncs_makeTypeIcon($options, $this);
	}

	function drawOverlay() {
        $options = [
            'ovx' => 0
            ,'ovy' => 0
            ,'ovw' => 750
            ,'ovh' => 987
        ];

		cardfuncs_makeOverlay($options, $this);
	}

	function createFinalImage() {
		// start creation of image
		$this->drawArt();
		$this->drawBackground();
		$this->drawBlend();
		$this->drawWatermark();
		$this->drawSetIcon();
		$this->drawCardName();
		$this->drawTypeIcon();
		$this->drawType();
		$this->drawManaCost();
		$this->drawPT();
		$this->drawRulesText();
		$this->drawArtistCreator();
		$this->drawOverlay();

		/* Output the image*/
		return $this->im;
	}
} // end class def

$cardframe = new CardFrame_VogonDeco($qarray);
?>
