<?php
error_reporting(E_ALL); ini_set('display_errors', 1);
include_once("siteurl.php");
include_once("cclog_oop.php");
$parentdir = preg_replace("/^(\/.*)\/cc.*?\.php$/", '$1', $_SERVER['SCRIPT_NAME']);
$CARDIMAGEURL = SITE_URL . "/$parentdir/cardimage_oop.php?";

parse_str($_SERVER['QUERY_STRING'], $qarray);
$cardname = rawurldecode($qarray['cardname']);
$creator = rawurldecode($qarray['creator']);

include_once "../magic/cccreds.php"; // holds $sqlhost, $sqluser, $sqlpass, $sqldb, $sqltbl

$mysqli = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
if ($mysqli->connect_errno) {
	printf("Connect failed: %s\n", $mysqli->connect_error);
	exit();
}
//echo 'Connected successfully';

$query = "call get_card ('" . $mysqli->real_escape_string($cardname) . "', '" . $mysqli->real_escape_string($creator). "');";

/*
echo '$query:</br>';
echo $query;
echo '</br></br>';
exit;
//*/

$mysqli->next_result();
$mysqli->multi_query($query);
if ($result = $mysqli->store_result()) {
	while ($row = $result->fetch_assoc()) {
		//echo 'QUERY FOUND';

		$query = '';
		foreach ($row as $key=>$value) {
			$query .= "$key=" . rawurlencode($value) . "&";
		}
		$imgurl = $CARDIMAGEURL . $query;

		//echo '\n$imgurl = ' . $imgurl;

		$ch = @curl_init($imgurl);

		$timeout = 60; // seconds

		@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	// print output
		@curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);	// prevent waiting forever to get a result

		$image = curl_exec($ch);
		curl_close($ch);

		header('Cache-Control: no-cache, must-revalidate');
		//header('Content-Location: ' . $_SERVER['REQUEST_URI']);
		header('Content-Type: image/png');
		$filename = "$cardname by $creator.png";
		header('Content-Disposition: inline; filename="' . $filename . '"');


		// resize if query has w= or h=
		if (isset($qarray['w']) || isset($qarray['h'])) {
			try {
				$output = new Imagick();
				$output->readImageBlob($image);
				$output->setImageFormat("png");

				if (isset($qarray['w']) && 0!=intval($qarray['w']) && !isset($qarray['h'])) {
					$output->thumbnailImage($qarray['w'], 0);
				}
				elseif (isset($qarray['h']) && 0!=intval($qarray['h']) && !isset($qarray['w'])) {
					$output->resizeImage(0, $qarray['h'], imagick::FILTER_CATROM, 0.0);
				}
				elseif (isset($qarray['w']) && 0!=intval($qarray['w']) && isset($qarray['h']) && 0!=intval($qarray['h'])) {
					$output->thumbnailImage($qarray['w'], $qarray['h'], true);
				}
				echo $output;
			}
			catch (Exception $e) {
				cclog("ccimg resize failed\n");
				cclog("error reading blob\n");
				echo $image;
			}
		}
		else {
			echo $image;
		}
	}
}
/*
else {
	 echo 'QUERY NOT FOUND';
	 echo '</br></br>';
}
return;
//*/
?>