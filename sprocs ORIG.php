<?php

// create stored procedures

echo "<pre>";
echo "STARTED Creating Stored Procedures. . . \n\n";

include_once "cccreds.php"; // holds $sqlhost, $sqluser, $sqlpass, $sqldb, $sqltbl

$mysqli = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
if ($mysqli->connect_errno) {
	printf("Connect failed: %s\n", $mysqli->connect_error);
	exit();
}
//echo 'Connected successfully';

$defHost = 'iea.atwebpages.com';
$definer = "'$sqluser'@$defHost";

// get_card
$q = "DROP PROCEDURE IF EXISTS get_card;";

$mysqli->query($q);

$q = <<<LONGSQL
CREATE
PROCEDURE get_card(IN cn VARCHAR(127), IN cr VARCHAR(127))
READS SQL DATA
SQL SECURITY INVOKER
BEGIN

SELECT * FROM $sqltbl
WHERE lower(cardname) = cn AND lower(creator) = cr;

-- CREATED by sprocs.php
END;
LONGSQL;

$mysqli->query($q);

echo "\n\n";
echo $mysqli->error;

// save_card
$q = "DROP PROCEDURE IF EXISTS save_card;";

$mysqli->query($q);

$q = <<<LONGSQL
CREATE PROCEDURE save_card (IN in_cardname VARCHAR(127), IN in_creator VARCHAR(127), IN in_manacost tinytext, IN in_powertoughness tinytext, IN in_extra text, IN in_rulestext text, IN in_flavortext text, IN in_rarity VARCHAR(16), IN in_seticonurl text, IN in_typeiconurl text, IN in_wmurl text, IN in_overlayurl text, IN in_arturl text, IN in_artist tinytext, IN in_color VARCHAR(16), IN in_border VARCHAR(16), IN in_frame VARCHAR(127), IN in_symbol tinytext, IN in_genre VARCHAR(127), IN in_supertype tinytext, IN in_cardtype tinytext, IN in_subtype tinytext, IN in_custommana text, IN in_customframe text)
MODIFIES SQL DATA
SQL SECURITY INVOKER
BEGIN

REPLACE INTO $sqltbl (
cardname,
creator,
manacost,
powertoughness,
extra,
rulestext,
flavortext,
rarity,
seticonurl,
typeiconurl,
wmurl,
overlayurl,
arturl,
artist,
color,
border,
frame,
symbol,
genre,
supertype,
cardtype,
subtype,
custommana,
customframe
)

VALUES

(
in_cardname,
in_creator,
in_manacost,
in_powertoughness,
in_extra,
in_rulestext,
in_flavortext,
in_rarity,
in_seticonurl,
in_typeiconurl,
in_wmurl,
in_overlayurl,
in_arturl,
in_artist,
in_color,
in_border,
in_frame,
in_symbol,
in_genre,
in_supertype,
in_cardtype,
in_subtype,
in_custommana,
in_customframe
)
;

-- CREATED by sprocs.php
END;
LONGSQL;

$mysqli->query($q);

echo "\n\n";
echo $mysqli->error;

// get_random
$q = "DROP PROCEDURE IF EXISTS get_random;";

$mysqli->query($q);

$q = <<<LONGSQL
CREATE PROCEDURE get_random (IN rn INT)
READS SQL DATA
SQL SECURITY INVOKER
BEGIN

SELECT cardname, creator FROM $sqltbl ORDER BY RAND() LIMIT rn;

-- CREATED by sprocs.php
END;
LONGSQL;

$mysqli->query($q);

echo "\n\n";
echo $mysqli->error;

// get_random_by_creator
$q = "DROP PROCEDURE IF EXISTS get_random_by_creator;";

$mysqli->query($q);

$q = <<<LONGSQL
CREATE PROCEDURE get_random_by_creator (IN rn INT, IN cr VARCHAR(127))
READS SQL DATA
SQL SECURITY INVOKER
BEGIN

SELECT cardname, creator FROM $sqltbl WHERE lower(creator) = cr ORDER BY RAND() LIMIT rn;

-- CREATED by sprocs.php
END;
LONGSQL;

$mysqli->query($q);

echo "\n\n";
echo $mysqli->error;

// get_count
$q = "DROP PROCEDURE IF EXISTS get_count;";

$mysqli->query($q);

$q = <<<LONGSQL
CREATE PROCEDURE get_count ()
READS SQL DATA
SQL SECURITY INVOKER
BEGIN

SELECT COUNT(1) FROM $sqltbl;

-- CREATED by sprocs.php
END;
LONGSQL;

$mysqli->query($q);

echo "\n\n";
echo $mysqli->error;

// get_count_by_creator
$q = "DROP PROCEDURE IF EXISTS get_count_by_creator;";

$mysqli->query($q);

$q = <<<LONGSQL
CREATE PROCEDURE get_count_by_creator (IN cr VARCHAR(127))
READS SQL DATA
SQL SECURITY INVOKER
BEGIN

SELECT count(1) FROM $sqltbl WHERE lower(creator) = cr;

-- CREATED by sprocs.php
END;
LONGSQL;

$mysqli->query($q);

echo "\n\n";
echo $mysqli->error;

// get_list
$q = "DROP PROCEDURE IF EXISTS get_list;";

$mysqli->query($q);

$q = <<<LONGSQL
CREATE PROCEDURE get_list (IN cr VARCHAR(127), IN page INT, IN perpage INT)
READS SQL DATA
SQL SECURITY INVOKER
BEGIN

SELECT cardname, creator
FROM $sqltbl
WHERE lower(creator) = cr
ORDER BY cardname
LIMIT page, perpage;

-- CREATED by sprocs.php
END;
LONGSQL;

$mysqli->query($q);

echo "\n\n";
echo $mysqli->error;

echo "\n\nENDED Creating Stored Procedures . . . \n\n";

echo "</pre>";
?>
