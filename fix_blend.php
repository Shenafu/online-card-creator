<?php

function isDualColor($manacost) {
	$colors = [];

	// extract mana from manacost
	$manacost = preg_replace('/(.)/', '$1,', $manacost);
	$manacost = explode(',', $manacost);

	// parse manacost
	foreach($manacost as $value) {
		if ( preg_match('/[bgruw]/', $value)) {
			if (!in_array($value, $colors)) {
				$colors[] = $value;
				
			}
		}
	}	
	return count($colors) == 2;
}


include_once "cccreds.php"; // holds $sqlhost, $sqluser, $sqlpass, $sqldb, $sqltbl
$mysqli = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
if ($mysqli->connect_errno) {
	printf("Connect failed: %s\n", $mysqli->connect_error);
	exit();
}
//echo 'Connected successfully';

$table = $sqltbl;

$query = "SELECT `cardname`, `creator`, `manacost`, `color` FROM $table WHERE `creator` = 'WotC' AND `color` = 'm' ORDER BY cardname;";

//$total = $mysqli->query($query)->fetch_array()[0];

//echo "Num Items: $total";

$result = $mysqli->query($query);

$delim = "`";
while ($row = $result->fetch_assoc()) {
	$manacost = strtolower($row['manacost']);
	$cardname = $mysqli->escape_string($row['cardname']);
	if (isDualColor($manacost)) {
		echo "$delim$cardname";
		echo "$delim$manacost";
		echo "<br>";
		
		$query = "UPDATE $table SET `color` = 'q' WHERE `cardname` = '$cardname' AND lower(creator)=lower('wotc');";
		$mysqli->query($query);
	}
	else {
		//echo "$delim<br>";
	}
}

?>


