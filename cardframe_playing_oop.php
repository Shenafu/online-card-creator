<?php

// playing card frames

class CardFrame_Playing {

	////// CONSTANTS //////
	const WIDTH = 750;
	const HEIGHT = 1046;

	////// PROPERTIES //////

	public $dirframe, $dirmana, $dirfont; // directories to resources
	public $q; // array holding query variables
	public $cardnamefont, $typefont, $manafont, $ptfont, $textfont, $italicfont;
	public $im, $mcircle, $textdivider; // images
	public $black, $white, $clear, $gray; // ImagickPixel constants
	public $typetext; // final type line

	////// METHODS //////

	function __construct($qarray) {
		// data from query
		$this->q = $qarray;

		$this->typetext = cardimage_gettypeline($this->q['supertype'], $this->q['cardtype'], $this->q['subtype'], $this->q['genre'], "— ");

		// file resources
		$this->dirframe = "frames/frame_" . $this->q['frame'];
		$this->textdivider = new Imagick( "$this->dirframe/horiz-divider.png" );
		$this->dirmana = "symbols/" . ((empty($this->q['symbol'])) ? "suits" : $this->q['symbol']);
		$this->mcircle = new Imagick( "$this->dirmana/mana_circle.png");
		$this->dirfont = $_SERVER['DOCUMENT_ROOT'] . "/fonts";
		$this->cardnamefont = "$this->dirfont/kelvinch-bold.ttf";
		$this->typefont = "$this->dirfont/kelvinch-bold.ttf";
		$this->manafont = "$this->dirfont/ubuntumono-regular.ttf";
		$this->ptfont = "$this->dirfont/crimson-regular.ttf";
		$this->boldfont = "$this->dirfont/crimson-bold.ttf";
		$this->textfont = "$this->dirfont/crimson-regular.ttf";
		$this->italicfont = "$this->dirfont/crimson-italic.ttf";
		$this->bolditalicfont = "$this->dirfont/crimson-bolditalic.ttf";
		$this->dyratio = 1.0;

		// drawing resources
		$this->black = new ImagickPixel("black");
		$this->white = new ImagickPixel("white");
		$this->clear = new ImagickPixel("transparent");
		$this->textColor = $this->black;
		$this->gray = new ImagickPixel("gray");
		$this->draw = new ImagickDraw();
		$this->manadraw = new ImagickDraw();
		$this->im = null;
	}

	function drawBackground() {
		// background base

		$this->q['cardback'] = str_replace( "jpg", "png", $this->q['cardback']);
		$this->q['cardback'] = str_replace( "lcard", "card", $this->q['cardback']);
		$cardback = "$this->dirframe/" . $this->q['cardback'];
		$this->im = new Imagick($cardback);

		// textColor is white if background is black
		if ($this->im->getImagePixelColor(0, 0)->isPixelSimilar( $this->black, 0.0)) {
			$this->textColor = $this->white;
		}
	}

	function drawBlend() {
		// blending for multicolor and hybrid

		$this->blendoptions = cardfuncs_getblendoptions($this->q['manacost'], $this->q['rulestext']);
		$options = [
			'width' => $this::WIDTH
			,'height' => $this::HEIGHT
			,'black' => $this->black
			,'blendstartrate' => 0.4
			,'blendendrate' => 0.6
			,'contrast' => 20
			,'midtone' => 0.166
		];
		$this->im = cardfuncs_renderblend($options, $this);
	}

	function drawArt() {
		// art

		$options = [
			'artx' => 108
			,'arty' => 104
			,'artwidth' => 534
			,'artheight' => 838
			,'confirm' => false
		];

		$this->art = cardfuncs_makeArtImage($options, $this);

		isset($art) && cclog($art);
	}

	function drawArtistCreator() {
		// artist and creator

		$options = [
			'font' => $this->typefont
			,'fontsize' => 30.0
			,'textx' => 120
			,'texty' => 988
			,'wmax' => $this::WIDTH
			,'text' => "Illus. by " . $this->q['artist']
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
		];

		cardfuncs_makeLinearText($options, $this);

		$options = [
			'font' => $this->typefont
			,'fontsize' => 30.0
			,'textx' => 120
			,'texty' => 1034
			,'wmax' => $this::WIDTH
			,'text' => "Created by " . $this->q['creator']
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawWatermark() {
		// watermark

		$options = [
			'wmx' => 374
			,'wmy' => 522
			,'wmw' => 534
			,'wmh' => 838
		];

		cardfuncs_makeWatermark($options, $this);
	}

	function drawManaCost() {
		// write mana cost

		$options = [
			'font' => $this->manafont
			,'fontsize' => 72.0
			,'fontcolor' => $this->black
			,'manacostx' => 16
			,'manacosty' => 166
			,'manaiconwidth' => 72
			,'manaiconheight' => 72
			,'width' => 72
			,'height' => 1036
			,'vertical' => true
		];

		cardfuncs_makeManaCost($options, $this);

		$options = [
			'font' => $this->manafont
			,'fontsize' => 72.0
			,'fontcolor' => $this->black
			,'manacostx' => 734
			,'manacosty' => 880
			,'manaiconwidth' => 72
			,'manaiconheight' => 72
			,'width' => 72
			,'height' => 1036
			,'vertical' => true
			,'angle' => 180
		];

		cardfuncs_makeManaCost($options, $this);
	}

	function drawCardName() {
		// write card name

		$options = [
			'font' => $this->cardnamefont
			,'fontsize' => 40.0
			,'textx' => 68
			,'texty' => 96
			,'wmax' => 610
			,'text' => $this->q['cardname']
			,'textcolor' => $this->black
		];
		cardfuncs_makeLinearText($options, $this);

	}

	function drawType() {
		// write card type

		$text = "";

		if (!($this->q['subtype']!="" && $this->q['extra']!="")) {
			$text = $this->q['subtype']!="" ? $this->q['subtype'] : $this->q['extra'];

			$options = [
				'font' => $this->typefont
				,'fontsize' => 44.0
				,'textx' => 120
				,'texty' => 52
				,'wmax' => 526
				,'text' => $text
				,'textcolor' => $this->textColor
			];

			cardfuncs_makeLinearText($options, $this);

			$options['textx'] = 630;
			$options['texty'] = 1012;
			$options['angle'] = 180.0;

			cardfuncs_makeLinearText($options, $this);
		} else {

			// extra
			$text = $this->q['extra'];

			$options = [
				'font' => $this->typefont
				,'fontsize' => 44.0
				,'textx' => 120
				,'texty' => 34
				,'wmax' => 526
				,'text' => $text
				,'textcolor' => $this->textColor
			];

			cardfuncs_makeLinearText($options, $this);

			$options['textx'] = 630;
			$options['texty'] = 1032;
			$options['angle'] = 180.0;

			cardfuncs_makeLinearText($options, $this);

			// subtype
			$options['text'] = $this->q['subtype'];
			$options['fontsize'] = 36.0;
			$options['textx'] = 120;
			$options['texty'] = 80;
			$options['angle'] = 0;

			cardfuncs_makeLinearText($options, $this);

			$options['textx'] = 630;
			$options['texty'] = 986;
			$options['angle'] = 180.0;

			cardfuncs_makeLinearText($options, $this);
		}
	}

	function drawPT() {
		// write power toughness

		$x = 60;
		$y = 132;

		$options = [
			'font' => $this->ptfont
			,'fontsize' => 72.0
			,'textx' => $x
			,'texty' => $y
			,'wmax' => 750
			,'text' => $this->q['powertoughness']
			,'textcolor' => $this->textColor
			,'textalign' => imagick::ALIGN_CENTER
		];
		cardfuncs_makeLinearText($options, $this);

		$options = [
			'font' => $this->ptfont
			,'fontsize' => 72.0
			,'textx' => $this::WIDTH - $x
			,'texty' => $this::HEIGHT - $y
			,'wmax' => 750
			,'text' => $this->q['powertoughness']
			,'textcolor' => $this->textColor
			,'angle' => 180
			,'textalign' => imagick::ALIGN_CENTER
		];
		cardfuncs_makeLinearText($options, $this);
	}

	function drawRulesText() {
		// write rules and flavor texts

		cardfuncs_conjoined_cardframe_op($this->q, ['rulestext', 'flavortext'], ["cardfuncs_replaceMemtext", "cardfuncs_replaceCardTraits"]);

		$textboxx = 108;
		$textboxy = 104;

		$options = [
			'textfont' => $this->textfont
			,'boldfont' => $this->boldfont
			,'italicfont' => $this->italicfont
			,'bolditalicfont' => $this->bolditalicfont
			,'fontsize' => 52.0
			,'minfontsize' => 12.0
			,'textcolor' => $this->textColor
			,'shadowcolor' => $this->textColor
			,'textletterx' => 0
			,'textlettery' => 0
			,'textboxx' => $textboxx
			,'textboxy' => $textboxy
			,'textboxwidth' => 534
			,'textboxheight' => 838
			,'rulestext' => $this->q['rulestext']
			,'flavortext' => $this->q['flavortext']
		];

		$box = cardfuncs_makeRulesboxImage($options, $this);

		if (isset($box)) {
			// center rules
			$deltax = abs($options['textboxwidth'] - $box->getImageWidth())/2;
			$deltay = abs($options['textboxheight'] - $box->getImageHeight())/2;

			// create see-through areas in the art with rules text
			if ($this->art) {
				try {
					$this->art->compositeImage( $box, imagick::COMPOSITE_OVER, $deltax+1, $deltay+1);
					$this->art->compositeImage( $box, imagick::COMPOSITE_OVER, $deltax+2, $deltay+2);
					$this->art->compositeImage( $box, imagick::COMPOSITE_XOR, $deltax, $deltay);
					$this->im->compositeImage( $this->art, imagick::COMPOSITE_OVER, $textboxx, $textboxy);
				}
				catch (ImagickException $e) {
					cclog( "\n" . 'rules + art error : ' . $e);
				}
			}
			else {
				// draw black text behind art
				$this->im->compositeImage( $box, imagick::COMPOSITE_OVER, $textboxx + $deltax, $textboxy + $deltay);
			}
		}
		elseif ($this->art) {
			$this->im->compositeImage( $this->art, imagick::COMPOSITE_OVER, $textboxx, $textboxy);
		}
	}

	function drawSetIcon() {
		// draw set icon, with rarity colors filled
		// in top left and bottom right corners

		$x = 16;
		$y = 156;
		$options = [
			'w' => 72
			,'h' => 72
			,'x' => $x
			,'y' => $y
			,'align' => Imagick::ALIGN_LEFT
			,'ratioheight' => true
		];
		cardfuncs_makeSetIcon($options, $this);

		$options = [
			'w' => 72
			,'h' => 72
			,'x' => $this::WIDTH - $x
			,'y' => $this::HEIGHT - $y
			,'angle' => 180
			,'align' => Imagick::ALIGN_LEFT
			,'ratioheight' => true
		];
		cardfuncs_makeSetIcon($options, $this);
	}

	function drawTypeIcon() {
		// draw set icon, with rarity colors filled
		// in top right and bottom left corners

		$w = 72;
		$h = 72;
		$x = 16;
		$y = 16;
		$options = [
			'w' => $w
			,'h' => $h
			,'x' => $this::WIDTH - $x
			,'y' => $y
			,'align' => Imagick::ALIGN_RIGHT
			,'ratioheight' => true
		];
		cardfuncs_makeTypeIcon($options, $this);

		$options = [
			'w' => $w
			,'h' => $h
			,'x' => $x + $w + $w
			,'y' => $this::HEIGHT - $y
			,'angle' => 180
			,'align' => Imagick::ALIGN_RIGHT
			,'ratioheight' => true
		];
		cardfuncs_makeTypeIcon($options, $this);
	}

	function drawOverlay() {
        $options = [
            'ovx' => 0
            ,'ovy' => 0
            ,'ovw' => 750
            ,'ovh' => 1046
        ];

		cardfuncs_makeOverlay($options, $this);
	}

	function createFinalImage() {
		// start creation of image
		$this->drawBackground();
		//$this->drawBlend();
		$this->drawPT();

		// suit = set icon | mana cost
		if ($this->q['seticonurl'] != "") {
			$this->drawSetIcon();
		}
		elseif ($this->q['manacost'] != "") {
			$this->drawManaCost();
		}

		$this->drawTypeIcon();

		$this->drawType();
		//$this->drawCardName();
		$this->drawArt();
		$this->drawRulesText();
		$this->drawWatermark();
		//$this->drawArtistCreator();
		$this->drawOverlay();

		/* Output the image*/
		return $this->im;
	}
} // end class def

$cardframe = new CardFrame_Playing($qarray);
?>
