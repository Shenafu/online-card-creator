<?php

// planeshifted frames

function cardimage_createfinalimage($query) {
	global $HYBRIDMANA;
//echo '1';
	parse_str($query);

	$angle = 0.0; // horiz
	if ( !isset($width) )
		$width = 375;
	if ( !isset($height) )
		$height = 523;

	if (!isset($frame)) {
		$frame = "planeshift";
	}
	if (!isset($cardback)) {
		$cardback = "ccard.jpg";
	}

	$dirframe = "frames/frame_" . $frame;
	$dirmana = $dirframe . "/mana";

	$cb = preg_replace('/^(.*)card.jpg/', '$1', $cardback);
	$mcircle = new Imagick( "$dirmana/mana_circle.png");
	$seticon = "$this->dirframe/seticon.png";

	$cardnamefont = 'amiri-bold.ttf';
	$textfont = 'amiri-bold.ttf';
	$textratio = 1.25; // font character height / width
	$italicfont = 'amiri-boldslanted.ttf';
	$italicratio = 1.25; // font character height / width

	$cardnamesize = 18.0; //pt
	$cardnamex = 34;
	$cardnamey = 46;
	$cardnamewmax = 305;

	$typefont = 'amiri-bold.ttf';
	$typesize = 15.0; //pt
	$typex = 35;
	$typey = 313;
	$typewmax = 305;

	$powertoughnesssize = 22.0; //pt
	$powertoughnessx = 314; // align center
	$powertoughnessy = 486;
	$ptimgx = 271;
	$ptimgy = 461;

	$manafont = 'amiri-regular.ttf';
	$manacostsize = 19.0; //pt
	$manatextsize = 13.0; // rules text
	$manacostx = 343; // align right
	$manacosty = 33;
	$manaiconwidth = 18;
	$manaiconheight = 18;

	$textboxsize = 20.5; //pt
	$mintextboxsize = 6.0; //pt
	$textletterx = 0;
	$textlettery = 0;

	$textboxx = 35;
	$textboxy = 330;
	$textboxwidth = 304;
	$textboxheight = 135;

	$artx = 32;
	$arty = 62;
	$artwidth = 311;
	$artheight = 228;
	$artistsize = 13.0; // pt
	$artistx = 60;
	$artisty = 488;
	$creatorx = 60;
	$creatory = 501;

	if (!isset($genre) || $genre=="") {
		$genre="magic";
	}
	if (!isset($cardtype) || $cardtype=="") {
		$cardtype="00000000000";
	}
	if (!isset($supertype) || $supertype=="") {
		$supertype="00000";
	}

	$typetext = cardimage_gettypeline($supertype, $cardtype, $genre);
	if ( isset($subtype) && $subtype!="") {
		$typetext = $typetext . "— " . $subtype;
	}

	if (preg_match('/artifact/i', $typetext) && preg_match('/^c/', $cardback)) {
		$cardback = "acard.jpg";
	}

	if (!isset($cardname)) {
		$cardname = "";
	}
	$cardname = stripslashes($cardname);
	if (!isset($powertoughness)) {
		$powertoughness = "";
	}
	$powertoughness = stripslashes($powertoughness);
	if (!isset($manacost)) {
		$manacost = "";
	}
	$manacost = stripslashes($manacost);
	if (!isset($rulestext)) {
		$rulestext = "";
	}
	$rulestext = stripslashes($rulestext);
	$rulestext = preg_replace('/(~this~|~)/', $cardname, $rulestext);
	$rulestext = preg_replace('/(\*)/', '•', $rulestext);
	$rulestext = preg_replace('/(\-\-)/', '—', $rulestext);
	if (!isset($flavortext)) {
		$flavortext = "";
	}
	$flavortext = stripslashes($flavortext);
	if (!isset($rarity)) {
		$rarity = 1;
	}
	$rarity = stripslashes($rarity);
	if (!isset($artist)) {
		$artist = "";
	}
	$artist = stripslashes($artist);
	$artist = "Illustrated by " . $artist;
	if (!isset($creator)) {
		$creator = "";
	}
	$creator = stripslashes($creator);
	$creator = "Created by " . $creator;

	// start creation of image
	$black = new ImagickPixel("black");
	$white = new ImagickPixel("white");
	$transparent = new ImagickPixel("transparent");

	$draw = new ImagickDraw();
	$manadraw = new ImagickDraw();

	$im = new Imagick();
	$im->newImage($width, $height, $black, "png");

	$bg = new Imagick("$dirframe/$cardback");
	$textfontcolor = $black;

	// copy background and P/T box
	$im->compositeImage($bg, imagick::COMPOSITE_OVER, 0, 0);
	if (preg_match('/creature/i', $typetext) || ($powertoughness != "")) {
		$pt = preg_replace('/^(.*)card.jpg/', '/$1pt.png', $cardback);
		$ptimage = new Imagick("$dirframe/$pt");
		$im->compositeImage($ptimage, imagick::COMPOSITE_OVER, $ptimgx, $ptimgy);
	}

	// write mana cost, simplified, align right
	$manacost = preg_replace('/[{}]/', '', strtolower($manacost));
	$manacost = preg_replace('/(([0-9wubrgpc]\/[0-9wubrgpc])|[0-9]|[a-z])/', '$1,',$manacost);
	$manacost = explode(',', $manacost);
	$mcbox = new Imagick();
	$mcbox->newImage($width, $manaiconheight, $transparent);
	$mcboxw = 0;

	$draw->setFont($manafont);
	$draw->setFillColor($black);
	$manadraw->setFont($manafont);
	$manadraw->setFillColor($black);

	foreach($manacost as $value) {
		$value = trim($value);
		//echo "<br>" . $value;
		if ( preg_match('/^[0-9]$/', $value)) {
			// is number
		//echo "_$value";
			$mc = clone $mcircle;
			$mc->thumbnailImage($manaiconwidth, $manaiconheight);
			$manadraw->setFontSize($manacostsize);
			$mc->annotateImage($manadraw, $manaiconwidth * 0.25, $manaiconheight * 0.8, 0, $value);
		}
		elseif ( preg_match('/^[bcegqrstuwxyz]$/', $value)) {
			// is valid color
		//echo "_$value";
			$mc = new Imagick( "$dirmana/mana_$value.png");
			$mc->thumbnailImage($manaiconwidth, $manaiconheight);
		}
		elseif ( preg_match('/^([0-9wubrgpc]\/[0-9wubrgpc])$/', $value)) {
			// is hybrid mana
		//echo "_$value";
			if (preg_match('/^([wubrgpc]\/[wubrgpc])$/', $value, $matches)) {
				// c/d or d/c
				$halfvalue = $HYBRIDMANA[ $matches[0] ];
				$mc = new Imagick( "$dirmana/mana_$halfvalue.png");
				$mc->thumbnailImage($manaiconwidth, $manaiconheight);
			}
			elseif (preg_match('/^([0-9wubrg]\/[0-9wubrg])$/', $value, $matches)) {
				// 2/c or c/2
				$halfcolor = preg_replace('/^.*([wubrg]).*$/', "$1", $value);
				$halfvalue = preg_replace('/^.*([0-9]).*$/', "$1", $value);
				$mc = new Imagick();
				$mc->newImage( $manaiconwidth, $manaiconheight, $transparent);
				$mchalf = new Imagick( "$dirmana/mana_n$halfcolor.png");
				if ($mchalf) {
					$mchalf->thumbnailImage($manaiconwidth, $manaiconheight);
					$mc->compositeImage($mchalf, imagick::COMPOSITE_OVER, 0, 0);
				}
				$manadraw->setFontSize($manacostsize * 0.6);
				$mc->annotateImage($manadraw, $manaiconwidth * 0.2, $manaiconheight * 0.5, 0, $halfvalue);
			}
		}

		// copy mana symbol to $mcbox
		if (isset($mc)) {
			$mcbox->compositeImage($mc, imagick::COMPOSITE_OVER, $mcboxw, 0);
			$mcboxw += $manaiconwidth;
		}
		unset($mc);

	}
	// copy $mcbox to image
	$im->compositeImage($mcbox, imagick::COMPOSITE_OVER, $manacostx - $mcboxw , $manacosty);

	// write card name
	// TODO: resize if necessary
	if ($cardname != "") {
		$draw->setFont($cardnamefont);
		$draw->setFontSize($cardnamesize);
		$draw->setFillColor($black);
		$im->annotateImage($draw, $cardnamex+1, $cardnamey+1, 0, $cardname);
		$draw->setFillColor($white);
		$im->annotateImage($draw, $cardnamex, $cardnamey, 0, $cardname);
	}

	// write card type
	// TODO: resize if necessary
	if ($typetext != "") {
		$draw->setFont($typefont);
		$draw->setFontSize($typesize);
		$draw->setFillColor($black);
		$im->annotateImage($draw, $typex+1, $typey+1, 0, $typetext);
		$draw->setFillColor($white);
		$im->annotateImage($draw, $typex, $typey, 0, $typetext);

	}

	// write power / toughness, centered
	$draw->setFont($textfont);
	$draw->setFontSize($powertoughnesssize);
	$draw->setTextAlignment(Imagick::ALIGN_CENTER);
		$draw->setFillColor($black);
	$im->annotateImage($draw, $powertoughnessx+1, $powertoughnessy+1, 0, $powertoughness);
		$draw->setFillColor($white);
	$im->annotateImage($draw, $powertoughnessx, $powertoughnessy, 0, $powertoughness);
	$draw->setTextAlignment(Imagick::ALIGN_LEFT);

	
	// write rules text and flavor text
	
	$im->compositeImage( makeRulesboxImage($textboxwidth, $textboxheight, $textletterx, $textlettery, $textfont, $textboxsize, $mintextboxsize, $textratio, $italicfont, $italicratio, $rulestext, $flavortext, $dirmana, $mcircle, $im, $draw, $manadraw, $transparent, $black ), imagick::COMPOSITE_OVER, $textboxx, $textboxy);


	// rarity and set icon
	if (isset($seticonurl) && $seticonurl != "") {
		// add http://
		if (0==preg_match("/^http:\/\//", $seticonurl)) {
			$seticonurl = "http://$seticonurl";
		}
		try {
			$seticon = new Imagick($seticonurl);
			if ($seticon) {
				$seticonw = 21;
				$seticonh = 21;
				$seticonx = 320;
				$seticony = 296;
				$raritymask = 'rgb( 255, 0, 255 )';

				$raritycolors = array();
				$raritycolors[1] = 'rgb( 0, 0, 0 )'; // black
				$raritycolors[2] = 'rgb(  208,  208, 208 )'; // silver
				$raritycolors[3] = 'rgb( 196,  170, 93 )'; // #c4aa5d
				$raritycolors[4] = 'rgb( 198, 94, 41 )'; // #c65e29
				$raritycolors[5] = 'rgb( 0,  0, 255 )'; // blue

				//$seticon->paintOpaqueImage( $raritymask, $raritycolors[$rarity], 10 );
				$seticon->OpaquePaintImage( $raritymask, $raritycolors[$rarity], 10, false );
				$seticon->thumbnailImage($seticonw, $seticonh);
				$im->compositeImage($seticon, imagick::COMPOSITE_OVER, $seticonx, $seticony);
			}
		}
		catch (ImagickException $e) {
				cclog( "\n" . '$seticon error : ' . $e);
		}
	}

	// artist and creator
	
	//black or white text depends on border color
	$cbcolor = (preg_match('/(b.?|.l)/', $cb)) ? $white : $black;
	$draw->setFillColor($cbcolor);
	$draw->setFont($textfont);
	$draw->setFontSize($artistsize);
	$im->annotateImage($draw, $artistx, $artisty, 0, $artist);
	$im->annotateImage($draw, $creatorx, $creatory, 0, $creator);
	
	// art

	if (isset($arturl) && $arturl!="") {
		// add http://
		if (0==preg_match("/^http:\/\//", $arturl)) {
			$arturl = "http://$arturl";
		}
		try {
			$art = new Imagick($arturl);
			if ($art) {
				$art->thumbnailImage($artwidth, $artheight);
				$im->compositeImage($art, imagick::COMPOSITE_OVER, $artx, $arty);
			}
		}
		catch (ImagickException $e) {
				cclog( "\n" . '$art error : ' . $e);
		}
	}
	
	/* Output the image*/
	//header("Content-Type: image/png");
	return $im;
}
?>
