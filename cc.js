// script for Card Creator (cc)

var GENRE_TYPES = {
	"magic": {
		"supertype": ["Legendary", "Basic", "Snow", "World", "Ongoing", "Fast", "Delayed", "Token", "Elite", "Host"],
		"cardtype": ["Tribal", "Artifact", "Enchantment", "Land", "Creature", "Instant", "Sorcery", "Planeswalker", "Plane", "Scheme", "Emblem", "Hero", "Conspiracy", "Phenomenon", "Avatar"]
	},
	"earth": {
		"supertype": ["Famous", "Basic", "Magical", "Global", "Ongoing", "Fast", "Delayed", "Clone", "Super", "Host"],
		"cardtype": ["Faction", "Object", "Agenda", "Land", "Agent", "Mod", "Event", "Paragon", "Scene", "Point", "Paradigm", "Hero", "Villain", "Power", "Avatar"]
	},
	"space": {
		"supertype": ["Mythical", "Primary", "Dark", "Universe", "Ongoing", "Fast", "Delayed", "Clone", "Mega", "Host"],
		"cardtype": ["Faction", "Device", "Augment", "Resource", "Unit", "Tactic", "Strategy", "Galaxystalker", "Galaxy", "Scheme", "Paradigm", "Hero", "Conspiracy", "Phenomenon", "Avatar"]
	},
	"pokemon": {
		"supertype": ["Baby", "Basic", "Stage 1", "Stage 2", "", "", "Special", "", "", ""],
		"cardtype": ["", "", "", "Energy", "Pokémon", "Trainer", "", "Pokémon", "", "", "", "", "", "", ""]
	},
	"mbs": {
		"supertype": ["", "", "", "", "", "", "", "", "", ""],
		"cardtype": ["", "Equipment", "", "Battleground", "Unit", "", "Action", "", "", "", "", "", "", "", ""]
	}
};

var ALIASES = [
	[/@a@/,"artifact "],
	[/@add@/,"additional "],
	[/@anyt@/,"any target."],
	[/@bf@/,"battlefield "],
	[/@C@/,"Creature "],
	[/@c@/,"creature "],
	[/@cmc@/,"converted mana cost "],
	[/@Ctr(.*?)@/,"Put a $1 counter on "],
	[/@ctr(.*?)@/,"put a $1 counter on "],
	[/@cy@/,"creature you control "],
	[/@Cys@/,"Creatures you control "],
	[/@cys@/,"creatures you control "],
	[/@dcd@/,"deals combat damage "],
	[/@dd([0-9xyzXYZ]+)@/,"deals $1 damage "],
	[/@dde@/,"deals damage equal to "],
	[/@drawall@/,"At the beginning of each draw step, "],
	[/@draweach@/,"At the beginning of each draw step, "],
	[/@drawnext@/,"At the beginning of the next draw step, "],
	[/@drawopp@/,"At the beginning of each opponent's draw step, "],
	[/@drawyou@/,"At the beginning of your draw step, "],
	[/@e@/,"enchantment "],
	[/@Ec@/,"Each creature "],
	[/@ec@/,"each creature "],
	[/@Eco@/,"Each creature your opponents control "],
	[/@eco@/,"each creature your opponents control "],
	[/@Ecy@/,"Each creature you control "],
	[/@ecy@/,"each creature you control "],
	[/@emb@/,'You get an emblem with "'],
	[/@emblem@/,'You get an emblem with "'],
	[/@endall@/,"At the beginning of each end step, "],
	[/@endeach@/,"At the beginning of each end step, "],	
	[/@endnext@/,"At the beginning of the next end step, "],
	[/@endopp@/,"At the beginning of each opponent's end step, "],
	[/@endyou@/,"At the beginning of your end step, "],
	[/@Eo@/,"Each opponent "],
	[/@eo@/,"each opponent "],
	[/@et@/,"enters the battlefield, "],
	[/@ett@/,"enters the battlefield tapped."],
	[/@g(\+|\-)([0-9xyzXYZ]+)@/,"get $1$2/$1$2 "],
	[/@gs(\+|\-)([0-9xyzXYZ]+)@/,"gets $1$2/$1$2 "],
	[/@gy@/,"graveyard "],
	[/@ias@/,"instant and sorcery "],
	[/@ios@/,"instant or sorcery "],
	[/@lib@/,"library "],
	[/@o@/,"opponent "],
	[/@oc@/,"opponent controls"],
	[/@osc@/,"opponents control"],
	[/@p@/,"player "],
	[/@pm@/,"permanent "],
	[/@ppw@/,"player or planeswalker "],
	[/@Ps@/,"Players "],
	[/@pt@/,"power and toughness "],
	[/@pw@/,"planeswalker "],
	[/@Sac@/,"Sacrifice "],
	[/@sac@/,"sacrifice "],
	[/@Syl@/,"Search your library "],
	[/@syl@/,"search your library "],
	[/@T@/,"Target "],
	[/@t@/,"target "],
	[/@Tc@/,"Target creature "],
	[/@tc@/,"target creature "],
	[/@Tco@/,"Target creature an opponent controls "],
	[/@tco@/,"target creature an opponent controls "],
	[/@tcp@/,"target creature or player "],
	[/@tcpw@/,"target creature or planeswalker "],
	[/@Tcy@/,"Target creature you control "],
	[/@tcy@/,"target creature you control "],
	[/@tn@/,"toughness"],
	[/@To@/,"Target opponent "],
	[/@to@/,"target opponent "],
	[/@Tp@/,"Target player "],
	[/@tp@/,"target player "],
	[/@tppw@/,"target player or planeswalker "],
	[/@tpw@/,"target planeswalker "],
	[/@Uet@/,"Until end of turn, "],
	[/@uet@/,"until end of turn."],
	[/@upkall@/,"At the beginning of each player's upkeep, "],
	[/@upkeach@/,"At the beginning of each player's upkeep, "],
	[/@upknext@/,"At the beginning of the next upkeep, "],
	[/@upkopp@/,"At the beginning of each opponent's upkeep, "],
	[/@upkyou@/,"At the beginning of your upkeep, "],
	[/@uynt@/,"until your next turn."],
	[/@we@/,"Whenever "],
	[/@yc@/,"you control"],
	[/@youpw@/,"you or a planeswalker you control"],
];

function get_radio_value(radio) {
	for (var i=-1; radio[++i];) {
		if (radio[i].checked) {
			return radio[i].value;
		}
	}
}

function get_select_value(select) {
	return select.options[select.selectedIndex].value;
}

function CCOnload() {
	ORIG_SUPERTYPE = document.getElementById("supertype").value;
	ORIG_CARDTYPE = document.getElementById("cardtype").value;

	// if creator is just a space, trim it from text box
	var cbox = document.getElementById("creator");
	if (cbox.value == " ") {
		cbox.value = "";
	}
}

function clear_form() {
	var form = document.getElementById("ccform");

	form.cardname.value = '';
	form.manacost.value = '';
	form.powertoughness.value = '';
	form.subtype.value = '';
	form.extra.value = '';
	form.rulestext.value = '';
	form.flavortext.value = '';
	form.arturl.value = '';

	form.submit();
}

function fix_creator(creator) {
	if (creator=='') {
		creator = ' ';
	}
	return creator;
}

function retrieve_card() {
	var form = document.getElementById("ccform");
	var creator = form.creator.value;
	var cardname = form.cardname.value;
	creator = fix_creator(creator);
	var url = "/magic/cc/" + creator + "/" + cardname;
	window.location = url;
	return false;
}

function supertype_onclick() {
	var genre = get_radio_value(document.getElementById("ccform").genre);
	var supertype = document.getElementById("supertype");
	var value = '';
	var checkbox_prefix = "supertype";

	for (var i=0, max=GENRE_TYPES[genre].supertype.length; i<max; i++) {
		var checkbox = document.getElementById(checkbox_prefix + i);
		value += checkbox.checked ? '1' : '0';
	}
	supertype.value = value;
}

function cardtype_onclick() {
	var genre = get_radio_value(document.getElementById("ccform").genre);
	var cardtype = document.getElementById("cardtype");
	var value = '';
	var checkbox_prefix = "cardtype";

	for (var i=0, max=GENRE_TYPES[genre].cardtype.length; i<max; i++) {
		var checkbox = document.getElementById(checkbox_prefix + i);
		value += checkbox.checked ? '1' : '0';
	}
	cardtype.value = value;
}

function genre_onclick() {
	var form = document.getElementById("ccform");
	var genre = get_radio_value(form.genre);
	var label_id_prefix = "label_supertype";

	for (var i=0, max=GENRE_TYPES[genre].supertype.length; i<max; i++) {
		var id = label_id_prefix + i;
		var label = document.getElementById(id);
		var newtext = GENRE_TYPES[genre].supertype[i];

		label.innerHTML = newtext;
	}

	label_id_prefix = "label_cardtype";
	for (var i=0, max=GENRE_TYPES[genre].cardtype.length; i<max; i++) {
		var id = label_id_prefix + i;
		var label = document.getElementById(id);
		var newtext = GENRE_TYPES[genre].cardtype[i];

		label.innerHTML = newtext;
	}
}

function genre_onload() {
	var form = document.getElementById("ccform");
	// check boxes for card types
	var cardtype = form.cardtype.value;
	var length = cardtype.length;
	for (var i=0; i<length && form["cardtype"+i]; i++) {
		if ( cardtype.substring(i, i+1)=='1') {
			form["cardtype"+i].checked = true;
		}
		else {
			form["cardtype"+i].checked = false;
		}
	}
	// check boxes for super types
	var supertype = form.supertype.value;
	length = supertype.length;
	for (var i=0; i<length && form["supertype"+i]; i++) {
		if ( supertype.substring(i, i+1)=='1') {
			form["supertype"+i].checked = true;
		}
		else {
			form["supertype"+i].checked = false;
		}
	}
	genre_onclick();
}

function reset_form() {
	var form = document.getElementById("ccform");

	form.reset();
	form.supertype.value = ORIG_SUPERTYPE;
	form.cardtype.value = ORIG_CARDTYPE;
	genre_onload();
}

function update_creator_link(creator) {
	creator = fix_creator(creator);
	var a = document.getElementById("creatorlink");
	a.href = "/magic/cclist/" + creator + "/0";
	var span = a.getElementsByTagName('span')[0];
	span.innerHTML = creator;
}

function replaceAliases(area) {
	var text = area.value;
	var tl = text.length;
	var cursor = area.selectionEnd;
	var found = true;

	while (found) {
		found = false;
		for (var i=-1; ALIASES[++i];) {
			var alias = ALIASES[i][0];
			if (alias.test(text)) {
				var sub = ALIASES[i][1];
				var matchAt = text.match(alias).index;
				text = text.replace(alias, sub);
				if (matchAt < cursor) {
					cursor += text.length - tl;
				}
				tl = text.length;
				found = true;
			}
		}
	}

	return [text, cursor];
}

function onRulesTextUpdate(event) {
	// realtime replacement of rulestext, i.e. aliases
	var target = event.target;

	// aliases go between two @@ symbols
	if (/@.+?@/.test(target.value)) {
		var data = replaceAliases(target);
		target.value = data[0];
		target.setSelectionRange(data[1], data[1]);
	}
}


window.addEventListener('load',
	function() {
		CCOnload();
	}
, false);
