<?php

// vogon full art frames

class CardFrame_VogonFullArt {

	////// CONSTANTS //////
	const WIDTH = 750;
	const HEIGHT = 1046;
	const BORDER = 32;
	const BOTTOMBAR = 66;
	const TINT = [
		"c" => "#DEB887"
		,"w" => "#F5F5DC"
		,"u" => "#99CCFF"
		,"b" => "#A0A0A0"
		,"r" => "#DC143C"
		,"g" => "#6B8E23"
		,"m" => "#FFD700"
	];

	////// PROPERTIES //////

	public $q; // array holding query variables
	public $dirframe, $dirmana, $dirfont; // directories to resources
	public $cardnamefont, $typefont, $manafont, $ptfont, $textfont, $italicfont;
	public $im, $mcircle, $textdivider; // images
	public $black, $white, $clear; // ImagickPixel constants
	public $typetext; // final type line
	public $tints; // tint the entire card based on color provided

	////// METHODS //////

	function __construct($qarray) {
		// data from query
		$this->q = $qarray;

		// file resources
		$this->dirframe = "frames/frame_" . $this->q['frame'];
		$this->textdivider = new Imagick( "$this->dirframe/horiz-divider.png" );
		$this->dirmana = "symbols/" . ((empty($this->q['symbol'])) ? "modern" : $this->q['symbol']);
		$this->mcircle = new Imagick( "$this->dirmana/mana_circle.png");
		$this->seticon = "$this->dirframe/seticon.png";
		$this->dirfont = $_SERVER['DOCUMENT_ROOT'] . "/fonts";
		$this->cardnamefont = "$this->dirfont/kelvinch-bold.ttf";
		$this->typefont = "$this->dirfont/kelvinch-bold.ttf";
		$this->manafont = "$this->dirfont/ubuntumono-regular.ttf";
		$this->ptfont = "$this->dirfont/ubuntumono-regular.ttf";
		$this->textfont = "$this->dirfont/ubuntu-regular.ttf";
		$this->boldfont = "$this->dirfont/ubuntu-bold.ttf";
		$this->italicfont = "$this->dirfont/ubuntu-italic.ttf";
		$this->bolditalicfont = "$this->dirfont/ubuntu-bolditalic.ttf";

		// drawing resources
		$this->black = new ImagickPixel("black");
		$this->white = new ImagickPixel("#ffffff");
		$this->clear = new ImagickPixel("transparent");
		$this->draw = new ImagickDraw();
		$this->manadraw = new ImagickDraw();
		$this->im = new Imagick();
		$this->im->newImage($this::WIDTH, $this::HEIGHT, $this->black, "png");

		$this->q['cardback'] = preg_replace("/\.jpg/", ".png", $this->q['cardback']);
	}

	function drawBackground() {
		// background base

		global $ENUM_ARTIFACT;

		// colorless artifact
		if ($this->q['color'] == 'c' && substr($this->q['cardtype'], $ENUM_ARTIFACT, 1)=='1') {
			$this->q['cardback'] = 'a' . substr( $this->q['cardback'], 1);
		}

		$cardback = "$this->dirframe/" . $this->q['cardback'];
		if (file_exists($cardback)) {
			$bg = new Imagick($cardback);
		}
		else {
			$bg = new Imagick("$this->dirframe/cardback.png");
		}
		$this->im->compositeImage($bg, imagick::COMPOSITE_OVER, 0, 0);
	}

	function drawBlend() {
		// blending for multicolor and hybrid
		// only for Magic/Space genres

		if ($this->q['color']=='q' && (in_array($this->q['genre'], array('magic','space') ))) {

			$this->blendoptions = cardfuncs_getblendoptions($this->q['manacost'], $this->q['extra'] . $this->q['rulestext']);
			$options = [
				'width' => $this::WIDTH
				,'height' => $this::HEIGHT
				,'black' => $this->black
				,'blendstartrate' => 0.1
				,'blendendrate' => 0.9
				,'contrast' => 2.0
				,'midtone' => 0.0
				,'extension' => "png"
				,'blendframemask' => "blend_frame_mask.png"
			];

			$this->im = cardfuncs_renderblend($options, $this);
		}
	}

	function drawArt() {
		// art

		$options = [
			'artx' => 0
			,'arty' => 0
			,'artwidth' => 750
			,'artheight' => 977
		];

		cardfuncs_makeArtImage($options, $this);
	}

	function drawArtistCreator() {
		// artist and creator

		$options = [
			'font' => $this->typefont
			,'fontsize' => 21.0
			,'textx' => 375
			,'texty' => 998
			,'wmax' => $this::WIDTH
			,'text' => "Illus. by " . $this->q['artist']
			,'textcolor' => $this->white
			,'textalign' => imagick::ALIGN_CENTER
		];

		cardfuncs_makeLinearText($options, $this);

		$options = [
			'font' => $this->typefont
			,'fontsize' => 21.0
			,'textx' => 375
			,'texty' => 1026
			,'wmax' => $this::WIDTH
			,'text' => "Created by " . $this->q['creator']
			,'textcolor' => $this->white
			,'textalign' => imagick::ALIGN_CENTER
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawManaCost() {
		// write mana cost, simplified, align right

		$options = [
			'font' => $this->manafont
			,'fontsize' => 40.0
			,'fontcolor' => $this->black
			,'manacostx' => 45
			,'manacosty' => 110
			,'manaiconwidth' => 36
			,'manaiconheight' => 36
			,'width' => 36
			,'height' => 922
			,'vertical' => true
		];

		cardfuncs_makeManaCost($options, $this);
	}

	function drawCardName() {
		// write card name

		$options = [
			'font' => $this->cardnamefont
			,'fontsize' => 48.0
			,'textx' => 375
			,'texty' => 69
			,'wmax' => 571
			,'text' => $this->q['cardname']
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
			,'textalign' => imagick::ALIGN_CENTER
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawType() {
		// write card type

		$this->typetext = cardimage_gettypeline($this->q['supertype'], $this->q['cardtype'], $this->q['subtype'], $this->q['genre'], "— ");

		$options = [
			'font' => $this->typefont
			,'fontsize' => 30.0
			,'textx' => 686
			,'texty' => 538
			,'wmax' => 855
			,'angle' => 90.0
			,'text' => $this->typetext
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
			,'textalign' => imagick::ALIGN_CENTER
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawPT() {
		// write power toughness

		global $ENUM_PLANESWALKER, $ENUM_SCENE, $ENUM_CREATURE;

		// write power / toughness, centered

		if (substr($this->q['cardtype'], $ENUM_PLANESWALKER, 1)=='1' || substr($this->q['cardtype'], $ENUM_SCENE, 1)=='1') {
			$framex = 5;
			$framey = 966;
			$framew = 135;
			$frameh = 86;

			$loyaltyFrame = new Imagick( "$this->dirframe/loyalty.png");
			$loyaltyFrame->thumbnailImage($framew, $frameh);
			$this->im->compositeImage($loyaltyFrame, imagick::COMPOSITE_OVER, $framex, $framey);

			$options = [
				'font' => $this->ptfont
				,'fontsize' => 44.0
				,'textx' => 72
				,'texty' => 1008
				,'wmax' => 85
				,'text' => $this->q['powertoughness']
				,'textcolor' => $this->white
				,'textalign' => imagick::ALIGN_CENTER
			];

			cardfuncs_makeLinearText($options, $this);
		}
		elseif (strpos($this->q['powertoughness'], ';;') !== false) {
			// is Leveler

			if ($this->q['extra'] != '') {
				$this->q['powertoughness'] = ';;' . $this->q['powertoughness'];
			}
			$matches = preg_split("/;;/", $this->q['powertoughness']);
			$numtotal = count($matches);
			if ($this->q['flavortext'] != '') {
				$numtotal++;
			}
			$starty = 663;
			$strokeWidth = 5;
			$textboxheight = 299;

			$options = [
				'font' => $this->ptfont
				,'fontsize' =>  34.0
				,'angle' => 270.0
				,'textx' => 66
				,'wmax' => 360
				,'textcolor' => $this->white
				,'shadowcolor' => $this->black
				,'textalign' => imagick::ALIGN_CENTER
			];

			$abilh = $numtotal ? ($textboxheight - ($numtotal - 1) * $strokeWidth) / $numtotal : 0;
			$dy = $abilh + $strokeWidth;
			$starty -= $dy / 2;

			foreach ($matches as $i => $pt) {
				$options['text'] = $pt;
				$options['texty'] = $starty + $dy * ($i + 1);

				cardfuncs_makeLinearText($options, $this);
			}
		}
		elseif (substr($this->q['cardtype'], $ENUM_CREATURE, 1)=='1' || $this->q['powertoughness']!="") {

			$options = [
				'font' => $this->ptfont
				,'fontsize' => 44.0
				,'textx' => 65
				,'texty' => 785
				,'wmax' => 360
				,'angle' => 270.0
				,'text' => $this->q['powertoughness']
				,'textcolor' => $this->white
				,'shadowcolor' => $this->black
				,'textalign' => imagick::ALIGN_CENTER
			];

			cardfuncs_makeLinearText($options, $this);
		}

	}

	function drawWatermark() {
		// watermark

		$options = [
			'wmx' => 375
			,'wmy' => 850
			,'wmw' => 485
			,'wmh' => 190
		];

		cardfuncs_makeWatermark($options, $this);
	}

	function drawRulesText() {
		// write rules and flavor texts

		cardfuncs_conjoined_cardframe_op($this->q, ['extra', 'rulestext', 'flavortext'], ["cardfuncs_replaceMemtext", "cardfuncs_replaceCardTraits"]);

		global $ENUM_PLANESWALKER, $ENUM_SCENE;

		// write rules text and flavor text

		// if not leveler, join extra into rules text
		if ($this->q['extra'] != '' && !preg_match("/;;/", $this->q['rulestext'])) {
			$this->q['rulestext'] = $this->q['extra'] . "\n" . $this->q['rulestext'];
		}

		if (substr($this->q['cardtype'], $ENUM_PLANESWALKER, 1)=='1') {
			// Planeswalker
			$options = [
				'textfont' => $this->textfont
				,'boldfont' => $this->boldfont
				,'italicfont' => $this->italicfont
				,'bolditalicfont' => $this->bolditalicfont
				,'fontsize' => 36.0
				,'minfontsize' => 12.0
				,'textcolor' => $this->white
				,'shadowcolor' => $this->black
				,'textletterx' => 0
				,'textlettery' => 0
				,'textboxx' => 140
				,'textboxy' => 668
				,'textboxwidth' => 507
				,'textboxheight' => 299

				,'lcfontsize' => 44.0
				,'loyaltycostx' => 5
				,'loyaltycosty' => 668
				,'loyaltycostw' => 135
				,'loyaltycosth' => 86

				,'dividerx' => 0
				,'dividerw' => 658

				,'isshading' => false
			];

			cardfuncs_makeRulesboxPW($options, $this);
		}
		elseif (substr($this->q['cardtype'], $ENUM_SCENE, 1)=='1') {
			// Plane / Scene
			$options = [
				'textfont' => $this->textfont
				,'boldfont' => $this->boldfont
				,'italicfont' => $this->italicfont
				,'bolditalicfont' => $this->bolditalicfont
				,'fontsize' => 38.0
				,'minfontsize' => 12.0
				,'textcolor' => $this->white
				,'shadowcolor' => $this->black
				,'textletterx' => 0
				,'textlettery' => 0
				,'textboxx' => 140
				,'textboxy' => 738
				,'textboxwidth' => 507
				,'textboxheight' => 224

				,'lcfontsize' => 44.0
				,'loyaltycostx' => 5
				,'loyaltycosty' => 668
				,'loyaltycostw' => 135
				,'loyaltycosth' => 86

				,'isshading' => false
			];

			cardfuncs_makeRulesboxPW($options, $this);
		}
		elseif (preg_match("/;;/", $this->q['rulestext'])) {
			// is Leveler

			if ($this->q['extra'] != '') {
				$this->q['rulestext'] = $this->q['extra'] . ';;' . $this->q['rulestext'];
			}

			$options = [
				'textfont' => $this->textfont
				,'boldfont' => $this->boldfont
				,'italicfont' => $this->italicfont
				,'bolditalicfont' => $this->bolditalicfont
				,'fontsize' => 42.0
				,'minfontsize' => 12.0
				,'textcolor' => $this->white
				,'shadowcolor' => $this->black
				,'textletterx' => 0
				,'textlettery' => 0
				,'textboxx' => 102
				,'textboxy' => 668
				,'textboxwidth' => 545
				,'textboxheight' => 295

				,'levelfontsize' => 44.0
				,'levelcostx' => 88
				,'levelcosty' => 668
				,'levelcostw' => 127
				,'levelcosth' => 88

				,'isshading' => false
			];

			cardfuncs_makeRulesboxLeveler($options, $this);
		}
		else {
			// regular card

			$options = [
				'textfont' => $this->textfont
				,'italicfont' => $this->italicfont
				,'fontsize' => 38.0
				,'minfontsize' => 12.0
				,'textcolor' => $this->white
				,'shadowcolor' => $this->black
				,'textletterx' => 0
				,'textlettery' => 0
				,'textboxx' => 96
				//96 //648
				,'textboxy' => 738
				//738 //962
				,'textboxwidth' => 545
				,'textboxheight' => 224
				,'rulestext' => $this->q['rulestext']
				,'flavortext' => $this->q['flavortext']
				,'xalign' => 'center'
				,'yalign' => 'bottom'
				,'angle' => '0'

			];

			cardfuncs_makeRulesbox($options, $this);
		}
	}

	function drawSetIcon() {
		// draw set icon, with rarity colors filled

		$options = [
			'w' => 54
			,'h' => 54
			,'x' => 742
			,'y' => 984
		];

		cardfuncs_makeSetIcon($options, $this);
	}

	function drawTypeIcon() {
		// write card type icon
		$options = [
			'x' => 46
			,'y' => 47
			,'w' => 48
			,'h' => 40
			,'invert' => true
			,'genre' => $this->q['genre']
			,'cardtype' => $this->q['cardtype']
		];

		cardfuncs_makeTypeIcon($options, $this);
	}

	function drawOverlay() {
        $options = [
            'ovx' => 0
            ,'ovy' => 0
            ,'ovw' => 750
            ,'ovh' => 987
        ];

		cardfuncs_makeOverlay($options, $this);
	}

	function createFinalImage() {
		// start creation of image
		$this->drawArt();
		$this->drawBackground();
		$this->drawBlend();
		$this->drawWatermark();
		$this->drawSetIcon();
		$this->drawManaCost();
		$this->drawArtistCreator();

		// to make text easier to read,
		// create temp image for text that will have colors inverted from art

		$this->tempim = $this->im;
		$this->im = new Imagick();
		$this->im->newImage($this::WIDTH, $this::HEIGHT, "none", "png");

		$this->drawCardName();
		$this->drawType();
		$this->drawTypeIcon();
		$this->drawPT();
		$this->drawRulesText();

		// merge tempim and im
		$this->tempim->compositeImage($this->im, imagick::COMPOSITE_LUMINIZE, 0, 0);
		$this->im = $this->tempim;

		$this->drawOverlay();

		/* Output the image*/
		return $this->im;
	}
} // end class def

$cardframe = new CardFrame_VogonFullArt($qarray);
?>
