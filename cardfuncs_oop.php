<?php

// autoload classes and libraries
$path = $_SERVER['DOCUMENT_ROOT'].'/lib';
set_include_path(get_include_path() . PATH_SEPARATOR . $path);
spl_autoload_register(function ($class) {
	include $class . '.php';
});

// globals

// store mana codes and corresponding URL to symbol image file
// from custommana field provided by user
$CUSTOMMANA = array();

$HYBRIDMANA = array(
	// c/d
	"w/u" => "wu",
	"u/w" => "wu",
	"w/b" => "wb",
	"b/w" => "wb",
	"w/r" => "rw",
	"r/w" => "rw",
	"w/g" => "gw",
	"g/w" => "gw",
	"u/b" => "ub",
	"b/u" => "ub",
	"u/r" => "ur",
	"r/u" => "ur",
	"u/g" => "gu",
	"g/u" => "gu",
	"b/r" => "br",
	"r/b" => "br",
	"b/g" => "bg",
	"g/b" => "bg",
	"r/g" => "rg",
	"g/r" => "rg",
	"c/w" => "cw",
	"w/c" => "cw",
	"c/u" => "cu",
	"u/c" => "cu",
	"c/b" => "cb",
	"b/c" => "cb",
	"c/r" => "cr",
	"r/c" => "cr",
	"c/g" => "cg",
	"g/c" => "cg",
	// phyrexian mana
	"p/c" => "pc",
	"c/p" => "pc",
	"p/w" => "pw",
	"w/p" => "pw",
	"p/u" => "pu",
	"u/p" => "pu",
	"p/b" => "pb",
	"b/p" => "pb",
	"p/r" => "pr",
	"r/p" => "pr",
	"p/g" => "pg",
	"g/p" => "pg",
	// hybrid phyrexian mana
	// always sort letters alphabetically in key
	"puw" => "pwu",
	"bpu" => "pub",
	"bpr" => "pbr",
	"gpr" => "prg",
	"gpw" => "pgw",
	"bpw" => "pwb",
	"pru" => "pur",
	"bgp" => "pbg",
	"prw" => "prw",
	"gpu" => "pgu",
	// tribrid mana
	// always sort letters alphabetically in key
	"bgw" => "bgw",
	"bgr" => "brg",
	"bgu" => "gub",
	"guw" => "gwu",
	"grw" => "rgw",
	"ruw" => "rwu",
	"bru" => "ubr",
	"gru" => "urg",
	"brw" => "wbr",
	"buw" => "wub",
	"fin" => "infinite",
	"ach" => "chaos",
);

$GENRES = [
	'magic' => [
		'supertypes' => ["Legendary", "Basic", "Snow", "World", "Ongoing", "Fast", "Delayed", "Token", "Elite", "Host"],
		'types' => ["Tribal", "Artifact", "Enchantment", "Land", "Creature", "Instant", "Sorcery", "Planeswalker", "Plane", "Scheme", "Emblem", "Hero", "Conspiracy", "Phenomenon", "Avatar"]
	],

	'earth' => [
		'supertypes' => ["Famous", "Basic", "Magical", "Global", "Ongoing", "Fast", "Delayed", "Clone", "Super", "Host"],
		'types' => ["Faction", "Object", "Agenda", "Land", "Agent", "Mod", "Event", "Paragon", "Scene", "Point", "Paradigm", "Hero", "Villain", "Power", "Avatar"]
	],

	'space' => [
		'supertypes' => ["Mythical", "Primary", "Dark", "Universe", "Ongoing", "Fast", "Delayed", "Clone", "Mega", "Host"],
		'types' => ["Faction", "Device", "Augment", "Resource", "Unit", "Tactic", "Strategy", "Galaxystalker", "Galaxy", "Scheme", "Paradigm", "Hero", "Conspiracy", "Phenomenon", "Avatar"]
	],

	'pokemon' => [
		'supertypes' => ["Baby", "Basic", "Stage 1", "Stage 2", "", "", "Special", "", "", ""],
		'types' => ["", "", "", "Energy", "Pokémon", "Trainer", "", "Pokémon", "", "", "", "", "", "", ""]
	],

	'mbs' => [
		'supertypes' => ["", "", "", "", "", "", "", "", "", ""],
		'types' => ["", "Equipment", "", "Battleground", "Unit", "", "Action", "", "", "", "", "", "", "", ""]
	],
];

$ENUM_ARTIFACT = 1;
$ENUM_CREATURE = 4;
$ENUM_PLANESWALKER = 7;
$ENUM_SCENE = 8;


// define functions

//*
function storeCustomMana($input) {
	global $CUSTOMMANA;

	$isFile = stripos(trim($input), 'http');

	if ($isFile !== false && $isFile == 0) {
		// check URL with cURL

		$input = trim($input);

		$ch = @curl_init($input);
		if($ch === false){
			return false;
		}

		$timeout = 60; // seconds

		@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	// catch output (do NOT print!)
		@curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);	// prevent waiting forever to get a result

		$content = @curl_exec($ch);
		$header = curl_getinfo($ch);
		curl_close($ch);

		// VSV or JSON file
		if (stristr($input, '.vsv')) {
			spl_autoload("vsv_oop");
			$vsv = new vsv($content);
			$CUSTOMMANA = array_merge($CUSTOMMANA, $vsv->mapToHash());
			return;
		}
		elseif ($header['content_type'] == 'application/json') {
			$CUSTOMMANA = array_merge($CUSTOMMANA, json_decode($content, true));
			return;
		}
		else {
			return;
		}
	}
	else {
		$codes = explode( '#', $input);

		// remove blanks from the start of array
		while (count($codes) > 0 && isset($codes[0]) && trim($codes[0]) == '') {
			array_shift($codes);
		}
	}

	// attempt to store pairs of (manacode => URL)
	for ($i=0; $i<count($codes); $i+=2) {
		// TODO: more checks? alphanumeric and / slash only;- format c or c/d

		$b0 = isset($codes[$i]);
		$b1 = isset($codes[$i+1]);

		if ($b0 && $b1) {
			$CUSTOMMANA[$codes[$i]] = $codes[$i+1];
		}
	}
}

function cardfuncs_conjoined_cardframe_op(&$obj, $vars, $func) {
	// $obj = container array (ex. cardframe_vogonhd->q)
	// $vars = array of variable names found in container (ex. ['extra', 'rulestext', 'flavortext'])
	// $func = function to operate on conjoined vars (ex. "cardfuncs_replaceMemtext")

	// join the texts given by the $obj->q[var]
	// call func() on entire text
	// separate the returned text back into $obj->q[var]

	/*
	cclog("\n");
	cclog("\n");
	cclog("CALLED cardfuncs_conjoined_frame_op()");
	cclog("\n");
	cclog('obj: ' . print_r($obj, true));
	cclog("\n");
	cclog('vars: ' . print_r($vars, true));
	cclog('count($vars): ' . count($vars));
	cclog("\n");
	cclog('func: ' . print_r($func, true));
	//*/

	$arr = [];
	for ($i=0, $len=count($vars); $i < $len; $i++) {
		$arr[] = $obj[$vars[$i]];
	}

	$text = implode("CARDFUNCSDIVIDER", $arr);

	if (is_string($func)) {
		$func = [$func];
	}

	for ($i=0, $len=count($func); $i < $len; $i++) {
		$text = call_user_func($func[$i], $text, $obj);
	}

	$arr = explode("CARDFUNCSDIVIDER", $text);

	for ($i=0, $len=count($vars); $i < $len; $i++) {
		@$obj[$vars[$i]] = $arr[$i];
	}
}

function cardfuncs_replaceMemtext($text) {
	$memtext = [];

	// remember text
	while (preg_match('/(?<!`)`([^`]+)`/u', $text, $matches)) {
		$memtext[] = $matches[1];
		$text = preg_replace('/(?<!`)`([^`]+)`/u', "$1", $text, 1);
	}
	// replace memtext
	while(preg_match('/``(\d+)/u', $text, $matches)) {
		$memi = intval($matches[1]);
		$repltext = isset($memtext[$memi]) ? $memtext[$memi] : "";
		$text = preg_replace('/``(\d+)/u', $repltext, $text, 1);
	}
	return $text;
}

function cardfuncs_replaceCardTraits($text, $obj) {
	// turn ~ or ~this~ or CARDNAME or ``CN into card's name
	// after the first replacement, use only the short name
	// short name = part before the first comma in name

	$namepattern = '/(~this~|~|CARDNAME|``CN|``NAME)/u';

	$name = $obj['cardname'];
	$shortName = preg_replace('/(.+),.*/u', "$1", $name, 1);
	$text = preg_replace('/(~~)/u', $shortName, $text);
	$text = preg_replace($namepattern, $name, $text, 1);
	$text = preg_replace($namepattern, $shortName, $text);

	// other card traits
	$text = preg_replace('/(``PT)/u', $obj['powertoughness'], $text);
	$text = preg_replace('/(``SUPER|``SUP)/u', cardfuncs_getsupertype($obj['supertype'], $obj['genre']), $text);
	$text = preg_replace('/(``TYPE)/u', cardfuncs_getcardtype($obj['cardtype'], $obj['genre']), $text);
	$text = preg_replace('/(``SUB)/u', $obj['subtype'], $text);
	$text = preg_replace('/(``MC|``COST)/u', '{' . $obj['manacost'] . '}', $text);
	$text = preg_replace('/(``CR)/u', $obj['creator'], $text);

	return $text;
}

function cardfuncs_getblendoptions($manacost, $rulestext) {
	$blendoptions = [];
	$blendoptions['colors'] = [];

	// deduce mana symbols in rules text and add it to manacost for parsing
	preg_match_all('/\{(.*?)}/', strtolower($rulestext), $matches);
	$manacost .= implode($matches[0]);

	// extract mana from manacost
	$manacost = preg_replace('/(.)/', '$1,', $manacost);
	$manacost = explode(',', $manacost);

	// parse manacost
	foreach($manacost as $value) {
		if ( preg_match('/[bgruw]/', $value)) {
			if (!in_array($value, $blendoptions['colors'])) {
				$blendoptions['colors'][] = $value;
			}
		}
	}

	return $blendoptions;
}


// http://phpimagick.com/Tutorial/imagickCompositeGen
function cardfuncs_renderblend($options, &$frame) {
	$defaults = [
		'width' => 375
		,'height' => 523
		,'black' => "#000"
		,'blendstartrate' => 0.4
		,'blendendrate' => 0.6
		,'contrast' => 20
		,'midtone' => 0.0
		,'extension' => "jpg"
		,'blendframemask' => "multicolor_blend_card.png"
		,'landscape' => false
		,'blendgradientmask' => ''
	];
	$options = array_merge($defaults, (array)$options);
	// set variables within local scope
	foreach($options as $key => $option) {
		// this is to make sure that only intended arguments are passed
		if(isset($defaults[$key])) {
			${$key} = $option;
		}
	}

	$im = $frame->im;
	$dirframe = $frame->dirframe;
	$cardback = $frame->q['cardback'];
	$blendoptions = $frame->blendoptions;
	$blendframemask = "$dirframe/$blendframemask";

	if (isset($blendoptions["colors"])) {
		$land = preg_match('/lcard.jpg/', $cardback) ? 'l' : '';
		if (count($blendoptions["colors"]) == 1) {
			$blendim = new Imagick($dirframe . "/" . $blendoptions["colors"][0] . $land . "card.$extension");
		}

		if (count($blendoptions["colors"]) == 2) {

			$blendstart = $width * $blendstartrate;
			$blendend = $width * $blendendrate;
			$blendim = new Imagick();
			$blendim->newImage($width, $height, $black, "png");
			$blendleft = new Imagick($dirframe . "/" . $blendoptions["colors"][0] . $land . "card.$extension");
			$blendright = new Imagick($dirframe . "/" . $blendoptions["colors"][1] . $land . "card.$extension");

			$blendleft->cropImage($blendend, $height, 0, 0);
			$blendright->cropImage($width - $blendstart, $height, $blendstart, 0);

			$blendgradientleft = new Imagick();
			$blendgradientleft->newPseudoImage($height, $blendend, 'gradient:black-white');
			$quantum = $blendgradientleft->getQuantum();
			$blendgradientleft->sigmoidalContrastImage(true, $contrast, $midtone * $quantum);
			$blendgradientleft->rotateImage('black', 90);

			$blendgradientright = clone $blendgradientleft;
			$blendgradientright->rotateImage('black', 180);

			$blendleft->compositeImage($blendgradientleft, imagick::COMPOSITE_COPYOPACITY, 0, 0);
			$blendim->compositeImage($blendleft, imagick::COMPOSITE_ATOP, 0, 0);

			$blendright->compositeImage($blendgradientright, imagick::COMPOSITE_COPYOPACITY, 0, 0);
			$blendim->compositeImage($blendright, imagick::COMPOSITE_BLEND, $blendstart, 0);
		}

		if (3 == count($blendoptions["colors"])) {
			$blendleft = new Imagick($dirframe . "/" . $blendoptions["colors"][0] . $land . "card.$extension");
			$blendim = new Imagick($dirframe . "/" . $blendoptions["colors"][1] . $land . "card.$extension");
			$blendright = new Imagick($dirframe . "/" . $blendoptions["colors"][2] . $land . "card.$extension");

			if ('' != $blendgradientmask) {
				// $blendgradientmask is prefix to 1st, 2nd, 3rd mask files
				$extension = 'png';
				$finalim = new Imagick();
				$finalim->newImage($width, $height, $frame->clear);
				$blendimages = [$blendleft, $blendim, $blendright];
				for ($i=1; file_exists("$dirframe/$blendgradientmask$i.$extension"); $i++) {
					$mask = new Imagick("$dirframe/$blendgradientmask$i.$extension");
					$mask->setImageMatte(false);
					$blendimages[$i-1]->compositeImage($mask, imagick::COMPOSITE_COPYOPACITY, 0, 0);
					$finalim->compositeImage($blendimages[$i-1], imagick::COMPOSITE_OVER, 0, 0);
				}
				$blendim = $finalim;
			}
			elseif ($options['landscape']) {
				$newWidth = $width/2;
				$blendstart = $newWidth * $blendstartrate;
				$blendend = $newWidth * $blendendrate;
				$blendleft->cropImage($blendend, $height, 0, 0);
				$blendright->cropImage($blendend, $height, $width - $blendend, 0);
				$blendgradientleft = new Imagick();
				$blendgradientleft->newPseudoImage($height, $blendend, 'gradient:white-black');
				$quantum = $blendgradientleft->getQuantum();
				$blendgradientleft->sigmoidalContrastImage(true, $contrast, $midtone * $quantum);
				$blendgradientleft->rotateImage('black', 270);
				$blendgradientright = clone $blendgradientleft;
				$blendgradientright->rotateImage('black', 180);

				$blendleft->compositeImage($blendgradientleft, imagick::COMPOSITE_COPYOPACITY, 0, 0);
				$blendim->compositeImage($blendleft, imagick::COMPOSITE_OVER, 0, 0);

				$blendright->compositeImage($blendgradientright, imagick::COMPOSITE_COPYOPACITY, 0, 0);
				$blendim->compositeImage($blendright, imagick::COMPOSITE_OVER, $width - $blendend, 0);
			}
			else {
				$blendleft->cropImage($width, $height/2, 0, 0);
				$blendright->cropImage($width, $height/2, 0, $height/2);

				$blendgradientleft = new Imagick();
				$blendgradientleft->newPseudoImage($width, $height/2, 'gradient:white-black');

				$blendgradientright = clone $blendgradientleft;
				$blendgradientright->rotateImage('black', 180);

				$blendleft->compositeImage($blendgradientleft, imagick::COMPOSITE_COPYOPACITY, 0, 0);
				$blendim->compositeImage($blendleft, imagick::COMPOSITE_OVER, 0, 0);

				$blendright->compositeImage($blendgradientright, imagick::COMPOSITE_COPYOPACITY, 0, 0);
				$blendim->compositeImage($blendright, imagick::COMPOSITE_OVER, 0, $height/2);
			}
		}

		if (isset($blendim)) {
			$blendmask = new Imagick($blendframemask);
			$blendim->compositeImage($blendmask, imagick::COMPOSITE_COPYOPACITY, 0, 0);

			$im->compositeImage($blendim, imagick::COMPOSITE_OVER, 0, 0);
		}
	}

	return $im;
}

function cardfuncs_getsupertype($supertype, $genre) {
	global $GENRES;
	$typetext = "";
	extract($GENRES[strtolower($genre)]);

	for ($i=-1; isset($supertypes[++$i]);) {
		if ( substr($supertype, $i, 1)=='1' ) {
			$typetext .= $supertypes[$i] . " ";
		}
	}

	return trim($typetext);
}

function cardfuncs_getcardtype($cardtype, $genre) {
	global $GENRES;
	$typetext = "";
	extract($GENRES[strtolower($genre)]);

	for ($i=-1; isset($types[++$i]);) {
		if ( substr($cardtype, $i, 1)=='1' ) {
			$typetext .= $types[$i] . " ";
		}
	}

	return trim($typetext);
}

function cardimage_gettypeline($supertype, $cardtype, $subtype, $genre = "magic", $delimiter = "— ") {
	global $GENRES;
	$typetext = "";
	$genre = strtolower($genre);

	extract($GENRES[$genre]);

	for ($i=-1; isset($supertypes[++$i]);) {
		if ( substr($supertype, $i, 1)=='1' ) {
			$typetext .= $supertypes[$i] . " ";
		}
	}
	for ($i=-1; isset($types[++$i]);) {
		if ( substr($cardtype, $i, 1)=='1' ) {
			$typetext .= $types[$i] . " ";
		}
	}

	$subtype = str_replace('--', '—', $subtype);

	$typetext .= ($typetext != "" && $subtype != "" ? $delimiter : '') . $subtype;

	return $typetext;
}

function ccwrap($fontSize, $angle, $fontFace, $string, $width) {
	$ret = "";
	$arr = explode(' ', $string);
	foreach ( $arr as $word ) {
		$teststring = $ret.' '.$word;
		$testbox = imagettfbbox($fontSize, $angle, $fontFace, $teststring);
		if ( $testbox[2] > $width ) {
			$ret.=($ret==""?"":"\n").$word;
		} else {
			$ret.=($ret==""?"":" ").$word;
		}
	}
	return $ret;
}

function gdwrap($fontSize, $angle, $fontFace, $string, $width) {
	$ret = "";
	$arr = explode(' ', $string);
	foreach ( $arr as $word ) {
		$teststring = $ret.' '.$word;
		$testbox = imagettfbbox($fontSize, $angle, $fontFace, $teststring);
		if ( $testbox[2] > $width ) {
			$ret.=($ret==""?"":"\n").$word;
		} else {
			$ret.=($ret==""?"":" ").$word;
		}
	}
	return $ret;
}

function draw_line(&$im, $x, $y, $w, $h, &$draw = null) {
	$draw = $draw ?? new ImagickDraw();
	$draw->clear();
	$draw->rectangle($x, $y, $x + $w, $y + $h);
	$im->drawImage($draw);
}

function draw_column_lines(&$im, $columnws, $y, $h, $wmax, &$draw = null) {
	// vertical lines between columns of previous row
	$draw = $draw ?? new ImagickDraw();
	$x = 0;
	foreach ($columnws as $dx) {
		$x += $dx;
		draw_line($im, $x, $y, 1, $h, $draw);
	}
	draw_line($im, $wmax-2, $y, 1, $h, $draw); // right edge
}

function cardfuncs_makeRulesboxImage($options, $frame) {
	global $HYBRIDMANA, $CUSTOMMANA;

	// write rules and flavor text
	// print text word by word and mana icons

	$defaults = [
		'textfont' => $_SERVER['DOCUMENT_ROOT'] . "/fonts/crimson-regular.ttf"
		,'boldfont' => $_SERVER['DOCUMENT_ROOT'] . "/fonts/crimson-bold.ttf"
		,'italicfont' => $_SERVER['DOCUMENT_ROOT'] . "/fonts/crimson-italic.ttf"
		,'bolditalicfont' => $_SERVER['DOCUMENT_ROOT'] . "/fonts/crimson-bolditalic.ttf"
		,'fontsize' => 26.0
		,'minfontsize' => 6.0
		,'angle' => 0
		,'textcolor' => "#000"
		,'textletterx' => 0
		,'textlettery' => 0
		,'textboxx' => 38
		,'textboxy' => 329
		,'textboxwidth' => 301
		,'textboxheight' => 139
		,'rulestext' => ''
		,'flavortext' => ''
		,'shadowcolor' => FALSE
		,'shadowratio' => 0.075
		,'outlinecolor' => FALSE
		,'outlineratio' => 0.01
		,'allupper' => FALSE
		,'alllower' => FALSE
		,'dyratio' => 1.0
		,'manaratio' => 0.85
		,'manafontratio' => 1.2
	];
	$options = array_merge($defaults, (array)$options);
	// set variables within local scope
	foreach($options as $key => $option) {
		// this is to make sure that only intended arguments are passed
		if(isset($defaults[$key])) {
			${$key} = $option;
		}
	}

	$im = $frame->im;
	$draw = new ImagickDraw();
	$manadraw = $frame->manadraw;
	$clear = $frame->clear;
	$dirmana = $frame->dirmana;
	$mcircle = $frame->mcircle;
	$textdivider = isset($frame->textdivider) ? $frame->textdivider : null;
	$newlineratio = 1.30;
	$flavorlinepadding = 1.60;
	$fontsizedelta = $fontsize * 0.038;
	$kerningratio = 0.2;

	$black = new ImagickPixel("black");
	$white = new ImagickPixel("white");
	$draw->setTextAlignment(Imagick::ALIGN_LEFT);
	$draw->setTextDecoration(Imagick::DECORATION_NO);
	$drawshadow = clone $draw;
	if ($shadowcolor) {
		$drawshadow->setFillColor($shadowcolor);
	}

	// clean up text first
	// remove consecutive newlines and extra newlines
	$rulestext = trim($rulestext);
	$rulestext = preg_replace("/[\r\n]+/", "\n", $rulestext);
	$flavortext = trim($flavortext);
	$flavortext = preg_replace("/[\r\n]+/", "\n", $flavortext);
	if (''== trim($rulestext) . trim($flavortext)) {
		return;
	}

	if ($allupper) {
		$rulestext = strtoupper($rulestext);
		$flavortext = strtoupper($flavortext);
	}
	elseif ($alllower) {
		$rulestext = strtolower($rulestext);
		$flavortext = strtolower($flavortext);
	}

	// columns

	// find optimal font size
	do {
		$fontsize -= $fontsizedelta;
		$draw->setFontSize($fontsize);
		$drawshadow->setFontSize($fontsize);

		// render rules and flavor text
		$isitalic = false;
		$isbold = false;
		$isunderline = false;
		$ismanamode = false;
		$undery = $fontsize * 1.15;
		$undery1 = $undery + 1;
		$textbox = new Imagick();
		$letterx = $letterxstart = $recordx = $textletterx;
		$lettery = $textlettery;
		$boxwidth = $textboxwidth;
		$columnystart = $textlettery;
		$columnhmax = 0;
		$columnnow = 0;
		$columnparts = 0;
		$columnws = [0];
		$columnpadding = $fontsize * 0.25;
		$currfont = $textfont;
		$draw->setFont($currfont);
		$drawshadow->setFont($currfont);
		$draw->setFillColor($textcolor);
		$timg = new Imagick();
		$mana = '';
		$manadraw->setFont($currfont);
		$manadraw->setTextAlignment(imagick::ALIGN_CENTER);
		$bbox = $im->queryFontMetrics($draw, "fg");
		$bboxheight = $fontsize; //$bbox['ascender'] - $bbox['descender'];
		$dy = max($bboxheight * $dyratio, 6);
		$dynew = $dy * $newlineratio;
		$manaiconfactor = $bboxheight * $manaratio;
		$manaiconwidth = $bboxheight * $manaratio;
		$manaiconheight = $bboxheight * $manaratio;
		$kerning = $fontsize * $kerningratio * $manaratio;
		$memtext = [];

		if ($outlinecolor) {
			$draw->setStrokeColor($outlinecolor);
			$draw->setStrokeWidth($outlineratio * $fontsize);
		}

		$textbox->newImage($textboxwidth, $textboxheight + $textboxheight, $clear);

		// Rules Text
		$text = $rulestext;
		while ($text != "") {
			// check columns
			if (preg_match('/^[\n ]*(<<<<|>>>>|<<#(\d*(,\d*)*)\||<<\|\d*\||\||<<>>)[\n ]*/', $text, $matches)) {
				$recordx = $textboxwidth;
				$columntype = $matches[1];
				$text = preg_replace('/^([\n ]*(<<<<|>>>>|<<#(\d*(,\d*)*)\||<<\|\d*\||\||<<>>)[\n ]*)/', "", $text);

				switch ($columntype) {
					case '<<<<':
						$text = "<<|2|$text";
						continue 2;
						break;
					case '>>>>':
						if ($columnnow == 1) {
							// previously a left column
							$text = "|$text";
							continue 2;
						}
						else {
							// create new table row
							$text = "<<|2||$text";
							continue 2;
						}
						break;
					case '<<>>':
						// end column mode
						if ($columnparts) {
							// is column mode
							// draw vertical lines between columns of previous row
							draw_column_lines($textbox, $columnws, $columnystart, $columnhmax + $dynew, $textboxwidth);
							$lettery = $columnystart + $columnhmax + $dynew;
						}
						else {
							// was not column mode
							if ($lettery != $textlettery) {
								$lettery += $dynew;
							}
						}

						// horizontal line
						draw_line($textbox, 0, $lettery-1, $textboxwidth, 1);

						$columnnow = $columnparts = 0;
						$boxwidth = $textboxwidth;
						$letterx = $letterxstart = $textletterx;
						$liney = $lettery;
						break;
					case '|':
						if (!$columnparts) {
							// not in column mode, just print the pipe
							$text = "\\|$text";
							continue 2;
						}

						// create new row

						// horizontal lines above new row
						if ($columnnow >= $columnparts) {
							// draw vertical lines between columns of previous row
							draw_column_lines($textbox, $columnws, $columnystart, $columnhmax + $dynew, $textboxwidth);
							$columnystart += $columnhmax + $dynew;
						}

						if (-1 == $columnnow || $columnnow >= $columnparts) {
							// reset stats for new row
							$columnhmax = 0;
							$columnxleft = $textletterx + $columnpadding;
							$columnnow = 0;
						}
						draw_line($textbox, 0, $columnystart, $textboxwidth, 1);

						// advance column
						$lettery = $columnystart;
						$liney = $columnystart;
						$columnwidth = $columnws[$columnnow];
						$columnxleft += $columnwidth;
						$letterx = $letterxstart = $columnxleft;
						$columnwidth = $columnws[++$columnnow];
						$boxwidth = $columnwidth - $columnpadding * 2; // space to write text, minus padding
						break;
					default:
						// applies only to left table starter
						// either <<| or <<#

						// initiate new table
						if (preg_match( '/^<<\|(\d*)\|/', $columntype, $matches)) {
							// columns of equal width
							if (is_numeric($matches[1]) && intval($matches[1]) > 1) {
								$parts = intval($matches[1]) ?? 1;
							}
							else {
								$parts = 1; // default number of columns
							}
							$ratios = str_repeat("1,", $parts);
							$text = "<<#$ratios|$text";
							continue 2;
						}
						elseif (preg_match( '/^<<#(\d*(,\d*)*)\|/', $columntype, $matches)) {
							// columns of variable widths

							// remember previous table
							if ($columnparts) {
								// is column mode
								draw_column_lines($textbox, $columnws, $columnystart, $columnhmax + $dynew, $textboxwidth);

								$lettery = $columnystart + $columnhmax + $dynew;
							}
							else {
								// was not column mode
								$lettery += $dynew;
							}

							// find column parts for new table
							$columnparts = 0;
							$columnsum = 0;
							$columnws = [0];
							foreach (explode(",", $matches[1]) as $value) {
								$v = intval($value);
								if ($v) {
									$columnparts++;
									$columnsum += $v;
									$columnws[] = $v;
								}
							}
							$columnws = array_map(function($v) use ($textboxwidth, $columnsum) {
								return (int)round($textboxwidth * $v / $columnsum);
							}, $columnws);

							// advance to first cell
							$columnnow = -1;
							$columnystart = $lettery;
							$text = "|$text";
							continue 2;
						}
						break;
				}

			}
			elseif ($ismanamode) {
				if (preg_match('/^}+/u', $text, $matches)) {
					$ismanamode = false;
					$text = preg_replace('/^}+/u', "", $text);
					continue;
				}

				// start mana symbols
				$manaimg = new Imagick();
				$manaimg->newImage( $boxwidth, $manaiconheight, $clear);
				$manaimgw = 0;
				$manafound = false;

				if (preg_match('/^([0-9a-z])\/([0-9a-z])\/([0-9a-z])/i', $text, $hybmatch)) {
					// mana is a/b/c ; e.g. hybrid phyrexian or tribrid
					$mana = strtolower($hybmatch[0]);
					if (isset($CUSTOMMANA[$mana])) {
						// custom mana
						$manafound = true;
						$file = $CUSTOMMANA[$mana];
						if (0==preg_match("/^http(s)?:\/\//", $file)) {
							$file = "http://$file";
						}
						if (isValidUrl($file)) {
							try {
								$mc = new Imagick($file);
								$mc->thumbnailImage($manaiconwidth, $manaiconheight);
							}
							catch (ImagickException $e) {
								//cclog( "\n" . 'FAILED tribrid custom mana ' . $e);
							}
						}
					}
					else {
						$hybmatch = array_slice($hybmatch, 1);
						natcasesort($hybmatch);
						$halfvalue = strtolower(implode('', $hybmatch));
						try {
							$halfvalue = $HYBRIDMANA[$halfvalue];
							$mc = new Imagick( "$dirmana/mana_$halfvalue.png" );
							$manafound = true;
						} catch (ImagickException $e) {
							//cclog( "\n" . 'FAILED tribrid mana ' . $e);
						}
					}
				}

				if (!$manafound) {
					if ( preg_match('/^([0-9a-z]\/[0-9a-z])/i', $text, $hybmatch)) {
						// is hybrid mana
						$mana = strtolower($hybmatch[0]);
						if (isset($CUSTOMMANA[$mana])) {
							// custom mana
							$manafound = true;
							$file = $CUSTOMMANA[$mana];
							if (0==preg_match("/^http(s)?:\/\//", $file)) {
								$file = "http://$file";
							}
							if (isValidUrl($file)) {
								try {
									$mc = new Imagick($file);
									$mc->thumbnailImage($manaiconwidth, $manaiconheight);
								}
								catch (ImagickException $e) {
									//cclog( "\n" . 'FAILED hybrid custom mana ' . $e);
								}
							}
						}
						elseif (isset($HYBRIDMANA[$mana])) {
							try {
								$halfvalue = $HYBRIDMANA[$mana];
								$mc = new Imagick( "$dirmana/mana_$halfvalue.png" );
								$manafound = true;
							} catch (ImagickException $e) {
								//cclog( "\n" . 'FAILED hybrid mana ' . $e);
							}
						}
						else {
							// probably includes number, like 2/c or c/2
							$halfcolor = strtolower(preg_replace('/^.*([a-z]).*/i', "$1", $mana));
							$halfvalue = preg_replace('/^.*([0-9]).*/', "$1", $mana);
							try {
								$mc = new Imagick( "$dirmana/mana_n$halfcolor.png" );
								$manafound = true;
								$mana = '...';
								$mcw = $mc->getImageWidth();
								$mch = $mc->getImageHeight();
								$manadraw->setFontSize( $mcw * $manafontratio * 0.58 );
								$mcw = $mcw * .4;
								$mch = $mch * .5;
								$mc->annotateImage($manadraw, $mcw, $mch, 0, $halfvalue);
							} catch (ImagickException $e) {
								//cclog( "\n" . 'FAILED hybrid mana ' . $e);
							}
						}
					}
					elseif ( preg_match('/^[0-9a-z]/i', $text)) {
						// is valid color
						$mana = strtolower(substr($text, 0, 1));
						if (isset($CUSTOMMANA[$mana])) {
							// custom mana
							$manafound = true;
							$file = $CUSTOMMANA[$mana];
							if (0==preg_match("/^http(s)?:\/\//", $file)) {
								$file = "http://$file";
							}
							if (isValidUrl($file)) {
								try {
									$mc = new Imagick($file);
									$mc->thumbnailImage($manaiconwidth, $manaiconheight);
								}
								catch (ImagickException $e) {
									//cclog( "\n" . 'FAILED single custom mana ' . $e);
								}
							}
						}
						else {
							try {
								$mc = new Imagick( "$dirmana/mana_$mana.png");
								$manafound = true;
							} catch (ImagickException $e) {
								//cclog( "\n" . 'FAILED single mana ' . $e);
							}
						}
					}
				}

				if (!$manafound) {
					// is any other text
					$mana = strtoupper(substr($text, 0, 1));
					$mc = clone $mcircle;
					$mcw = $mc->getImageWidth();
					$mch = $mc->getImageHeight();
					$manadraw->setFontSize( $mcw * $manafontratio );
					$manadraw->setFillColor($black);
					$mc->annotateImage($manadraw, $mcw * .52, $mch * .8, 0, $mana);
				}

				if (isset($mc)) {
					$mch = $manaiconheight;
					$mcw = $mch * $mc->getImageWidth() / $mc->getImageHeight();
					$mc->scaleImage( $mcw, $mch );

					$manaimg->compositeImage( $mc, imagick::COMPOSITE_OVER, $manaimgw, 0 );
					$manaimgw += $mcw + 1;
				}
				unset($mc);

				$text = substr($text, strlen($mana));

				// punctuation directly after mana symbols
				preg_match('/^}+([,.:]+)/', $text, $matches);
				$posttext = !isset($matches[1]) ? '' : $matches[1];
				$bbox = $timg->queryFontMetrics($draw, $posttext);
				$bboxw = max($bbox['originX'], 1);
				$bboxh = max($bbox['textHeight'], 1);
				$timg->newImage($bboxw, $bboxh, $clear);
				if ($posttext != '') {
					$text = substr($text, strlen($matches[0]));
					$ismanamode = false;
					if ($shadowcolor) {
						for ($i = (int)round($shadowratio * $fontsize); $i; $i--) {
							$timg->annotateImage($drawshadow, $i, $fontsize+$i, 0, $posttext);
						}
					}
					$timg->annotateImage($draw, 0, $fontsize, 0, $posttext);
				}

				// check if mana + text goes past text box borders
				if ($letterx > $letterxstart && $letterx + $manaimgw + $timg->getImageWidth() - $letterxstart >= $boxwidth) {
					$lettery += $dy;
					if ($columnparts) {
						$columnhmax = max($columnhmax, $lettery - $columnystart);
					}

					// early escape
					if ($lettery >= $textboxheight) {
						if ($fontsize > $minfontsize) {
							continue;
						} else {
							break;
						}
					}

					$letterx = $letterxstart;
				}

				// finally copy mana and text to text box
				$textbox->compositeImage( $manaimg, imagick::COMPOSITE_OVER, $letterx, $lettery + $fontsize * .3 );
				$letterx += $manaimgw;
				$textbox->compositeImage( $timg, imagick::COMPOSITE_OVER, $letterx, $lettery);
				$letterx += $timg->getImageWidth();
				$recordx = max($recordx, $letterx);
			} // end mana symbols
			// normal text
			else {
				// check if starting or ending mana mode
				if (preg_match('/^{+/u', $text, $matches)) {
					$ismanamode = true;
					$text = preg_replace('/^{+/u', "", $text);
					continue;
				}

				// check italic, bold, underline, or normal font
				if (preg_match('/^\(+[^ {[<)]*/u', $text, $matches)) {
					$isitalic = true;
					$text = preg_replace('/^\(+[^ {[<)]*/u', "", $text);
					$t = $matches[0];
				}
				elseif (preg_match('/^\)+/u', $text, $matches) ) {
					$isitalic = false;
					$text = preg_replace('/^\)+/u', "", $text);
					$t = $matches[0];
				}
				elseif (preg_match('/^\[+/u', $text, $matches)) {
					$isitalic = true;
					$text = preg_replace('/^\[+/u', "", $text);
					$t = "\x00";
				}
				elseif (preg_match('/^]+/u', $text, $matches) ) {
					$isitalic = false;
					$text = preg_replace('/^]+/u', "", $text);
					$t = "\x00";
				}
				elseif (preg_match('/^<+/u', $text, $matches)) {
					$isbold = true;
					$text = preg_replace('/^<+/u', "", $text);
					$t = "\x00";
				}
				elseif (preg_match('/^>+/u', $text, $matches) ) {
					$isbold = false;
					$text = preg_replace('/^>+/u', "", $text);
					$t = "\x00";
				}
				elseif (preg_match('/^_+/u', $text, $matches) ) {
					$isunderline = !$isunderline;
					$text = preg_replace('/^_+/u', "", $text);
					$t = "\x00";
				}
				elseif (preg_match('/^\\\\(.?)/u', $text, $matches) ) {
					// escape kara '\'
					$text = preg_replace('/^\\\\(.?)/u', "", $text);
					$t = $matches[1];
				}
				else {
					$pattern = "/^(\"?[A-Za-z0-9'\/\+\-]+[.,:-]*\"?\)?|[^[-\]({<_])/u";
					preg_match("$pattern", $text, $matches);
					$text = preg_replace("$pattern", "", $text);
					$t = $matches[0];
				}

				if ($isbold && $isitalic) {
					$currfont = $bolditalicfont;
				}
				elseif ($isbold) {
					$currfont = $boldfont;
				}
				elseif ($isitalic) {
					$currfont = $italicfont;
				}
				else {
					$currfont = $textfont;
				}
				$draw->setFont($currfont);
				$drawshadow->setFont($currfont);

				if (isset($t)) {
					$bbox = $textbox->queryFontMetrics($draw, $t);
					$bboxw = max($bbox['originX'], 1) + ($t=="" ? -1 : $kerning);
					$bboxh = max($bbox['textHeight'], 1);
					try {
						$timg->newImage( $bboxw, $bboxh, "none");

						if ($shadowcolor) {
							for ($i = (int)round($shadowratio * $fontsize); $i; $i--) {
								$timg->annotateImage($drawshadow, $i, $fontsize+$i, 0, $t);
							}
						}
						$timg->annotateImage($draw, 0, $fontsize, 0, $t);
								if ($isunderline) {
									 $draw->line(0, $undery, $bboxw, $undery);
									 $draw->line(0, $undery1, $bboxw, $undery1);
									 $timg->drawImage($draw);
									 $draw->resetVectorGraphics();
								}
					} catch (ImagickException $e) {
						/*
						cclog( "\n" . '$t = ' .  $t);
						cclog( "\n");
						cclog( $e );
						//*/
					}

					if ( strpos( $t, ")" ) ) {
						$isitalic = false;
					}
				}

				// check if text goes past text box borders, or newline
				$newline = preg_match("/\n/", $t);
				if ( $newline || ($letterx > $letterxstart && $letterx - $letterxstart + $bboxw >= $boxwidth) ) {
					$lettery += $newline ? $dynew : $dy;

					if ($columnparts) {
						$columnhmax = max($columnhmax, $lettery - $columnystart);
					}

					// early escape
					if ($lettery >= $textboxheight) {
						if ($fontsize > $minfontsize) {
							continue 2;
						} else {
							break 2;
						}
					}

					$letterx = $letterxstart;
				}

				// finally copy text to text box, skipping leading space
				if ( ! ($letterx == $letterxstart && preg_match("/\s/", $t))) {

					try {
						$textbox->compositeImage($timg, imagick::COMPOSITE_OVER, $letterx, $lettery);
					} catch (ImagickException $e) {
						//cclog( $e );
					}
					$letterx += $bboxw - $kerning;
					$recordx = max($recordx, $letterx);
				}
			} // end normal text
		} // end while text

		$isbold = false;
		$isunderline = false;

		// draw lines around table
		if ($columnparts) {
			// vertical lines between columns of previous row
			$lettery = $columnystart + $columnhmax + $dynew;
			draw_column_lines($textbox, $columnws, $columnystart, $lettery - $columnystart, $textboxwidth);

			// horizontal line
			draw_line($textbox, 0, $lettery-1, $textboxwidth, 1);
		}

		// Flavor Text
		if ($flavortext != "") {
			$text = $flavortext;
			$letterx = $letterxstart = $textletterx;
			$columnnow = 0;
			$boxwidth = $textboxwidth;
			if ($rulestext != "") {
				if ($columnparts) {
					// fix table alignment
					$lettery += $fontsize * $flavorlinepadding * 0.382;
				}
				else {
					$lettery += $fontsize * $flavorlinepadding;
				}

				// horizontal divider
				if ($textdivider) {
					$dividery = $lettery;
					$lettery += $textdivider->getImageHeight();
				}
			}
			$draw->setFont($italicfont);
			$drawshadow->setFont($italicfont);
			$pattern = "/^([^\s[-\]<>]+|\s+)/u";

			$isitalic = true;
			$isbold = false;
			$isunderline = false;
			while ($text != "") {
				// change font if text inside brackets [] <>
				if (preg_match('/^\[+/u', $text, $matches)) {
					$isitalic = false;
					$text = preg_replace('/^\[+/u', "", $text);
					continue;
				}
				elseif (preg_match('/^]+/u', $text, $matches) ) {
					$isitalic = true;
					$text = preg_replace('/^]+/u', "", $text);
					continue;
				}
				elseif (preg_match('/^<+/u', $text, $matches)) {
					$isbold = true;
					$text = preg_replace('/^<+/u', "", $text);
					$t = "\x00";
				}
				elseif (preg_match('/^>+/u', $text, $matches) ) {
					$isbold = false;
					$text = preg_replace('/^>+/u', "", $text);
					$t = "\x00";
				}
				elseif (preg_match('/^\\\\(.?)/u', $text, $matches) ) {
					// escape kara '\'
					$text = preg_replace('/^\\\\(.?)/u', "", $text);
					$t = $matches[1];
				}
				elseif (preg_match("$pattern", $text, $matches)) {
					$text = preg_replace("$pattern", "", $text);
					$t = $matches[0];
				}

				if ($isbold && $isitalic) {
					$currfont = $bolditalicfont;
				}
				elseif ($isbold) {
					$currfont = $boldfont;
				}
				elseif ($isitalic) {
					$currfont = $italicfont;
				}
				else {
					$currfont = $textfont;
				}
				$draw->setFont($currfont);
				$drawshadow->setFont($currfont);

				if (isset($t)) {
					$bbox = $textbox->queryFontMetrics($draw, $t);
					$bboxw = max($bbox['originX'], 1) + ($t=="" ? -1 : $kerning);
					$bboxh = max($bbox['textHeight'], 1);
					try {
						$timg->newImage( $bboxw, $bboxh, "none");

						if ($shadowcolor){
							for ($i = (int)round($shadowratio * $fontsize); $i; $i--) {
								$timg->annotateImage($drawshadow, $i, $fontsize+$i, 0, $t);
							}
						}
						$timg->annotateImage($draw, 0, $fontsize, 0, $t);
					} catch (ImagickException $e) {
						/*
						cclog( "\n" . '$t = ' .  $t);
						cclog( "\n");
						cclog( $e );
						//*/
					}

					if ( strpos( $t, ")" ) ) {
						$currfont = $textfont;
					}
				}

				// check if text goes past text box borders, or newline
				$newline = preg_match("/\n/", $t);
				if ( $newline || ($letterx > $letterxstart && $letterx - $letterxstart + $bboxw >= $boxwidth) ) {
					$lettery += $dy;

					// early escape
					if ($lettery >= $textboxheight) {
						if ($fontsize > $minfontsize) {
							continue 2;
						} else {
							break 2;
						}
					}

					$letterx = $letterxstart;
				}

				// finally copy text to text box, skipping leading space
				if ( ! ($letterx == $letterxstart && preg_match("/\s/", $t))) {

					try {
						$textbox->compositeImage($timg, imagick::COMPOSITE_OVER, $letterx, $lettery);
					} catch (ImagickException $e) {
						//cclog( $e );
					}
					$letterx += $bboxw - $kerning;
					$recordx = max($recordx, $letterx);
				}
			}
		}
		if (isset($dividery) && $textdivider) {
			// horizontal divider
			try {
				$dividerw = $textdivider->getImageWidth();
				$dividerx = ($recordx - $dividerw) / 2;
				$dividercopy = clone $textdivider;
				if ($recordx < $dividerw) {
					$dividercopy->cropImage( $recordx, $dividercopy->getImageHeight(), ($dividerw - $recordx) / 2, 0);
					$dividerx = 0;
				}
				$textbox->compositeImage($dividercopy, imagick::COMPOSITE_OVER, $dividerx, $dividery);
			}
			catch(Exception $e) {
			}

		}

		// trim image
		$textbox->trimimage(0);
		$textbox->setImagePage(0, 0, 0, 0);

	} while (($textbox->getImageHeight() > $textboxheight) && ($fontsize > $minfontsize) );

	// post editing, if image exists and dimensions valid
	try {
		// trim image
		$textbox->trimimage(0);
		$textbox->setImagePage(0, 0, 0, 0);

		if ($angle != 0) {
			$textbox->rotateImage($clear, $angle);
		}
	}
	catch (ImagickException $e) {
		// invalid image or dimensions
		// i.e. 0 width or height, due to blank text
	}

	return $textbox;
}

function cardfuncs_makeRulesbox( $options, &$frame ) {
	$defaults = [
		'fontsize' => 26.0
		,'minfontsize' => 6.0
		,'textcolor' => "#000"
		,'shadowcolor' => FALSE
		,'textletterx' => 0
		,'textlettery' => 0
		,'textboxx' => 38
		,'textboxy' => 329
		,'textboxwidth' => 301
		,'textboxheight' => 139
		,'rulestext' => ''
		,'flavortext' => ''
		,'angle' => 0
		,'xalign' => 'center'
		,'yalign' => 'center'
		,'manaratio' => 0.85
		,'manafontratio' => 1.2
	];
	$options = array_merge($defaults, (array)$options);
	// set variables within local scope
	foreach($options as $key => $option) {
		// this is to make sure that only intended arguments are passed
		if(isset($defaults[$key])) {
			${$key} = $option;
		}
	}

	$im = $frame->im;

	$box = cardfuncs_makeRulesboxImage($options, $frame);
	if (!$box) {
		return;
	}

	// consider angle, xalign, yalign
	$anglemod = cos($angle);
	$boxwidth = $box->getImageWidth();
	$boxheight = $box->getImageHeight();
	switch ($xalign) {
		case 'left':
			if ($angle==180) {
				$pastex = $textboxx - $boxwidth;
			}
			else {
				$pastex = $textboxx;
			}
			break;
		case 'right':
			if ($angle==180) {
				$pastex = $textboxx - $textboxwidth;
			}
			else {
				$pastex = $textboxx + ($textboxwidth - $boxwidth);
			}
			break;
		case 'center':
		default:
			if ($angle==180) {
				$pastex = $textboxx - $boxwidth - abs($textboxwidth - $boxwidth)/2;
			}
			else {
				$pastex = $textboxx + abs($textboxwidth - $boxwidth)/2;
			}
	}
	switch ($yalign) {
		case 'top':
			if ($angle==180) {
				$pastey = $textboxy - $boxheight;
			}
			else {
				$pastey = $textboxy;
			}
			break;
		case 'bottom':
			if ($angle==180) {
				$pastey = $textboxy - $textboxheight;
			}
			else {
				$pastey = $textboxy + ($textboxheight - $boxheight);
			}
			break;
		case 'center':
		default:
			if ($angle==180) {
				$pastey = $textboxy - $boxheight - abs($textboxheight - $boxheight)/2;
			}
			else {
				$pastey = $textboxy + abs($textboxheight - $boxheight)/2;
			}
	}
	$im->compositeImage( $box, imagick::COMPOSITE_OVER, $pastex, $pastey);
}

function cardfuncs_makeRulesboxPW( $options, &$frame) {
	$defaults = [
		'fontsize' => 26.0
		,'minfontsize' => 6.0
		,'textcolor' => "#000"
		,'shadowcolor' => FALSE
		,'textletterx' => 0
		,'textlettery' => 0
		,'textboxx' => 140
		,'textboxy' => 518
		,'textboxwidth' => 507
		,'textboxheight' => 449

		,'lcfontsize' => 23.0
		,'loyaltycostx' => 5
		,'loyaltycosty' => 518
		,'loyaltycostw' => 135
		,'loyaltycosth' => 86

		,'strokeWidth' => 5
		,'dividerColor' => "#000"
		,'dividerx' => 44
		,'dividerw' => 647

		,'isshading' => true
		,'manaratio' => 0.85
		,'manafontratio' => 1.2
	];
	$options = array_merge($defaults, (array)$options);
	// set variables within local scope
	foreach($options as $key => $option) {
		// this is to make sure that only intended arguments are passed
		if(isset($defaults[$key])) {
			${$key} = $option;
		}
	}

	$rulestext = trim($frame->q['rulestext']);
	$flavortext = trim($frame->q['flavortext']);
	$draw = $frame->draw;
	$im = &$frame->im;
	$dirframe = $frame->dirframe;
	$mcircle = $frame->mcircle;
	$manadraw = $frame->manadraw;
	$mcircle = $frame->mcircle;
	$white = $frame->white;
	$black = $frame->black;
	$clear = $frame->clear;
	$textfont = $frame->textfont;
	$italicfont = $frame->italicfont;
	$loyaltyfont = $frame->ptfont;

	$padding = 3;

	// count number of abilities and flavor text paragraphs
	$abils = $rulestext != "" ? explode("\n", $rulestext) : NULL;
	$flavors = $flavortext != "" ? explode("\n", $flavortext) : NULL;
	$numabils = $abils ? count($abils) : 0;
	$numflavors = $flavors ? count($flavors) : 0;
	$totallength = (strlen($rulestext) + strlen($flavortext));

	// store expected height of each ability
	$abilheight = [];
	for ($i = 0; $i < $numabils; $i++) {
		$abil = trim($abils[$i]);
		$abilh = $textboxheight * strlen($abil) / $totallength;
		$abilheight[$i] = [$i, $abilh];
	}
	for ($i = 0; $i < $numflavors; $i++) {
		$abil = trim($flavors[$i]);
		$abilh = $textboxheight * strlen($abil) / $totallength;
		$abilheight[$i + $numabils] = [$i + $numabils, $abilh];
	}
	$sumboxes = count($abilheight);

	// sort by height
	function cmpheight($a, $b) {
		if ($a[1] == $b[1]) {
			return 0;
		}
		return ($a[1] < $b[1]) ? -1 : 1;
	}

	usort($abilheight, "cmpheight");

	// ensure minimum height per ability
	$fontsize *= 1 - max($numabils + $numflavors - 2, 0) * 0.075;
	$minperheight = ($fontsize * 1.0 + $padding);
	$lastindex = $sumboxes - 1;
	for ($i = 0; $i < $lastindex; $i++) {
		if ($abilheight[0][1] < $minperheight) {
			$diff = $minperheight - $abilheight[0][1];
			$abilheight[0][1] += $diff;

			if ($numabils > 2) {
				// take the space from the highest two abities, proportionally
				$bothheights = $abilheight[$lastindex][1] + $abilheight[$lastindex-1][1];
				$abilheight[$lastindex][1] -= $diff * $abilheight[$lastindex][1] / $bothheights;
				$abilheight[$lastindex-1][1] -= $diff * $abilheight[$lastindex-1][1] / $bothheights;
			} else {
				// only one other ability
				$abilheight[$lastindex][1] -= $diff;
			}

			usort($abilheight, "cmpheight");
		}
		else {
			break;
		}
	}

	// sort by index
	function cmpindex($a, $b) {
		if ($a[0] == $b[0]) {
			return 0;
		}
		return ($a[0] < $b[0]) ? -1 : 1;
	}

	usort($abilheight, "cmpindex");

	// rules text
	$linex = $textboxx;
	$liney = $textboxy;
	$rect = new ImagickDraw();
	$rect->setFillColor( $dividerColor );

	for ($i = 0; $i < $numabils; $i++) {
		$abil = trim($abils[$i]);
		$abilh = $abilheight[$i][1];
		$options['textboxheight'] = $abilh - $padding * 2;

		// shaded overlay for each partition
		// alternating every other partition
		if ($isshading) {
			if ( $i % 2) {
				$shade = new Imagick();
				$shade->newImage($dividerw, $abilh, $black, "png");
				// second parameter sets alpha level
				$shade->transparentPaintImage($black, 0.125, 0, false);
				$im->compositeImage($shade, imagick::COMPOSITE_OVER, $dividerx, $liney);
			}
		}

		// effect
		$rules = preg_replace("/^ *[\[\(\{<]* *[+-]? *(\d+|x) *[\]\)\}>]* *:?(.*)$/i", "$2", $abil);
		$options['rulestext'] = $rules;
		$options['flavortext'] = '';
		$box = cardfuncs_makeRulesboxImage($options, $frame);
		if (isset($box)) {
			$im->compositeImage( $box, imagick::COMPOSITE_OVER, $textboxx, $liney + abs($abilh - $box->getImageHeight())/2);
		}

		if ($i > 0) {
			$rect->clear();
			$rect->rectangle( $dividerx, $liney, $dividerw + $dividerx, $liney+1);
			$im->drawImage($rect);
		}

		$liney += $abilh;
	}

	// flavor text
	for ($i = 0; $i < $numflavors; $i++) {
		$abil = trim($flavors[$i]);
		$abilh = $abilheight[$i + $numabils][1];
		$options['textboxheight'] = $abilh - $padding * 2;
		$options['flavortext'] = $abil;
		$options['rulestext'] = '';
		$box = cardfuncs_makeRulesboxImage($options, $frame);
		$im->compositeImage( $box, imagick::COMPOSITE_OVER, $textboxx, $liney + abs($abilh - $box->getImageHeight())/2);

		if ($i + $numabils > 0) {
			$rect->clear();
			$rect->rectangle( $linex, $liney, $textboxwidth + $linex, $liney+1);
			$im->drawImage($rect);
		}

		$liney += $abilh;
	}

	$liney = $textboxy;
	for ($i = 0; $i < $numabils; $i++) {
		// loyalty cost
		$abil = trim($abils[$i]);
		$abilh = $abilheight[$i][1];
		// print loyalty if valid value
		if (preg_match("/^ *[\[\(\{<]* *([+-]? *(\d+|x)) *[\]\)\}>]* *:?/i", $abil, $matches)) {
			$lcimgy = $liney + ($abilh-$loyaltycosth) / 2;
			$lc = $matches[1];
			if ($lc > 0) {
				$lcimg = new Imagick("$dirframe/loyaltyup.png");
			}
			elseif ($lc < 0) {
				$lcimg = new Imagick("$dirframe/loyaltydown.png");
			}
			else {
				$lcimg = new Imagick("$dirframe/loyaltynaught.png");
			}
			$lcimg->resizeImage($loyaltycostw, $loyaltycosth, imagick::FILTER_QUADRATIC, 1.0);
			$im->compositeImage($lcimg, imagick::COMPOSITE_OVER, $loyaltycostx, $lcimgy);
			$draw->setFillColor($white);
			$draw->setFont($loyaltyfont);
			$draw->setFontSize($lcfontsize);
			$draw->setTextAlignment(imagick::ALIGN_CENTER);
			$im->annotateImage($draw, $loyaltycostx + $loyaltycostw * 0.475, $lcimgy + $loyaltycosth * 0.62, 0, $lc);
			$draw->setTextAlignment(imagick::ALIGN_LEFT);
		}

		$liney += $abilh;
	}
}

function cardfuncs_makeRulesboxLeveler( $options, &$frame) {
	$defaults = [
		'fontsize' => 20.0
		,'minfontsize' => 6.0
		,'textcolor' => "#000"
		,'shadowcolor' => FALSE
		,'shadowratio' => 0.075
		,'textletterx' => 0
		,'textlettery' => 0
		,'textboxx' => 140
		,'textboxy' => 518
		,'textboxwidth' => 507
		,'textboxheight' => 449
		,'levelfontsize' => 44.0
		,'levelcostx' => 5
		,'levelcosty' => 518
		,'levelcostw' => 135
		,'levelcosth' => 86
		,'strokeWidth' => 5
		,'linex' => 45
		,'linew' => 615
		,'isshading' => true
		,'manaratio' => 0.85
		,'manafontratio' => 1.2
		,'levelimage' => $frame->dirframe . "/levelup.png"
		,'rulestext' => $frame->q['rulestext']
		,'flavortext' => $frame->q['flavortext']
	];
	$options = array_merge($defaults, (array)$options);
	// set variables within local scope
	foreach($options as $key => $option) {
		// this is to make sure that only intended arguments are passed
		if(isset($defaults[$key])) {
			${$key} = $option;
		}
	}

	$rulestext = trim($rulestext);
	$flavortext = trim($flavortext);
	$draw = $frame->draw;
	$im = &$frame->im;
	$dirframe = $frame->dirframe;
	$mcircle = $frame->mcircle;
	$manadraw = $frame->manadraw;
	$mcircle = $frame->mcircle;
	$white = $frame->white;
	$black = $frame->black;
	$clear = $frame->clear;
	$textfont = $frame->textfont;
	$italicfont = $frame->italicfont;
	$levelfont = $frame->ptfont;
	$dividerColor = new ImagickPixel( "#000");

	// count number of levels and flavor text paragraphs
	$levels = preg_split("/;;/", $rulestext);
	$numlevels = $levels ? count($levels) : 0;
	$numflavors = $flavortext != "" ? 1 : 0;
	$numtotal = $numlevels + $numflavors;
	$levelh = $numtotal ? ($textboxheight - ($numtotal - 1) * $strokeWidth) / $numtotal : 0;
	$dy = $levelh + $strokeWidth;
	$fontsize *= 1 - $numtotal / 12;
	$fontsize = max($fontsize, $minfontsize);
	$options['textboxheight'] = $levelh;
	$options['fontsize'] = $fontsize;

	// dividers
	$draw->setFillColor( $dividerColor );
	for ($i = 1, $liney = $textboxy - $strokeWidth; $i < $numtotal; $i++) {
		$liney += $dy;
		$draw->rectangle( $linex, $liney, $linew + $linex, $liney+1);
		$im->drawImage($draw);
	}

	// do flavor text first, using given dimensions
	if ($numflavors) {
		$options['rulestext'] = '';
		$options['flavortext'] = $flavortext;
		$box = cardfuncs_makeRulesboxImage($options, $frame);
		$im->compositeImage( $box, imagick::COMPOSITE_OVER, $textboxx, $textboxy + $numlevels * $dy + abs($levelh - $box->getImageHeight())/2);
	}

	// rules text
	for ($i = 0, $lvlimgy = $textboxy + ($levelh-$levelcosth) / 2; $i < $numlevels; $i++) {

		// shaded overlay for each partition
		// increasing in dimness
		if ($isshading) {
			$shade = new Imagick();
			$shade->newImage($linew, $levelh + $strokeWidth, $black, "png");
			// second parameter sets alpha level
			$shade->transparentPaintImage($black, $i/$numlevels/2.5, 0, false);
			$im->compositeImage($shade, imagick::COMPOSITE_OVER, $linex, $textboxy + $i * $dy - $strokeWidth / 2);
		}

		$level = trim($levels[$i]);

		// effect
		$found = preg_match("/^ *[\[\(\{]* *(\d+ *[+-]? *\d*) *[\]\)\}]* *:?(.*)$/ms", $level, $matches);
		if ($found) {
			// new level found

			// draw level arrow and text
			$lvlimg = new Imagick($levelimage);
			$lvlimg->resizeImage($levelcostw, $levelcosth, imagick::FILTER_QUADRATIC, 1.0);
			$im->compositeImage($lvlimg, imagick::COMPOSITE_OVER, $levelcostx, $lvlimgy + $dy * $i);
			$draw->setTextAlignment(imagick::ALIGN_CENTER);
			$draw->setFont($levelfont);
			$costx = $levelcostx + $levelcostw * 0.4;
			$costy1 = $lvlimgy + $dy * $i + $levelcosth * 0.75;
			$costy2 = $lvlimgy + $dy * $i + $levelcosth * 0.35;
			if ($shadowcolor) {
					$draw->setFillColor($shadowcolor);
					for ($e = (int)round($shadowratio * $levelfontsize); $e; $e--) {
					$draw->setFontSize($levelfontsize);
					$im->annotateImage($draw, $costx + $e, $costy1 + $e, 0, $matches[1]);
					$draw->setFontSize($levelfontsize / 2);
					$im->annotateImage($draw, $costx + $e, $costy2 + $e, 0, "LEVEL");
				}
			}

			$draw->setFillColor($textcolor);
			$draw->setFontSize($levelfontsize);
			$im->annotateImage($draw, $costx, $costy1, 0, $matches[1]);
			$draw->setFontSize($levelfontsize / 2);
			$im->annotateImage($draw, $costx, $costy2, 0, "LEVEL");
			$draw->setTextAlignment(imagick::ALIGN_LEFT);

			$rules = $matches[2];
			$finaltextw = $textboxwidth - $levelcostw;
			$finaltextx = $textboxx + $levelcostw;
		}
		else {
			$rules = $level;
			$finaltextw = $textboxwidth;
			$finaltextx = $textboxx;
		}

		$options['rulestext'] = $rules;
		$options['flavortext'] = '';
		$options['textboxwidth'] = $finaltextw;
		$options['textboxx'] = $finaltextx;
		$box = cardfuncs_makeRulesboxImage($options, $frame);
		if (isset($box)) {
			$im->compositeImage( $box, imagick::COMPOSITE_OVER, $finaltextx, $textboxy + $i * $dy + abs($levelh - $box->getImageHeight())/2);
		}
	}
}

function isValidUrl($url){
	// first do some quick sanity checks:
	if(!$url || !is_string($url)){
		return false;
	}
	// quick check url is roughly a valid http request: ( http://blah/... )
	if( ! preg_match('/^http(s)?:\/\/[a-z0-9-]+(\.[a-z0-9-]+)*(:[0-9]+)?(\/.*)?$/i', $url) ){
		return false;
	}

	// check URL with cURL

	$ch = @curl_init($url);
	if($ch === false){
		return false;
	}

	$timeout = 9; // seconds
	$maxredirs = 5;

	@curl_setopt($ch, CURLOPT_HEADER, true);	// we want headers
	@curl_setopt($ch, CURLOPT_NOBODY, true);	// dont need body
	@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	// catch output (do NOT print!)
	@curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);	// prevent waiting forever to get a result
	@curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);	// prevent waiting forever to get a result
	@curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.89 Safari/537.1");	// pretend we're a regular browser
	@curl_setopt($ch, CURLOPT_FOLLOWLOCATION,true);
	@curl_setopt($ch, CURLOPT_MAXREDIRS, $maxredirs);	// fairly random number, but could prevent unwanted endless redirects with followlocation=true

	@curl_exec($ch);
	if(@curl_errno($ch)){	// should be 0
		@curl_close($ch);
		return false;
	}

	$code = @curl_getinfo($ch, CURLINFO_HTTP_CODE); // note: php.net documentation shows this returns a string, but really it returns an int
	@curl_close($ch);
	if ($code != 200) {
		return false;
	}
	return true;
}

function cardfuncs_makeLinearText($options, &$frame) {
	$defaults = [
		'font' => '../fonts/kelvinch-bold.ttf'
		,'fontsize' => 20.0
		,'textx' => 34
		,'texty' => 48
		,'wmax' => 375
		,'angle' => 0.0
		,'text' => ''
		,'textcolor' => '#000'
		,'shadowcolor' => FALSE
		,'shadowratio' => 0.075
		,'outlinecolor' => FALSE
		,'outlineratio' => 0.01
		,'allupper' => FALSE
		,'alllower' => FALSE
		,'textalign' => imagick::ALIGN_LEFT
		,'textdecoration' => imagick::DECORATION_NO
		,'interlinespacing' => -5
	];
	$options = array_merge($defaults, (array)$options);
	// set variables within local scope
	foreach($options as $key => $option) {
		// this is to make sure that only intended arguments are passed
		if(isset($defaults[$key])) {
			${$key} = $option;
		}
	}
/*
echo $text;
echo '<br>';
//*/
	if ($allupper) {
		$text = strtoupper($text);
	}
	elseif ($alllower) {
		$text = strtolower($text);
	}

	$draw = new ImagickDraw();
	$draw->setFont($font);
	$draw->setFontSize($fontsize);
	$draw->setTextDecoration($textdecoration);
	$draw->setFillColor($textcolor);
	$drawshadow = clone $draw;
	if ($outlinecolor) {
		$draw->setStrokeColor($outlinecolor);
		$draw->setStrokeWidth($outlineratio * $fontsize);
	}

	$draw->setTextInterlineSpacing($interlinespacing);

	$clear = new ImagickPixel('transparent');
	$timg = new Imagick();
	$timg->newImage($wmax * 2, $fontsize * 2, $clear);

	$draw->setTextAlignment(imagick::ALIGN_LEFT);
	$tx = 5;
	$ty = $fontsize;

	if ($shadowcolor) {
		$drawshadow->setFillColor($shadowcolor);
		for ($i = (int)round($shadowratio * $fontsize); $i; $i--) {
			$timg->annotateImage($drawshadow, $tx+$i, $ty+$i, 0, $text);
		}
	}
	$timg->annotateImage($draw, $tx, $ty, 0, $text);
	$timg->trimImage(0.0);
	$timg->setImagePage(0, 0, 0, 0);

	//$qfm = $timg->queryFontMetrics($frame->draw, $text);

	// squeeze text to within max width
	if ($timg->getImageWidth() > $wmax) {
		$timg->thumbnailImage( $wmax, $timg->getImageHeight());
	}

	// rotate angled text
	if ($angle!=0.0) {
		$timg->rotateImage($clear, $angle);
	}

	// handle aligned text
	if ($textalign == imagick::ALIGN_CENTER) {
		if ($angle==90.0 || $angle==270.0) {
			$textx -= $timg->getImageWidth()/2;
		}
		else {
			// $angle == 0.0
			$textx -= $timg->getImageWidth()/2;
		}
		$texty -= $timg->getImageHeight()/2;
	}
	elseif ($textalign == imagick::ALIGN_RIGHT) {
		if ($angle==90.0 || $angle==270.0) {
			$textx -= $timg->getImageWidth()/2;
		}
		elseif ($angle==180.0) {
			$texty -= $timg->getImageHeight()/2;
		}
		else {
			// $angle == 0.0
			$textx -= $timg->getImageWidth();
			$texty -= $timg->getImageHeight()/2;
		}
	}
	else {
		// this means $textalign == imagick::ALIGN_LEFT
		if ($angle==270.0 ) {
			$texty -= $timg->getImageHeight();
		}
		elseif ($angle==90.0) {
			// do nothing
		}
		elseif ($angle==180.0) {
			$textx -= $timg->getImageWidth();
			$texty -= $timg->getImageHeight();
		}
		else {
			// $angle == 0.0
			$texty -= $timg->getImageHeight()/2;
		}
	}

	$frame->im->compositeImage($timg, imagick::COMPOSITE_DEFAULT, $textx, $texty);
}

function cardfuncs_artExtras($art, $arturl) {
	// read URL fragment after double hashes ## for extra instructions, like crop, rotate

		/*/ debug
		cclog("\n");
		cclog("cardfuncs_artExtras");
		//*/



	if (strpos($arturl, '##') !== FALSE) {
		$MAX_INST = 9; // max instructions to execute, to limit CPU usage
		$artextra = explode('##', $arturl, $MAX_INST+2);
		$clear = new ImagickPixel('transparent');

		$art = clone $art;
		$art->setImageBackgroundColor($clear);

		/*/ debug
		cclog("\n");
		cclog(print_r($artextra, true));
		//*/

		// $artextra[0] = art URL
		// $artextra[1]+ = art extra instructions

		for ($i=1; $i<count($artextra) && $i<=$MAX_INST; $i++) {
			$inst = explode(',', $artextra[$i], 255);
			// $inst[0] = verb
			// $inst[1]+ = parameters

			/*/ debug
			cclog("\n");
			cclog(print_r($inst, true));
			//*/

			$verb = strtolower(trim($inst[0]));

			try {
				if ($verb == 'crop') {
					// crop, x, y, w, h
					$art->cropImage($inst[3], $inst[4], $inst[1], $inst[2]);
					$art->setImagePage(0, 0, 0, 0);
				}
				else if ($verb == 'rotate') {
					// rotate, degrees
					$art->rotateImage($clear, $inst[1]);
					$art->setImagePage(0, 0, 0, 0);
				}
				else if ($verb == 'alpha') {
					// alpha, value (between 0.0 and 1.0)
					if ($art->getImageAlphaChannel()) {
						$art->evaluateImage(Imagick::EVALUATE_MULTIPLY, floatval($inst[1]), Imagick::CHANNEL_ALPHA);
					}
					else {
						$art->setImageAlpha(floatval($inst[1]));
					}
					$art->setImagePage(0, 0, 0, 0);
				}
				else if ($verb == 'transparent') {
					// transparent, color, alpha
					// color hex 6 digits
					// alpha between 0.0 and 1.0
					$art->setImageAlphaChannel(imagick::ALPHACHANNEL_ACTIVATE);
					$color = isset($inst[1]) ? $inst[1] : "#000000";
					$alpha = isset($inst[2]) ? floatval($inst[2]) : 0.0;
					$art->transparentPaintImage ($color, $alpha, 0.0, false);
					$art->despeckleimage(); // removes leftover edges?
					$art->setImagePage(0, 0, 0, 0);
				}
				else if ($verb == 'flip') {
					// flip (around x-axis)
					$art->flipImage();
					$art->setImagePage(0, 0, 0, 0);
				}
				else if ($verb == 'flop') {
					// flop (around y-axis)
					$art->flopImage();
					$art->setImagePage(0, 0, 0, 0);
				}
				else if ($verb == 'tint') {
					// tint image, color
					$tint = new ImagickPixel($inst[1]);
					$art->tintImage($tint, $tint);
					$art->setImagePage(0, 0, 0, 0);
				}
				else if ($verb == 'pad') {
					// pad edges, direction, amount, color
					// directions = lurd
					// 0 <= amount <= 1000
					// color hex 6 digits

					$alldirs = "lurd";

					if (!isset($inst[1])) {
						$inst[1] = "lurd";
					}
					if (!isset($inst[2])) {
						$inst[2] = 0;
					}
					else {
						$inst[2] = intval($inst[2]);
					}
					if (!isset($inst[3])) {
						$inst[3] = $clear;
					}

					$newW = $art->getImageWidth();
					$newH = $art->getImageHeight();
					$newX = 0;
					$newY = 0;

					foreach (str_split($alldirs) as $dir) {

						if (!stristr($inst[1], $dir)) {
							continue;
						}

						switch ($dir) {
							case "l":
								$newW += $inst[2];
								$newX += $inst[2];
								break;

							case "u":
								$newH += $inst[2];
								$newY += $inst[2];
								break;

							case "r":
								$newW += $inst[2];
								break;

							case "d":
								$newH += $inst[2];
								break;
						}
					}

					$newArt = new Imagick();
					$newArt->newImage($newW, $newH, $inst[3], "png");
					if ($inst[3] == $clear) {
						$newArt->setImageAlphaChannel(Imagick::ALPHACHANNEL_ACTIVATE);
					}
					$box = $newArt->getImageRegion($art->getImageWidth(), $art->getImageHeight(), $newX, $newY);
					if ($inst[3] == $clear) {
						$box->setImageAlphaChannel(Imagick::ALPHACHANNEL_TRANSPARENT);
					}
					$newArt->compositeImage($box,Imagick::COMPOSITE_REPLACE, $newX, $newY);
					$newArt->compositeImage($art, imagick::COMPOSITE_OVER, $newX, $newY);

					$newArt->setImagePage(0, 0, 0, 0);
					$art = $newArt;
				}
			} catch (Exception $e) {
				cclog("\ncardfuncs_artExtras error : ");
				cclog($e);
			}
		}
	}
	return $art;
}

function cardfuncs_makeArtImage($options, &$frame) {
	$defaults = [
		'artx' => 92
		,'arty' => 92
		,'artwidth' => 566
		,'artheight' => 416
		,'mask' => false
		,'confirm' => true
		,'arturl' => $frame->q['arturl']
	];
	$options = array_merge($defaults, (array)$options);
	// set variables within local scope
	foreach($options as $key => $option) {
		// this is to make sure that only intended arguments are passed
		if(isset($defaults[$key])) {
			${$key} = $option;
		}
	}

	$im = $frame->im;

	if (isset($arturl) && $arturl!="") {
		// split by $$
		$urls = explode("$$", $arturl);
		foreach($urls as $url) {
			// add http://
			if (0==preg_match("/^http(s)?:\/\//", $url)) {
				$url = "http://$url";
			}
			if (!isValidUrl($url)) {
				return false;
			}
			try {
				$art = new Imagick($url);
				if ($art) {
					$art = cardfuncs_artExtras($art, $url);

					$art->thumbnailImage($artwidth, $artheight);
					if ($mask) {
						$white = new ImagickPixel('white');
						$magenta = new ImagickPixel('magenta');
						$mask = new Imagick($mask);
						$mask->transparentPaintImage($white, 0.0, 0, false);
						$art->compositeImage($mask, imagick::COMPOSITE_OVER, 0, 0);
						$art->transparentPaintImage($magenta, 0.0, 0, false);
					}

					if ($confirm) {
						$im->compositeImage($art, imagick::COMPOSITE_OVER, $artx, $arty);
					}

					/*
					// get number of sub-images to see if animation
					$numimages = $art->getNumberImages() - 1;
					//cclog( "\n" . '$numimages: ' . $numimages);

					if ($numimages) {
						// animate
						$images = $art->coalesceImages();
						foreach ($images as $image) {
							$image->thumbnailImage($artwidth, $artheight);
							$im->compositeImage($image, imagick::COMPOSITE_OVER, $artx, $arty);
							//$im->setImageDispose(1);
							$im->addImage($im->getImage());
						}
						$im->removeImage();
						//$im = $im->deconstructImages();
					}
					else {
						$art->thumbnailImage($artwidth, $artheight);
						$im->compositeImage($art, imagick::COMPOSITE_OVER, $artx, $arty);
					}
					//*/

				}

				//cclog( "\n" . '$im images: ' . $im->getNumberImages());

			}
			catch (ImagickException $e) {
				cclog( "\n" . '$art error : ' . $e);
			}
		}
	}

	//cclog( "\n" . 'cardfuncs_makeArtImage() ended');

	return $art ?? null;
}

function cardfuncs_makeWatermark($options, &$frame) {
	$defaults = [
		'wmx' => 310
		,'wmy' => 142
		,'wmw' => 187
		,'wmh' => 399
		,'wmurl' => $frame->q['wmurl']
	];
	$options = array_merge($defaults, (array)$options);
	// set variables within local scope
	foreach($options as $key => $option) {
		// this is to make sure that only intended arguments are passed
		if(isset($defaults[$key])) {
			${$key} = $option;
		}
	}

	$im = $frame->im;
	if (isset($wmurl) && $wmurl != "") {
		// split by $$
		$wmurls = explode("$$", $wmurl);
		foreach($wmurls as $url) {
			// add http://
			if (0==preg_match("/^http(s)?:\/\//", $url)) {
				$url = "http://$url";
			}
			if (isValidUrl($url)) {
				try {
					$wm = new Imagick($url);
					if ($wm) {
						$wm = cardfuncs_artExtras($wm, $url);
						$wm->thumbnailImage($wmw, $wmh, true);
						$im->compositeImage($wm, imagick::COMPOSITE_OVER, $wmx - $wm->getImageWidth()/2, $wmy - $wm->getImageHeight()/2);
					}
				}
				catch (ImagickException $e) {
						cclog( "\n" . '$wm error : ' . $e);
				}
			}
		}
	}
}

function cardfuncs_makeOverlay($options, &$frame) {
	$defaults = [
		'ovx' => 0
		,'ovy' => 0
		,'ovw' => 375
		,'ovh' => 523
		,'overlayurl' => $frame->q['overlayurl']
	];
	$options = array_merge($defaults, (array)$options);
	// set variables within local scope
	foreach($options as $key => $option) {
		// this is to make sure that only intended arguments are passed
		if(isset($defaults[$key])) {
			${$key} = $option;
		}
	}

	$im = $frame->im;

	if (isset($overlayurl) && $overlayurl != "") {
		// split by $$
		$overlayurls = explode("$$", $overlayurl);
		foreach($overlayurls as $url) {
			// add http://
			if (0==preg_match("/^http(s)?:\/\//", $url)) {
				$url = "http://$url";
			}
			if (isValidUrl($url)) {
				try {
					$overlaybase = new Imagick($url);
					if ($overlaybase) {
						$overlaybase->thumbnailImage($ovw, $ovh);
						$overlay = new Imagick();
						$overlay->newImage($ovw, $ovh, new ImagickPixel("transparent"), "png");
						$overlay->compositeImage($overlaybase, imagick::COMPOSITE_OVER, 0, 0);
						$overlay = cardfuncs_artExtras($overlay, $url);
						$im->compositeImage($overlay, imagick::COMPOSITE_OVER, $ovx, $ovy);
					}
				}
				catch (ImagickException $e) {
						//cclog( "\n" . '$overlay error : ' . $e);
				}
			}
		}
	}
}

function cardfuncs_makeSetIcon($options, &$frame) {
	$defaults = [
		'w' => 21
		,'h' => 21
		,'x' => 320
		,'y' => 297
		,'align' => Imagick::ALIGN_RIGHT
		,'angle' => false
		,'keepratio' => true
		,'ratioheight' => false
		,'seticonurl' => $frame->q['seticonurl']
	];
	$options = array_merge($defaults, (array)$options);
	// set variables within local scope
	foreach($options as $key => $option) {
		// this is to make sure that only intended arguments are passed
		if(isset($defaults[$key])) {
			${$key} = $option;
		}
	}

	$im = $frame->im;
	$rarity = $frame->q['rarity'];
	$seticondefault = isset($frame->seticon) ? $frame->seticon : null;
	$defW = $w;
	$defH = $h;
	$defX = $x;
	$defY = $y;

	// rarity and set icon
	if (isset($seticonurl) && $seticonurl != "") {
		// split by $$
		$urls = explode("$$", $seticonurl);
	}
	elseif (isset($seticondefault)) {
		$urls = array($seticondefault);
	}
	else {
		return;
	}

	foreach($urls as $url) {
		if (!isset($seticondefault)) {
			// add http://
			if (0==preg_match("/^http(s)?:\/\//", $url)) {
				$url = "http://$url";
			}
			if (!isValidUrl($url)) {
				continue;
			}
		}
		try {
			$seticon = new Imagick($url);
			if (isset($seticon)) {
				/*
				$raritycolors = array();
				$raritycolors[1] = 'rgb( 0, 0, 0 )'; // black #000000
				$raritycolors[2] = 'rgb(  208,  208, 208 )'; // silver #c0c0c0
				$raritycolors[3] = 'rgb( 196,  170, 93 )'; // #c4aa5d
				$raritycolors[4] = 'rgb( 198, 94, 41 )'; // #c65e29
				$raritycolors[5] = 'rgb( 0,  0, 255 )'; // blue #0000ff
				//*/

				// reset values in case of multiple arts
				$w = $defW;
				$h = $defH;
				$x = $defX;
				$y = $defY;

				$seticon = cardfuncs_artExtras($seticon, $url);

				// target color that will be replaced by rarity colors
				$masktarget = '#ff00ff'; // magenta

				// must clone to allow transparentPaintImage and OpaquePaintImage
				$mask = clone $seticon;
				$icon = clone $mask;

				if ($keepratio) {
					if ($ratioheight) {
						$h *= $icon->getimageheight() / $icon->getimagewidth();
					} else {
						$w *= $icon->getimagewidth() / $icon->getimageheight();
					}
				}

				$mask->OpaquePaintImage( $masktarget, '#000000', 0, true );
				$mask->OpaquePaintImage( $masktarget, '#ffffff', 0, false );
				$mask->transparentPaintImage('#000000', 0.0, 0.0, false);
				$mask->thumbnailImage($w, $h, true);


				// apply rarity texture onto mask's shape
				$underlay = new Imagick("$frame->dirframe/rarity_$rarity.png");
				$underlay->thumbnailImage($w, $h);
				$underlay->compositeImage($mask, imagick::COMPOSITE_COPYOPACITY, 0, 0);
				$underlay->transparentPaintImage('#000000', 0.0, 0, false);

				// merge rarity color onto icon
				$icon->transparentPaintImage($masktarget, 0.0, 0.0, false);
				$icon->thumbnailImage($w, $h, true);
				$icon->compositeImage($underlay, imagick::COMPOSITE_OVER, 0, 0);

				// merge icon onto card image
				if ($align == Imagick::ALIGN_RIGHT) {
					$x -= $icon->getimagewidth();
				}
				elseif ($align == Imagick::ALIGN_CENTER) {
					$x -= $icon->getimagewidth() / 2;
				}

				if ($angle) {
					$icon->rotateImage($frame->clear, $angle);
					if ($angle == 180) {
						$x -= $w;
						$y -= $h;
					} elseif ($angle == 90) {
						$newx = $y;
						$newy = $x;
						$neww = $w;
						$newh = $h;
						$x = $newx;
						$y = $newy;
						$w = $neww;
						$h = $newh;
					}
				}

				$im->compositeImage($icon, imagick::COMPOSITE_OVER, $x, $y);
			}
		}
		catch (ImagickException $e) {
			//cclog( "\n" . '$icon error : ' . $e);
		}
	}
}

function cardfuncs_makeTypeIcon($options, &$frame) {
	// draw card type icon

	global $GENRES;

	$defaults = [
		'x' => 46
		,'y' => 46
		,'w' => 36
		,'h' => 36
		,'invert' => false
		,'genre' => 'magic'
		,'cardtype' => '0'
		,'align' => Imagick::ALIGN_LEFT
		,'angle' => false
		,'keepratio' => true
		,'ratioheight' => false
		,'typeiconurl' => $frame->q['typeiconurl']
	];
	$options = array_merge($defaults, (array)$options);
	// set variables within local scope
	foreach($options as $key => $option) {
		// this is to make sure that only intended arguments are passed
		if(isset($defaults[$key])) {
			${$key} = $option;
		}
	}

	$defW = $w;
	$defH = $h;
	$defX = $x;
	$defY = $y;

	if (isset($typeiconurl) && $typeiconurl != "") {
		// split by $$
		$urls = explode("$$", $typeiconurl);
	}
	elseif (intval($cardtype)) {
		try {
			// use predefined type icons
			$dirgenre = "genres/$genre";
			$type2icon = $GENRES[$genre]['types'];
			// trim useless type = tribal
			$cardtype = '0' . substr($cardtype, 1, count($type2icon));

			if (intval($cardtype) == 0) {
				// no card type assigned
				return;
			}

			// try default type icon
			if (substr_count($cardtype, '1') > 1) {
				// multiple types
				$iconfile = "$dirgenre/type_" . ($invert ? "white_" : "") . "multiple.png";
			}
			else {
				// assign single card type to image file name
				$icontype = strtolower($type2icon[ strpos($cardtype, '1') ]);
				$iconfile = "$dirgenre/type_" . ($invert ? "white_" : "") . "$icontype.png";
			}
			$urls = array($iconfile);
		}
		catch (Exception $e) {
			//cclog( "\n" . '$icon error : ' . $e);
			return;
		}
	}
	else {
		return;
	}

	foreach($urls as $url) {
		// find image file
		if ("" != $typeiconurl) {
			// add http://
			if (0==preg_match("/^http(s)?:\/\//", $url)) {
				$url = "http://$url";
			}
			if (isValidUrl($url)) {
				$icon = new Imagick($url);
			}
		}
		else {
			// probably default icon, local file
			if (!file_exists($url)) {
				continue;
			}
			$icon = new Imagick($url);
		}

		// draw icon
		try {
			if (isset($icon)) {
				// reset values in case of multiple arts
				$w = $defW;
				$h = $defH;
				$x = $defX;
				$y = $defY;

				$icon = cardfuncs_artExtras($icon, $url);

				if ($keepratio) {
					if ($ratioheight) {
						$h *= $icon->getimageheight() / $icon->getimagewidth();
					} else {
						$w *= $icon->getimagewidth() / $icon->getimageheight();
					}
				}

				$icon->thumbnailImage($w, $h, true);

				if ($align == Imagick::ALIGN_RIGHT) {
					$x -= $icon->getimagewidth();
				}
				elseif ($align == Imagick::ALIGN_CENTER) {
					$x -= $icon->getimagewidth() / 2;
				}

				if ($angle) {
					$icon->rotateImage($frame->clear, $angle);
					if ($angle == 180) {
						$x -= $w;
						$y -= $h;
					} elseif ($angle == 90) {
						$newx = $y;
						$newy = $x;
						$neww = $w;
						$newh = $h;
						$x = $newx;
						$y = $newy;
						$w = $neww;
						$h = $newh;
					}
				}

				$frame->im->compositeImage($icon, imagick::COMPOSITE_OVER, $x, $y);
			}
		}
		catch (ImagickException $e) {
			//cclog( "\n" . '$icon error : ' . $e);
		}
	}
	return isset($icon) ? $icon : null;
}

function cardfuncs_makeArtistCreator($options, &$frame) {
	$defaults = [
		'font' => '../fonts/kelvinch-bold.ttf'
		,'fontsize' => 12.0
		,'artistx' => 32
		,'artisty' => 500
		,'creatorx' => 32
		,'creatory' => 518
		,'artistalign' => Imagick::ALIGN_LEFT
		,'creatoralign' => Imagick::ALIGN_LEFT
		,'cbcolor1' => "#fff" // main color
		,'cbcolor2' => "#000" // outline color
		,'artistpre' => "Illus. by "
		,'creatorpre' => "Created by "
		,'interlinespacing' => 0
	];
	$options = array_merge($defaults, (array)$options);
	// set variables within local scope
	foreach($options as $key => $option) {
		// this is to make sure that only intended arguments are passed
		if(isset($defaults[$key])) {
			${$key} = $option;
		}
	}

	$artist = $artistpre . $frame->q['artist'];
	$creator = $creatorpre . $frame->q['creator'];

	$draw = $frame->draw;
	$draw->setFont($font);
	$draw->setFontSize($fontsize);
	$draw->setTextAlignment($align);
	$oldinterlinespacing = $draw->getTextInterlineSpacing();
	$draw->setTextInterlineSpacing($interlinespacing);

	$im = $frame->im;

	// outline first
	$draw->setFillColor($cbcolor2);
	$im->annotateImage($draw, $artistx+1, $artisty+1, 0, $artist);
	$im->annotateImage($draw, $creatorx+1, $creatory+1, 0, $creator);

	// main color
	$draw->setFillColor($cbcolor1);
	$im->annotateImage($draw, $artistx, $artisty, 0, $artist);
	$im->annotateImage($draw, $creatorx, $creatory, 0, $creator);

	$draw->setTextInterlineSpacing($oldinterlinespacing);
}

function cardfuncs_makeManaCost($options, &$frame) {
	global $HYBRIDMANA, $CUSTOMMANA;

	$defaults = [
		'font' => "../fonts/ubuntumono-regular.ttf"
		,'fontsize' => 18.0
		,'fontcolor' => "#000"
		,'manacostx' => 343
		,'manacosty' => 33
		,'manaiconwidth' => 18
		,'manaiconheight' => 18
		,'width' => 375
		,'height' => 523
		,'vertical' => false
		,'manaalign' => ''
		,'manapos' => false
		,'angle' => 0
		,'altmana' => false
		,'manacost' => ''
	];
	$options = array_merge($defaults, (array)$options);
	// set variables within local scope
	foreach($options as $key => $option) {
		// this is to make sure that only intended arguments are passed
		if(isset($defaults[$key])) {
			${$key} = $option;
		}
	}

	$im = $frame->im;
	$clear = $frame->clear;
	$draw = $frame->draw;
	$dirmana = $frame->dirmana;
	$mcircle = $frame->mcircle;
	$manacost = $altmana ? $manacost : $frame->q['manacost'];
	$manacost = preg_replace('/(([0-9a-z]\/[0-9a-z]\/[0-9a-z])|([0-9a-z]\/[0-9a-z])|[0-9a-z*])| /i', '$1,', $manacost);
	$manacost = explode(',', $manacost);
	array_pop($manacost); //removes last extra empty item
	$mcbox = new Imagick();
	$mcbox->newImage($width, $height, $clear);
	// different for vertically
	$mcboxw = 0;
	$mcboxh = 0;
	if ($vertical) {
		//$manacost = array_reverse($manacost);
		$dx = 0;
		$dy = $manaiconheight;
	}
	else {
		$dx = $manaiconwidth;
		$dy = 0;
	}
	// if manapos present
	if ($manapos) {
		$manaposi = 0;
		$manaposct = count($manapos);
	}

	$draw->setFont($font);
	$draw->setFillColor($fontcolor);
	$draw->setTextAlignment(imagick::ALIGN_CENTER);

	foreach($manacost as $value) {
		try {
			$value = trim($value);

			// use space to advance manapos to next position
			if (''==$value) {
				if ($manapos) {
					$manaposi++;
					if ($manaposi >= $manaposct) {
						return;
					}
				}
				continue;
			}

			$manafound = false;
			//echo "<br>" . $value;
			if (isset($CUSTOMMANA[$value])) {
				// custom mana
				$manafound = true;
				$file = $CUSTOMMANA[$value];
				if (0==preg_match("/^http(s)?:\/\//", $file)) {
					$file = "http://$file";
				}
				if (isValidUrl($file)) {
					try {
						$mc = new Imagick($file);
						$mc->thumbnailImage($manaiconwidth, $manaiconheight);
					}
					catch (ImagickException $e) {
						//cclog( "\n" . '$art error : ' . $e);
					}
				}
			}
			elseif (preg_match('/^[a-z]$/', $value)) {
				// is valid color
			//echo "_$value";
				$file = "$dirmana/mana_$value.png";
				if (file_exists($file)) {
					$manafound = true;
					$mc = new Imagick( $file);
					$mc->thumbnailImage($manaiconwidth, $manaiconheight);
				}
			}
			elseif (preg_match('/^([0-9a-z])\/([0-9a-z])\/([0-9a-z])$/', $value, $matches)) {
				// mana is a/b/c ; e.g. hybrid phyrexian or tribrid
				$matches = array_slice($matches, 1);
				natcasesort($matches);
				$halfvalue = strtolower(implode('', $matches));
				if (isset($HYBRIDMANA[$halfvalue])) {
					$halfvalue = $HYBRIDMANA[$halfvalue];
					$file = "$dirmana/mana_$halfvalue.png";
					if (file_exists($file)) {
						$manafound = true;
						$mc = new Imagick($file);
						$mc->thumbnailImage($manaiconwidth, $manaiconheight);
					}
				}
			}
			elseif ( preg_match('/^([0-9wubrgpc]\/[0-9wubrgpc])$/', $value)) {
			// is hybrid mana
			//echo "_$value";
				if (preg_match('/^([wubrgpc]\/[wubrgpc])$/', $value, $matches)) {
					// c/d or d/c
					if (isset($HYBRIDMANA[ $matches[0] ])) {
						$file = "$dirmana/mana_" . $HYBRIDMANA[ $matches[0] ] . ".png";
						if (file_exists($file)) {
							$manafound = true;
							$mc = new Imagick($file);
							$mc->thumbnailImage($manaiconwidth, $manaiconheight);
						}
					}
				}
				elseif (preg_match('/^([0-9wubrgc]\/[0-9wubrgc])$/', $value, $matches)) {
					// 2/c or c/2
					$halfcolor = preg_replace('/^.*([wubrgc]).*$/', "$1", $value);
					$halfvalue = preg_replace('/^.*([0-9]).*$/', "$1", $value);
					$mc = new Imagick();
					$mc->newImage( $manaiconwidth, $manaiconheight, $clear);
					$mchalf = file_exists("$dirmana/mana_n$halfcolor.png") ? new Imagick( "$dirmana/mana_n$halfcolor.png") : null;
					if ($mchalf) {
						$manafound = true;
						$mchalf->thumbnailImage($manaiconwidth, $manaiconheight);
						$mc->compositeImage($mchalf, imagick::COMPOSITE_OVER, 0, 0);
					}
					$draw->setFontSize($fontsize * 0.6);
					$mc->annotateImage($draw, $manaiconwidth * 0.33, $manaiconheight * 0.5, 0, $halfvalue);
				}
			}
			if (!$manafound) {
				// any other text
				//echo "_$value";
				$value = strtoupper($value);
				$mc = clone $mcircle;
				$mc->thumbnailImage($manaiconwidth, $manaiconheight);
				$draw->setFontSize($fontsize);
				$mc->annotateImage($draw, $manaiconwidth * 0.5, $manaiconheight * 0.8, 0, $value);
			}

			// copy mana symbol to $mcbox
			if (isset($mc)) {
				if ($manapos) {
					$im->compositeImage($mc, imagick::COMPOSITE_OVER, $manapos[$manaposi][0], $manapos[$manaposi][1]);
				}
				else {
					$mcbox->compositeImage($mc, imagick::COMPOSITE_OVER, $mcboxw, $mcboxh);
					$mcboxw += $dx;
					$mcboxh += $dy;
				}
			}
		} catch (Exception $e) {

		}
		unset($mc);

		if ($manapos) {
			$manaposi++;
			if ($manaposi >= $manaposct) {
				return;
			}
		}

	}
	if ($manapos) {
		return;
	}

	// copy $mcbox to image

	$mcbox->trimImage(0);
	$mcbox->setImagePage(0, 0, 0, 0);
	if ($manaalign == 'CENTER' || $manaalign == Imagick::ALIGN_CENTER) {
		$manacostx -= $mcbox->width / 2;
		$manacosty -= $mcbox->height / 2;
	}
	elseif ($manaalign == 'LEFT' || $manaalign == Imagick::ALIGN_LEFT) {
		// do nothing
	}
	elseif ($vertical) {
		$manacostx = $manacostx;
		$manacosty = $manacosty;
	}
	else {
		// default align right
		$manacostx -= $mcboxw;
		$manacosty = $manacosty;
	}
	if ($angle == 180) {
		$mcbox->rotateImage($clear, 180.0);
		$manacostx -= $mcbox->getImageWidth();
		$manacosty -= $mcbox->getImageHeight();
	}
	$im->compositeImage($mcbox, imagick::COMPOSITE_OVER, $manacostx, $manacosty);

	return $mcbox->getImageGeometry();
}

function saveFinalImage($im, $savefilename=null) {
	global $SAVEFILEPNG, $SAVEFILEGIF;
	if (!isset($savefilename)) {
		if (is_object($im) && $im->getNumberImages() > 1) {
			$savefilename = $SAVEFILEGIF;
		}
		else {
			$savefilename = $SAVEFILEPNG;
		}
	}
	if (file_exists($savefilename)) {
		unlink($savefilename);
	}
	if (is_object($im)){
		// is Imagick
		$im->setImageDepth(8);
		$im->writeImage($savefilename);
	}
	else {
		// is GD
		imagepng($im, $savefilename, 8);
	}
}

function sendFinalImage($im) {
	/* Output the image*/
	$im->setFirstIterator();
	$width = $im->getImageWidth();
	$height = $im->getImageHeight();

	// see if animation
	$numimages = $im->getNumberImages() - 1;
	if ($numimages && FALSE) { // not active, can't make animation
		//cclog( "\n" . 'is GIF');
		header("Content-Type: image/gif");
		// copy all images

		$out = new Imagick();
		$out->newImage($width, $height, 'black', 'gif');

		/*
		$out = $im->coalesceImages();
		//*/

		//*
		$images = $im->coalesceImages();
		foreach ($images as $frame) {
			$frame->thumbnailImage($width, $height);
			$out->compositeImage($frame, imagick::COMPOSITE_OVERLAY, 0, 0);
			$out->setImageDispose(1);
			$out->addImage($out->getImage());
		}
		$out->removeImage();
		//*/

		/*
		for ($i=0; $i <= $numimages; $i++) {
			$out->addImage($im->getImage());
			$out->setImageDispose(1);
			$im->nextImage();
		//cclog( "\n" . $out->getImage());
		}
		//*/

		//*
		$out = $out->deconstructImages();
		//cclog( "\n" . $out);
		//*/

		return $out;
		//return $im->deconstructImages();
	}
	else {
		header("Content-Type: image/png");
		$out = new Imagick();
		$out->newImage($width, $height, 'black', 'png');
		$out->compositeImage($im, imagick::COMPOSITE_OVER, 0, 0);

		//cclog( "\n" . 'is PNG');
	}

	return $out;
}

function validate_color_hex($color) {
	// check if hex color string is valid
	// ex. #c1c2b4
	if (preg_match('/^#[a-f0-9]{6}$/i', trim($color))) {
		//hex color is valid
		return true;
	}
	return false;
}

// sanitize URLs in query to prevent, e.g. recursive images
// infinite looping will use up server processes and hang website
function sanitizeUrl(&$url) {
	$unsafeURLs = [
		"ieants.cc/imgd/ymtc" => "",
		"localhost/imgd/ymtc" => "",
	];
	if (isset($url)) {
		$url = strtr($url, $unsafeURLs);
	}
}

function initQueryVar(&$q, $key, $value) {
	if (!isset($q[$key])) {
		$q[$key] = $value;
	}
	$q[$key] = rawurldecode(stripslashes($q[$key]));
}

function sanitizeQuery(&$q) {
	// replace certain strings, e.g. unsafe URLS, cardname

	// init query vars
	$vars = [
		'cardname' => '',
		'creator' => ' ',
		'manacost' => '',
		'powertoughness' => '',
		'extra' => '',
		'rulestext' => '',
		'flavortext' => '',
		'rarity' => 1,
		'seticonurl' => '',
		'typeiconurl' => '',
		'wmurl' => '',
		'overlayurl' => '',
		'arturl' => '',
		'artist' => '',
		'color' => 'c',
		'border' => '',
		'frame' => 'classicshift',
		'symbol' => '',
		'genre' => 'magic',
		'cardtype' => '00000000000000',
		'supertype' => '00000000',
		'subtype' => '',
		'custommana' => '',
		'customframe' => '',
	];
	foreach ($vars as $key => $value) {
		initQueryVar($q, $key, $value);
	}

	// create cardback
	$q['cardback'] = $q['color'] . $q['border'] . "card.jpg";

	// sanitize URLS
	$urllist = ['seticonurl', 'typeiconurl', 'wmurl', 'overlayurl', 'arturl',  'custommana',  'customframe'];
	foreach ($urllist as $url) {
		if ($q[$url] != "") {
			sanitizeUrl($q[$url]);
		}
	}

	// replace brackets in mana cost
	$q['manacost'] = preg_replace('/[{}\()[\],]+/', '', strtolower($q['manacost']));

	// replace strings in rules flavor, extra texts
	$texts = ['rulestext', 'flavortext', 'extra'];
	foreach ($texts as $t) {
		$q[$t] = trim($q[$t]);
		$q[$t] = preg_replace('/(\*)/', '•', $q[$t]);			// asterisk => bullet
		$q[$t] = preg_replace('/(\-\-)/', '—', $q[$t]);			// double dash => emdash
		$q[$t] = preg_replace('/([\r\n]+)/', "\n", $q[$t]);	// collapse consecutive newlines
	}

	// store custom mana codes, URLs

	storeCustomMana($q['custommana']);
}
?>