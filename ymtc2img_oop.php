<?php
error_reporting(E_ALL); ini_set('display_errors', 1);
$CARDIMAGEURL = "../magic/cardimage_oop.php?";

/*
echo '$_SERVER[\'QUERY_STRING\']:</br>';
echo $_SERVER['QUERY_STRING'];
echo '</br></br>';
//*/

//$cc = substr($_SERVER['QUERY_STRING'], 3);

parse_str($_SERVER['QUERY_STRING'], $qarray);
$cc = $qarray['cc'];
//$cc = preg_replace('/_/', " ", $cc);
$cc = rawurldecode($cc);

/*
echo '$CC:</br>';
echo $cc;
echo '</br></br>';
//*/

$cardname = preg_replace('/(.*)_by_(.*?)$/', "$1", $cc);
$creator = strtolower(preg_replace('/(.*)_by_(.*?)$/', "$2", $cc));

/*
echo '$cardname:</br>';
echo $cardname;
echo '</br></br>';

echo '$creator:</br>';
echo $creator;
echo '</br></br>';
//*/

include_once "../magic/cccreds.php"; // holds $sqlhost, $sqluser, $sqlpass, $sqldb, $sqltbl

$mysqli = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
if ($mysqli->connect_errno) {
	printf("Connect failed: %s\n", $mysqli->connect_error);
	exit();
}
//echo 'Connected successfully';

$table = $sqltbl;

$query = "SELECT * FROM " . $table . " WHERE ";
$query .= "cardname='" . $mysqli->real_escape_string($cardname) . "'";
$query .= "AND lower(creator)='" . $mysqli->real_escape_string($creator) . "'";

/*
echo '$query:</br>';
echo $query;
echo '</br></br>';
exit;
//*/

$result = $mysqli->query($query);

// Check result
// This shows the actual query sent to MySQL, and the error. Useful for debugging.
if (!$result) {
	$message  = 'Invalid query: ' . $mysqli->error . "\n";
	$message .= 'Whole query: ' . $query;
	die($message);
}

$row = $result->fetch_assoc();

if ($row) {
	//echo 'QUERY FOUND';
	
	$query = '';
	foreach ($row as $key=>$value) {
		$query .= "$key=" . rawurlencode($value) . "&";
	}
	$imgurl = $CARDIMAGEURL . $query;
	
	header('HTTP/1.1 303 See Other');
	header('Cache-Control: no-cache, must-revalidate');
	header('Content-Location: ' . $_SERVER['REQUEST_URI']);
	header('Content-Type: image/png');
	header('Location: ' . $imgurl);
}
/*
else {
	 echo 'QUERY NOT FOUND'; 
	 echo '</br></br>';   
}
return;
//*/
?>