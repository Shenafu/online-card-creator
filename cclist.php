<?php
// Magic card creator
// using Imagick library

error_reporting(E_ALL);

header('Cache-Control: no-cache, no-store, must-revalidate');

function fix_rewrite_decode($string) {
	$string = rawurldecode($string);
	return $string;
}

function fix_rewrite_encode($string) {
	$string = rawurlencode($string);
	$string = str_replace("%", "%25", $string);
	return $string;
}

$DEFAULT_ALIAS = "Anonymous";

$finalHeight = 523;

$serverURI = explode('/', $_SERVER['REQUEST_URI']);
$creator = isset($serverURI[3]) ? fix_rewrite_decode($serverURI[3]) : '';
$page = isset($serverURI[4]) ? $serverURI[4] : '';

if (trim($creator) == '') {
	$creator = "";
	$alias = $DEFAULT_ALIAS;
} else {
	$alias = $creator . "'s";
}

?>
<!doctype html>
<html class="shenafu" id="cardcreatorlist">
<head>
	<title><?php echo "$alias Cards : Page " . "$page"; ?> @ Shenafu's Card Creator</title>
	<meta  charset="utf-8" />
	<link rel="icon" type="image/jpg" href="/magic/favicon.ico"/>
<style type="text/css">
.pagenav {max-height: 4em; max-width: 100%; overflow: auto;}
.cardimage {height: <?php echo $finalHeight;?>px; }
.cardimage img {max-height: <?php echo $finalHeight;?>px; max-width: 100%; }
</style>
</head>
<body>
<div id="BodyBox">
<div id="MainBox">
<a href="/magic/cc"><h1> Shena'Fu's Card Creator Card List</h1></a>

<?php
$perpage = 12;

@parse_str($_SERVER['QUERY_STRING']);

if (!isset($page) || !is_numeric($page)) {
	$page = 0;
}

$URLcreator = fix_rewrite_encode($creator);

include_once "cccreds.php"; // holds $sqlhost, $sqluser, $sqlpass, $sqldb, $sqltbl
$mysqli = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
if ($mysqli->connect_errno) {
	printf("Connect failed: %s\n", $mysqli->connect_error);
	exit();
}
//echo 'Connected successfully';

echo "<p>$alias cards: ";

$creator = $mysqli->escape_string($creator);
$query = "call $sqldb.get_count_by_creator('$creator');";

$totalcards = $mysqli->query($query)->fetch_array()[0];
$mysqli->next_result(); // must clear before calling next query

echo "$totalcards</p>";

// print navigation bar
$totalpages = max( floor(($totalcards-1)/$perpage), 0);
$page = min($page, $totalpages);
$pagenav = "<div class=\"pagenav\">Pages: "; // HTML code to navigate between pages

if ($page > 0) {
	$pagenav .= "<a href=\"/magic/cclist/$URLcreator/" . ($page-1) ."\" rel=\"prev\">Prev</a> ";
}

$pagenav .= " ($page) ";

if ($page < $totalpages) {
	$pagenav .= "<a href=\"/magic/cclist/$URLcreator/" . ($page+1) ."\" rel=\"next\">Next</a> ";
}

for ($i=0; $i<=$totalpages; $i++) {
	if ($i==$page) {
		$pagenav .= $i . "&nbsp;";
	}
	else {
		$pagenav .= "<a href=\"/magic/cclist/$URLcreator/$i\">$i</a>&nbsp;";
	}
}

$pagenav .= "</div>";
echo $pagenav;

// show the cards

echo "<br>\n<div id=\"cardlist\">\n\n";

$cardstart = $page * $perpage;
$query = "call get_list('$creator', $cardstart, $perpage);";
$mysqli->next_result(); // must clear before calling next query
$mysqli->multi_query($query);

if ($result = $mysqli->store_result()) {
	while ($row = $result->fetch_assoc()) {
		$cardname = stripslashes($row['cardname']);
		$creator = stripslashes($row['creator']);

		// fix characters for mod_rewrite
		$URLcardname = fix_rewrite_encode($cardname);
		$URLcreator = fix_rewrite_encode($creator);
		$imagename = "$URLcreator/$URLcardname";
		$target = ''==$cardname ? '_blank' : $cardname;
		echo "<span class=\"cardimage\"><a href=\"/magic/cc/$URLcreator/$URLcardname\" target=\"$target\"><img src=\"/magic/ccimg/$imagename.png?h=$finalHeight\" alt=\"$cardname by $creator\"/></a></span>\n\n";
	}
}

echo "</div>";
echo "\n\n";

echo "<script>
function showCards() {
	creator = input_creator.value;
	newUrl = window.location.protocol + '//' + window.location.hostname + '/magic/cclist/' + creator + '/';
	window.location = newUrl;
}
</script>";
echo "\n\n";

$onSubmit = '"showCards();"';
	echo "<hr>\n<p>";
	echo "<label>View Another Creator's Cards: </label><input id=\"input_creator\" placeholder=\"Type Creator's name\" onchange=$onSubmit></input>";
	echo "<button onclick=$onSubmit>Show Cards</button>";
	echo "</p>\n<hr>";
?>


</div>
</div>
</body>
</html>