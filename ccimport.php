<!doctype html>
<html class="shenafu" id="cardcreatorimport">
<head>
	<title>Set Importer @ Shenafu's Card Creator</title>
	<meta  charset="utf-8" />
	<link rel="icon" type="image/jpg" href="/magic/favicon.ico"/>
<style type="text/css">
#BodyBox {display: flex; }
#BodyBox > div { margin: 10px; }
pre { white-space: pre-wrap; }
h2 {text-align: center; font: small-caps 21pt 'Fondamento', cursive; color: yellow; background: green; }
h3 {text-align: center; color: black; background: orange; margin: 2px; font: 16pt 'Fondamento', cursive; text-decoration: underline; }
.markdown h2 {margin-bottom: -40px; }
.markdown h3 {margin-bottom: -40px; margin-top: -40px; }

</style>
<script src="/lib/vsv.js"></script>
<script src="/lib/jquery.js"></script>
<script src="/lib/markdown.js"></script>
</head>
<body onload="CCOnload();" >
<a href="ccimport"><h1> Shena'Fu's Card Creator -- Set Importer</h1></a>

<script>
var reNewline = new RegExp('\\^', 'g');

var GENRE_TYPES = {
	"magic": {
		"supertype": ["legendary", "basic", "snow", "world", "ongoing", "fast", "delayed", "token", "elite", "host"],
		"cardtype": ["tribal", "artifact", "enchantment", "land", "creature", "instant", "sorcery", "planeswalker", "plane", "scheme", "emblem", "hero", "conspiracy", "phenomenon"]
	},
	"earth": {
		"supertype": ["famous", "basic", "magical", "global", "ongoing", "fast", "delayed", "clone", "super", "host"],
		"cardtype": ["faction", "object", "agenda", "land", "agent", "mod", "event", "paragon", "scene", "point", "paradigm", "hero", "villain", "power"]
	},
	"space": {
		"supertype": ["mythical", "primary", "dark", "universe", "ongoing", "fast", "delayed", "clone", "mega", "host"],
		"cardtype": ["faction", "device", "augment", "resource", "unit", "tactic", "strategy", "galaxystalker", "galaxy", "scheme", "paradigm", "hero", "conspiracy", "phenomenon"]
	},
	"pokemon": {
		"supertype": ["baby", "basic", "stage 1", "stage 2", "", "", "special", "", "", ""],
		"cardtype": ["", "", "", "energy", "pokémon", "trainer", "", "pokémon", "", "", "", "", "", ""]
	},
	"mbs": {
		"supertype": ["", "", "", "", "", "", "", "", "", ""],
		"cardtype": ["", "equipment", "", "battleground", "unit", "", "action", "", "", "", "", "", "", ""]
	}
};

// set defaults
var cardinfo={
	'creator':'',
	'baseframe':'classicshift',
	'seticonurl':'',
	'symbol':'',
	'genre':'magic',
	'custommana':'',
	'customframe':''
};
resetFields(cardinfo);


function importVSV(textId) {
	var text = document.getElementById(textId).value;
	var vsv = VSV.mapTo.array(text);

	// parse by row
	vsv.forEach(function(row) {
		var rowType = row.shift();

		switch (rowType) {
			case 'header':
				break;

			case 'data':
				var field = row.shift().toLowerCase();
				var data = row[0];

				if (field == 'cardname') {
					// submit previous card before reading next card
					submitCard(cardinfo);

					// reset fields for new card
					resetFields(cardinfo);
				}

				processData(cardinfo, field, data);
				break;
		}

	});
	submitCard(cardinfo);

	log("Import VSV completed.");
	alert("Import VSV completed.");
}

function importJSON(textId) {
	var text = document.getElementById(textId).value;
	var json = JSON.parse(text);
	for (var field in json["meta"]) {
		var data = json["meta"][field];
		processData(cardinfo, field, data);
	}
	json["cards"].forEach(function(card) {
		resetFields(cardinfo);

		for (var field in card) {
			var data = card[field];
			processData(cardinfo, field, data);
		}

		submitCard(cardinfo);
	});

	log("Import JSON completed.");
	alert("Import JSON completed.");
}

function processData(cardinfo, field, data) {
	field = field.toLowerCase();

	// set default value if empty string
	cardinfo[field] = setValue(cardinfo, field, data);
}

function setValue(cardinfo, field, data) {
	switch (field) {
		case 'baseframe':
			data = data || cardinfo.baseframe;
			cardinfo.frame = data;
			return data;
		case 'frame':
			if (data) {
				return data;
			}
			if (cardinfo.cardname.match(';;')) {
				return 'split';
			}
			return cardinfo.baseframe;
		case 'genre':
			return data || 'magic';
		case 'color':
			return data ? getColor(data.toLowerCase()) : 'c';
		case 'supertype':
			return data ? getTypeIndex(cardinfo, field, data) : '0000000000';
		case 'cardtype':
			return data ? getTypeIndex(cardinfo, field, data) : '00000000000000';
		case 'rarity':
			data = data ? data.toLowerCase() : '1';
			switch (data) {
				case 'c':
					return '1';
				case 'u':
					return '2';
				case 'r':
					return '3';
				case 'm':
					return '4';
				case 'e':
					return '5';
				default:
					return data;
			}
		case 'extra':
		case 'rulestext':
		case 'flavortext':
			data = data ? data : '';
			data = data.replace(reNewline, "\n");
			return data;
		case 'loyalty':
			data = data ? data : '0';
			cardinfo.powertoughness = data;
			return data;
		case 'border':
			data = data ? data.toLowerCase() : "";
			if (data == "l") {
				return "l";
			}
			return data;
		case 'cardname':
		case 'wmurl':
		case 'overlayurl':
		case 'arturl':
		case 'artist':
		case 'subtype':
		case 'powertoughness':
		case 'symbol':
		case 'seticonurl':
		case 'typeiconurl':
		case 'manacost':
		case 'creator':
		case 'custommana':
		case 'customframe':
		default:
			return data || '';
	}
}

function submitCard(cardinfo) {
	if (cardinfo.cardname.trim() == "") {
		return;
	}
	var url = "cardsave.php?";
	var query = "";
	for (field in cardinfo) {
		query += "&" + field + "=" + encodeURIComponent(cardinfo[field]);
	};
	query = query.substr(1); // remove leading '&'

	//log(query);

	// send card data
	$.ajax({
		type: "POST",
		url: url + query,
		success: function(response) {
			//log(response);
		}
	});

}

function resetFields(card) {
	card.cardname = '';
	card.frame = card.baseframe;
	card.manacost = '';
	card.powertoughness = '';
	card.subtype = '';
	card.extra = '';
	card.rulestext = '';
	card.flavortext = '';
	card.rarity = '1';
	card.wmurl = '';
	card.overlayurl = '';
	card.arturl = '';
	card.artist = '';
	card.color = 'c';
	card.border = '';
	card.supertype = '0000000000';
	card.cardtype = '00000000000000';
}

function getTypeIndex(cardinfo, field, data) {
	var list = data.toLowerCase().split(' ');
	var f = field.toLowerCase();

	list.forEach(function(t) {
		var i = GENRE_TYPES[cardinfo.genre][f].lastIndexOf(t);

		if (i >= 0) {
			cardinfo[field] = cardinfo[field].substr(0, i) + 1 + cardinfo[field].substr(i+1);
		}
	});
	return cardinfo[field];
}

function getColor(data) {
	data = data.trim();
	if (data.length == 1 && data.match('[cwubrgmqaoykpt]')) {
		return data; // valid frame colors
	}
	if (data.length > 2) {
		return 'm'; // multicolor
	}
	if (data.length > 1) {
		return 'q'; // blend
	}
	return 'c'; // else colorless
}

</script>

<div id="BodyBox">
<div>
<p>
<button onclick="importVSV('textarea');">Import VSV</button>
__
<button onclick="importJSON('textarea');">Import JSON</button>
</p>
<p>
<textarea id="textarea" cols="60" rows="44" style="background: #F0FFF0">
[[Meta]]
`creator`
`baseframe`
`seticonurl`
`genre`
`symbol`
`custommana`
`customframe`


[[Cards]]
`cardname`
`manacost`
`color`
`supertype`
`cardtype`
`subtype`
`powertoughness`
`rarity`
`extra`
`rulestext`
`flavortext`
`typeiconurl`
`wmurl`
`overlayurl`
`arturl`
`artist`
`frame`
`border`

`cardname`
`manacost`
`color`
`supertype`
`cardtype`
`subtype`
`powertoughness`
`rarity`
`extra`
`rulestext`
`flavortext`
`typeiconurl`
`wmurl`
`overlayurl`
`arturl`
`artist`
`frame`
`border`
</textarea>
</p>
<p>
<button onclick="importVSV('textarea');">Import VSV</button> __
<button onclick="importJSON('textarea');">Import JSON</button>
</p>
<ul>
	<li><a href="cc/" target="cc">Go to Shena'Fu's Card Creator</a></li>
	<li><a href="cclist/creator" target="cclist">Go to Card List</a></li>
</ul>
</div>

<script>

function renderMarkdown(element) {
	var text = element.innerHTML;
	element.innerHTML = markdown.toHTML(text);
}

function CCOnload() {
	renderMarkdown(hintstext);
}
</script>

<div style="background: #FFFFF0">
<pre id="hintstext" class="markdown">
<?php readfile('ccimport_hints.txt'); ?>
</pre>
</div>

</div>

</body>
</html>