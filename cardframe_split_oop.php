<?php

// split frames

class CardFrame_Split {

	////// CONSTANTS //////
	const WIDTH = 472;
	const WIDTHHALF = 236;
	const HEIGHT = 338;

	////// PROPERTIES //////

	public $dirframe, $dirmana, $dirfont; // directories to resources
	public $q; // array holding query variables
	public $cardnamefont, $typefont, $manafont, $ptfont, $textfont, $italicfont;
	public $im, $mcircle, $textdivider; // images
	public $black, $white, $clear; // ImagickPixel constants
	public $typetext; // final type line

	////// METHODS //////

	function __construct($qarray) {
		// data from query
		$this->q = $qarray;

		$this->typetext = cardimage_gettypeline($this->q['supertype'], $this->q['cardtype'], $this->q['subtype'], $this->q['genre'], "— ");

		if (preg_match('/artifact/i', $this->typetext) && preg_match('/^c/', $this->q['cardback'])) {
			$this->q['cardback'] = "acard.jpg";
		}

		$this->manacosts = explode(";;", $this->q['manacost'], 2);

		// file resources
		$this->dirframe = "frames/frame_" . $this->q['frame'];
		$this->textdivider = new Imagick( "$this->dirframe/horiz-divider.png" );
		$this->dirmana = "symbols/" . ((empty($this->q['symbol'])) ? "modern" : $this->q['symbol']);
		$this->mcircle = new Imagick( "$this->dirmana/mana_circle.png");
		$this->seticon = "$this->dirframe/seticon.png";
		$this->dirfont = $_SERVER['DOCUMENT_ROOT'] . "/fonts";
		$this->cardnamefont = "$this->dirfont/kelvinch-bold.ttf";
		$this->typefont = "$this->dirfont/kelvinch-bold.ttf";
		$this->manafont = "$this->dirfont/ubuntumono-regular.ttf";
		$this->ptfont = "$this->dirfont/crimson-regular.ttf";
		$this->boldfont = "$this->dirfont/crimson-bold.ttf";
		$this->textfont = "$this->dirfont/crimson-regular.ttf";
		$this->italicfont = "$this->dirfont/crimson-italic.ttf";
		$this->bolditalicfont = "$this->dirfont/crimson-bolditalic.ttf";
		$this->dyratio = 1.0;

		// drawing resources
		$this->black = new ImagickPixel("black");
		$this->white = new ImagickPixel("white");
		$this->clear = new ImagickPixel("transparent");
		$this->draw = new ImagickDraw();
		$this->manadraw = new ImagickDraw();
		$this->im = new Imagick();
		$this->im->newImage($this::WIDTH, $this::HEIGHT, $this->black, "png");
	}

	function drawBackground() {
		// background base

        $cardback = "$this->dirframe/" . $this->q['cardback'];
		if (file_exists($cardback)) {
			$bg = new Imagick($cardback);
            $this->im->compositeImage($bg, imagick::COMPOSITE_OVER, 0, 0);
        }
	}

	function drawBlend() {
		// find color of each side from mana costs
		// then print card frames and mana symbols

		$this->cbs = []; // cardback prefixes = color
		$x = 0;
		for($i=0, $ct=count($this->manacosts); $i<$ct; $i++) {
			$colors = cardfuncs_getblendoptions($this->manacosts[$i], '');
			$this->cbs[$i] = 'c'; // default colorless
			if (count($colors["colors"]) == 1) {
				// mono-color
				$this->cbs[$i] = $colors["colors"][0];

			}
			elseif (count($colors["colors"]) >= 2) {
				// multi-color
				$this->cbs[$i] = 'm';
			}

			$cardback = substr_replace($this->q['cardback'], $this->cbs[$i], 0, 1);

			$bg = new Imagick("$this->dirframe/" . $cardback);
			$this->im->compositeImage($bg, imagick::COMPOSITE_OVER, $x, 0);

			$x += $this::WIDTHHALF;
		}
	}

	function drawArt() {
		// art

		if ($this->q['arturl']!="") {
			// split art URLs
			$arts = explode(";;", $this->q['arturl'], 2);

			$x = 10;
			for($i=0, $ct=count($arts); $i<$ct; $i++) {
				$this->q['arturl'] = $arts[$i];

				$options = [
					'artx' => $x
					,'arty' => 31
					,'artwidth' => 216
					,'artheight' => 158
				];
				cardfuncs_makeArtImage($options, $this);

				$x += $this::WIDTHHALF;
			}
		}
	}

	function drawArtistCreator() {
		// artist and creator

		$options = [
			'font' => $this->typefont
			,'fontsize' => 12.0
			,'textx' => 30
			,'texty' => 327
			,'wmax' => 147
			,'text' => $this->q['artist']
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
		];

		cardfuncs_makeLinearText($options, $this);

		$options = [
			'font' => $this->typefont
			,'fontsize' => 12.0
			,'textx' => 267
			,'texty' => 327
			,'wmax' => 147
			,'text' => "Created by " . $this->q['creator']
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
		];

		cardfuncs_makeLinearText($options, $this);

	}

	function drawWatermark() {
		// watermark

		$wms = preg_split('/;;/', $this->q['wmurl'] . ';;');

		$x = $this::WIDTHHALF / 2;

		for ($i=0; $i<2; $i++) {
			$options = [
				'wmx' => $x
				,'wmy' => 265
				,'wmw' => 208
				,'wmh' => 97
				,'wmurl' => $wms[$i]
			];

			cardfuncs_makeWatermark($options, $this);

			$x += $this::WIDTHHALF;
		}
	}

	function drawManaCost() {
		// write mana cost, simplified, align right

		$x = 226;
        $i = 0;
        $this->mcbox = [];
		foreach ($this->manacosts as $manacost) {
			$this->q['manacost'] = $manacost;
			$options = [
				'font' => $this->manafont
				,'fontsize' => 14.0
				,'fontcolor' => $this->black
				,'manacostx' => $x
				,'manacosty' => 10
				,'manaiconwidth' => 14
				,'manaiconheight' => 14
				,'width' => $this::WIDTHHALF
				,'height' => 14
			];
			$this->mcbox[++$i] = cardfuncs_makeManaCost($options, $this);

			$x += $this::WIDTHHALF;
		}
	}

	function drawCardName() {
		// write card name

		$this->cardnames = preg_split('/;;/', $this->q['cardname'], 2);

		$x = 13;
		$i = 0;
		foreach ($this->cardnames as $cardname) {
			$iconx = 0;
			try {
				if (isset($this->typeicons[$i])) {
					$iconx = $this->typeicons[$i]->getimagewidth();
				}
			}
			catch (Exception $e) {
				cclog( "\n" . '$icon error : ' . $e);
			}

			$options = [
				'font' => $this->cardnamefont
				,'fontsize' => 16.0
				,'textx' => $x + $iconx
				,'texty' => 16
				,'wmax' => 224 - $this->mcbox[++$i]['width'] - $iconx
				,'text' => $cardname
				,'textcolor' => $this->black
			];
			cardfuncs_makeLinearText($options, $this);

			$x += $this::WIDTHHALF;
		}
	}

	function drawType() {
		// write card type

		// split card type
		$typetexts = explode(";;", $this->typetext, 2);

		$x = 13;

		for ($i=0, $ct=count($typetexts); $i<$ct; $i++) {
			// trim extra spaces and dashes
			$typetexts[$i] = preg_replace("/[-—][-—]+/", "—", $typetexts[$i]);
			$typetexts[$i] = trim($typetexts[$i]);
			$typetexts[$i] = trim($typetexts[$i], "—");
			$typetexts[$i] = trim($typetexts[$i]);

			$options = [
				'font' => $this->typefont
				,'fontsize' => 14.0
				,'textx' => $x
				,'texty' => 202
				,'wmax' => 195
				,'text' => $typetexts[$i]
				,'textcolor' => $this->black
			];
			cardfuncs_makeLinearText($options, $this);

			$x += $this::WIDTHHALF;
		}

	}

	function drawPT() {
		// write power toughness

		$pts = explode(";;", $this->q['powertoughness'], 2);
		for ($i=0, $ct=count($pts); $i<$ct; $i++) {
			$pts[$i] = trim($pts[$i]);
		}

		$x = 206;
		$y = 322;
		$ptx = 176;
		$pty = 308;

		for ($i=0, $ct=count($pts); $i<$ct; $i++) {
			if ($pts[$i] != '') {
				$ptfile = "$this->dirframe/" . $this->cbs[$i] . "pt.jpg";
                if (file_exists($ptfile)) {
                    $ptimg = new Imagick($ptfile);
                    $this->im->compositeImage($ptimg, imagick::COMPOSITE_OVER, $ptx, $pty);
                }

				$options = [
					'font' => $this->ptfont
					,'fontsize' => 16.0
					,'textx' => $x
					,'texty' => $y
					,'wmax' => 36
					,'text' => $pts[$i]
					,'textcolor' => $this->black
					,'textalign' => imagick::ALIGN_CENTER
				];
				cardfuncs_makeLinearText($options, $this);
			}

			$x += $this::WIDTHHALF;
			$ptx += $this::WIDTHHALF;
		}
	}

	function drawRulesText() {
		// write rules and flavor texts

		$x = 11;

      $this->q['rulestext'] = $this->q['extra'] . "\n". $this->q['rulestext'];

		cardfuncs_conjoined_cardframe_op($this->q, ['rulestext', 'flavortext'], ["cardfuncs_replaceMemtext"]);

		$rulestexts = explode(";;", $this->q['rulestext'], 2);
		$cardnames = explode(";;", $this->q['cardname']);

		for ($i=0, $ct=count($rulestexts); $i<$ct; $i++) {
			$q = $this->q;
			$q['cardname'] = $cardnames[$i];
			$rulestexts[$i] = trim(cardfuncs_replaceCardTraits($rulestexts[$i], $q));

			if ($rulestexts[$i] != '') {
				$options = [
					'textfont' => $this->textfont
					,'boldfont' => $this->boldfont
					,'italicfont' => $this->italicfont
					,'bolditalicfont' => $this->bolditalicfont
					,'fontsize' => 20.0
					,'minfontsize' => 6.0
					,'textletterx' => 2
					,'textlettery' => 4
					,'textboxx' => $x
					,'textboxy' => 216
					,'textboxwidth' => 208
					,'textboxheight' => 97
					,'rulestext' => $rulestexts[$i]
					,'flavortext' => ''
				];
				cardfuncs_makeRulesbox($options, $this);

				$x += $this::WIDTHHALF;
			}
		}
	}


	function drawSetIcon() {
		// draw set icon, with rarity colors filled

		$x = 225;

		for ($i=0; $i<2; $i++) {
			$options = [
				'w' => 14
				,'h' => 14
				,'x' => $x
				,'y' => 194
			];

			cardfuncs_makeSetIcon($options, $this);

			$x += $this::WIDTHHALF;
		}
	}

	function drawTypeIcon() {
		// write card type icon

		$icons = preg_split('/;;/', $this->q['typeiconurl'] . ';;');
		$this->typeicons = [];

		$x = 8;
		$y = 9;

		for ($i=0; $i<2; $i++) {
			$options = [
				'x' => $x
				,'y' => $y
				,'w' => 16
				,'h' => 16
				,'typeiconurl' => $icons[$i]
			];

			$this->typeicons[$i] = cardfuncs_makeTypeIcon($options, $this);

			$x += $this::WIDTHHALF;
		}
	}

	function drawOverlay() {
		// overlays

		$ovs = preg_split('/;;/', $this->q['overlayurl'] . ';;');

		$x = 0;
		$y = 0;

		for ($i=0; $i<2; $i++) {
        $options = [
            'ovx' => $x
            ,'ovy' => $y
            ,'ovw' => $this::WIDTHHALF
            ,'ovh' => $this::HEIGHT
				,'overlayurl' => $ovs[$i]
        ];

			cardfuncs_makeOverlay($options, $this);

			$x += $this::WIDTHHALF;
		}
	}

	function createFinalImage() {
		// start creation of image
		//$this->drawBackground();
		$this->drawBlend();
		$this->drawArt();
		$this->drawWatermark();
		$this->drawSetIcon();
		$this->drawManaCost();
		$this->drawTypeIcon();
		$this->drawCardName();
		$this->drawType();
		$this->drawPT();
		$this->drawRulesText();
		$this->drawArtistCreator();
		$this->drawOverlay();

		/* Output the image*/
		return $this->im;
	}
} // end class def

$cardframe = new CardFrame_Split($qarray);
?>
