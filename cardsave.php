<?php
error_reporting(E_ALL); ini_set('display_errors', 1);

$CC_DEBUG = true; // true | false;
$SAVEFILELOG = "customsave.log";

function cclog( $message ) {
	global $CC_DEBUG;
	if ( $CC_DEBUG ) {
		error_log( $message, 3, "cc.log" );
	}
}

function fix_creator($creator) {
	if (!isset($creator) || $creator=='') {
		return ' ';
	}

	return $creator;
}

function build_query($qarray, $table, $mysqli) {
	$vars = [
	'cardname',
	'creator',
	'manacost',
	'powertoughness',
	'extra',
	'rulestext',
	'flavortext',
	'rarity',
	'seticonurl',
	'typeiconurl',
	'wmurl',
	'overlayurl',
	'arturl',
	'artist',
	'color',
	'border',
	'frame',
	'symbol',
	'genre',
	'supertype',
	'cardtype',
	'subtype',
	'custommana',
	'customframe',
	];
	$query = "call save_card (";
	foreach ($vars as $key) {
		if (isset($qarray[$key])) {
		$query .= "'";
		$query .= $mysqli->real_escape_string($qarray[$key]);
		$query .= "', ";
		}
		else {
			$query .= "'', ";
		}
	}

	$query = rtrim($query, ', ');
	$query .= ");";
	return $query;
}

function save2db($qarray) {
    include_once "cccreds.php"; // holds $sqlhost, $sqluser, $sqlpass, $sqldb, $sqltbl

	$mysqli = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
	if ($mysqli->connect_errno) {
		printf("Connect failed: %s\n", $mysqli->connect_error);
		cclog("Connect failed: %s\n", $mysqli->connect_error);
		exit();
	}
	//echo 'Connected successfully';

	$table = $sqltbl;
	$query = build_query($qarray, $table, $mysqli);
	$sqlerr = $mysqli->query($query);

	// Check result
	// This shows the actual query sent to MySQL, and the error. Useful for debugging.
	if (!$sqlerr) {
		$message  = 'Invalid query: ' . $mysqli->error . "\n";
		$message .= 'Whole query: ' . $query;
		cclog($message);
		die($message);
	}
}

function save2log($cardname, $creator, $filename, $query) {
	// save latest cardname and creator to log file for later use
	// such as customsave.png
	// but only periodically, e.g. once every hour

	$timediff = 1; //in seconds, eg 1 minutes = 60, 1 hour = 3600
	$now = time();
	if ( ! file_exists($filename) || $now - filemtime($filename) >= $timediff) {
		$file = fopen($filename, "w");
		fwrite($file, $cardname. ' by ' . $creator);
		fwrite($file, "\n" . http_build_query($query));
		fclose($file);
	}
}

//BEGIN

$qarray = $_POST;
if (count($qarray) == 0) {
	parse_str($_SERVER['QUERY_STRING'], $qarray);
}

$cardname = isset($qarray['cardname']) ? $qarray['cardname'] : '';
$creator = isset($qarray['creator']) ? $qarray['creator'] : ' ';
$creator = fix_creator($creator);
if (trim($cardname)!="" || trim($creator)!="") {
	save2db($qarray);
	save2log($cardname, $creator, $SAVEFILELOG, $qarray);
}

header('Location: /magic/cc/' . $creator . '/' . $cardname);
exit;

?>
