<?php

// planeswalker frames

class CardFrame_Planeswalker {

	////// CONSTANTS //////
	const WIDTH = 750;
	const HEIGHT = 1046;

	////// PROPERTIES //////

	public $dirframe, $dirmana, $dirfont; // directories to resources
	public $q; // array holding query variables
	public $cardnamefont, $typefont, $manafont, $ptfont, $textfont, $italicfont;
	public $im, $mcircle, $textdivider; // images
	public $black, $white, $clear; // ImagickPixel constants
	public $typetext; // final type line

	////// METHODS //////

	function __construct($qarray) {
		// data from query
		$this->q = $qarray;

		$this->typetext = cardimage_gettypeline($this->q['supertype'], $this->q['cardtype'], $this->q['subtype'], $this->q['genre'], "— ");

		$this->q['cardback'] = str_replace("lcard", 'card', $this->q['cardback']);
		if (preg_match('/artifact/i', $this->typetext) && preg_match('/^c/', $this->q['cardback'])) {
			$this->q['cardback'] = "acard.jpg";
		}

		$this->q['rulestext'] = $this->q['extra'] . "\n". $this->q['rulestext'];

		// file resources
		$this->dirframe = "frames/frame_" . $this->q['frame'];
		$this->textdivider = new Imagick( "$this->dirframe/horiz-divider.png" );
		$this->dirmana = "symbols/" . ((empty($this->q['symbol'])) ? "modern" : $this->q['symbol']);
		$this->mcircle = new Imagick( "$this->dirmana/mana_circle.png");
		$this->seticon = "$this->dirframe/seticon.png";
		$this->dirfont = $_SERVER['DOCUMENT_ROOT'] . "/fonts";
		$this->cardnamefont = "$this->dirfont/kelvinch-bold.ttf";
		$this->typefont = "$this->dirfont/kelvinch-bold.ttf";
		$this->manafont = "$this->dirfont/ubuntumono-regular.ttf";
		$this->ptfont = "$this->dirfont/crimson-regular.ttf";
		$this->boldfont = "$this->dirfont/crimson-bold.ttf";
		$this->textfont = "$this->dirfont/crimson-regular.ttf";
		$this->italicfont = "$this->dirfont/crimson-italic.ttf";
		$this->bolditalicfont = "$this->dirfont/crimson-bolditalic.ttf";
		$this->dyratio = 1.0;

		// drawing resources
		$this->black = new ImagickPixel("black");
		$this->white = new ImagickPixel("white");
		$this->magenta = new ImagickPixel("magenta");
		$this->clear = new ImagickPixel("transparent");
		$this->draw = new ImagickDraw();
		$this->manadraw = new ImagickDraw();
		$this->im = new Imagick();
		$this->im->newImage($this::WIDTH, $this::HEIGHT, $this->white, "png");
	}

	function drawBackground() {
		// background base

		$cardback = "$this->dirframe/" . $this->q['cardback'];
		if (file_exists($cardback)) {
			$bg = new Imagick($cardback);
			$this->im->compositeImage($bg, imagick::COMPOSITE_OVER, 0, 0);
		}
	}

	function drawBlend() {
		// blending for multicolor and hybrid

		$this->blendoptions = cardfuncs_getblendoptions($this->q['manacost'], $this->q['rulestext']);
		$options = [
			'width' => $this::WIDTH
			,'height' => $this::HEIGHT
			,'black' => $this->black
			,'blendstartrate' => 0.4
			,'blendendrate' => 0.6
			,'contrast' => 20
			,'midtone' => 0.166
		];
		if ($this->q['color']=='q') {
			$options['blendframemask'] = "blend_frame_mask.png";
		}
		$this->im = cardfuncs_renderblend($options, $this);
	}

	function drawArt() {
		// art

		$artx = 52;
		$arty = 104;
		$artw = 646;
		$arth = 852;
		$maskw = 643;
		$maskh = 850;

		$arturl = $this->q['arturl'];
		if (isset($arturl) && $arturl!="") {
			// add http://
			if (0==preg_match("/^http(s)?:\/\//", $arturl)) {
				$arturl = "http://$arturl";
			}
			if (isValidUrl($arturl)) {
				try {
					$art = new Imagick($arturl);
					if ($art) {
						$art = cardfuncs_artExtras($art, $arturl);

						$art->thumbnailImage($maskw, $maskh);

						// mask bottom
						$mask = new Imagick($this->dirframe . '/imagemask-bot.png');
						$mask->transparentPaintImage($this->white, 0.0, 0, false);
						$art->compositeImage($mask, imagick::COMPOSITE_SCREEN, 0, 0);

						// mask top
						$mask = new Imagick($this->dirframe . '/imagemask.png');
						$mask->transparentPaintImage($this->white, 0.0, 101, false);
						$art->compositeImage($mask, imagick::COMPOSITE_OVER, 0, 0);

						$art->resizeImage($artw, $arth, imagick::FILTER_QUADRATIC, 1, false);
						$art->transparentPaintImage($this->magenta, 0.0, Imagick::getQuantum() * 0.4, false);
						$this->im->compositeImage($art, imagick::COMPOSITE_OVER, $artx, $arty);
					}
				}
				catch (ImagickException $e) {
						cclog( "\n" . '$art error : ' . $e);
				}
			}
		}
	}

	function drawArtistCreator() {
		// artist and creator

		$options = [
			'font' => $this->typefont
			,'fontsize' => 26.0
			,'textx' => 50
			,'texty' => 995
			,'wmax' => 535
			,'text' => "Illus. by " . $this->q['artist']
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
		];

		cardfuncs_makeLinearText($options, $this);

		$options = [
			'font' => $this->typefont
			,'fontsize' => 26.0
			,'textx' => 50
			,'texty' => 1027
			,'wmax' => 535
			,'text' => "Created by " . $this->q['creator']
			,'textcolor' => $this->white
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawWatermark() {
		// watermark

		$options = [
			'wmx' => 392
			,'wmy' => 806
			,'wmw' => 596
			,'wmh' => 302
		];

		cardfuncs_makeWatermark($options, $this);
	}

	function drawManaCost() {
		// write mana cost, simplified, align right

		$options = [
			'font' => $this->manafont
			,'fontsize' => 38.0
			,'fontcolor' => $this->black
			,'manacostx' => 696
			,'manacosty' => 48
			,'manaiconwidth' => 36
			,'manaiconheight' => 36
			,'width' => 375
			,'height' => 36
		];

		$this->mcbox = cardfuncs_makeManaCost($options, $this);
	}

	function drawCardName() {
		// write card name
		$x = 56;
		try {
			if (isset($this->typeicon)) {
				$x += $this->typeicon->getimagewidth() + 10;
			}
		}
		catch (Exception $e) {
			cclog( "\n" . '$icon error : ' . $e);
		}

		$options = [
			'font' => $this->cardnamefont
			,'fontsize' => 40.0
			,'textx' => $x
			,'texty' => 66
			,'wmax' => 691 - $this->mcbox['width'] - $x
			,'text' => $this->q['cardname']
			,'textcolor' => $this->black
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawType() {
		// write card type

		$options = [
			'font' => $this->typefont
			,'fontsize' => 30.0
			,'textx' => 60
			,'texty' => 616
			,'wmax' => 590
			,'text' => $this->typetext
			,'textcolor' => $this->black
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawPT() {
		// write power toughness

		// draw PT frame
		$framex = 592;
		$framey = 926;
		$framew = 135;
		$frameh = 86;

		$loyaltyFrame = new Imagick( "$this->dirframe/loyalty.png");
		$loyaltyFrame->thumbnailImage($framew, $frameh);
		$this->im->compositeImage($loyaltyFrame, imagick::COMPOSITE_OVER, $framex, $framey);

		// write power / toughness, centered
		$options = [
			'font' => $this->ptfont
			,'fontsize' => 44.0
			,'textx' => 660
			,'texty' => 970
			,'wmax' => 87
			,'text' => $this->q['powertoughness']
			,'textcolor' => $this->white
			,'textalign' => imagick::ALIGN_CENTER
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawRulesText() {
		// write rules and flavor texts

		cardfuncs_conjoined_cardframe_op($this->q, ['rulestext', 'flavortext'], ["cardfuncs_replaceMemtext", "cardfuncs_replaceCardTraits"]);

		$options = [
			'textfont' => $this->textfont
			,'boldfont' => $this->boldfont
			,'italicfont' => $this->italicfont
			,'bolditalicfont' => $this->bolditalicfont
			,'fontsize' => 36.0
			,'minfontsize' => 12.0
			,'textletterx' => 0
			,'textlettery' => 0
			,'textboxx' => 120
			,'textboxy' => 656
			,'textboxwidth' => 570
			,'textboxheight' => 296

			,'lcfontsize' => 36.0
			,'loyaltycostx' => 24
			,'loyaltycosty' => 652
			,'loyaltycostw' => 90
			,'loyaltycosth' => 76
			,'dividerx' => 44
			,'dividerw' => 647
		];

		cardfuncs_makeRulesboxPW($options, $this);
	}

	function drawSetIcon() {
		// draw set icon, with rarity colors filled

		$options = [
			'w' => 42
			,'h' => 42
			,'x' => 698
			,'y' => 594
		];

		cardfuncs_makeSetIcon($options, $this);
	}

	function drawTypeIcon() {
		// write card type icon
		$options = [
			'x' => 54
			,'y' => 46
			,'w' => 40
			,'h' => 40
		];

		$this->typeicon = cardfuncs_makeTypeIcon($options, $this);
	}

	function drawOverlay() {
        $options = [
            'ovx' => 29
            ,'ovy' => 31
            ,'ovw' => 691
            ,'ovh' => 977
        ];

		cardfuncs_makeOverlay($options, $this);
	}

	function createFinalImage() {
		// start creation of image
		$this->drawBackground();
		$this->drawBlend();
		$this->drawArt();
		$this->drawWatermark();
		$this->drawSetIcon();
		$this->drawManaCost();
		$this->drawTypeIcon();
		$this->drawCardName();
		$this->drawType();
		$this->drawPT();
		$this->drawRulesText();
		$this->drawArtistCreator();
		$this->drawOverlay();

		/* Output the image*/
		return $this->im;
	}
} // end class def

$cardframe = new CardFrame_Planeswalker($qarray);
?>
