<?php

// vogon vt frames

class CardFrame_VogonVT {

	////// CONSTANTS //////
	const WIDTH = 750;
	const HEIGHT = 1046;
	const BORDER = 32;
	const BOTTOMBAR = 66;
	const TINT = [
		"c" => "#DEB887"
		,"w" => "#F5F5DC"
		,"u" => "#99CCFF"
		,"b" => "#A0A0A0"
		,"r" => "#DC143C"
		,"g" => "#6B8E23"
		,"m" => "#FFD700"
	];

	////// PROPERTIES //////

	public $q; // array holding query variables
	public $dirframe, $dirmana, $dirfont; // directories to resources
	public $cardnamefont, $typefont, $manafont, $ptfont, $textfont, $italicfont;
	public $im, $mcircle, $textdivider; // images
	public $black, $white, $legendcolor, $clear; // ImagickPixel constants
	public $typetext; // final type line
	public $tints; // tint the entire card based on color provided
	public $artbevel, $artbevelpw; // bevel around art

	////// METHODS //////

	function __construct($qarray) {
		// data from query
		$this->q = $qarray;

		// file resources
		$this->dirframe = "frames/frame_" . $this->q['frame'];
		$this->textdivider = new Imagick( "$this->dirframe/horiz-divider.png" );
		$this->dirmana = "symbols/" . ((empty($this->q['symbol'])) ? "modern" : $this->q['symbol']);
		$this->mcircle = new Imagick( "$this->dirmana/mana_circle.png");
		$this->seticon = "$this->dirframe/seticon.png";
		$this->dirfont = $_SERVER['DOCUMENT_ROOT'] . "/fonts";
		$this->cardnamefont = "$this->dirfont/kelvinch-bold.ttf";
		$this->typefont = "$this->dirfont/kelvinch-bold.ttf";
		$this->manafont = "$this->dirfont/ubuntumono-regular.ttf";
		$this->ptfont = "$this->dirfont/crimson-regular.ttf";
		$this->boldfont = "$this->dirfont/crimson-bold.ttf";
		$this->textfont = "$this->dirfont/crimson-regular.ttf";
		$this->italicfont = "$this->dirfont/crimson-italic.ttf";
		$this->bolditalicfont = "$this->dirfont/crimson-bolditalic.ttf";
		$this->dyratio = 1.0;

		// drawing resources
		$this->black = new ImagickPixel("black");
		$this->white = new ImagickPixel("white");
		$this->legendcolor = new ImagickPixel("#9400D3"); // crimson #DC143C ; darkviolet #9400D3 ;
		$this->clear = new ImagickPixel("transparent");
		$this->halfclear = new ImagickPixel("#ffffff00");
		$this->draw = new ImagickDraw();
		$this->manadraw = new ImagickDraw();
		$this->artbevel = new Imagick( "$this->dirframe/art_bevel.png");
		$this->artbevelpw = new Imagick( "$this->dirframe/art_bevel_pw.png");
		$this->im = new Imagick();
		$this->im->newImage($this::WIDTH, $this::HEIGHT, $this->white, "png");

	}

	function drawBackground() {
		// background base

		global $ENUM_ARTIFACT;

		// colorless artifact
		if ($this->q['color'] == 'c' && substr($this->q['cardtype'], $ENUM_ARTIFACT, 1)=='1') {
			$this->q['cardback'] = 'a' . substr( $this->q['cardback'], 1);
		}

		$cardback = "$this->dirframe/" . $this->q['cardback'];
		if (file_exists($cardback)) {
			$bg = new Imagick($cardback);
            $this->im->compositeImage($bg, imagick::COMPOSITE_OVER, 0, 0);
        }

		// tint based on color
        /*
		$tint = new ImagickPixel( $this::TINT[ $this->q['color'] ]);
		$this->im->OpaquePaintImage( $this->white, $tint, 10, false );
//*/
	}

	function drawBlend() {
		// blending for multicolor and hybrid
		// only for Magic/Space genres

		if (in_array($this->q['genre'], array('magic','space','earth') )) {

			$this->blendoptions = cardfuncs_getblendoptions($this->q['manacost'], $this->q['extra'] . $this->q['rulestext']);
			$options = [
				'width' => $this::WIDTH
				,'height' => $this::HEIGHT
				,'black' => $this->black
				,'blendstartrate' => 0.1
				,'blendendrate' => 0.9
				,'contrast' => 2.0
				,'midtone' => 0.0
			];
			if ('q' == $this->q['color']) {
				$options['blendframemask'] = "blend_frame_mask.png";
			}
			if (3 == count($this->blendoptions["colors"])) {
				$options['blendgradientmask'] = 'blend_diag_3-';
			}
			$this->im = cardfuncs_renderblend($options, $this);
		}

	}

	function drawArt() {
		// art
		$options = [
			'artx' => 0
			,'arty' => 91
			,'artwidth' => 373
			,'artheight' => 816
		];
		cardfuncs_makeArtImage($options, $this);
	}

	function drawArtistCreator() {
		// artist and creator

		$options = [
			'font' => $this->typefont
			,'fontsize' => 18.0
			,'textx' => 375
			,'texty' => 1005
			,'wmax' => $this::WIDTH
			,'text' => "Illus. by " . $this->q['artist']
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
			,'textalign' => imagick::ALIGN_CENTER
		];

		cardfuncs_makeLinearText($options, $this);

		$options = [
			'font' => $this->typefont
			,'fontsize' => 18.0
			,'textx' => 375
			,'texty' => 1028
			,'wmax' => $this::WIDTH
			,'text' => "Created by " . $this->q['creator']
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
			,'textalign' => imagick::ALIGN_CENTER
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawManaCost() {
		// write mana cost, simplified, align right

		$options = [
			'font' => $this->manafont
			,'fontsize' => 70.0
			,'fontcolor' => $this->black
			,'manacostx' => 15
			,'manacosty' => 106
			,'manaiconwidth' => 64
			,'manaiconheight' => 64
			,'width' => 70
			,'height' => 904
			,'vertical' => true
		];

		cardfuncs_makeManaCost($options, $this);
	}

	function drawCardName() {
		// write card name

		$options = [
			'font' => $this->cardnamefont
			,'fontsize' => 48.0
			,'textx' => 375
			,'texty' => 42
			,'wmax' => 620
			,'text' => $this->q['cardname']
			,'textcolor' => substr($this->q['supertype'], 0, 1)=='1' ? $this->legendcolor : $this->black
			,'textalign' => imagick::ALIGN_CENTER
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawType() {
		// write card type

		$this->typetext = cardimage_gettypeline($this->q['supertype'], $this->q['cardtype'], $this->q['subtype'], $this->q['genre'], "— ");

		$options = [
			'font' => $this->typefont
			,'fontsize' => 30.0
			,'textx' => 338
			,'texty' => 948
			,'wmax' => 652
			,'text' => $this->typetext
			,'textcolor' => $this->black
			,'textalign' => imagick::ALIGN_CENTER
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawPT() {
		// write power toughness

		global $ENUM_PLANESWALKER, $ENUM_SCENE, $ENUM_CREATURE;

		// write power / toughness, centered

		if (substr($this->q['cardtype'], $ENUM_PLANESWALKER, 1)=='1' || (substr($this->q['cardtype'], $ENUM_SCENE, 1)=='1' && $this->q['powertoughness'] != '') ) {
			// is Planeswalker or Scene
			$framex = 625;
			$framey = 910;
			$framew = 120;
			$frameh = 76;

			$loyaltyFrame = new Imagick( "$this->dirframe/loyalty.png");
			$loyaltyFrame->thumbnailImage($framew, $frameh);
			$this->im->compositeImage($loyaltyFrame, imagick::COMPOSITE_OVER, $framex, $framey);

			$options = [
				'font' => $this->ptfont
				,'fontsize' => 40.0
				,'textx' => 685
				,'texty' => 947
				,'wmax' => 76
				,'text' => $this->q['powertoughness']
				,'textcolor' => $this->white
				,'textalign' => imagick::ALIGN_CENTER
			];

			cardfuncs_makeLinearText($options, $this);
		}
		elseif (preg_match("/;;/", $this->q['powertoughness'])) {
			// is Leveler

			$matches = preg_split("/;;/", $this->q['powertoughness']);
			$numtotal = count($matches);
			if ($this->q['flavortext'] != '') {
				$numtotal++;
			}
			$starty = 97;
			$strokeWidth = 5;
			$textboxheight = 816;

			$options = [
				'font' => $this->ptfont
				,'fontsize' =>  44.0
				,'angle' => 90.0
				,'textx' => 710
				,'wmax' => 816
				,'textcolor' => $this->black
				,'textalign' => imagick::ALIGN_CENTER
			];

			$abilh = $numtotal ? ($textboxheight - ($numtotal - 1) * $strokeWidth) / $numtotal : 0;
			$dy = $abilh + $strokeWidth;
			$starty -= $dy / 2;

			foreach ($matches as $i => $pt) {
				$options['text'] = $pt;
				$options['texty'] = $starty + $dy * ($i + 1);

				cardfuncs_makeLinearText($options, $this);
			}
		}
		elseif (substr($this->q['cardtype'], $ENUM_CREATURE, 1)=='1' || $this->q['powertoughness']!="") {
			// is Creature or has value in P/T field

			$options = [
				'font' => $this->ptfont
				,'fontsize' => 44.0
				,'textx' => 710
				,'texty' => 536
				,'wmax' => 879
				,'angle' => 90.0
				,'text' => $this->q['powertoughness']
				,'textcolor' => $this->black
				,'textalign' => imagick::ALIGN_CENTER
			];

			cardfuncs_makeLinearText($options, $this);
		}
	}

	function drawWatermark() {
		// watermark
        $options = [
            'wmx' => 524
            ,'wmy' => 499
            ,'wmw' => 284
            ,'wmh' => 808
        ];

		cardfuncs_makeWatermark($options, $this);
	}

	function drawRulesText() {
		// write rules, flavor, extra texts

		cardfuncs_conjoined_cardframe_op($this->q, ['extra', 'rulestext', 'flavortext'], ["cardfuncs_replaceMemtext", "cardfuncs_replaceCardTraits"]);

		global $ENUM_PLANESWALKER, $ENUM_SCENE;

		// extra text merged into rules
		$this->q['rulestext'] = $this->q['extra'] . "\n" . $this->q['rulestext'];

		// actual rules and flavor text

		if (substr($this->q['cardtype'], $ENUM_PLANESWALKER, 1)=='1') {
			// is planeswalker

			$options = [
				'textfont' => $this->textfont
				,'boldfont' => $this->boldfont
				,'italicfont' => $this->italicfont
				,'bolditalicfont' => $this->bolditalicfont
				,'fontsize' => 50.0
				,'minfontsize' => 12.0
				,'textcolor' => $this->black
				,'textletterx' => 0
				,'textlettery' => 0
				,'textboxx' => 381
				,'textboxy' => 91
				,'textboxwidth' => 256
				,'textboxheight' => 816
				,'dyratio' => $this->dyratio

				,'lcfontsize' => 38.0
				,'loyaltycostx' => 625
				,'loyaltycosty' => 518
				,'loyaltycostw' => 120
				,'loyaltycosth' => 76

				,'dividerx' => 375
				,'dividerw' => 375
			];

			cardfuncs_makeRulesboxPW($options, $this);
		}
		elseif ((substr($this->q['cardtype'], $ENUM_SCENE, 1)=='1' && $this->q['powertoughness'] != '')) {
			// is scene and has loyalty counters

			$options = [
				'textfont' => $this->textfont
				,'boldfont' => $this->boldfont
				,'italicfont' => $this->italicfont
				,'bolditalicfont' => $this->bolditalicfont
				,'fontsize' => 50.0
				,'minfontsize' => 12.0
				,'textletterx' => 0
				,'textlettery' => 0
				,'textboxx' => 378
				,'textboxy' => 91
				,'textboxwidth' => 292
				,'textboxheight' => 816
				,'dyratio' => $this->dyratio

				,'lcfontsize' => 38.0
				,'loyaltycostx' => 5
				,'loyaltycosty' => 518
				,'loyaltycostw' => 120
				,'loyaltycosth' => 76

				,'dividerx' => 0
				,'dividerw' => 680
			];

			cardfuncs_makeRulesboxPW($options, $this);
		}
		elseif (preg_match("/;;/", $this->q['powertoughness'])) {
			// is Leveler

			$options = [
				'textfont' => $this->textfont
				,'boldfont' => $this->boldfont
				,'italicfont' => $this->italicfont
				,'bolditalicfont' => $this->bolditalicfont
				,'fontsize' => 66.0
				,'minfontsize' => 12.0
				,'textcolor' => $this->black
				,'textletterx' => 0
				,'textlettery' => 0
				,'textboxx' => 381
				,'textboxy' => 91
				,'textboxwidth' => 286
				,'textboxheight' => 816
				,'dyratio' => $this->dyratio

				,'levelfontsize' => 44.0
				,'levelcostx' => 751
				,'levelcosty' => 580
				,'levelcostw' => 127
				,'levelcosth' => 88
				,'linex' => 375
				,'linew' => 375

				,'isshading' => false
			];

			cardfuncs_makeRulesboxLeveler($options, $this);
		}
		else {
			// regular card

			$options = [
				'textfont' => $this->textfont
				,'boldfont' => $this->boldfont
				,'italicfont' => $this->italicfont
				,'bolditalicfont' => $this->bolditalicfont
				,'fontsize' => 40.0
				,'minfontsize' => 12.0
				,'textletterx' => 0
				,'textlettery' => 0
				,'textboxx' => 381
				,'textboxy' => 91
				,'textboxwidth' => 286
				,'textboxheight' => 816
				,'rulestext' => $this->q['rulestext']
				,'flavortext' => $this->q['flavortext']
				,'dyratio' => $this->dyratio
			];

			cardfuncs_makeRulesbox($options, $this);
		}
	}

	function drawSetIcon() {
		// draw set icon, with rarity colors filled

		$options = [
			'w' => 48
			,'h' => 48
			,'x' => 741
			,'y' => 992
		];
		cardfuncs_makeSetIcon($options, $this);
	}

	function drawTypeIcon() {
		// write card type icon
		$options = [
			'x' => 15,
			'y' => 17,
			'w' => 54,
			'h' => 54,
			'genre' => $this->q['genre'],
			'cardtype' => $this->q['cardtype']
		];

		cardfuncs_makeTypeIcon($options, $this);
	}

	function drawOverlay() {
        $options = [
            'ovx' => 0
            ,'ovy' => 0
            ,'ovw' => 750
            ,'ovh' => 987
        ];

		cardfuncs_makeOverlay($options, $this);
	}

	function createFinalImage() {
		// start creation of image
		$this->drawBackground();
		$this->drawBlend();
		$this->drawArt();
		$this->drawWatermark();
		$this->drawSetIcon();
		$this->drawCardName();
		$this->drawTypeIcon();
		$this->drawType();
		$this->drawManaCost();
		$this->drawPT();
		$this->drawRulesText();
		$this->drawArtistCreator();
		$this->drawOverlay();

		/* Output the image*/
		return $this->im;
	}
} // end class def

$cardframe = new CardFrame_VogonVT($qarray);
?>
