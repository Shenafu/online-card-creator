<?php

// CBG frames

class CardFrame_Cbg {

	////// CONSTANTS //////
	const WIDTH = 500;
	const HEIGHT = 697;

	////// PROPERTIES //////

	public $dirframe, $dirmana, $dirfont; // directories to resources
	public $q; // array holding query variables
	public $cardnamefont, $typefont, $manafont, $ptfont, $textfont, $italicfont;
	public $im, $mcircle, $textdivider; // images
	public $black, $white, $clear; // ImagickPixel constants
	public $typetext; // final type line

	////// METHODS //////

	function __construct($qarray) {
		// data from query
		$this->q = $qarray;

		$this->typetext = cardimage_gettypeline($this->q['supertype'], $this->q['cardtype'], $this->q['subtype'], $this->q['genre'], "— ");

		if (preg_match('/artifact/i', $this->typetext) && preg_match('/^c/', $this->q['cardback'])) {
			$this->q['cardback'] = "acard.jpg";
		}

      $this->q['rulestext'] = $this->q['extra'] . "\n". $this->q['rulestext'];

		// file resources
		$this->dirframe = "frames/frame_" . $this->q['frame'];
		$this->textdivider = new Imagick( "$this->dirframe/horiz-divider.png" );
		$this->dirmana = "symbols/" . ((empty($this->q['symbol'])) ? "modern" : $this->q['symbol']);
		$this->mcircle = new Imagick( "$this->dirmana/mana_circle.png");
		$this->seticon = "$this->dirframe/seticon.png";
		$this->dirfont = $_SERVER['DOCUMENT_ROOT'] . "/fonts";
		$this->cardnamefont = "$this->dirfont/kelvinch-bold.ttf";
		$this->typefont = "$this->dirfont/kelvinch-regular.ttf";
		$this->manafont = "$this->dirfont/ubuntumono-regular.ttf";
		$this->ptfont = "$this->dirfont/crimson-regular.ttf";
		$this->boldfont = "$this->dirfont/crimson-bold.ttf";
		$this->textfont = "$this->dirfont/crimson-regular.ttf";
		$this->italicfont = "$this->dirfont/crimson-italic.ttf";
		$this->bolditalicfont = "$this->dirfont/crimson-bolditalic.ttf";
		$this->dyratio = 1.0;

		// drawing resources
		$this->black = new ImagickPixel("black");
		$this->white = new ImagickPixel("white");
		$this->gold = new ImagickPixel("gold");
		$this->clear = new ImagickPixel("transparent");
		$this->draw = new ImagickDraw();
		$this->manadraw = new ImagickDraw();
		$this->im = new Imagick();
		$this->im->newImage($this::WIDTH, $this::HEIGHT, $this->white, "png");
	}

	function drawBackground() {
		// background base

        $cardback = "$this->dirframe/" . $this->q['cardback'];
		if (file_exists($cardback)) {
			$bg = new Imagick($cardback);
            $this->im->compositeImage($bg, imagick::COMPOSITE_OVER, 0, 0);
        }
	}

	function drawBlend() {
		// blending for multicolor and hybrid

		$this->blendoptions = cardfuncs_getblendoptions($this->q['manacost'], $this->q['rulestext']);
		$options = [
			'width' => $this::WIDTH
			,'height' => $this::HEIGHT
			,'black' => $this->black
			,'blendstartrate' => 0.4
			,'blendendrate' => 0.6
			,'contrast' => 20
			,'midtone' => 0.166
		];
		if ($this->q['color']=='q') {
			$options['blendframemask'] = "blend_frame_mask.png";
		}
		$this->im = cardfuncs_renderblend($options, $this);
	}

	function drawArt() {
		// art

		$options = [
			'artx' => 42
			,'arty' => 81
			,'artwidth' => 415
			,'artheight' => 379
		];

		cardfuncs_makeArtImage($options, $this);

		$cb = preg_replace('/^(.+)card(.+)/', '$1', $this->q['cardback']);
        $overlay = $this->dirframe . "/$cb" . "textbox.png";
        if (file_exists($overlay)) {
            $overlay = new Imagick($overlay);
            $overlay->thumbnailImage($options['artwidth'], $options['artheight']);
            $this->im->compositeImage($overlay, imagick::COMPOSITE_OVER, $options['artx'], $options['arty']);
        }
	}

	function drawArtistCreator() {
		// artist and creator

		$options = [
			'font' => $this->typefont
			,'fontsize' => 13.0
			,'textx' => 76
			,'wmax' => 121
			,'textcolor' => $this->black
			,'shadowcolor' => $this->gold
			,'textalign' => imagick::ALIGN_LEFT
		];
		$options['texty'] = 651;
		$options['text'] = "Illus. by";
		cardfuncs_makeLinearText($options, $this);
		$options['texty'] = 665;
		$options['text'] = $this->q['artist'];
		cardfuncs_makeLinearText($options, $this);

		$options['textx'] = 428;
		$options['textalign'] = imagick::ALIGN_RIGHT;

		$options['texty'] = 651;
		$options['text'] = "Created by";
		cardfuncs_makeLinearText($options, $this);
		$options['texty'] = 665;
		$options['text'] = $this->q['creator'];
		cardfuncs_makeLinearText($options, $this);
	}

	function drawWatermark() {
		// watermark

		$options = [
			'wmx' => 250
			,'wmy' => 545
			,'wmw' => 393
			,'wmh' => 158
		];

		cardfuncs_makeWatermark($options, $this);
	}

	function drawManaCost() {
		// write mana cost, simplified, align right

		$options = [
			'font' => $this->manafont
			,'fontsize' => 21.0
			,'fontcolor' => $this->black
			,'manacostx' => 250
			,'manacosty' => 97
			,'manaiconwidth' => 21
			,'manaiconheight' => 21
			,'width' => 375
			,'height' => 21
			,'manaalign' => 'CENTER'
		];

		cardfuncs_makeManaCost($options, $this);
	}

	function drawCardName() {
		// write card name

		$options = [
			'font' => $this->cardnamefont
			,'fontsize' => 24.0
			,'textx' => 250
			,'texty' => 55
			,'wmax' => 348
			,'text' => $this->q['cardname']
			,'textcolor' => $this->black
			,'textalign' => imagick::ALIGN_CENTER
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawType() {
		// write card type

		$options = [
			'font' => $this->typefont
			,'fontsize' => 14.0
			,'textx' => 250
			,'texty' => 452
			,'wmax' => 264
			,'text' => mb_strtoupper($this->typetext)
			,'textcolor' => $this->black
			,'textalign' => imagick::ALIGN_CENTER
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawPT() {
		// write power toughness

		global $ENUM_CREATURE;

		$ptimgx = 201;
		$ptimgy = 619;

		// draw PT frame
		if (substr($this->q['cardtype'], $ENUM_CREATURE, 1)=='1' || ($this->q['powertoughness'] != "")) {
			$ptimage = new Imagick("$this->dirframe/pt.png");
			$this->im->compositeImage($ptimage, imagick::COMPOSITE_OVER, $ptimgx, $ptimgy);
		}

		// write power / toughness, centered
		$options = [
			'font' => $this->ptfont
			,'fontsize' => 29.0
			,'textx' => 253
			,'texty' => 643
			,'wmax' => 77
			,'text' => $this->q['powertoughness']
			,'textcolor' => $this->black
			,'textalign' => imagick::ALIGN_CENTER
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawRulesText() {
		// write rules and flavor texts

		cardfuncs_conjoined_cardframe_op($this->q, ['rulestext', 'flavortext'], ["cardfuncs_replaceMemtext", "cardfuncs_replaceCardTraits"]);

		$options = [
			'textfont' => $this->textfont
			,'boldfont' => $this->boldfont
			,'italicfont' => $this->italicfont
			,'bolditalicfont' => $this->bolditalicfont
			,'fontsize' => 33.0
			,'minfontsize' => 6.0
			,'textletterx' => 0
			,'textlettery' => 0
			,'textboxx' => 51
			,'textboxy' => 468
			,'textboxwidth' => 393
			,'textboxheight' => 155
			,'rulestext' => $this->q['rulestext']
			,'flavortext' => $this->q['flavortext']
		];

		cardfuncs_makeRulesbox($options, $this);
	}

	function drawSetIcon() {
		// draw set icon, with rarity colors filled

		$options = [
			'w' => 21
			,'h' => 21
			,'x' => 452
			,'y' => 647
			,'align' => Imagick::ALIGN_LEFT
			,'keepratio' => false
		];

		cardfuncs_makeSetIcon($options, $this);
	}

	function drawTypeIcon() {
		// write card type icon
		$options = [
			'x' => 74
			,'y' => 40
			,'w' => 30
			,'h' => 30
		];

		$this->typeicon = cardfuncs_makeTypeIcon($options, $this);
	}

	function drawOverlay() {
        $options = [
            'ovx' => 22
            ,'ovy' => 22
            ,'ovw' => 454
            ,'ovh' => 651
        ];

		cardfuncs_makeOverlay($options, $this);
	}

	function createFinalImage() {
		// start creation of image
		$this->drawBackground();
		$this->drawArt();
		$this->drawBlend();
		$this->drawWatermark();
		$this->drawSetIcon();
		$this->drawTypeIcon();
		$this->drawCardName();
		$this->drawManaCost();
		$this->drawType();
		$this->drawPT();
		$this->drawRulesText();
		$this->drawArtistCreator();
		$this->drawOverlay();

		/* Output the image*/
		return $this->im;
	}
} // end class def

$cardframe = new CardFrame_Cbg($qarray);
?>
