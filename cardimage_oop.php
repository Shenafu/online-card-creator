<?php
error_reporting(E_ALL); ini_set('display_errors', 1);

include_once('cclog_oop.php');

$SAVEFILEPNG = "customsave_oop.png";
$SAVEFILEGIF = "customsave_oop.gif";
$SAVEFILELOG = "customsave_oop.log";

function setImageMetaData(&$image, $cardname, $creator) {
	//$image->setimageproperty("URL", "http://ieants.cc/magic/cc_oop/$creator/$cardname");
	//$image->setimageattribute("comment", "http://ieants.cc/magic/cc_oop/$creator/$cardname");
	//$image->setregistry("URL", "http://ieants.cc/magic/cc_oop/$creator/$cardname");
	//$image->commentimage("http://ieants.cc/magic/cc_oop/$creator/$cardname");
	//$image->comment("http://ieants.cc/magic/cc_oop/$creator/$cardname");

	$image->setimageprofile("URL", "http://ieants.cc/magic/cc_oop/$creator/$cardname");

}

//BEGIN
$query = $_SERVER['QUERY_STRING'];
parse_str($query, $qarray);

include_once('cardfuncs_oop.php');

// replace certain strings, e.g. unsafe URLS, cardname, aliases
sanitizeQuery($qarray);

// blank card
if ('' == trim($qarray['cardname']) && '' == trim($qarray['creator'])) {
	header('Content-Type:image/jpeg');
	readfile("frames/frame_classicshift/ccard.jpg");
	exit();
}

$filename = 'cardframe_' . $qarray['frame'] . '_oop.php';
include_once($filename);
if (isset($cardframe)) {
	$im = $cardframe->createFinalImage();
}
else {
	// legacy non-OOP
	$im = cardimage_createFinalImage(http_build_query($qarray));
}

// customsave.png update periodically
$timediff = 1; //in seconds, eg 1 minutes = 60, 1 hour = 3600
if (! file_exists($SAVEFILEPNG) || $timediff <= filemtime($SAVEFILELOG) - filemtime($SAVEFILEPNG)) {
	saveFinalImage($im);
}

if (is_object($im)) {
	// is Imagick
	header('Content-Type: image/png');
	header('Content-Disposition: inline; filename="' . $qarray['cardname'] . ' by ' . $qarray['creator'] . '.png"');
	$out = sendFinalImage($im);
	//setImageMetaData($out, $qarray['cardname'], $qarray['creator']);

	echo $out;
} else {
	// is GD
	header('Content-Type: image/png');
	imagepng($im);
}
?>
