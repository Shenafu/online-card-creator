<?php

// vogon hd frames

class CardFrame_VogonHD {

	////// CONSTANTS //////
	const WIDTH = 750;
	const HEIGHT = 1046;
	const BORDER = 32;
	const BOTTOMBAR = 66;
	const TINT = [
		"c" => "#DEB887"
		,"w" => "#F5F5DC"
		,"u" => "#99CCFF"
		,"b" => "#A0A0A0"
		,"r" => "#DC143C"
		,"g" => "#6B8E23"
		,"m" => "#FFD700"
	];

	////// PROPERTIES //////

	public $q; // array holding query variables
	public $dirframe, $dirmana, $dirfont; // directories to resources
	public $cardnamefont, $typefont, $manafont, $ptfont, $textfont, $italicfont;
	public $im, $mcircle, $textdivider; // images
	public $black, $white, $clear; // ImagickPixel constants
	public $typetext; // final type line
	public $tints; // tint the entire card based on color provided

	////// METHODS //////

	function __construct($qarray) {
		// data from query
		$this->q = $qarray;

		// file resources
		$this->dirframe = "frames/frame_" . $this->q['frame'];
		$this->textdivider = new Imagick( "$this->dirframe/horiz-divider.png" );
		$this->dirmana = "symbols/" . ((empty($this->q['symbol'])) ? "modern" : $this->q['symbol']);
		$this->mcircle = new Imagick( "$this->dirmana/mana_circle.png");
		$this->seticon = "$this->dirframe/seticon.png";
		$this->dirfont = "../fonts";
		$this->cardnamefont = "$this->dirfont/fondamento-regular.ttf";
		$this->typefont = "$this->dirfont/fondamento-regular.ttf";
		$this->manafont = "$this->dirfont/ubuntumono-regular.ttf";
		$this->ptfont = "$this->dirfont/amiri-regular.ttf";
		$this->textfont = "$this->dirfont/amiri-regular.ttf";
		$this->italicfont = "$this->dirfont/amiri-italic.ttf";

		// drawing resources
		$this->black = new ImagickPixel("black");
		$this->white = new ImagickPixel("white");
		$this->clear = new ImagickPixel("transparent");
		$this->draw = new ImagickDraw();
		$this->manadraw = new ImagickDraw();
		$this->im = new Imagick();
		$this->im->newImage($this::WIDTH, $this::HEIGHT, $this->white, "png");

	}

	function drawBackground() {
		// background base
		
		global $ENUM_ARTIFACT;
		
		// colorless artifact
		if ($this->q['color'] == 'c' && substr($this->q['cardtype'], $ENUM_ARTIFACT, 1)=='1') {
			$this->q['cardback'] = 'a' . substr( $this->q['cardback'], 1);
		}

		$cardback = "$this->dirframe/" . $this->q['cardback'];
		if (file_exists($cardback)) {
			$bg = new Imagick($cardback);
		}
		else {
			$bg = new Imagick("$this->dirframe/cardback.png");
		}
		$this->im->compositeImage($bg, imagick::COMPOSITE_OVER, 0, 0);
		
		// tint based on color
		$tint = new ImagickPixel( $this::TINT[ $this->q['color'] ]);
		$this->im->OpaquePaintImage( $this->white, $tint, 10, false );		
	}

	function drawBlend() {
		// blending for multicolor and hybrid

		$this->blendoptions = cardfuncs_getblendoptions($this->q['manacost'], $this->q['rulestext']);
		$options = [
			'width' => $this::WIDTH
			,'height' => $this::HEIGHT
			,'black' => $this->black
			,'blendstartrate' => 0.1
			,'blendendrate' => 0.9
			,'contrast' => 2.0
			,'midtone' => 0.0
		];
		$this->im = cardfuncs_renderblend($options, $this);
	}

	function drawArt() {
		// art
		
		global $ENUM_PLANESWALKER;
        
		if (substr($this->q['cardtype'], $ENUM_PLANESWALKER, 1)=='1') {
			// is Planeswalker
            $options = [
                'artx' => 92
                ,'arty' => 92
                ,'artwidth' => 566
                ,'artheight' => 880
                ,'mask' => $this->dirframe . '/mask_art.png'          
            ];
            cardfuncs_makeArtImage($options, $this);            
        }
        else {
            $options = [
                'artx' => 92
                ,'arty' => 92
                ,'artwidth' => 566
                ,'artheight' => 484
            ];
            cardfuncs_makeArtImage($options, $this);
        }
	}

	function drawArtistCreator() {
		// artist and creator

		$options = [
			'font' => $this->cardnamefont
			,'fontsize' => 21.0
			,'textx' => 375
			,'texty' => 996
			,'wmax' => $this::WIDTH
			,'text' => "Illus. by " . $this->q['artist']
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
			,'textalign' => imagick::ALIGN_CENTER
		];

		cardfuncs_makeLinearText($options, $this);
		
		$options = [
			'font' => $this->cardnamefont
			,'fontsize' => 21.0
			,'textx' => 375
			,'texty' => 1024
			,'wmax' => $this::WIDTH
			,'text' => "Created by " . $this->q['creator']
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
			,'textalign' => imagick::ALIGN_CENTER
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawManaCost() {
		// write mana cost, simplified, align right

		$options = [
			'font' => $this->manafont
			,'fontsize' => 40.0
			,'fontcolor' => $this->black
			,'manacostx' => 45
			,'manacosty' => 98
			,'manaiconwidth' => 36
			,'manaiconheight' => 36
			,'width' => 36
			,'height' => 934
			,'vertical' => true
		];
		
		cardfuncs_makeManaCost($options, $this);
	}

	function drawCardName() {
		// write card name

		$options = [
			'font' => $this->cardnamefont
			,'fontsize' => 36.0
			,'textx' => 375
			,'texty' => 62
			,'wmax' => 571
			,'text' => $this->q['cardname']
			,'textcolor' => $this->black
			,'textalign' => imagick::ALIGN_CENTER
		];
		
		cardfuncs_makeLinearText($options, $this);
	}

	function drawTypeIcon() {
		// write card type icon
		$options = [
			'x' => 46,
			'y' => 46,
			'w' => 36,
			'h' => 36		
		];
		
		cardfuncs_makeTypeIcon($options, $this);	
	}
	
	function drawType() {
		// write card type
		
		$this->typetext = cardimage_gettypeline($this->q['supertype'], $this->q['cardtype'], $this->q['subtype'], $this->q['genre'], "— ");

		$options = [
			'font' => $this->typefont
			,'fontsize' => 30.0
			,'textx' => 688
			,'texty' => 533
			,'wmax' => 870
			,'angle' => 90.0
			,'text' => $this->typetext
			,'textcolor' => $this->black
			,'textalign' => imagick::ALIGN_CENTER
		];
		
		cardfuncs_makeLinearText($options, $this);
	}

	function drawPT() {
		// write power toughness
		
		global $ENUM_PLANESWALKER, $ENUM_SCENE, $ENUM_CREATURE;

		// write power / toughness, centered
		
		if (substr($this->q['cardtype'], $ENUM_PLANESWALKER, 1)=='1' || substr($this->q['cardtype'], $ENUM_SCENE, 1)=='1') {
			// is Planeswalker or Scene
			$framex = 5;
			$framey = 966;
			$framew = 135;
			$frameh = 86;
			
			$loyaltyFrame = new Imagick( "$this->dirframe/loyalty.png");
			$loyaltyFrame->thumbnailImage($framew, $frameh);
			$this->im->compositeImage($loyaltyFrame, imagick::COMPOSITE_OVER, $framex, $framey);

			$options = [
				'font' => $this->ptfont
				,'fontsize' => 44.0
				,'textx' => 72
				,'texty' => 1008
				,'wmax' => 85
				,'text' => $this->q['powertoughness']
				,'textcolor' => $this->white
				,'textalign' => imagick::ALIGN_CENTER
			];
		
			cardfuncs_makeLinearText($options, $this);
		}
		elseif (strpos($this->q['powertoughness'], ';;')>=0) {
			// is Leveler
            
            $matches = preg_split("/;;/", $this->q['powertoughness']);
			$numtotal = count($matches);
			$starty = 586;
			$strokeWidth = 5;
			$textboxheight = 381;

			$options = [
				'font' => $this->ptfont
				,'fontsize' =>  44.0
				,'angle' => 270.0
				,'textx' => 66
				,'wmax' => 360
				,'textcolor' => $this->black
				,'textalign' => imagick::ALIGN_CENTER
			];

			$abilh = $numtotal ? ($textboxheight - ($numtotal - 1) * $strokeWidth) / $numtotal : 0;
			$dy = $abilh + $strokeWidth;
			$starty -= $dy / 2;

			foreach ($matches as $i => $pt) {
				$options['text'] = $pt;
				$options['texty'] = $starty + $dy * ($i + 1);
				
				cardfuncs_makeLinearText($options, $this);
			}
		}		
		elseif (substr($this->q['cardtype'], $ENUM_CREATURE, 1)=='1' || $this->q['powertoughness']!="") {
			// is Creature or has value in P/T field

			$options = [
				'font' => $this->ptfont
				,'fontsize' => 44.0
				,'textx' => 65
				,'texty' => 743
				,'wmax' => 450
				,'angle' => 270.0
				,'text' => $this->q['powertoughness']
				,'textcolor' => $this->black
				,'textalign' => imagick::ALIGN_CENTER
			];
		
			cardfuncs_makeLinearText($options, $this);
		}
	}

	function drawWatermark() {
		// watermark

		global $ENUM_PLANESWALKER;
        
		if (substr($this->q['cardtype'], $ENUM_PLANESWALKER, 1)=='1') {
            $options = [
                'wmx' => 375
                ,'wmy' => 775
                ,'wmw' => 485
                ,'wmh' => 350
            ];
        }
        else {
            $options = [
                'wmx' => 375
                ,'wmy' => 775
                ,'wmw' => 485
                ,'wmh' => 350
            ];
        }

		cardfuncs_makeWatermark($options, $this);
	}

	function drawRulesText() {
		// write rules and flavor texts

		global $ENUM_PLANESWALKER, $ENUM_SCENE;

		// write rules text and flavor text

		if (substr($this->q['cardtype'], $ENUM_PLANESWALKER, 1)=='1') {
			
			$options = [
				'textfont' => $this->textfont
				,'italicfont' => $this->italicfont
				,'fontsize' => 42.0
				,'minfontsize' => 12.0
				,'textcolor' => $this->black
				,'textletterx' => 0
				,'textlettery' => 0
				,'textboxx' => 140
				,'textboxy' => 580
				,'textboxwidth' => 507
				,'textboxheight' => 393
				
				,'lcfontsize' => 44.0
				,'loyaltycostx' => 5
				,'loyaltycosty' => 518
				,'loyaltycostw' => 135
				,'loyaltycosth' => 86
			];
	
			cardfuncs_makeRulesboxPW($options, $this);
		}
		elseif (substr($this->q['cardtype'], $ENUM_SCENE, 1)=='1') {
			$options = [
				'textfont' => $this->textfont
				,'italicfont' => $this->italicfont
				,'fontsize' => 50.0
				,'minfontsize' => 12.0
				,'textletterx' => 0
				,'textlettery' => 0
				,'textboxx' => 140
				,'textboxy' => 586
				,'textboxwidth' => 507
				,'textboxheight' => 381
				
				,'lcfontsize' => 44.0
				,'loyaltycostx' => 5
				,'loyaltycosty' => 518
				,'loyaltycostw' => 135
				,'loyaltycosth' => 86
			];
	
			cardfuncs_makeRulesboxPW($options, $this);
		}
		elseif (preg_match("/;;/", $this->q['powertoughness'])) {
			// is Leveler
			
			$options = [
				'textfont' => $this->textfont
				,'italicfont' => $this->italicfont
				,'fontsize' => 66.0
				,'minfontsize' => 12.0
				,'textcolor' => $this->black
				,'textletterx' => 0
				,'textlettery' => 0
				,'textboxx' => 102
				,'textboxy' => 586
				,'textboxwidth' => 545
				,'textboxheight' => 381
				
				,'levelfontsize' => 44.0
				,'levelcostx' => 88
				,'levelcosty' => 518
				,'levelcostw' => 127
				,'levelcosth' => 88
				,'linex' => 45
			];
			
			cardfuncs_makeRulesboxLeveler($options, $this);
		}
		else {
			// regular card

			$options = [
				'textfont' => $this->textfont
				,'italicfont' => $this->italicfont
				,'fontsize' => 50.0
				,'minfontsize' => 12.0
				,'textletterx' => 0
				,'textlettery' => 0
				,'textboxx' => 102
				,'textboxy' => 586
				,'textboxwidth' => 545
				,'textboxheight' => 381
				,'rulestext' => $this->q['rulestext']
				,'flavortext' => $this->q['flavortext']
			];
		
			cardfuncs_makeRulesbox($options, $this);
		}
	}

	function drawSetIcon() {
		// draw set icon, with rarity colors filled
		
		$options = [
			'w' => 54
			,'h' => 54
			,'x' => 688
			,'y' => 984
		];

		cardfuncs_makeSetIcon($options, $this);
	}

	function createFinalImage() {
		// start creation of image
		$this->drawBackground();
		$this->drawBlend();
		$this->drawArt();
		$this->drawWatermark();
		$this->drawSetIcon();
		$this->drawCardName();
		$this->drawTypeIcon();
		$this->drawType();
		$this->drawManaCost();
		$this->drawPT();
		$this->drawRulesText();
		$this->drawArtistCreator();

		/* Output the image*/
		return $this->im;
	}
} // end class def

$cardframe = new CardFrame_VogonHD($qarray);
?>
