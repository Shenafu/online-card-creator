<?php
// Magic card creator
// using Imagick library

error_reporting(E_ALL);

header('Cache-Control: no-cache, no-store, must-revalidate');

include_once('cardretrieve.php');

// fix characters for mod_rewrite

function fix_rewrite_encode($string) {
    $string = rawurlencode(rawurlencode($string));

    return $string;
}

function fix_rewrite_decode($string) {
    $string = rawurldecode(rawurldecode($string));

    return $string;
}

function get_redirect_url($cardname, $creator) {
	return "/magic/cc/$creator/$cardname";
}
function get_canon_url($cardname, $creator) {
	return "https://ieants.cc/magic/cc/$creator/$cardname";
}

function get_page_id($cardname, $creator) {
	return "/magic/cc/$creator/$cardname";
}

$serverURI = explode('/', $_SERVER['REQUEST_URI']);

if (isset($serverURI[3]) || isset($serverURI[4])) {
	$creator = isset($serverURI[3]) ? fix_rewrite_decode($serverURI[3]) : ' ';
	$creator = fix_creator($creator);
	$cardname = isset($serverURI[4]) ? fix_rewrite_decode($serverURI[4]) : '';
}
else {
	@parse_str($_SERVER['QUERY_STRING']);
	if (!isset($cardname)) {
		$cardname = "";
	}
	$creator = fix_creator($creator || '');
	header('Location: ' . get_redirect_url($cardname, $creator));
}

$urlquery = retrieve(rawurldecode($cardname), rawurldecode($creator));

@parse_str ($urlquery);

if (!isset($genre)) {
	$genre = "magic";
}
if (!isset($frame)) {
	$frame = "classicshift";
}
if (!isset($color)) {
	$color = "c";
}
if (!isset($border)) {
	$border = "";
}
if (!isset($symbol)) {
	$symbol = "";
}
if (!isset($cardname)) {
	$cardname = "";
}
if (!isset($manacost)) {
	$manacost = "";
}
if (!isset($cardtype)) {
	$cardtype = "";
}
$cardtype = str_pad($cardtype, 15, '0');
if (!isset($supertype)) {
	$supertype = "";
}
$supertype = str_pad($supertype, 10, '0');
if (!isset($subtype)) {
	$subtype = "";
}
if (!isset($powertoughness)) {
	$powertoughness = "";
}
if (!isset($extra)) {
	$extra = "";
}
if (!isset($rulestext)) {
	$rulestext = "";
}
if (!isset($flavortext)) {
	$flavortext = "";
}
if (!isset($rarity)) {
	$rarity = "1";
}
if (!isset($seticonurl)) {
	$seticonurl = "";
}
if (!isset($typeiconurl)) {
	$typeiconurl = "";
}
if (!isset($wmurl)) {
	$wmurl = "";
}
if (!isset($overlayurl)) {
	$overlayurl = "";
}
if (!isset($arturl)) {
	$arturl = "";
}
if (!isset($artist)) {
	$artist = "";
}
if (!isset($custommana)) {
	$custommana = "";
}
if (!isset($customframe)) {
	$customframe = "";
}
$creator = fix_creator($creator);

$imgcardname = fix_rewrite_encode($cardname);
$imgcreator = fix_rewrite_encode($creator);

$parentdir = preg_replace("/^(\/.*)\/cc.*?\.php$/", '$1', $_SERVER['SCRIPT_NAME']);
$imgsrc = "$parentdir/ccimg/$imgcreator/$imgcardname.png";
$imgqr = "/imgd/qr/text=http://" . $_SERVER['HTTP_HOST'] . $imgsrc;

?>
<!doctype html>
<html class="shenafu" id="cardcreatorpage">
<head>
	<title><?php echo "$cardname" . " by " . "$creator"; ?> @ Shenafu's Card Creator</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="icon" type="image/jpg" href="<?php echo $imgsrc; ?>"/>
<style>
@font-face {
  font-family: 'Albertus';
  src: url('/fonts/albertus_medium-regular.ttf')  format('truetype')
}
@font-face {
  font-family: 'Crimson';
  src: url('/fonts/crimson-regular.ttf')  format('truetype')
}

html {background-color: #f0e68c; /* khaki */}
body * {font: 14pt 'Crimson', sans-serif; }
h1, h1 * {font: 21pt 'Albertus', cursive; }
h2 {font: small-caps 21pt 'Albertus', cursive; }
h3 {text-align: center; color: blue; margin: 2px; font: 16pt 'Albertus', cursive; text-decoration: underline; }
pre { white-space: pre-wrap; }
#note {font-style: italic !important; }
input[type=button], input[type=reset] {margin: 10px; text-align: center; }

#ccform {display: flex; flex-flow: row wrap; }
#ccform > * {display: inline-flex; padding: 12px; }

#imagediv {flex-flow: column; }

#inputtypes {flex-flow: column; }
#inputtypes > div, .flexrow > div { display: flex; flex-flow: row; align-items: center; }
#inputtypes h3 {width: 8em; text-align: center; }
#inputtypes input, #inputtypes textarea, #inputtypes select, .flexrow select { width: 288px; }

@media (min-width: 1271px) {
	#selecttypes {flex-flow: column wrap; }
}
@media (max-width: 1270px) {
	#selecttypes {flex-flow: row wrap; }
}

.flexcolumn > div {display: inline-flex; flex-flow: column; margin: 5px; }
#selecttypes h3 {text-align: left; padding-left: 0.8em; width: 4em; }

#buttontypes > * {height: 2em; }

</style>
<script src="/lib/markdown.js"></script>
<script src="/lib/dojo/dojo-release-1.15.0/dojo/dojo.js" data-dojo-config="async: true"></script>
<script src="/magic/cc.js"></script>
</head>

<body>
<div id="BodyBox">
<div id="MainBox">
<h1><a href="/magic/cc/">Shena'Fu's Card Creator</a></h1>

<form id="ccform" name="ccform" method="post" action="/magic/cardsave.php">
	<div id="imagediv">
		<h3>Card Image</h3>
		<p><img id="cardimage" <?php echo "src=\"$imgsrc\" alt=\"$cardname by $creator\""; ?> height="600"/></p>
		<h3>QR to Card Image</h3>
		<p style="text-align: center"><img id="imageqr" width="96" height="96" <?php echo "src=\"$imgqr\" alt=\"QR code to $cardname by $creator\""; ?>></p>
	</div>
	<div id="inputtypes">
		<div>
			<h3>Card Name</h3>
			<input type="text" name="cardname" id="cardname" value="<?php echo htmlentities($cardname);?>" maxlength="50">
		</div>
		<div>
			<h3>Mana Cost</h3>
			<input type="text" name="manacost" id="manacost" value="<?php echo htmlentities($manacost); ?>">
		</div>
		<div>
			<h3>P/T ; Loyalty</h3>
			<input type="text" name="powertoughness" id="powertoughness" value="<?php echo htmlentities($powertoughness); ?>">
		</div>
		<div>
			<h3>Subtypes</h3>
			<input type="text" name="subtype" id="subtype" value="<?php echo htmlentities($subtype); ?>">
		</div>
		<div>
			<h3>Extra</h3>
			<textarea name="extra" id="extra"><?php echo $extra; ?></textarea>
		</div>
		<div>
			<h3>Rules Text</h3>
			<textarea name="rulestext" id="rulestext" rows="12" oninput="onRulesTextUpdate(event);"><?php echo $rulestext; ?></textarea>
		</div>
		<div>
			<h3>Flavor Text</h3>
			<textarea name="flavortext" id="flavortext" rows="4"><?php echo $flavortext; ?></textarea>
		</div>
		<div>
			<h3>Rarity</h3>
			<select name="rarity" id="rarity">
				<option value="1"<?php if ($rarity=="1") echo " selected=\"selected\""; ?>>Common</option>
				<option value="2"<?php if ($rarity=="2") echo " selected=\"selected\""; ?>>Uncommon</option>
				<option value="3"<?php if ($rarity=="3") echo " selected=\"selected\""; ?>>Rare</option>
				<option value="4"<?php if ($rarity=="4") echo " selected=\"selected\""; ?>>Mythic Rare</option>
				<option value="5"<?php if ($rarity=="5") echo " selected=\"selected\""; ?>>Bank Breaking Rare</option>
			</select>
		</div>
		<div>
			<h3>Set Icon [URL]</h3>
			<input type="text" name="seticonurl" id="seticonurl" value="<?php echo $seticonurl; ?>">
		</div>
		<div>
			<h3>Type Icon [URL]</h3>
			<input type="text" name="typeiconurl" id="typeiconurl" value="<?php echo $typeiconurl; ?>">
		</div>
		<div>
			<h3>Watermark [URL]</h3>
			<input type="text" name="wmurl" id="wmurl" value="<?php echo $wmurl; ?>">
		</div>
		<div>
			<h3>Overlay [URL]</h3>
			<input type="text" name="overlayurl" id="overlayurl" value="<?php echo $overlayurl; ?>">
		</div>
		<div>
			<h3>Art [URL]</h3>
			<input type="text" name="arturl" id="arturl" value="<?php echo $arturl; ?>">
		</div>
		<div>
			<h3>Artist</h3>
			<input type="text" name="artist" id="artist" value="<?php echo htmlentities($artist); ?>">
		</div>
		<div>
			<h3>Creator</h3>
			<input type="text" name="creator" id="creator" value="<?php echo htmlentities($creator); ?>" oninput="update_creator_link(this.value);">
		</div>
	</div>
	<div id="selecttypes">
		<div class="flexrow">
			<div>
				<h3>Color</h3>
				<select name="color" id="color">
					<option value="c"<?php if ($color=="c" || $color=="") echo " selected=\"selected\""; ?>>Colorless</option>
					<option value="w"<?php if ($color=="w") echo " selected=\"selected\""; ?>>White</option>
					<option value="u"<?php if ($color=="u") echo " selected=\"selected\""; ?>>Blue</option>
					<option value="b"<?php if ($color=="b") echo " selected=\"selected\""; ?>>Black</option>
					<option value="r"<?php if ($color=="r") echo " selected=\"selected\""; ?>>Red</option>
					<option value="g"<?php if ($color=="g") echo " selected=\"selected\""; ?>>Green</option>
					<option value="a"<?php if ($color=="a") echo " selected=\"selected\""; ?>>Artifact</option>
					<option value="m"<?php if ($color=="m") echo " selected=\"selected\""; ?>>Gold</option>
					<option value="q"<?php if ($color=="q") echo " selected=\"selected\""; ?>>Blend</option>
					<option value="o"<?php if ($color=="o") echo " selected=\"selected\""; ?>>Orange</option>
					<option value="y"<?php if ($color=="y") echo " selected=\"selected\""; ?>>Yellow</option>
					<option value="k"<?php if ($color=="k") echo " selected=\"selected\""; ?>>Cyan</option>
					<option value="p"<?php if ($color=="p") echo " selected=\"selected\""; ?>>Purple</option>
					<option value="t"<?php if ($color=="t") echo " selected=\"selected\""; ?>>Brown</option>
				</select>
			</div>
			<div>
				<h3>Border</h3>
				<select name="border" id="border">
					<option value=""<?php if ($border=="") echo " selected=\"selected\""; ?>>Spell</option>
					<option value="l"<?php if ($border=="l") echo " selected=\"selected\""; ?>>Land</option>
				</select>
			</div>
			<div>
				<h3>Frame</h3>
				<select name="frame" id="frame">
					<option value="classicshift"<?php if ($frame=="classicshift") echo " selected=\"selected\""; ?>>Classicshift</option>
					<option value="vogonhd"<?php if ($frame=="vogonhd") echo " selected=\"selected\""; ?>>Vogon HD</option>
					<option value="vogonvt"<?php if ($frame=="vogonvt") echo " selected=\"selected\""; ?>>Vogon VT</option>
					<option value="vogonls"<?php if ($frame=="vogonls") echo " selected=\"selected\""; ?>>Vogon LS</option>
					<option value="vogonfullart"<?php if ($frame=="vogonfullart") echo " selected=\"selected\""; ?>>Vogon Full Art</option>
					<option value="vogonsplit"<?php if ($frame=="vogonsplit") echo " selected=\"selected\""; ?>>Vogon Split</option>
					<option value="vogondeco"<?php if ($frame=="vogondeco") echo " selected=\"selected\""; ?>>Vogon Deco</option>
					<option value="vogongc"<?php if ($frame=="vogongc") echo " selected=\"selected\""; ?>>Vogon GC</option>
					<option value="vogonbiz"<?php if ($frame=="vogonbiz") echo " selected=\"selected\""; ?>>Vogon Biz</option>
					<option value="flip"<?php if ($frame=="flip") echo " selected=\"selected\""; ?>>Flip</option>
					<option value="classic"<?php if ($frame=="classic") echo " selected=\"selected\""; ?>>Classic</option>
					<option value="modern"<?php if ($frame=="modern") echo " selected=\"selected\""; ?>>Modern</option>
					<option value="planeswalker"<?php if ($frame=="planeswalker") echo " selected=\"selected\""; ?>>Planeswalker</option>
					<option value="future"<?php if ($frame=="future") echo " selected=\"selected\""; ?>>Future</option>
					<option value="classicshiftartbg"<?php if ($frame=="classicshiftartbg") echo " selected=\"selected\""; ?>>Classic-Shifted Art</option>
					<option value="cbg"<?php if ($frame=="cbg") echo " selected=\"selected\""; ?>>CBG</option>
					<option value="cbghoriz"<?php if ($frame=="cbghoriz") echo " selected=\"selected\""; ?>>CBG Horizontal</option>
					<option value="fullart"<?php if ($frame=="fullart") echo " selected=\"selected\""; ?>>Full Art</option>
					<option value="fpm"<?php if ($frame=="fpm") echo " selected=\"selected\""; ?>>FPM</option>
					<option value="space"<?php if ($frame=="space") echo " selected=\"selected\""; ?>>Space</option>
					<option value="planeshift"<?php if ($frame=="planeshift") echo " selected=\"selected\""; ?>>Planeshift</option>
					<option value="split"<?php if ($frame=="split") echo " selected=\"selected\""; ?>>Split</option>
					<option value="playing"<?php if ($frame=="playing") echo " selected=\"selected\""; ?>>Playing Card</option>
					<option value="custom"<?php if ($frame=="custom") echo " selected=\"selected\""; ?>>Custom</option>
				<!--
					<option value="flownetic"<?php if ($frame=="flownetic") echo " selected=\"selected\""; ?>>Flownetic</option>
				-->
				</select>
			</div>
			<div>
				<h3>Symbols</h3>
				<select name="symbol" id="symbol">
					<option value=""<?php if ($symbol=="") echo " selected=\"selected\""; ?>>Default</option>
					<option value="modern"<?php if ($symbol=="modern") echo " selected=\"selected\""; ?>>Modern</option>
					<option value="classic"<?php if ($symbol=="classic") echo " selected=\"selected\""; ?>>Classic</option>
					<option value="guilds"<?php if ($symbol=="guilds") echo " selected=\"selected\""; ?>>Guilds</option>
					<option value="future"<?php if ($symbol=="future") echo " selected=\"selected\""; ?>>Future</option>
					<option value="space"<?php if ($symbol=="space") echo " selected=\"selected\""; ?>>Space</option>
					<option value="pokemon"<?php if ($symbol=="pokemon") echo " selected=\"selected\""; ?>>Pokémon</option>
					<option value="mbs"<?php if ($symbol=="mbs") echo " selected=\"selected\""; ?>>MBS</option>
					<option value="suits"<?php if ($symbol=="suits") echo " selected=\"selected\""; ?>>Suits</option>
				</select>
			</div>
			<div>
				<h3>Custom Mana</h3>
				<input type="text" name="custommana" id="custommana" value="<?php echo $custommana; ?>">
			</div>
			<div>
				<h3>Custom Frame</h3>
				<input type="text" name="customframe" id="customframe" value="<?php echo $customframe; ?>">
			</div>
		</div>
		<div class="flexcolumn">
			<div>
				<h3>Genre</h3>
				<label><input type="radio" name="genre" value="magic"<?php if ($genre=="magic") echo " checked=\"checked\""; ?> onclick="genre_onclick();">Magic</label>
				<label><input type="radio" name="genre" value="earth"<?php if ($genre=="earth") echo " checked=\"checked\""; ?> onclick="genre_onclick();">Earth</label>
				<label><input type="radio" name="genre" value="space"<?php if ($genre=="space") echo " checked=\"checked\""; ?> onclick="genre_onclick();">Space</label>
				<label><input type="radio" name="genre" value="pokemon"<?php if ($genre=="pokemon") echo " checked=\"checked\""; ?> onclick="genre_onclick();">Pokémon</label>
				<label><input type="radio" name="genre" value="mbs"<?php if ($genre=="mbs") echo " checked=\"checked\""; ?> onclick="genre_onclick();">MBS</label>
			</div>
			<div>
				<h3>Supertypes</h3>
				<div><input type="hidden" name="supertype" id="supertype" value="<?php echo $supertype; ?>"></div>
				<div><input type="checkbox" name="supertype0" id="supertype0" onclick="supertype_onclick();"><label id="label_supertype0" for="supertype0">Legendary</label></div>
				<div><input type="checkbox" name="supertype1" id="supertype1" onclick="supertype_onclick();"><label id="label_supertype1" for="supertype1">Basic</label></div>
				<div><input type="checkbox" name="supertype2" id="supertype2" onclick="supertype_onclick();"><label id="label_supertype2" for="supertype2">Snow</label></div>
				<div><input type="checkbox" name="supertype3" id="supertype3" onclick="supertype_onclick();"><label id="label_supertype3" for="supertype3">World</label></div>
				<div><input type="checkbox" name="supertype4" id="supertype4" onclick="supertype_onclick();"><label id="label_supertype4" for="supertype4">Ongoing</label></div>
				<div><input type="checkbox" name="supertype5" id="supertype5" onclick="supertype_onclick();"><label id="label_supertype5" for="supertype5">Fast</label></div>
				<div><input type="checkbox" name="supertype6" id="supertype6" onclick="supertype_onclick();"><label id="label_supertype6" for="supertype6">Delayed</label></div>
				<div><input type="checkbox" name="supertype7" id="supertype7" onclick="supertype_onclick();"><label id="label_supertype7" for="supertype7">Token</label></div>
				<div><input type="checkbox" name="supertype8" id="supertype8" onclick="supertype_onclick();"><label id="label_supertype8" for="supertype8">Elite</label></div>
				<div><input type="checkbox" name="supertype9" id="supertype9" onclick="supertype_onclick();"><label id="label_supertype9"
for="supertype9">Host</label></div>
			</div>
			<div>
				<h3>Types</h3>
				<div><input type="hidden" name="cardtype" id="cardtype" value="<?php echo $cardtype; ?>"></div>
				<div><input type="checkbox" name="cardtype0" id="cardtype0" onclick="cardtype_onclick();"><label id="label_cardtype0" for="cardtype0">Tribal</label></div>
				<div><input type="checkbox" name="cardtype1" id="cardtype1" onclick="cardtype_onclick();"><label id="label_cardtype1" for="cardtype1">Artifact</label></div>
				<div><input type="checkbox" name="cardtype2" id="cardtype2" onclick="cardtype_onclick();"><label id="label_cardtype2" for="cardtype2">Enchantment</label></div>
				<div><input type="checkbox" name="cardtype3" id="cardtype3" onclick="cardtype_onclick();"><label id="label_cardtype3" for="cardtype3">Land</label></div>
				<div><input type="checkbox" name="cardtype4" id="cardtype4" onclick="cardtype_onclick();"><label id="label_cardtype4" for="cardtype4">Creature</label></div>
				<div><input type="checkbox" name="cardtype5" id="cardtype5" onclick="cardtype_onclick();"><label id="label_cardtype5" for="cardtype5">Instant</label></div>
				<div><input type="checkbox" name="cardtype6" id="cardtype6" onclick="cardtype_onclick();"><label id="label_cardtype6" for="cardtype6">Sorcery</label></div>
				<div><input type="checkbox" name="cardtype7" id="cardtype7" onclick="cardtype_onclick();"><label id="label_cardtype7" for="cardtype7">Planeswalker</label></div>
				<div><input type="checkbox" name="cardtype8" id="cardtype8" onclick="cardtype_onclick();"><label id="label_cardtype8" for="cardtype8">Plane</label></div>
				<div><input type="checkbox" name="cardtype9" id="cardtype9" onclick="cardtype_onclick();"><label id="label_cardtype9" for="cardtype9">Scheme</label></div>
				<div><input type="checkbox" name="cardtype10" id="cardtype10" onclick="cardtype_onclick();"><label id="label_cardtype10" for="cardtype10">Emblem</label></div>
				<div><input type="checkbox" name="cardtype11" id="cardtype11" onclick="cardtype_onclick();"><label id="label_cardtype11" for="cardtype11">Hero</label></div>
				<div><input type="checkbox" name="cardtype12" id="cardtype12" onclick="cardtype_onclick();"><label id="label_cardtype12" for="cardtype12">Conspiracy</label></div>
				<div><input type="checkbox" name="cardtype13" id="cardtype13" onclick="cardtype_onclick();"><label id="label_cardtype13" for="cardtype13">Phenomenon</label></div>
				<div><input type="checkbox" name="cardtype14" id="cardtype14" onclick="cardtype_onclick();"><label id="label_cardtype14" for="cardtype14">Avatar</label></div>
			</div>
		</div>
		<script>genre_onload();</script>

		<div id="buttontypes">
			<input type="submit" id="submit_btn" value="Submit"/>
			<input type="button" onclick="reset_form();" value="Reset"/>
			<input type="button" onclick="clear_form();" value="Clear"/>
			<input type="button" onclick="retrieve_card(); return false;" value="Retrieve"/>
		</div>
	</div>
</form>
<hr>
<h2>Related Links:</h2>
<ul>
<li><a href="/magic/cchints.php" target="cchints">Read Instructions for Card Creator</a></li>
<li><!--<a id='discusslink' href='https://dissenter.com/discussion/begin?url=<?php echo get_canon_url($imgcardname, $imgcreator); ?>' target='discuss'>--> <a href='#disqus_thread'>Discuss this card</a></li>
<li><a id='creatorlink' href='/magic/cclist/<?php echo $imgcreator; ?>/0' target='<?php echo $imgcreator; ?>'>View all cards by <span><?php echo $creator; ?></span></a></li>
<li><a href="/magic/ccimport" target="ccimport">Import Cards</a></li>
</ul>
<hr>
<pre>
<a href="https://ieants.cc/smf/index.php?topic=29.0" target="forums">Support forums</a>

Please support my Patreon:
<a href="https://www.patreon.com/shenafu"><img src="https://ieants.cc/images/become_a_patron_button.png" alt="Become a Patreon"/></a>

By: Lord of Atlantis

Images by: Vogon, FirePenguinMaster, ComicBookGuy, et al.

<i class="note">(Over 146,000 cards have been created since 2009.)</i>
</pre>
</div>

<hr>
<div id="disqus_thread"></div>
<hr>
<script>
	var disqus_config = function () {
		this.page.url = "<?php echo get_canon_url($imgcardname, $imgcreator);?>";
		this.page.identifier = "<?php echo get_page_id($imgcardname, $imgcreator);?>";
	};

	(function() {
		var d = document, s = d.createElement('script');
		s.src = 'https://sfocc.disqus.com/embed.js';
		s.setAttribute('data-timestamp', +new Date());
		(d.head || d.body).appendChild(s);
	})();
</script>
<noscript>
	Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a>
</noscript>

</div>

</body>
</html>