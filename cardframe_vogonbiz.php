<?php

// vogon biz frames

class CardFrame_VogonBiz {

	////// CONSTANTS //////
	const WIDTH = 1050;
	const HEIGHT = 600;
	const BORDER = 32;
	const BOTTOMBAR = 30;
	const TINT = [
		"c" => "#DEB887"
		,"w" => "#F5F5DC"
		,"u" => "#99CCFF"
		,"b" => "#A0A0A0"
		,"r" => "#DC143C"
		,"g" => "#6B8E23"
		,"m" => "#FFD700"
	];

	////// PROPERTIES //////

	public $q; // array holding query variables
	public $dirframe, $dirmana, $dirfont; // directories to resources
	public $cardnamefont, $typefont, $manafont, $ptfont, $textfont, $italicfont;
	public $im, $mcircle, $textdivider; // images
	public $black, $white, $legendcolor, $clear; // ImagickPixel constants
	public $typetext, $newrules, $newflavor; // final type line, compounded rules and flavor
	public $tints; // tint the entire card based on color provided

	////// METHODS //////

	function __construct($qarray) {
		// data from query
		$this->q = $qarray;

		$this->typetext = cardimage_gettypeline($this->q['supertype'], $this->q['cardtype'], $this->q['subtype'], $this->q['genre'], "— ");

		// file resources
		$this->dirframe = "frames/frame_" . $this->q['frame'];
		$this->textdivider = new Imagick( "$this->dirframe/horiz-divider.png" );
		$this->dirmana = "symbols/" . ((empty($this->q['symbol'])) ? "modern" : $this->q['symbol']);
		$this->mcircle = new Imagick( "$this->dirmana/mana_circle.png");
		$this->seticon = null; //"$this->dirframe/seticon.png";
		$this->dirfont = $_SERVER['DOCUMENT_ROOT'] . "/fonts";
		$this->cardnamefont = "$this->dirfont/kelvinch-bold.ttf";
		$this->typefont = "$this->dirfont/kelvinch-bold.ttf";
		$this->manafont = "$this->dirfont/ubuntumono-regular.ttf";
		$this->ptfont = "$this->dirfont/crimson-regular.ttf";
		$this->boldfont = "$this->dirfont/crimson-bold.ttf";
		$this->textfont = "$this->dirfont/crimson-regular.ttf";
		$this->italicfont = "$this->dirfont/crimson-italic.ttf";
		$this->bolditalicfont = "$this->dirfont/crimson-bolditalic.ttf";
		$this->dyratio = 1.0;

		// drawing resources
		$this->black = new ImagickPixel("black");
		$this->white = new ImagickPixel("white");
		$this->legendcolor = new ImagickPixel("darkviolet"); // crimson #DC143C ; darkviolet #9400D3 ;
		$this->clear = new ImagickPixel("transparent");
		$this->draw = new ImagickDraw();
		$this->manadraw = new ImagickDraw();
		$this->im = null;

	}

	function drawBackground() {
		// background base

		$this->q['cardback'] = str_replace( "jpg", "png", $this->q['cardback']);
		$this->q['cardback'] = str_replace( "lcard", "card", $this->q['cardback']);
		$cardback = "$this->dirframe/" . $this->q['cardback'];
		$this->im = new Imagick($cardback);
	}

	function drawBlend() {
		// blending for multicolor and hybrid
		// only for Magic/Space genres

	  if ($this->q['color']=='q' && (in_array($this->q['genre'], array('magic','space') ))) {

			$this->blendoptions = cardfuncs_getblendoptions($this->q['manacost'], $this->q['extra'] . $this->q['rulestext']);
			$options = [
				 'width' => $this::WIDTH
				 ,'height' => $this::HEIGHT
				 ,'black' => $this->black
				 ,'blendstartrate' => 0.5
				 ,'blendendrate' => 1.0
				 ,'contrast' => 5.0
				 ,'midtone' => 0.0
			];
			$this->im = cardfuncs_renderblend($options, $this);

			if (count($this->blendoptions["colors"]) == 2) {
				// blend left half separately

				$imright = clone $this->im;

				$this->blendoptions["colors"] = array_reverse($this->blendoptions["colors"]);
				$options['blendstartrate'] = 0.0;
				$options['blendendrate'] = 0.5;
				$options['midtone'] = 0.5;

				$imleft = clone cardfuncs_renderblend($options, $this);
				$imleft->cropImage($this::WIDTH/2, $this::HEIGHT, 0, 0);

				$imright->compositeImage($imleft, imagick::COMPOSITE_OVER, 0, 0);
				$this->im = $imright;
			}
		}
	}

	function drawArt() {
		// art
		$options = [
			'artx' => 0
			,'arty' => 0
			,'artwidth' => $this::WIDTH
			,'artheight' => $this::HEIGHT
		];
		cardfuncs_makeArtImage($options, $this);
	}

	function drawArtistCreator() {
		// artist
		if ($this->q['artist'] != '') {

			// draw black bar at bottom
			$draw = new ImagickDraw();
			$draw->setStrokeColor($this->black);
			$draw->setFillColor($this->black);
			$draw->setStrokeOpacity(1);
			$draw->setStrokeWidth(2);
			$draw->rectangle(0, 550, 1050, 600);
			$this->im->drawImage($draw);

			$options = [
				'font' => $this->cardnamefont
				,'fontsize' => 24.0
				,'textx' => 525
				,'texty' => 578
				,'wmax' => $this::WIDTH
				,'text' => $this->q['artist']
				,'textcolor' => $this->white
				,'shadowcolor' => $this->black
				,'textalign' => imagick::ALIGN_CENTER
			];

			cardfuncs_makeLinearText($options, $this);
		}
	}

	function drawManaCost() {
		// write mana cost, simplified, align right

		$options = [
			'font' => $this->manafont
			,'fontsize' => 40.0
			,'fontcolor' => $this->black
			,'manacostx' => 995
			,'manacosty' => 86
			,'manaiconwidth' => 36
			,'manaiconheight' => 36
			,'width' => 36
			,'height' => 576
			,'vertical' => true
		];

		cardfuncs_makeManaCost($options, $this);
	}

	function drawCardName() {
		// write card name

		$options = [
			'font' => $this->cardnamefont
			,'fontsize' => 36.0
			,'textx' => 762
			,'texty' => 80
			,'wmax' => 459
			,'text' => $this->q['cardname']
			,'textcolor' => substr($this->q['supertype'], 0, 1)=='1' ? $this->legendcolor : $this->black
			,'textalign' => imagick::ALIGN_CENTER
			,'textdecoration' => imagick::DECORATION_UNDERLINE
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawPT() {
		// write company name

		$options = [
			'font' => $this->cardnamefont
			,'fontsize' => 48.0
			,'textx' => 1005
			,'texty' => 82
			,'wmax' => 614
			,'text' => $this->q['powertoughness']
			,'textcolor' => $this->black
			,'textalign' => imagick::ALIGN_RIGHT
			,'shadowcolor' => $this->white
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawType() {
		// write company motto

		$options = [
			'font' => $this->cardnamefont
			,'fontsize' => 32.0
			,'textx' => 1005
			,'texty' => 142
			,'wmax' => 614
			,'text' => $this->q['subtype']
			,'textcolor' => $this->black
			,'textalign' => imagick::ALIGN_RIGHT
			,'shadowcolor' => $this->white
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawWatermark() {
		if ($this->q['wmurl'] != '') {
			$this->q['wmurl'] = "https://ieants.cc/imgd/qr/text=" . $this->q['wmurl'];
			// watermark
			$options = [
				'wmx' => 300
				,'wmy' => 450
				,'wmw' => 180
				,'wmh' => 180
			];

			cardfuncs_makeWatermark($options, $this);
		}
	}

	function drawRulesText() {
		// write rules, flavor, extra texts

		// newrules combines type, rules, and pt
		$this->q['newrules'] = $this->q['extra'] . "\n" . $this->q['rulestext'];

		cardfuncs_conjoined_cardframe_op($this->q, ['newrules', 'flavortext'], ["cardfuncs_replaceMemtext", "cardfuncs_replaceCardTraits"]);

		// regular card
		$options = [
			'textfont' => $this->textfont
			,'boldfont' => $this->boldfont
			,'italicfont' => $this->italicfont
			,'bolditalicfont' => $this->bolditalicfont
			,'fontsize' => 36.0
			,'minfontsize' => 12.0
			,'textletterx' => 0
			,'textlettery' => 0
			,'textboxx' => 524
			,'textboxy' => 242
			,'textboxwidth' => 480
			,'textboxheight' => 298
			,'rulestext' => $this->q['newrules']
			,'flavortext' => $this->q['flavortext']
			,'dyratio' => $this->dyratio
			,'shadowcolor' => $this->white
		];

		cardfuncs_makeRulesbox($options, $this);
	}

	function drawSetIcon() {
		// draw set icon, with rarity colors filled

		$options = [
			'w' => 246
			,'h' => 246
			,'x' => 46
			,'y' => 46
			,'align' => Imagick::ALIGN_LEFT
		];

		cardfuncs_makeSetIcon($options, $this);
	}

	function drawTypeIcon() {
		// write card type icon
		$options = [
			'x' => 1040
			,'y' => 556
			,'w' => 40
			,'h' => 40
			,'cardtype' => 0
			,'align' => Imagick::ALIGN_RIGHT
		];

		cardfuncs_makeTypeIcon($options, $this);
	}

	function drawOverlay() {
        $options = [
            'ovx' => 0
            ,'ovy' => 0
            ,'ovw' => $this::WIDTH
            ,'ovh' => $this::HEIGHT
        ];

		cardfuncs_makeOverlay($options, $this);
	}

	function createFinalImage() {
		// start creation of image
		$this->drawBackground();
		//$this->drawBlend();
		$this->drawArt();
		$this->drawSetIcon();
		$this->drawWatermark();
		$this->drawPT();
		$this->drawType();
		$this->drawRulesText();
		$this->drawArtistCreator();
		$this->drawTypeIcon();
		/*
		//$this->drawCardName();
		$this->drawManaCost();
		//*/
		$this->drawOverlay();

		/* Output the image*/
		return $this->im;
	}
} // end class def

$cardframe = new CardFrame_VogonBiz($qarray);
?>
