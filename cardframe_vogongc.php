<?php

// vogon gc frames

class CardFrame_VogonGC {

	////// CONSTANTS //////
	const WIDTH = 1046;
	const HEIGHT = 750;
	const BORDER = 32;
	const BOTTOMBAR = 30;
	const TINT = [
		"c" => "#DEB887"
		,"w" => "#F5F5DC"
		,"u" => "#99CCFF"
		,"b" => "#A0A0A0"
		,"r" => "#DC143C"
		,"g" => "#6B8E23"
		,"m" => "#FFD700"
	];

	////// PROPERTIES //////

	public $q; // array holding query variables
	public $dirframe, $dirmana, $dirfont; // directories to resources
	public $cardnamefont, $typefont, $manafont, $ptfont, $textfont, $italicfont;
	public $im, $mcircle, $textdivider; // images
	public $black, $white, $legendcolor, $clear; // ImagickPixel constants
	public $typetext, $newrules, $newflavor; // final type line, compounded rules and flavor
	public $tints; // tint the entire card based on color provided

	////// METHODS //////

	function __construct($qarray) {
		// data from query
		$this->q = $qarray;

		$this->typetext = cardimage_gettypeline($this->q['supertype'], $this->q['cardtype'], $this->q['subtype'], $this->q['genre'], "— ");

		// file resources
		$this->dirframe = "frames/frame_" . $this->q['frame'];
		$this->textdivider = new Imagick( "$this->dirframe/horiz-divider.png" );
		$this->dirmana = "symbols/" . ((empty($this->q['symbol'])) ? "modern" : $this->q['symbol']);
		$this->mcircle = new Imagick( "$this->dirmana/mana_circle.png");
		$this->seticon = null; //"$this->dirframe/seticon.png";
		$this->dirfont = $_SERVER['DOCUMENT_ROOT'] . "/fonts";
		$this->cardnamefont = "$this->dirfont/anaktoria.otf";
		$this->typefont = "$this->dirfont/anaktoria.otf";
		$this->manafont = "$this->dirfont/anaktoria.otf";
		$this->ptfont = "$this->dirfont/anaktoria.otf";
		$this->boldfont = "$this->dirfont/anaktoria.otf";
		$this->textfont = "$this->dirfont/anaktoria.otf";
		$this->italicfont = "$this->dirfont/anaktoria.otf";
		$this->bolditalicfont = "$this->dirfont/anaktoria.otf";
		$this->dyratio = 1.0;

		// drawing resources
		$this->black = new ImagickPixel("black");
		$this->white = new ImagickPixel("white");
		$this->legendcolor = new ImagickPixel("darkviolet"); // crimson #DC143C ; darkviolet #9400D3 ;
		$this->clear = new ImagickPixel("transparent");
		$this->draw = new ImagickDraw();
		$this->manadraw = new ImagickDraw();
		$this->im = new Imagick();
		$this->im->newImage($this::WIDTH, $this::HEIGHT, $this->white, "png");

	}

	function drawBackground() {
		// background base

		global $ENUM_ARTIFACT;

		// colorless artifact
		if ($this->q['color'] == 'c' && substr($this->q['cardtype'], $ENUM_ARTIFACT, 1)=='1') {
			$this->q['cardback'] = 'a' . substr( $this->q['cardback'], 1);
		}

		$cardback = "$this->dirframe/" . $this->q['cardback'];
		if (file_exists($cardback)) {
			$bg = new Imagick($cardback);
         $this->im->compositeImage($bg, imagick::COMPOSITE_OVER, 0, 0);
      }
	}

	function drawBlend() {
		// blending for multicolor and hybrid
		// only for Magic/Space genres

	  if ($this->q['color']=='q' && (in_array($this->q['genre'], array('magic','space') ))) {

			$this->blendoptions = cardfuncs_getblendoptions($this->q['manacost'], $this->q['extra'] . $this->q['rulestext']);
			$options = [
				 'width' => $this::WIDTH
				 ,'height' => $this::HEIGHT
				 ,'black' => $this->black
				 ,'blendstartrate' => 0.5
				 ,'blendendrate' => 1.0
				 ,'contrast' => 5.0
				 ,'midtone' => 0.0
			];
			$this->im = cardfuncs_renderblend($options, $this);

			if (count($this->blendoptions["colors"]) == 2) {
				// blend left half separately

				$imright = clone $this->im;

				$this->blendoptions["colors"] = array_reverse($this->blendoptions["colors"]);
				$options['blendstartrate'] = 0.0;
				$options['blendendrate'] = 0.5;
				$options['midtone'] = 0.5;

				$imleft = clone cardfuncs_renderblend($options, $this);
				$imleft->cropImage($this::WIDTH/2, $this::HEIGHT, 0, 0);

				$imright->compositeImage($imleft, imagick::COMPOSITE_OVER, 0, 0);
				$this->im = $imright;
			}
		}
	}

	function drawArt() {
		// art
		$options = [
			'artx' => 0
			,'arty' => 0
			,'artwidth' => 523
			,'artheight' => 720
		];
		cardfuncs_makeArtImage($options, $this);
	}

	function drawArtistCreator() {
		// artist

		$options = [
			'font' => $this->cardnamefont
			,'fontsize' => 18.0
			,'textx' => 7
			,'texty' => 735
			,'wmax' => $this::WIDTH / 2
			,'text' => "Illus. by " . $this->q['artist']
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
			,'textalign' => imagick::ALIGN_LEFT
		];

		cardfuncs_makeLinearText($options, $this);

		// creator

		$options = [
			'font' => $this->cardnamefont
			,'fontsize' => 18.0
			,'textx' => 1039
			,'texty' => 735
			,'wmax' => $this::WIDTH / 2
			,'text' => "Created by " . $this->q['creator']
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
			,'textalign' => imagick::ALIGN_RIGHT
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawManaCost() {
		// write mana cost, simplified, align right

		$options = [
			'font' => $this->manafont
			,'fontsize' => 40.0
			,'fontcolor' => $this->black
			,'manacostx' => 995
			,'manacosty' => 86
			,'manaiconwidth' => 36
			,'manaiconheight' => 36
			,'width' => 36
			,'height' => 576
			,'vertical' => true
		];

		cardfuncs_makeManaCost($options, $this);
	}

	function drawCardName() {
		// write card name

		$options = [
			'font' => $this->cardnamefont
			,'fontsize' => 36.0
			,'textx' => 762
			,'texty' => 80
			,'wmax' => 459
			,'text' => $this->q['cardname']
			,'textcolor' => substr($this->q['supertype'], 0, 1)=='1' ? $this->legendcolor : $this->black
			,'textalign' => imagick::ALIGN_CENTER
			,'textdecoration' => imagick::DECORATION_UNDERLINE
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawWatermark() {
		// watermark
        $options = [
            'wmx' => 786
            ,'wmy' => 360
            ,'wmw' => 520
            ,'wmh' => 720
        ];

		cardfuncs_makeWatermark($options, $this);
	}

	function drawRulesText() {
		// write rules, flavor, extra texts

		// newrules combines type, rules, and pt
		$this->q['newrules'] = "<" . $this->q['powertoughness'] . ">\n<" .$this->typetext . ">\n" . $this->q['extra'] . "\n" . $this->q['rulestext'];

		cardfuncs_conjoined_cardframe_op($this->q, ['newrules', 'flavortext'], ["cardfuncs_replaceMemtext", "cardfuncs_replaceCardTraits"]);

		// regular card
		$options = [
			'textfont' => $this->textfont
			,'boldfont' => $this->boldfont
			,'italicfont' => $this->italicfont
			,'bolditalicfont' => $this->bolditalicfont
			,'fontsize' => 36.0
			,'minfontsize' => 12.0
			,'textletterx' => 0
			,'textlettery' => 0
			,'textboxx' => 578
			,'textboxy' => 110
			,'textboxwidth' => 414
			,'textboxheight' => 540
			,'rulestext' => $this->q['newrules']
			,'flavortext' => $this->q['flavortext']
			,'dyratio' => $this->dyratio
			//,'shadowcolor' => $this->white
		];

		cardfuncs_makeRulesbox($options, $this);
	}

	function drawSetIcon() {
		// draw set icon, with rarity colors filled

		$options = [
			'w' => 48
			,'h' => 48
			,'x' => 8
			,'y' => 670
			,'align' => Imagick::ALIGN_LEFT
		];

		cardfuncs_makeSetIcon($options, $this);
	}

	function drawTypeIcon() {
		// write card type icon
		$options = [
			'x' => 1030
			,'y' => 20
			,'w' => 40
			,'h' => 40
			,'genre' => $this->q['genre']
			,'cardtype' => $this->q['cardtype']
			,'align' => Imagick::ALIGN_RIGHT
		];

		cardfuncs_makeTypeIcon($options, $this);
	}

	function drawOverlay() {
        $options = [
            'ovx' => 0
            ,'ovy' => 0
            ,'ovw' => 1046
            ,'ovh' => 720
        ];

		cardfuncs_makeOverlay($options, $this);
	}

	function createFinalImage() {
		// start creation of image
		$this->drawBackground();
		$this->drawBlend();
		$this->drawArt();
		$this->drawWatermark();
		$this->drawSetIcon();
		$this->drawCardName();
		$this->drawTypeIcon();
		//$this->drawType();
		$this->drawManaCost();
		//$this->drawPT();
		$this->drawRulesText();
		$this->drawArtistCreator();
		$this->drawOverlay();

		/* Output the image*/
		return $this->im;
	}
} // end class def

$cardframe = new CardFrame_VogonGC($qarray);
?>
