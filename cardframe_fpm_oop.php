<?php

// FPM frames

class CardFrame_FPM {

	////// CONSTANTS //////
	const WIDTH = 375;
	const HEIGHT = 523;

	////// PROPERTIES //////

	public $dirframe, $dirmana, $dirfont; // directories to resources
	public $q; // array holding query variables
	public $cardnamefont, $typefont, $manafont, $ptfont, $textfont, $italicfont;
	public $im, $mcircle, $textdivider; // images
	public $black, $white, $clear; // ImagickPixel constants
	public $typetext; // final type line

	////// METHODS //////

	function __construct($qarray) {
		// data from query
		$this->q = $qarray;

		$this->typetext = cardimage_gettypeline($this->q['supertype'], $this->q['cardtype'], $this->q['subtype'], $this->q['genre'], "— ");

		$this->q['cardback'] = str_replace('.jpg', '.png', $this->q['cardback']);
		if (preg_match('/artifact/i', $this->typetext) && preg_match('/^c/', $this->q['cardback'])) {
			$this->q['cardback'] = "acard.png";
		}

		$this->q['rulestext'] = $this->q['extra'] . "\n". $this->q['rulestext'];

		// file resources
		$this->dirframe = "frames/frame_" . $this->q['frame'];
		$this->textdivider = new Imagick( "$this->dirframe/horiz-divider.png" );
		$this->dirmana = "symbols/" . ((empty($this->q['symbol'])) ? "modern" : $this->q['symbol']);
		$this->mcircle = new Imagick( "$this->dirmana/mana_circle.png");
		$this->seticon = "$this->dirframe/seticon.png";
		$this->dirfont = $_SERVER['DOCUMENT_ROOT'] . "/fonts";
		$this->cardnamefont = "$this->dirfont/kelvinch-bold.ttf";
		$this->typefont = "$this->dirfont/kelvinch-bold.ttf";
		$this->manafont = "$this->dirfont/ubuntumono-regular.ttf";
		$this->ptfont = "$this->dirfont/crimson-regular.ttf";
		$this->boldfont = "$this->dirfont/crimson-bold.ttf";
		$this->textfont = "$this->dirfont/crimson-regular.ttf";
		$this->italicfont = "$this->dirfont/crimson-italic.ttf";
		$this->bolditalicfont = "$this->dirfont/crimson-bolditalic.ttf";
		$this->dyratio = 1.0;

		// drawing resources
		$this->black = new ImagickPixel("black");
		$this->white = new ImagickPixel("white");
		$this->clear = new ImagickPixel("transparent");
		$this->draw = new ImagickDraw();
		$this->manadraw = new ImagickDraw();
		$this->im = new Imagick();
		$this->im->newImage($this::WIDTH, $this::HEIGHT, $this->white, "png");
	}

	function drawBackground() {
		// background base

        $cardback = "$this->dirframe/" . $this->q['cardback'];
		if (file_exists($cardback)) {
			$bg = new Imagick($cardback);
            $this->im->compositeImage($bg, imagick::COMPOSITE_OVER, 0, 0);
        }
	}

	function drawBlend() {
		// blending for multicolor and hybrid

		$this->blendoptions = cardfuncs_getblendoptions($this->q['manacost'], $this->q['rulestext']);
		$options = [
			'width' => $this::WIDTH
			,'height' => $this::HEIGHT
			,'black' => $this->black
			,'blendstartrate' => 0.4
			,'blendendrate' => 0.6
			,'contrast' => 20
			,'midtone' => 0.166
		];
		if ($this->q['color']=='q') {
			$options['blendframemask'] = "blend_frame_mask.png";
		}
		$this->im = cardfuncs_renderblend($options, $this);
	}

	function drawArt() {
		// art

		$options = [
			'artx' => 45
			,'arty' => 66
			,'artwidth' => 311
			,'artheight' => 223
		];

		cardfuncs_makeArtImage($options, $this);
	}

	function drawArtistCreator() {
		// artist and creator

		$options = [
			'font' => $this->typefont
			,'fontsize' => 10.0
			,'textx' => 60
			,'texty' => 501
			,'wmax' => 290
			,'text' => "Illus. by " . $this->q['artist']
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
		];

		cardfuncs_makeLinearText($options, $this);

		$options = [
			'font' => $this->typefont
			,'fontsize' => 10.0
			,'textx' => 60
			,'texty' => 515
			,'wmax' => 290
			,'text' => "Created by " . $this->q['creator']
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawWatermark() {
		// watermark

		$options = [
			'wmx' => 202
			,'wmy' => 414
			,'wmw' => 280
			,'wmh' => 136
		];

		cardfuncs_makeWatermark($options, $this);
	}

	function drawManaCost() {
		// write mana cost, simplified, align right

		$options = [
			'font' => $this->manafont
			,'fontsize' => 24.0
			,'fontcolor' => $this->black
			,'manacostx' => 21
			,'manacosty' => 57
			,'manaiconwidth' => 24
			,'manaiconheight' => 24
			,'width' => 24
			,'height' => 523
			,'vertical' => true
		];

		cardfuncs_makeManaCost($options, $this);
	}

	function drawCardName() {
		// write card name
		$x = 47;
		$iconx = 0;
		try {
			if (isset($this->typeicon)) {
				$iconx = $this->typeicon->getimagewidth() + 5;
			}
		}
		catch (Exception $e) {
			cclog( "\n" . '$icon error : ' . $e);
		}

		$options = [
			'font' => $this->cardnamefont
			,'fontsize' => 18.0
			,'textx' => $x + $iconx
			,'texty' => 34
			,'wmax' => 305 - $iconx
			,'text' => $this->q['cardname']
			,'textcolor' => $this->black
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawType() {
		// write card type

		$options = [
			'font' => $this->typefont
			,'fontsize' => 13.0
			,'textx' => 110
			,'texty' => 322
			,'wmax' => 243
			,'text' => $this->typetext
			,'textcolor' => $this->black
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawPT() {
		// write power toughness

		$pticons = new Imagick($this->dirframe . '/ptshieldsword.png');
		$this->im->compositeImage($pticons, imagick::COMPOSITE_OVER, 23, 358);

		$pts = explode('/', $this->q['powertoughness'], 2);
		for ($i=0, $ct=count($pts); $i<$ct; $i++) {
			$pts[$i] = trim($pts[$i]);
		}

		if ($pts[0] != '') {
			$options = [
				'font' => $this->ptfont
				,'fontsize' => 24.0
				,'textx' => 33
				,'texty' => 384
				,'wmax' => 27
				,'text' => $pts[0]
				,'textcolor' => $this->white
				,'shadowcolor' => $this->black
				,'textalign' => imagick::ALIGN_CENTER
			];

			cardfuncs_makeLinearText($options, $this);
		}
		if (isset($pts[1]) && $pts[1] != '') {
			$options = [
				'font' => $this->ptfont
				,'fontsize' => 24.0
				,'textx' => 33
				,'texty' => 443
				,'wmax' => 27
				,'text' => $pts[1]
				,'textcolor' => $this->white
				,'shadowcolor' => $this->black
				,'textalign' => imagick::ALIGN_CENTER
			];

			cardfuncs_makeLinearText($options, $this);
		}
	}

	function drawRulesText() {
		// write rules and flavor texts

		cardfuncs_conjoined_cardframe_op($this->q, ['rulestext', 'flavortext'], ["cardfuncs_replaceMemtext", "cardfuncs_replaceCardTraits"]);

		$options = [
			'textfont' => $this->textfont
			,'boldfont' => $this->boldfont
			,'italicfont' => $this->italicfont
			,'bolditalicfont' => $this->bolditalicfont
			,'fontsize' => 32.0
			,'minfontsize' => 6.0
			,'textletterx' => 0
			,'textlettery' => 0
			,'textboxx' => 57
			,'textboxy' => 344
			,'textboxwidth' => 282
			,'textboxheight' => 141
			,'rulestext' => $this->q['rulestext']
			,'flavortext' => $this->q['flavortext']
		];

		cardfuncs_makeRulesbox($options, $this);
	}

	function drawSetIcon() {
		// draw set icon, with rarity colors filled

		$options = [
			'w' => 21
			,'h' => 21
			,'x' => 76
			,'y' => 304
			,'align' => Imagick::ALIGN_LEFT
			,'keepratio' => false
		];

		cardfuncs_makeSetIcon($options, $this);
	}

	function drawTypeIcon() {
		// write card type icon
		$options = [
			'x' => 47
			,'y' => 22
			,'w' => 20
			,'h' => 20
		];

		$this->typeicon = cardfuncs_makeTypeIcon($options, $this);
	}

	function drawOverlay() {
        $options = [
            'ovx' => 17
            ,'ovy' => 17
            ,'ovw' => 340
            ,'ovh' => 488
        ];

		cardfuncs_makeOverlay($options, $this);
	}

	function createFinalImage() {
		// start creation of image
		$this->drawArt();
		$this->drawBackground();
		$this->drawBlend();
		$this->drawWatermark();
		$this->drawSetIcon();
		$this->drawTypeIcon();
		$this->drawCardName();
		$this->drawManaCost();
		$this->drawType();
		$this->drawPT();
		$this->drawRulesText();
		$this->drawArtistCreator();
		$this->drawOverlay();

		/* Output the image*/
		return $this->im;
	}
} // end class def

$cardframe = new CardFrame_FPM($qarray);
?>
