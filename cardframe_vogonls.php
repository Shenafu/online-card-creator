<?php

// vogon ls frames

class CardFrame_VogonLS {

	////// CONSTANTS //////
	const WIDTH = 1046;
	const HEIGHT = 750;
	const BORDER = 32;
	const BOTTOMBAR = 30;
	const TINT = [
		"c" => "#DEB887"
		,"w" => "#F5F5DC"
		,"u" => "#99CCFF"
		,"b" => "#A0A0A0"
		,"r" => "#DC143C"
		,"g" => "#6B8E23"
		,"m" => "#FFD700"
	];

	////// PROPERTIES //////

	public $q; // array holding query variables
	public $dirframe, $dirmana, $dirfont; // directories to resources
	public $cardnamefont, $typefont, $manafont, $ptfont, $textfont, $italicfont;
	public $im, $mcircle, $textdivider; // images
	public $black, $white, $legendcolor, $clear; // ImagickPixel constants
	public $typetext; // final type line
	public $tints; // tint the entire card based on color provided

	////// METHODS //////

	function __construct($qarray) {
		// data from query
		$this->q = $qarray;

		// file resources
		$this->dirframe = "frames/frame_" . $this->q['frame'];
		$this->textdivider = new Imagick( "$this->dirframe/horiz-divider.png" );
		$this->dirmana = "symbols/" . ((empty($this->q['symbol'])) ? "modern" : $this->q['symbol']);
		$this->mcircle = new Imagick( "$this->dirmana/mana_circle.png");
		$this->seticon = "$this->dirframe/seticon.png";
		$this->dirfont = $_SERVER['DOCUMENT_ROOT'] . "/fonts";
		$this->cardnamefont = "$this->dirfont/kelvinch-bold.ttf";
		$this->typefont = "$this->dirfont/kelvinch-bold.ttf";
		$this->manafont = "$this->dirfont/ubuntumono-regular.ttf";
		$this->ptfont = "$this->dirfont/crimson-regular.ttf";
		$this->boldfont = "$this->dirfont/crimson-bold.ttf";
		$this->textfont = "$this->dirfont/crimson-regular.ttf";
		$this->italicfont = "$this->dirfont/crimson-italic.ttf";
		$this->bolditalicfont = "$this->dirfont/crimson-bolditalic.ttf";
		$this->dyratio = 1.0;

		// drawing resources
		$this->black = new ImagickPixel("black");
		$this->white = new ImagickPixel("white");
		$this->legendcolor = new ImagickPixel("#9400D3"); // crimson #DC143C ; darkviolet #9400D3 ;
		$this->clear = new ImagickPixel("transparent");
		$this->draw = new ImagickDraw();
		$this->manadraw = new ImagickDraw();
		$this->im = new Imagick();
		$this->im->newImage($this::WIDTH, $this::HEIGHT, $this->white, "png");
		$this->im2 = null;
		$this->rulesbox = null;

	}

	function drawBackground() {
		// background base

		global $ENUM_ARTIFACT;

		// colorless artifact
		if ($this->q['color'] == 'c' && substr($this->q['cardtype'], $ENUM_ARTIFACT, 1)=='1') {
			$this->q['cardback'] = 'a' . substr( $this->q['cardback'], 1);
		}

		$cardback = "$this->dirframe/" . $this->q['cardback'];
		if (file_exists($cardback)) {
			$bg = new Imagick($cardback);
            $this->im->compositeImage($bg, imagick::COMPOSITE_OVER, 0, 0);
        }
	}

	function drawBlend() {
		// blending for multicolor and hybrid
	  // only for Magic/Space genres

	  if ($this->q['genre']=='magic' || $this->q['genre']=='space') {

			$this->blendoptions = cardfuncs_getblendoptions($this->q['manacost'], $this->q['extra'] . $this->q['rulestext']);
			$options = [
				 'width' => $this::WIDTH
				 ,'height' => $this::HEIGHT
				 ,'black' => $this->black
				 ,'blendstartrate' => 0.1
				 ,'blendendrate' => 0.9
				 ,'contrast' => 2.0
				 ,'midtone' => 0.0
				 ,'landscape' => true
			];
			if (3==count($this->blendoptions['colors'])) {
				$options['blendstartrate'] = 0.56;
				$options['blendendrate'] = 0.76;
				$options['contrast'] = 2.0;
				$options['midtone'] = -1.0;
			}
			if ($this->q['color']=='q') {
				$options['blendframemask'] = "blend_frame_mask.png";
			}
			$this->im = cardfuncs_renderblend($options, $this);
	  }
	}

	function backupImage() {
		$this->im2 = clone $this->im;
	}

	function drawArt() {
		// art
		$options = [
			'artx' => 0
			,'arty' => 72
			,'artwidth' => 1046
			,'artheight' => 648
		];

		if (!cardfuncs_makeArtImage($options, $this)) {
			// if invalid art, fill card with solid color
			$draw = new ImagickDraw();
			$draw->setFillColor($this->white);
			$draw->setStrokeColor($this->white);
			$draw->rectangle($options['artx'], $options['arty'], $options['artx']+$options['artwidth'], $options['arty']+$options['artheight']);
			$this->im->drawImage($draw);
		}
	}

	function drawArtistCreator() {
		// artist

		$options = [
			'font' => $this->typefont
			,'fontsize' => 18.0
			,'textx' => 7
			,'texty' => 735
			,'wmax' => $this::WIDTH / 2
			,'text' => "Illus. by " . $this->q['artist']
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
			,'textalign' => imagick::ALIGN_LEFT
		];

		cardfuncs_makeLinearText($options, $this);

		// creator

		$options = [
			'font' => $this->typefont
			,'fontsize' => 18.0
			,'textx' => 1039
			,'texty' => 735
			,'wmax' => $this::WIDTH / 2
			,'text' => "Created by " . $this->q['creator']
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
			,'textalign' => imagick::ALIGN_RIGHT
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawManaCost() {
		// write mana cost, simplified, align right

		$options = [
			'font' => $this->manafont
			,'fontsize' => 40.0
			,'fontcolor' => $this->black
			,'manacostx' => 995
			,'manacosty' => 86
			,'manaiconwidth' => 36
			,'manaiconheight' => 36
			,'width' => 36
			,'height' => 576
			,'vertical' => true
		];

		cardfuncs_makeManaCost($options, $this);
	}

	function drawCardName() {
		// write card name

		$options = [
			'font' => $this->cardnamefont
			,'fontsize' => 38.0
			,'textx' => 523
			,'texty' => 39
			,'wmax' => 914
			,'text' => $this->q['cardname']
			,'textcolor' => substr($this->q['supertype'], 0, 1)=='1' ? $this->legendcolor : $this->black
			,'textalign' => imagick::ALIGN_CENTER
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawType() {
		// write card type

		$this->typetext = cardimage_gettypeline($this->q['supertype'], $this->q['cardtype'], $this->q['subtype'], $this->q['genre'], "— ");

		if ($this->typetext == '') {
			return;
		}

		$settings = [
			'x' => 63
			,'y' => 664
			,'w' => 920
			,'h' => 56
			,'newy' => 66
		];
		$box = clone $this->im2;
		$box->cropImage($settings['w'], $settings['h'], $settings['x'], $settings['y']);
		$box->setImageAlphaChannel(imagick::ALPHACHANNEL_ACTIVATE);
		$box->transparentPaintImage ("#ffffff", 0.71, 65535.0, false);

		$this->im->compositeImage($box, imagick::COMPOSITE_OVER, $settings['x'], $settings['newy']);


		$options = [
			'font' => $this->typefont
			,'fontsize' => 26.0
			,'textx' => 523
			,'texty' => 96
			,'wmax' => 914
			,'angle' => 0.0
			,'text' => $this->typetext
			,'textcolor' => $this->black
			,'textalign' => imagick::ALIGN_CENTER
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawPT() {
		// write power toughness

		global $ENUM_PLANESWALKER, $ENUM_SCENE, $ENUM_CREATURE;

		if (substr($this->q['cardtype'], $ENUM_PLANESWALKER, 1)=='1' || (substr($this->q['cardtype'], $ENUM_SCENE, 1)=='1' && $this->q['powertoughness'] != '') ) {
			// is Planeswalker or Scene
			$framex = 5;
			$framey = 86;
			$framew = 90;
			$frameh = 57;

			$loyaltyFrame = new Imagick( "$this->dirframe/loyalty.png");
			$loyaltyFrame->thumbnailImage($framew, $frameh);
			$this->im->compositeImage($loyaltyFrame, imagick::COMPOSITE_OVER, $framex, $framey);

			$options = [
				'font' => $this->ptfont
				,'fontsize' => 30.0
				,'textx' => 50
				,'texty' => 114
				,'wmax' => 57
				,'text' => $this->q['powertoughness']
				,'textcolor' => $this->white
				,'textalign' => imagick::ALIGN_CENTER
			];

			cardfuncs_makeLinearText($options, $this);
		}
		else {
			// regular card
			$options = [
				'font' => $this->ptfont
				,'fontsize' => 44.0
				,'textx' => 13
				,'texty' => 86
				,'wmax' => 576
				,'angle' => 90.0
				,'text' => $this->q['powertoughness']
				,'textcolor' => $this->white
				,'shadowcolor' => $this->black
				,'textalign' => imagick::ALIGN_LEFT
			];

			cardfuncs_makeLinearText($options, $this);
		}
	}

	function drawWatermark() {
		// watermark
        $options = [
            'wmx' => 523
            ,'wmy' => 646
            ,'wmw' => 914
            ,'wmh' => 142
        ];

		cardfuncs_makeWatermark($options, $this);
	}

	function drawRulesBox() {
		if ( trim($this->q['rulestext']) . trim($this->q['flavortext']) . trim($this->q['extra']) == '') {
			return;
		}

		$settings = [
			'x' => 66
			,'y' => 518
			,'w' => 914
			,'h' => 142
			,'newy' => 574
		];

		$box = clone $this->im2;
		$box->cropImage($settings['w'], $settings['h'], $settings['x'], $settings['y']);
		$box->setImageAlphaChannel(imagick::ALPHACHANNEL_ACTIVATE);
		$box->transparentPaintImage ("#ffffff", 0.71, 65535.0, false);

		$this->rulesbox = $settings;
		$this->rulesbox['image'] = $box;

		global $ENUM_PLANESWALKER, $ENUM_SCENE;
		if ((substr($this->q['cardtype'], $ENUM_PLANESWALKER, 1)=='1')) {
			// Planeswalker enlarge rules box
			$diff = $this->rulesbox['h'] * 0.25;
			$this->rulesbox['h'] += $diff;
			$this->rulesbox['newy'] -= $diff;
			$this->rulesbox['image']->thumbnailImage($this->rulesbox['w'], $this->rulesbox['h']);
		}

		$this->im->compositeImage( $this->rulesbox['image'], imagick::COMPOSITE_DEFAULT, $this->rulesbox['x'], $this->rulesbox['newy']);
	}

	function drawRulesText() {
		// write rules, flavor, extra texts

		if ( trim($this->q['rulestext']) . trim($this->q['flavortext']) . trim($this->q['extra']) == '') {
			return;
		}

		// first join extra text into rules text
		$this->q['rulestext'] = $this->q['extra'] . "\n". $this->q['rulestext'];

		cardfuncs_conjoined_cardframe_op($this->q, ['rulestext', 'flavortext'], ["cardfuncs_replaceMemtext", "cardfuncs_replaceCardTraits"]);

		global $ENUM_PLANESWALKER, $ENUM_SCENE;

		if ((substr($this->q['cardtype'], $ENUM_PLANESWALKER, 1)=='1') || (substr($this->q['cardtype'], $ENUM_SCENE, 1)=='1' && $this->q['powertoughness'] != '') ) {
			// is Planeswalker or Scene

			$options = [
				'textfont' => $this->textfont
				,'boldfont' => $this->boldfont
				,'italicfont' => $this->italicfont
				,'bolditalicfont' => $this->bolditalicfont
				,'fontsize' => 36.0
				,'minfontsize' => 12.0
				,'textcolor' => $this->black
				,'textletterx' => 0
				,'textlettery' => 0
				,'textboxx' => 100
				,'textboxy' => $this->rulesbox['newy'] + 2
				,'textboxwidth' => 878
				,'textboxheight' => $this->rulesbox['h'] - 4
				,'dyratio' => $this->dyratio

				,'lcfontsize' => 28.0
				,'loyaltycostx' => 5
				,'loyaltycosty' => 518
				,'loyaltycostw' => 90
				,'loyaltycosth' => 57

				,'dividerx' => 66
				,'dividerw' => 910
			];

			cardfuncs_makeRulesboxPW($options, $this);

		}
		else {
			// regular card

			$options = [
				'textfont' => $this->textfont
				,'boldfont' => $this->boldfont
				,'italicfont' => $this->italicfont
				,'bolditalicfont' => $this->bolditalicfont
				,'fontsize' => 36.0
				,'minfontsize' => 12.0
				,'textletterx' => 0
				,'textlettery' => 0
				,'textboxx' => 74
				,'textboxy' => $this->rulesbox['newy'] + 2
				,'textboxwidth' => 904
				,'textboxheight' => 138
				,'rulestext' => $this->q['rulestext']
				,'flavortext' => $this->q['flavortext']
				,'dyratio' => $this->dyratio
			];

			cardfuncs_makeRulesbox($options, $this);
		}
	}

	function drawSetIcon() {
		// draw set icon, with rarity colors filled

		$options = [
			'w' => 48
			,'h' => 48
			,'x' => 13
			,'y' => 13
			,'align' => Imagick::ALIGN_LEFT
		];

		cardfuncs_makeSetIcon($options, $this);
	}

	function drawTypeIcon() {
		// write card type icon
		$options = [
			'x' => 1031
			,'y' => 13
			,'w' => 48
			,'h' => 48
			,'align' => Imagick::ALIGN_RIGHT
			,'genre' => $this->q['genre']
			,'cardtype' => $this->q['cardtype']
		];

		cardfuncs_makeTypeIcon($options, $this);
	}

	function drawOverlay() {
        $options = [
            'ovx' => 0
            ,'ovy' => 0
            ,'ovw' => 1046
            ,'ovh' => 720
        ];

		cardfuncs_makeOverlay($options, $this);
	}

	function createFinalImage() {
		// start creation of image
		$this->drawBackground();
		$this->drawBlend();
		$this->backupImage();
		$this->drawArt();
		$this->drawSetIcon();
		$this->drawCardName();
		$this->drawTypeIcon();
		$this->drawType();
		$this->drawManaCost();
		$this->drawPT();
		$this->drawRulesBox();
		$this->drawWatermark();
		$this->drawRulesText();
		$this->drawArtistCreator();
		$this->drawOverlay();

		/* Output the image*/
		return $this->im;
	}
} // end class def

$cardframe = new CardFrame_VogonLS($qarray);
?>
