<?php

// future frames

class CardFrame_Future {

	////// CONSTANTS //////
	const WIDTH = 375;
	const HEIGHT = 523;

	////// PROPERTIES //////

	public $dirframe, $dirmana, $dirfont; // directories to resources
	public $q; // array holding query variables
	public $cardnamefont, $typefont, $manafont, $ptfont, $textfont, $italicfont;
	public $im, $mcircle, $textdivider; // images
	public $black, $white, $clear; // ImagickPixel constants
	public $typetext; // final type line

	////// METHODS //////

	function __construct($qarray) {
		// data from query
		$this->q = $qarray;

		$this->typetext = cardimage_gettypeline($this->q['supertype'], $this->q['cardtype'], $this->q['subtype'], $this->q['genre'], "— ");

		$this->cb = preg_replace('/^(.*)card.jpg/', '$1', $this->q['cardback']);
		if (preg_match('/artifact/i', $this->typetext) && preg_match('/^c/', $this->q['cardback'])) {
			$this->q['cardback'] = "acard.jpg";
		}

		$this->q['rulestext'] = $this->q['extra'] . "\n". $this->q['rulestext'];

		// file resources
		$this->dirframe = "frames/frame_" . $this->q['frame'];
		$this->textdivider = new Imagick( "$this->dirframe/horiz-divider.png" );
		$this->dirmana = "symbols/" . ((empty($this->q['symbol'])) ? "future" : $this->q['symbol']);
		$this->mcircle = new Imagick( "$this->dirmana/mana_circle.png");
		$this->seticon = "$this->dirframe/seticon.png";
		$this->dirfont = $_SERVER['DOCUMENT_ROOT'] . "/fonts";
		$this->cardnamefont = "$this->dirfont/kelvinch-bold.ttf";
		$this->typefont = "$this->dirfont/kelvinch-bold.ttf";
		$this->manafont = "$this->dirfont/ubuntumono-regular.ttf";
		$this->ptfont = "$this->dirfont/crimson-regular.ttf";
		$this->boldfont = "$this->dirfont/crimson-bold.ttf";
		$this->textfont = "$this->dirfont/crimson-regular.ttf";
		$this->italicfont = "$this->dirfont/crimson-italic.ttf";
		$this->bolditalicfont = "$this->dirfont/crimson-bolditalic.ttf";
		$this->dyratio = 1.0;

		// drawing resources
		$this->black = new ImagickPixel("black");
		$this->white = new ImagickPixel("white");
		$this->clear = new ImagickPixel("transparent");
		$this->draw = new ImagickDraw();
		$this->manadraw = new ImagickDraw();
		$this->im = new Imagick();
		$this->im->newImage($this::WIDTH, $this::HEIGHT, $this->white, "png");
	}

	function drawBackground() {
		// background base

		$cardback = "$this->dirframe/" . $this->q['cardback'];
		if (file_exists($cardback)) {
			$bg = new Imagick($cardback);
			$this->im->compositeImage($bg, imagick::COMPOSITE_OVER, 0, 0);
		}
	}

	function drawBlend() {
		// blending for multicolor and hybrid

		$this->blendoptions = cardfuncs_getblendoptions($this->q['manacost'], $this->q['rulestext']);
		$options = [
			'width' => $this::WIDTH
			,'height' => $this::HEIGHT
			,'black' => $this->black
			,'blendstartrate' => 0.4
			,'blendendrate' => 0.6
			,'contrast' => 20
			,'midtone' => 0.166
		];
		$this->im = cardfuncs_renderblend($options, $this);
	}

	function drawArt() {
		// art

		$options = [
			'artx' => 32
			,'arty' => 46
			,'artwidth' => 327
			,'artheight' => 309
			,'mask' => $this->dirframe . '/mask_image.png'
		];

		cardfuncs_makeArtImage($options, $this);

		// overlays for typeline and rules box
		$dstx = 24;
		$dsty = 320;
        $maskfile = "$this->dirframe/$this->cb" . 'textbox.png';
        if (file_exists($maskfile)) {
            $mask = new Imagick($maskfile);
            $mask->transparentPaintImage($this->white, 0.0, 0, false);
            $this->im->compositeImage($mask, imagick::COMPOSITE_OVER, $dstx, $dsty);
        }

		$dstx = 20;
		$dsty = 20;
        $maskfile = "$this->dirframe/$this->cb" . 'typeline.png';
        if (file_exists($maskfile)) {
            $mask = new Imagick($maskfile);
            $mask->transparentPaintImage($this->white, 0.0, 0, false);
            $this->im->compositeImage($mask, imagick::COMPOSITE_OVER, $dstx, $dsty);
        }
	}

	function drawArtistCreator() {
		// artist and creator

		$options = [
			'font' => $this->typefont
			,'fontsize' => 13.0
			,'textx' => 30
			,'texty' => 482
			,'wmax' => 250
			,'text' => "Illus. by " . $this->q['artist']
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
		];

		cardfuncs_makeLinearText($options, $this);

		$options = [
			'font' => $this->typefont
			,'fontsize' => 13.0
			,'textx' => 30
			,'texty' => 500
			,'wmax' => 250
			,'text' => "Created by " . $this->q['creator']
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawWatermark() {
		// watermark

		$options = [
			'wmx' => 191
			,'wmy' => 399
			,'wmw' => 310
			,'wmh' => 135
		];

		cardfuncs_makeWatermark($options, $this);
	}

	function drawManaCost() {
		// write mana cost, simplified, align right

		$manapos = [
			[45, 73]
			,[30, 104]
			,[25, 141]
			,[25, 181]
			,[30, 221]
			,[54, 260]
		];

		$options = [
			'font' => $this->manafont
			,'fontsize' => 32.0
			,'fontcolor' => $this->black
			,'manacostx' => 343
			,'manacosty' => 33
			,'manaiconwidth' => 32
			,'manaiconheight' => 32
			,'width' => 375
			,'height' => 523
			,'manapos' => $manapos
		];

		cardfuncs_makeManaCost($options, $this);
	}

	function drawCardName() {
		// write card name

		$options = [
			'font' => $this->cardnamefont
			,'fontsize' => 20.0
			,'textx' => 69
			,'texty' => 46
			,'wmax' => 275
			,'text' => $this->q['cardname']
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawTypeIcon() {
		// write card type icon
		$options = [
			'x' => 24
			,'y' => 23
			,'w' => 21
			,'h' => 21
			,'invert' => strpos('cwm', substr($this->q['cardback'], 0, 1)) === FALSE
			,'genre' => $this->q['genre']
			,'cardtype' => $this->q['cardtype']
			,'keepratio' => false
		];

		cardfuncs_makeTypeIcon($options, $this);
	}

	function drawType() {
		// write card type

		$options = [
			'font' => $this->typefont
			,'fontsize' => 16.0
			,'textx' => 48
			,'texty' => 315
			,'wmax' => 278
			,'text' => $this->typetext
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawPT() {
		// write power toughness

		global $ENUM_CREATURE;

		$ptimgx = 287;
		$ptimgy = 455;

		// draw PT frame
		if (substr($this->q['cardtype'], $ENUM_CREATURE, 1)=='1' || ($this->q['powertoughness'] != "")) {
            $pt = preg_replace('/^(.*)card.jpg/', '/$1pt.png', $this->q['cardback']);
            $pt = "$this->dirframe/$pt";
            if (file_exists($pt)) {
                $ptimage = new Imagick($pt);
                $this->im->compositeImage($ptimage, imagick::COMPOSITE_OVER, $ptimgx, $ptimgy);
            }
		}

		// write power / toughness, centered
		$options = [
			'font' => $this->ptfont
			,'fontsize' => 22.0
			,'textx' => 320
			,'texty' => 487
			,'wmax' => 50
			,'text' => $this->q['powertoughness']
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
			,'textalign' => imagick::ALIGN_CENTER
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawRulesText() {
		// write rules and flavor texts

		cardfuncs_conjoined_cardframe_op($this->q, ['rulestext', 'flavortext'], ["cardfuncs_replaceMemtext", "cardfuncs_replaceCardTraits"]);

		$options = [
			'textfont' => $this->textfont
			,'boldfont' => $this->boldfont
			,'italicfont' => $this->italicfont
			,'bolditalicfont' => $this->bolditalicfont
			,'fontsize' => 26.0
			,'minfontsize' => 6.0
			,'textletterx' => 4
			,'textlettery' => 4
			,'textboxx' => 35
			,'textboxy' => 334
			,'textboxwidth' => 308
			,'textboxheight' => 129
			,'rulestext' => $this->q['rulestext']
			,'flavortext' => $this->q['flavortext']
		];

		cardfuncs_makeRulesbox($options, $this);
	}

	function drawSetIcon() {
		// draw set icon, with rarity colors filled

		$options = [
			'w' => 21
			,'h' => 21
			,'x' => 334
			,'y' => 306
			,'align' => Imagick::ALIGN_LEFT
			,'keepratio' => false
		];

		cardfuncs_makeSetIcon($options, $this);
	}

	function drawOverlay() {
        $options = [
            'ovx' => 19
            ,'ovy' => 19
            ,'ovw' => 340
            ,'ovh' => 492
        ];

		cardfuncs_makeOverlay($options, $this);
	}

	function createFinalImage() {
		// start creation of image
		$this->drawBackground();
		$this->drawBlend();
		$this->drawArt();
		$this->drawWatermark();
		$this->drawSetIcon();
		$this->drawCardName();
		$this->drawManaCost();
		$this->drawType();
		$this->drawTypeIcon();
		$this->drawPT();
		$this->drawRulesText();
		$this->drawArtistCreator();
		$this->drawOverlay();

		/* Output the image*/
		return $this->im;
	}
} // end class def

$cardframe = new CardFrame_Future($qarray);
?>
