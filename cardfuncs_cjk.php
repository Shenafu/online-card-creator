<?php

// globals

$HYBRIDMANA = array(
	// c/d
	"w/u" => "wu",
	"u/w" => "wu",
	"w/b" => "wb",
	"b/w" => "wb",
	"w/r" => "rw",
	"r/w" => "rw",
	"w/g" => "gw",
	"g/w" => "gw",
	"u/b" => "ub",
	"b/u" => "ub",
	"u/r" => "ur",
	"r/u" => "ur",
	"u/g" => "gu",
	"g/u" => "gu",
	"b/r" => "br",
	"r/b" => "br",
	"b/g" => "bg",
	"g/b" => "bg",
	"r/g" => "rg",
	"g/r" => "rg",
	// phyrexian mana
	"p/c" => "pc",
	"c/p" => "pc",
	"p/w" => "pw",
	"w/p" => "pw",
	"p/u" => "pu",
	"u/p" => "pu",
	"p/b" => "pb",
	"b/p" => "pb",
	"p/r" => "pr",
	"r/p" => "pr",
	"p/g" => "pg",
	"g/p" => "pg"
);

$GENRES = [
	'magic' => [
		'supertypes' => ["Legendary", "Basic", "Snow", "World", "Ongoing", "Fast", "Delayed", "Token", "Elite", "Host"],
		'types' => ["Tribal", "Artifact", "Enchantment", "Land", "Creature", "Instant", "Sorcery", "Planeswalker", "Plane", "Scheme", "Emblem", "Hero", "Conspiracy", "Phenomenon"]
	],

	'earth' => [
		'supertypes' => ["Famous", "Basic", "Magical", "Global", "Ongoing", "Fast", "Delayed", "Clone", "Super", "Host"],
		'types' => ["Faction", "Object", "Agenda", "Land", "Agent", "Mod", "Event", "Paragon", "Scene", "Point", "Paradigm", "Hero", "Villain", "Power"]
	],

	'space' => [
		'supertypes' => ["Mythical", "Primary", "Dark", "Universe", "Ongoing", "Fast", "Delayed", "Clone", "Mega", "Host"],
		'types' => ["Faction", "Device", "Augment", "Resource", "Unit", "Tactic", "Strategy", "Galaxystalker", "Galaxy", "Scheme", "Paradigm", "Hero", "Conspiracy", "Phenomenon"]
	],

	'pokemon' => [
		'supertypes' => ["Baby", "Basic", "Stage 1", "Stage 2", "", "", "Special", "", "", ""],
		'types' => ["", "", "", "Energy", "", "Trainer", "", "Pokémon", "", "", "", "", "", ""]
	],

	'mbs' => [
		'supertypes' => ["", "", "", "", "", "", "", "", "", ""],
		'types' => ["", "Equipment", "", "Battleground", "Unit", "", "Action", "", "", "", "", "", "", ""]
	],
];

$ENUM_ARTIFACT = 1;
$ENUM_CREATURE = 4;
$ENUM_PLANESWALKER = 7;
$ENUM_SCENE = 8;


// define functions

function replaceName($name, $text) {
	// turn ~ or ~this~ into cardname
	// after the first replacement, use only the short name
	// short name = part before the first comma in name
	// '/(~this~|~)/'
	$shortName = preg_replace('/(.+),.*/u', "$1", $name, 1);
	$text = preg_replace('/(~~)/u', $shortName, $text);
	$text = preg_replace('/(~this~|~)/u', $name, $text, 1);
	$text = preg_replace('/(~this~|~)/u', $shortName, $text);

	return $text;
}

function cardfuncs_getblendoptions($manacost, $rulestext) {
	$blendoptions = [];
	$blendoptions['colors'] = [];

	// deduce mana symbols in rules text and add it to manacost for parsing
	preg_match_all('/\{(.*?)}/', strtolower($rulestext), $matches);
	$manacost .= implode($matches[0]);

	// extract mana from manacost
	$manacost = preg_replace('/(.)/', '$1,', $manacost);
	$manacost = explode(',', $manacost);

	// parse manacost
	foreach($manacost as $value) {
//echo $value;
		if ( preg_match('/[bgruw]/', $value)) {
			if (!in_array($value, $blendoptions['colors'])) {
				$blendoptions['colors'][] = $value;
//echo "<br>" . $value;
			}
		}
	}

	// test debug
	//$blendoptions["colors"][] = "g";
	//$blendoptions["colors"][] = "u";
	//$blendoptions["hybrid"] = true;

	return $blendoptions;
}


// http://phpimagick.com/Tutorial/imagickCompositeGen
function cardfuncs_renderblend($options, &$frame) {
	$defaults = [
		'width' => 375
		,'height' => 523
		,'black' => "#000"
		,'blendstartrate' => 0.4
		,'blendendrate' => 0.6
		,'contrast' => 20
		,'midtone' => 0.0
		,'extension' => "jpg"
	];
	$options = array_merge($defaults, $options);
	// set variables within local scope
	foreach($options as $key => $option) {
		// this is to make sure that only intended arguments are passed
		if(isset($defaults[$key])) {
			${$key} = $option;
		}
	}

	$im = $frame->im;
	$dirframe = $frame->dirframe;
	$cardback = $frame->q['cardback'];
	$blendoptions = $frame->blendoptions;
	$blendstart = $width * $blendstartrate;
	$blendend = $width * $blendendrate;
	$blendframemask = "$dirframe/multicolor_blend_card.png";

	if (isset($blendoptions["colors"]) && count($blendoptions["colors"]) == 2) {
		$blendim = new Imagick();
		$blendim->newImage($width, $height, $black, "png");

		$land = preg_match('/lcard.jpg/', $cardback) ? 'l' : '';
		$blendleft = new Imagick($dirframe . "/" . $blendoptions["colors"][0] . $land . "card.$extension");
		$blendright = new Imagick($dirframe . "/" . $blendoptions["colors"][1] . $land . "card.$extension");

		$blendleft->cropImage($blendend, $height, 0, 0);
		$blendright->cropImage($width - $blendstart, $height, $blendstart, 0);

		$blendgradientleft = new Imagick();
		$blendgradientleft->newPseudoImage($height, $blendend, 'gradient:black-white');
		$quantum = $blendgradientleft->getQuantum();
		$blendgradientleft->sigmoidalContrastImage(true, $contrast, $midtone * $quantum);
		$blendgradientleft->rotateImage('black', 90);

		$blendgradientright = clone $blendgradientleft;
		$blendgradientright->rotateImage('black', 180);

		$blendleft->compositeImage($blendgradientleft, imagick::COMPOSITE_COPYOPACITY, 0, 0);
		$blendim->compositeImage($blendleft, imagick::COMPOSITE_ATOP, 0, 0);

		$blendright->compositeImage($blendgradientright, imagick::COMPOSITE_COPYOPACITY, 0, 0);
		$blendim->compositeImage($blendright, imagick::COMPOSITE_BLEND, $blendstart, 0);

		$blendmask = new Imagick($blendframemask);
		$blendim->compositeImage($blendmask, imagick::COMPOSITE_COPYOPACITY, 0, 0);

		$im->compositeImage($blendim, imagick::COMPOSITE_OVER, 0, 0);
	}

	return $im;
}

function cardimage_gettypeline($supertype, $cardtype, $subtype, $genre = "magic", $delimiter = "— ") {
	global $GENRES;
	$typetext = "";
	$genre = strtolower($genre);
//cclog("\n\ngenre: " . $genre);

	extract($GENRES[$genre]);
//cclog("\nsupertypes count: " . count($supertypes));
//cclog("\ntypes count: " . count($types));

	for ($i=-1; isset($supertypes[++$i]);) {
		if ( substr($supertype, $i, 1)=='1' ) {
			$typetext .= $supertypes[$i] . " ";
		}
	}
	for ($i=-1; isset($types[++$i]);) {
		if ( substr($cardtype, $i, 1)=='1' ) {
			$typetext .= $types[$i] . " ";
		}
	}

	$subtype = str_replace('--', '—', $subtype);

	$typetext .= ($typetext != "" && $subtype != "" ? $delimiter : '') . $subtype;

	return $typetext;
}

function cardfuncs_makeTypeIcon($options, &$frame) {
	// draw card type icon

	global $GENRES;

	$defaults = [
		'x' => 46,
		'y' => 46,
		'w' => 36,
		'h' => 36,
		'invert' => false,
		'genre' => 'magic',
		'cardtype' => '0'
	];
	$options = array_merge($defaults, $options);
	// set variables within local scope
	foreach($options as $key => $option) {
		// this is to make sure that only intended arguments are passed
		if(isset($defaults[$key])) {
			${$key} = $option;
		}
	}

	$dirgenre = "genres/$genre";

	$type2icon = $GENRES[$genre]['types'];

	// trim useless type = tribal
	$cardtype = '0' . substr($cardtype, 1, count($type2icon));

	if (intval($cardtype) == 0) {
		// no card type assigned
		return;
	}

	// draw icon
	if (substr_count($cardtype, '1') > 1) {
		// multiple types
		$iconfile = "$dirgenre/type_" . ($invert ? "white_" : "") . "multiple.png";
		if (file_exists($iconfile)) {
			$icon = new Imagick($iconfile);
			$icon->thumbnailImage($w, $h);
			$frame->im->compositeImage($icon, imagick::COMPOSITE_OVER, $x, $y);
		}

	}
	else {
		// assign single card type to image file name
		$icontype = strtolower($type2icon[ strpos($cardtype, '1') ]);
		$iconfile = "$dirgenre/type_" . ($invert ? "white_" : "") . "$icontype.png";
		if (file_exists($iconfile)) {
			$icon = new Imagick($iconfile);
			$icon->thumbnailImage($w, $h);
			$frame->im->compositeImage($icon, imagick::COMPOSITE_OVER, $x, $y);
		}
	}
}

function ccwrap($fontSize, $angle, $fontFace, $string, $width) {
	$ret = "";
	$arr = explode(' ', $string);
	foreach ( $arr as $word ) {
		$teststring = $ret.' '.$word;
		$testbox = imagettfbbox($fontSize, $angle, $fontFace, $teststring);
		if ( $testbox[2] > $width ) {
			$ret.=($ret==""?"":"\n").$word;
		} else {
			$ret.=($ret==""?"":" ").$word;
		}
	}
	return $ret;
}

function gdwrap($fontSize, $angle, $fontFace, $string, $width) {
	$ret = "";
	$arr = explode(' ', $string);
	foreach ( $arr as $word ) {
		$teststring = $ret.' '.$word;
		$testbox = imagettfbbox($fontSize, $angle, $fontFace, $teststring);
		if ( $testbox[2] > $width ) {
			$ret.=($ret==""?"":"\n").$word;
		} else {
			$ret.=($ret==""?"":" ").$word;
		}
	}
	return $ret;
}

function cardfuncs_makeRulesboxImage($options, $frame) {
	global $HYBRIDMANA;

	// write rules and flavor text
	// print text word by word and mana icons

	$defaults = [
		'textfont' => "../fonts/crimson-regular.ttf"
		,'boldfont' => "../fonts/crimson-bold.ttf"
		,'italicfont' => "../fonts/crimson-italic.ttf"
		,'bolditalicfont' => "../fonts/crimson-bolditalic.ttf"
		,'fontcjk' => '../fonts/sourcehan-regular.otf'
		,'fontsize' => 26.0
		,'minfontsize' => 6.0
		,'angle' => 0
		,'textcolor' => "#000"
		,'textletterx' => 0
		,'textlettery' => 0
		,'textboxx' => 38
		,'textboxy' => 329
		,'textboxwidth' => 301
		,'textboxheight' => 139
		,'rulestext' => ''
		,'flavortext' => ''
		,'shadowcolor' => FALSE
		,'dyratio' => 1.0
	];
	$options = array_merge($defaults, $options);
	// set variables within local scope
	foreach($options as $key => $option) {
		// this is to make sure that only intended arguments are passed
		if(isset($defaults[$key])) {
			${$key} = $option;
		}
	}

	$im = $frame->im;
	$draw = $frame->draw;
	$manadraw = $frame->manadraw;
	$clear = $frame->clear;
	$dirmana = $frame->dirmana;
	$mcircle = $frame->mcircle;
	$textdivider = isset($frame->textdivider) ? $frame->textdivider : null;
	$newlineratio = 1.30;
	$flavorlinepadding = 1.60;
	$fontsizedelta = $fontsize * 0.038;
	$kerningratio = 0.2;
	$manaratio = 0.85;

	$black = new ImagickPixel("black");
	$white = new ImagickPixel("white");
	$draw->setTextAlignment(Imagick::ALIGN_LEFT);
	$draw->setTextDecoration(Imagick::DECORATION_NO);

	// clean up text first
	// remove consecutive newlines and extra newlines
	$rulestext = trim($rulestext);
	$rulestext = preg_replace("/[\r\n]+/", "\n", $rulestext);
	$flavortext = trim($flavortext);
	$flavortext = preg_replace("/[\r\n]+/", "\n", $flavortext);

	// columns, left or right
	$columnhalf = $textboxwidth / 2;
	$columnwidth = $columnhalf * 0.98;
	$columnxleft = $textletterx + $columnwidth / 100;
	$columnxright = $columnxleft + $columnhalf + $columnwidth / 100;

	// find optimal font size
	do {
		$fontsize -= $fontsizedelta;
		$draw->setFontSize($fontsize);

		//cclog( "\n" . '$fontsize = ' .  $fontsize);

		// render rules and flavor text
		$iscolumn = false;
		$isitalic = false;
		$isbold = false;
		$isunderline = false;
		$ismanamode = false;
		$undery = $fontsize * 1.15;
		$undery1 = $undery + 1;
		$textbox = new Imagick();
		$letterx = $letterxstart = $recordx = $textletterx;
		$lettery = $textlettery;
		$boxwidth = $textboxwidth;
		$columnystart = $textlettery;
		$columnymax = $textlettery;
		$columnheight = 0;
		$currfont = $textfont;
		$draw->setFont($currfont);
		$draw->setFillColor($textcolor);
		$timg = new Imagick();
		$mana = '';
		$manadraw->setFont($currfont);
		$manadraw->setTextAlignment(imagick::ALIGN_CENTER);
		$bbox = $im->queryFontMetrics($draw, "fg");
		$bboxheight = $fontsize; //$bbox['ascender'] - $bbox['descender'];
		$dy = max($bboxheight * $dyratio, 6);
		$dynew = $dy * $newlineratio;
		$manaiconfactor = $bboxheight * $manaratio;
		$manaiconwidth = $bboxheight * $manaratio;
		$manaiconheight = $bboxheight * $manaratio;
		$kerning = $fontsize * $kerningratio * $manaratio;

		$textbox->newImage($textboxwidth, $textboxheight + $textboxheight, $clear);

		// Rules Text
		$text = $rulestext;
		while ($text != "") {
			// check columns
			if (preg_match('/^[\n ]*(<<<<|>>>>|<<>>)[\n ]*/', $text, $matches)) {
				$recordx = $textboxwidth;
				$columntype = $matches[1];
				$text = preg_replace('/^([\n ]*(<<<<|>>>>|<<>>)[\n ]*)/', "", $text);

				// draw lines
				$drawrect = new ImagickDraw();
				$drawrect->setFillColor( '#000' );
				// vertical line
				if ($iscolumn) {
					$drawrect->clear();
					$drawrect->rectangle( $columnhalf, $columnystart, $columnhalf-1, $columnymax + $dynew);
					$textbox->drawImage($drawrect);
				}

				switch ($columntype) {
					case '<<<<':
						$columnheight = 0;
						$boxwidth = $columnwidth;
						$letterx = $letterxstart = $columnxleft;
						if ($iscolumn) {
							$lettery = $columnymax + $dynew;
						}
						else {
							$lettery = $lettery + $dynew;
						}
						$columnymax = $columnystart = $lettery;
						$liney = $columnystart;
						$iscolumn = true;
						break;
					case '>>>>':
						if (!$iscolumn) {
							// previously not a column
							$columnymax = $columnystart = $lettery = $lettery + $dynew;
							$liney = $columnystart;
						}
						else if ($letterx >= $columnxright) {
							// previously a right column
							$columnymax = $columnystart = $lettery = $columnymax + $dynew;
							$liney = $columnystart;
						}
						else {
							// previously a left column
							$lettery = $columnystart;
							$liney = $columnystart;
						}
						$iscolumn = true;
						$columnheight = 0;
						$boxwidth = $columnwidth;
						$letterx = $letterxstart = $columnxright;
						break;
					case '<<>>':
					default:
						if ($iscolumn) {
							$lettery = $columnymax + $dynew;
						}
						else {
							$lettery = $lettery + $dynew;
						}
						$columnymax = $columnystart = $lettery;
						$iscolumn = false;
						$boxwidth = $textboxwidth;
						$letterx = $letterxstart = $textletterx;
						$liney = $lettery;
						break;
				}

				// horizontal line
				$drawrect->clear();
				$drawrect->rectangle( 0, $liney, $textboxwidth, $liney+1);
				$textbox->drawImage($drawrect);

			}
			elseif ($ismanamode) {				
				if (preg_match('/^}+/u', $text, $matches)) {
					$ismanamode = false;
					$text = preg_replace('/^}+/u', "", $text);
					continue;
				}
				
				// start mana symbols
				$manaimg = new Imagick();
				$manaimg->newImage( $boxwidth, $manaiconheight, $clear);
				$manaimgw = 0;
				$manafound = false;

				if ( preg_match('/^([0-9wubrgpc]\/[0-9wubrgpc])/i', $text)) {
					// is hybrid mana

					if (preg_match('/^([wubrgpc]\/[wubrgpc])/i', $text, $hybmatch)) {
						// c/d or d/c
						$mana = strtolower($hybmatch[0]);
						if (isset($HYBRIDMANA[$mana])) {
							try {
								$halfvalue = $HYBRIDMANA[$mana];
								$mc = new Imagick( "$dirmana/mana_$halfvalue.png" );
								$manafound = true;
							} catch (ImagickException $e) {
								cclog ( $e );
							}
						}
					}
					elseif (preg_match('/^([0-9wubrg]\/[0-9wubrg])/i', $text, $hybmatch)) {
						// 2/c or c/2
						$halfcolor = strtolower(preg_replace('/^.*([wubrg]).*/i', "$1", $hybmatch[0]));
						$halfvalue = preg_replace('/^.*([0-9]).*/', "$1", $hybmatch[0]);
						try {
							$mc = new Imagick( "$dirmana/mana_n$halfcolor.png" );
							$manafound = true;
							$mana = '...';
							$mcw = $mc->getImageWidth();
							$mch = $mc->getImageHeight();
							$manadraw->setFontSize( $mcw * 0.7 );
							$mcw = $mcw * .4;
							$mch = $mch * .5;
							$mc->annotateImage($manadraw, $mcw, $mch, 0, $halfvalue);
						} catch (ImagickException $e) {
							cclog ( $e );
						}
					}
				}
				elseif ( preg_match('/^[a-z]/i', $text)) {
					// is valid color
					try {
						$mana = strtolower(substr($text, 0, 1));
						$mc = new Imagick( "$dirmana/mana_$mana.png");
						$manafound = true;
					} catch (ImagickException $e) {
						cclog ( $e );
					}
				}
				
				if (!$manafound) {
					// is any other text
					$mana = strtoupper(substr($text, 0, 1));
					$mc = clone $mcircle;
					$mcw = $mc->getImageWidth();
					$mch = $mc->getImageHeight();
					$manadraw->setFontSize( $mcw * 1.0 );
					$manadraw->setFillColor($black);
					$mc->annotateImage($manadraw, $mcw * .52, $mch * .75, 0, $mana);
				}

				if (isset($mc)) {
					$mch = $manaiconheight;
					$mcw = $mch * $mc->getImageWidth() / $mc->getImageHeight();
					$mc->scaleImage( $mcw, $mch );

					$manaimg->compositeImage( $mc, imagick::COMPOSITE_OVER, $manaimgw, 0 );
					$manaimgw += $mcw + 1;
				}
				unset($mc);
				
				$text = substr($text, strlen($mana));

				// punctuation directly after mana symbols
				preg_match('/^}+([,.:]+)/', $text, $matches);
				$posttext = !isset($matches[1]) ? '' : $matches[1];
				$bbox = $timg->queryFontMetrics($draw, $posttext);
				$bboxw = max($bbox['originX'], 1);
				$bboxh = max($bbox['textHeight'], 1);
				$timg->newImage($bboxw, $bboxh, $clear);
				if ($posttext != '') {
					$text = substr($text, strlen($matches[0]));
					$ismanamode = false;
					if ($shadowcolor){
						$draw->setFillColor($shadowcolor);
						$timg->annotateImage($draw, 1, $fontsize+1, 0, $posttext);
						/*
						$draw->setFillColor($textcolor);
						$timg->annotateImage($draw, -1, $fontsize-1, 0, $posttext);
						//*/
					}
					$draw->setFillColor($textcolor);
					$timg->annotateImage($draw, 0, $fontsize, 0, $posttext);
				}

				// check if mana + text goes past text box borders
				if ($letterx > $letterxstart && $letterx + $manaimgw + $timg->getImageWidth() - $letterxstart >= $boxwidth) {
					if ($iscolumn) {
						$columnheight += $dy;
						$columnymax = max($columnymax, $columnystart + $columnheight);
					}
					$lettery += $dy;

					// early escape
					if ($lettery >= $textboxheight) {
						if ($fontsize > $minfontsize) {
							continue;
						} else {
							break;
						}
					}

					$letterx = $letterxstart;
				}

				// finally copy mana and text to text box
				$textbox->compositeImage( $manaimg, imagick::COMPOSITE_OVER, $letterx, $lettery + $fontsize * .3 );
				$letterx += $manaimgw;
				$textbox->compositeImage( $timg, imagick::COMPOSITE_OVER, $letterx, $lettery);
				$letterx += $timg->getImageWidth();
				$recordx = max($recordx, $letterx);
			} // end mana symbols
			// normal text
			else {
				// check if starting or ending mana mode
				if (preg_match('/^{+/u', $text, $matches)) {
					$ismanamode = true;
					$text = preg_replace('/^{+/u', "", $text);
					continue;
				}

				// check italic, bold, underline, or normal font
				if (preg_match('/^\(+[^ {[<)]*/u', $text, $matches)) {
					$isitalic = true;
					$text = preg_replace('/^\(+[^ {[<)]*/u', "", $text);
					$t = $matches[0];
				}
				elseif (preg_match('/^\)+/u', $text, $matches) ) {
					$isitalic = false;
					$text = preg_replace('/^\)+/u', "", $text);
					$t = $matches[0];
				}
				elseif (preg_match('/^\[+/u', $text, $matches)) {
					$isitalic = true;
					$text = preg_replace('/^\[+/u', "", $text);
					$t = "\x00";
				}
				elseif (preg_match('/^]+/u', $text, $matches) ) {
					$isitalic = false;
					$text = preg_replace('/^]+/u', "", $text);
					$t = "\x00";
				}
				elseif (preg_match('/^<+/u', $text, $matches)) {
					$isbold = true;
					$text = preg_replace('/^<+/u', "", $text);
					$t = "\x00";
				}
				elseif (preg_match('/^>+/u', $text, $matches) ) {
					$isbold = false;
					$text = preg_replace('/^>+/u', "", $text);
					$t = "\x00";
				}
				elseif (preg_match('/^_+/u', $text, $matches) ) {
					$isunderline = !$isunderline;
					$text = preg_replace('/^_+/u', "", $text);
					$t = "\x00";
				}
				else {
					$pattern = "/^(\"?[A-Za-z0-9'\/\+\-]+[.,:-]*\"?|[^({\[<)_])/u";
					preg_match("$pattern", $text, $matches);
					$text = preg_replace("$pattern", "", $text);
					$t = $matches[0];
				}

				if ($isbold && $isitalic) {
					$currfont = $bolditalicfont;
				}
				elseif ($isbold) {
					$currfont = $boldfont;
				}
				elseif ($isitalic) {
					$currfont = $italicfont;
				}
				else {
					$currfont = $textfont;
				}
				$draw->setFont($currfont);

				if (isset($t)) {
					$bbox = $textbox->queryFontMetrics($draw, $t);
					$bboxw = max($bbox['originX'], 1) + ($t=="" ? -1 : $kerning);
					$bboxh = max($bbox['textHeight'], 1);
					try {
						$timg->newImage( $bboxw, $bboxh, "none");

						if ($shadowcolor){
							$draw->setFillColor($shadowcolor);
							$timg->annotateImage($draw, 1, $fontsize+1, 0, $t);
						}
						$draw->setFillColor($textcolor);
						$timg->annotateImage($draw, 0, $fontsize, 0, $t);
								if ($isunderline) {
									 $draw->line(0, $undery, $bboxw, $undery);
									 $draw->line(0, $undery1, $bboxw, $undery1);
									 $timg->drawImage($draw);
									 $draw->resetVectorGraphics();
								}
					} catch (ImagickException $e) {
						cclog( "\n" . '$t = ' .  $t);
						cclog( "\n");
						cclog( $e );
					}

					if ( strpos( $t, ")" ) ) {
						//cclog( '$currfont = $textfont;' );
						$currfont = $textfont;
					}
				}

				// check if text goes past text box borders, or newline
				$newline = preg_match("/\n/", $t);
				if ( $newline || ($letterx > $letterxstart && $letterx - $letterxstart + $bboxw >= $boxwidth) ) {
					if ($iscolumn) {
						$columnheight += $newline ? $dynew : $dy;
						$columnymax = max($columnymax, $columnystart + $columnheight);
					}
					$lettery += $newline ? $dynew : $dy;

					// early escape
					if ($lettery >= $textboxheight) {
						if ($fontsize > $minfontsize) {
							continue 2;
						} else {
							break 2;
						}
					}

					$letterx = $letterxstart;
				}

				// finally copy text to text box, skipping leading space
				if ( ! ($letterx == $letterxstart && preg_match("/\s/", $t))) {

					try {
						$textbox->compositeImage($timg, imagick::COMPOSITE_OVER, $letterx, $lettery);
					} catch (ImagickException $e) {
						cclog( $e );
					}
					$letterx += $bboxw - $kerning;
					$recordx = max($recordx, $letterx);
				}
			} // end normal text
		} // end while text

		$isbold = false;
		$isunderline = false;

		// draw lines
		if ($iscolumn) {
			$drawrect = new ImagickDraw();
			$drawrect->setFillColor( '#000' );
			$drawrect->rectangle( $columnhalf, $columnystart, $columnhalf-1, $columnymax + $dynew);
			$textbox->drawImage($drawrect);
			$columnystart = $lettery = $columnymax;
		}

		// Flavor Text
		if ($flavortext != "") {
			$text = $flavortext;
			$letterx = $letterxstart = $textletterx;
			$iscolumn = false;
			$boxwidth = $textboxwidth;
			if ($rulestext != "") {
				$lettery += $fontsize * $flavorlinepadding;

				// horizontal divider
				if ($textdivider) {
					$dividery = $lettery;
					$lettery += $textdivider->getImageHeight();
				}
			}
			$draw->setFont($italicfont);
			$pattern = "/^([^\s]+|\s+)/u";

			while ($text != "") {
				preg_match("$pattern", $text, $matches);
				$text = preg_replace("$pattern", "", $text);
				$t = $matches[0];

				if (isset($t)) {
					$bbox = $textbox->queryFontMetrics($draw, $t);
					$bboxw = max($bbox['originX'], 1) + ($t=="" ? -1 : $kerning);
					$bboxh = max($bbox['textHeight'], 1);
					try {
						$timg->newImage( $bboxw, $bboxh, "none");

						if ($shadowcolor){
							$draw->setFillColor($shadowcolor);
							$timg->annotateImage($draw, 1, $fontsize+1, 0, $t);
							/*
							$draw->setFillColor($textcolor);
							$timg->annotateImage($draw, -1, $fontsize-1, 0, $t);
							//*/
						}
						$draw->setFillColor($textcolor);
						$timg->annotateImage($draw, 0, $fontsize, 0, $t);
					} catch (ImagickException $e) {
						cclog( "\n" . '$t = ' .  $t);
						cclog( "\n");
						cclog( $e );
					}

					if ( strpos( $t, ")" ) ) {
						//cclog( '$currfont = $textfont;' );
						$currfont = $textfont;
					}
				}

				// check if text goes past text box borders, or newline
				$newline = preg_match("/\n/", $t);
				if ( $newline || ($letterx > $letterxstart && $letterx - $letterxstart + $bboxw >= $boxwidth) ) {
					$lettery += $dy;

					// early escape
					if ($lettery >= $textboxheight) {
						if ($fontsize > $minfontsize) {
							continue 2;
						} else {
							break 2;
						}
					}

					$letterx = $letterxstart;
				}

				// finally copy text to text box, skipping leading space
				if ( ! ($letterx == $letterxstart && preg_match("/\s/", $t))) {

					try {
						$textbox->compositeImage($timg, imagick::COMPOSITE_OVER, $letterx, $lettery);
					} catch (ImagickException $e) {
						cclog( $e );
					}
					$letterx += $bboxw - $kerning;
					$recordx = max($recordx, $letterx);
				}
			}
		}
		if (isset($dividery) && $textdivider) {
			// horizontal divider
			try {
				$dividerw = $textdivider->getImageWidth();
				$dividerx = ($recordx - $dividerw) / 2;
				$dividercopy = clone $textdivider;
				if ($recordx < $dividerw) {
					$dividercopy->cropImage( $recordx, $dividercopy->getImageHeight(), ($dividerw - $recordx) / 2, 0);
					$dividerx = 0;
				}
				$textbox->compositeImage($dividercopy, imagick::COMPOSITE_OVER, $dividerx, $dividery);
			}
			catch(Exception $e) {
			}

		}

	// trim image
	$textbox->trimimage(0);
	$textbox->setImagePage(0, 0, 0, 0);

	} while (($textbox->getImageHeight() > $textboxheight) && ($fontsize > $minfontsize) );

	// trim image
	$textbox->trimimage(0);
	$textbox->setImagePage(0, 0, 0, 0);

	if ($angle != 0) {
		$textbox->rotateImage($clear, $angle);
	}

	return $textbox;
}

function cardfuncs_makeRulesbox( $options, &$frame ) {
	$defaults = [
		'fontsize' => 26.0
		,'minfontsize' => 6.0
		,'textcolor' => "#000"
		,'shadowcolor' => FALSE
		,'textletterx' => 0
		,'textlettery' => 0
		,'textboxx' => 38
		,'textboxy' => 329
		,'textboxwidth' => 301
		,'textboxheight' => 139
		,'rulestext' => ''
		,'flavortext' => ''
		,'angle' => 0
		,'xalign' => 'center'
		,'yalign' => 'center'
	];
	$options = array_merge($defaults, $options);
	// set variables within local scope
	foreach($options as $key => $option) {
		// this is to make sure that only intended arguments are passed
		if(isset($defaults[$key])) {
			${$key} = $option;
		}
	}

	$im = $frame->im;

	$box = cardfuncs_makeRulesboxImage($options, $frame);

	// consider angle, xalign, yalign
	$anglemod = cos($angle);
	$boxwidth = $box->getImageWidth();
	$boxheight = $box->getImageHeight();
	switch ($xalign) {
		case 'left':
			if ($angle==180) {
				$pastex = $textboxx - $boxwidth;
			}
			else {
				$pastex = $textboxx;
			}
			break;
		case 'right':
			if ($angle==180) {
				$pastex = $textboxx - $textboxwidth;
			}
			else {
				$pastex = $textboxx + ($textboxwidth - $boxwidth);
			}
			break;
		case 'center':
		default:
			if ($angle==180) {
				$pastex = $textboxx - $boxwidth - abs($textboxwidth - $boxwidth)/2;
			}
			else {
				$pastex = $textboxx + abs($textboxwidth - $boxwidth)/2;
			}
	}
	switch ($yalign) {
		case 'top':
			if ($angle==180) {
				$pastey = $textboxy - $boxheight;
			}
			else {
				$pastey = $textboxy;
			}
			break;
		case 'bottom':
			if ($angle==180) {
				$pastey = $textboxy - $textboxheight;
			}
			else {
				$pastey = $textboxy + ($textboxheight - $boxheight);
			}
			break;
		case 'center':
		default:
			if ($angle==180) {
				$pastey = $textboxy - $boxheight - abs($textboxheight - $boxheight)/2;
			}
			else {
				$pastey = $textboxy + abs($textboxheight - $boxheight)/2;
			}
	}
	$im->compositeImage( $box, imagick::COMPOSITE_OVER, $pastex, $pastey);
}

function cardfuncs_makeRulesboxPW( $options, &$frame) {
	$defaults = [
		'fontsize' => 20.0
		,'minfontsize' => 6.0
		,'textcolor' => "#000"
		,'shadowcolor' => FALSE
		,'textletterx' => 0
		,'textlettery' => 0
		,'textboxx' => 140
		,'textboxy' => 518
		,'textboxwidth' => 507
		,'textboxheight' => 449

		,'lcfontsize' => 23.0
		,'loyaltycostx' => 5
		,'loyaltycosty' => 518
		,'loyaltycostw' => 135
		,'loyaltycosth' => 86

		,'strokeWidth' => 5
		,'dividerColor' => "#000"
		,'dividerx' => 44
		,'dividerw' => 647
	];
	$options = array_merge($defaults, $options);
	// set variables within local scope
	foreach($options as $key => $option) {
		// this is to make sure that only intended arguments are passed
		if(isset($defaults[$key])) {
			${$key} = $option;
		}
	}

	$rulestext = trim($frame->q['rulestext']);
	$flavortext = trim($frame->q['flavortext']);
	$draw = $frame->draw;
	$im = &$frame->im;
	$dirframe = $frame->dirframe;
	$mcircle = $frame->mcircle;
	$manadraw = $frame->manadraw;
	$mcircle = $frame->mcircle;
	$white = $frame->white;
	$clear = $frame->clear;
	$textfont = $frame->textfont;
	$italicfont = $frame->italicfont;
	$loyaltyfont = $frame->ptfont;

	$padding = 3;

	// count number of abilities and flavor text paragraphs
	$abils = $rulestext != "" ? explode("\n", $rulestext) : NULL;
	$flavors = $flavortext != "" ? explode("\n", $flavortext) : NULL;
	$numabils = $abils ? count($abils) : 0;
	$numflavors = $flavors ? count($flavors) : 0;
	$totallength = (strlen($rulestext) + strlen($flavortext));

	// store expected height of each ability
	$abilheight = [];
	for ($i = 0; $i < $numabils; $i++) {
		$abil = trim($abils[$i]);
		$abilh = $textboxheight * strlen($abil) / $totallength;
		$abilheight[$i] = [$i, $abilh];
	}
	for ($i = 0; $i < $numflavors; $i++) {
		$abil = trim($flavors[$i]);
		$abilh = $textboxheight * strlen($abil) / $totallength;
		$abilheight[$i + $numabils] = [$i + $numabils, $abilh];
	}

	// sort by height
	function cmpheight($a, $b) {
		if ($a[1] == $b[1]) {
			return 0;
		}
		return ($a[1] < $b[1]) ? -1 : 1;
	}

	usort($abilheight, "cmpheight");

	// ensure minimum height per ability
	$minperheight = ($fontsize * 1.0 + $padding);
	$lastindex = $numabils - 1;
	for ($i = 0; $i < $lastindex; $i++) {
		if ($abilheight[0][1] < $minperheight) {
			$diff = $minperheight - $abilheight[0][1];
			$abilheight[0][1] += $diff;
			$abilheight[$lastindex][1] -= $diff;
			usort($abilheight, "cmpheight");
		}
		else {
			break;
		}
	}

	// sort by index
	function cmpindex($a, $b) {
		if ($a[0] == $b[0]) {
			return 0;
		}
		return ($a[0] < $b[0]) ? -1 : 1;
	}

	usort($abilheight, "cmpindex");

	// rules text
	$linex = $textboxx;
	$liney = $textboxy;
	$rect = new ImagickDraw();
	$rect->setFillColor( $dividerColor );

	for ($i = 0; $i < $numabils; $i++) {
		$abil = trim($abils[$i]);
		$abilh = $abilheight[$i][1];
		$options['textboxheight'] = $abilh - $padding * 2;

		// effect
		$rules = preg_replace("/^ *[\[\(\{<]* *[+-]? *(\d+|x) *[\]\)\}>]* *:?(.*)$/i", "$2", $abil);
		$options['rulestext'] = $rules;
		$options['flavortext'] = '';
		$box = cardfuncs_makeRulesboxImage($options, $frame);
		$im->compositeImage( $box, imagick::COMPOSITE_OVER, $textboxx, $liney + abs($abilh - $box->getImageHeight())/2);

		if ($i > 0) {
			$rect->clear();
			$rect->rectangle( $dividerx, $liney, $dividerw + $dividerx, $liney+1);
			$im->drawImage($rect);
		}

		$liney += $abilh;
	}

	// flavor text
	for ($i = 0; $i < $numflavors; $i++) {
		$abil = trim($flavors[$i]);
		$abilh = $abilheight[$i + $numabils][1];
		$options['textboxheight'] = $abilh - $padding * 2;
		$options['flavortext'] = $abil;
		$options['rulestext'] = '';
		$box = cardfuncs_makeRulesboxImage($options, $frame);
		$im->compositeImage( $box, imagick::COMPOSITE_OVER, $textboxx, $liney + abs($abilh - $box->getImageHeight())/2);

		if ($i + $numabils > 0) {
			$rect->clear();
			$rect->rectangle( $linex, $liney, $textboxwidth + $linex, $liney+1);
			$im->drawImage($rect);
		}

		$liney += $abilh;
	}

	$liney = $textboxy;
	for ($i = 0; $i < $numabils; $i++) {
		// loyalty cost
		$abil = trim($abils[$i]);
		$abilh = $abilheight[$i][1];
		$lcimgy = $liney + ($abilh-$loyaltycosth) / 2;
		$lc = preg_replace("/^ *[\[\(\{<]* *([+-]? *(\d+|x)) *[\]\)\}>]* *:?.*$/i", "$1", $abil);
		// print loyalty if valid value
		if ($abil != $lc) {
			if ($lc > 0) {
				$lcimg = new Imagick("$dirframe/loyaltyup.png");
			}
			elseif ($lc < 0) {
				$lcimg = new Imagick("$dirframe/loyaltydown.png");
			}
			else {
				$lcimg = new Imagick("$dirframe/loyaltynaught.png");
			}
			$lcimg->resizeImage($loyaltycostw, $loyaltycosth, imagick::FILTER_QUADRATIC, 1.0);
			$im->compositeImage($lcimg, imagick::COMPOSITE_OVER, $loyaltycostx, $lcimgy);
			$draw->setFillColor($white);
			$draw->setFont($loyaltyfont);
			$draw->setFontSize($lcfontsize);
			$draw->setTextAlignment(imagick::ALIGN_CENTER);
			$im->annotateImage($draw, $loyaltycostx + $loyaltycostw * 0.475, $lcimgy + $loyaltycosth * 0.62, 0, $lc);
			$draw->setTextAlignment(imagick::ALIGN_LEFT);
		}

		$liney += $abilh;
	}
}

function cardfuncs_makeRulesboxLeveler( $options, &$frame) {
	$defaults = [
		'fontsize' => 20.0
		,'minfontsize' => 6.0
		,'textcolor' => "#000"
		,'shadowcolor' => FALSE
		,'textletterx' => 0
		,'textlettery' => 0
		,'textboxx' => 140
		,'textboxy' => 518
		,'textboxwidth' => 507
		,'textboxheight' => 449
		,'levelfontsize' => 44.0
		,'levelcostx' => 5
		,'levelcosty' => 518
		,'levelcostw' => 135
		,'levelcosth' => 86
		,'strokeWidth' => 5
		,'linex' => 45
		,'linew' => 615
	];
	$options = array_merge($defaults, $options);
	// set variables within local scope
	foreach($options as $key => $option) {
		// this is to make sure that only intended arguments are passed
		if(isset($defaults[$key])) {
			${$key} = $option;
		}
	}

	$rulestext = $frame->q['rulestext'];
	$flavortext = $frame->q['flavortext'];
	$draw = $frame->draw;
	$im = &$frame->im;
	$dirframe = $frame->dirframe;
	$mcircle = $frame->mcircle;
	$manadraw = $frame->manadraw;
	$mcircle = $frame->mcircle;
	$white = $frame->white;
	$clear = $frame->clear;
	$textfont = $frame->textfont;
	$italicfont = $frame->italicfont;
	$levelfont = $frame->ptfont;
	$dividerColor = new ImagickPixel( "#000");

	// count number of levels and flavor text paragraphs
	$levels = preg_split("/;;/", $rulestext);
	$numlevels = $levels ? count($levels) : 0;
	$numflavors = $flavortext != "" ? 1 : 0;
	$numtotal = $numlevels;// + $numflavors;
	$levelh = $numtotal ? ($textboxheight - ($numtotal - 1) * $strokeWidth) / $numtotal : 0;
	$dy = $levelh + $strokeWidth;
	$fontsize *= 1 - $numtotal / 12;
	$fontsize = max($fontsize, $minfontsize);
	$options['textboxheight'] = $levelh;
	$options['fontsize'] = $fontsize;

	// dividers
	$draw->setFillColor( $dividerColor );
	for ($i = 1, $liney = $textboxy - $strokeWidth; $i < $numtotal; $i++) {
		$liney += $dy;
		$draw->rectangle( $linex, $liney, $linew + $linex, $liney+1);
		$im->drawImage($draw);
	}

	// rules text
	for ($i = 0, $lvlimgy = $textboxy + ($levelh-$levelcosth) / 2; $i < $numlevels; $i++) {
		$level = trim($levels[$i]);

		// effect
		$found = preg_match("/^ *[\[\(\{]* *(\d+ *[+-]? *\d*) *[\]\)\}]* *:?(.*)$/ms", $level, $matches);
		if ($found) {
			// new level found

			// draw level arrow and text
			$lvlimg = new Imagick("$dirframe/levelup.png");
			$lvlimg->resizeImage($levelcostw, $levelcosth, imagick::FILTER_QUADRATIC, 1.0);
			$im->compositeImage($lvlimg, imagick::COMPOSITE_OVER, $levelcostx, $lvlimgy + $dy * $i);
			$draw->setTextAlignment(imagick::ALIGN_CENTER);
			$draw->setFont($levelfont);
			$draw->setFontSize($levelfontsize);
			$im->annotateImage($draw, $levelcostx + $levelcostw * 0.4, $lvlimgy + $dy * $i + $levelcosth * 0.75, 0, $matches[1]);
			$draw->setFontSize($levelfontsize / 2);
			$im->annotateImage($draw, $levelcostx + $levelcostw * 0.4, $lvlimgy + $dy * $i + $levelcosth * 0.35, 0, "LEVEL");
			$draw->setTextAlignment(imagick::ALIGN_LEFT);

			$rules = $matches[2];
			$finaltextw = $textboxwidth - $levelcostw;
			$finaltextx = $textboxx + $levelcostw;
		}
		else {
			$rules = $level;
			$finaltextw = $textboxwidth;
			$finaltextx = $textboxx;
		}

		$options['rulestext'] = $rules;
		$options['flavortext'] = '';
		$options['textboxwidth'] = $finaltextw;
		$options['textboxx'] = $finaltextx;
		$box = cardfuncs_makeRulesboxImage($options, $frame);
		$im->compositeImage( $box, imagick::COMPOSITE_OVER, $finaltextx, $textboxy + $i * $dy + abs($levelh - $box->getImageHeight())/2);
	}

}

function isValidUrl($url){
	//cclog( "\n" . 'isValidUrl() called');
	//cclog( "\n" . 'url: '. $url);

	// first do some quick sanity checks:
	if(!$url || !is_string($url)){
		return false;
	}
	// quick check url is roughly a valid http request: ( http://blah/... )
	if( ! preg_match('/^http(s)?:\/\/[a-z0-9-]+(\.[a-z0-9-]+)*(:[0-9]+)?(\/.*)?$/i', $url) ){
		return false;
	}

	// check URL with cURL

	$ch = @curl_init($url);
	if($ch === false){
		return false;
	}

	$timeout = 9; // seconds
	$maxredirs = 5;

	@curl_setopt($ch, CURLOPT_HEADER, true);	// we want headers
	@curl_setopt($ch, CURLOPT_NOBODY, true);	// dont need body
	@curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	// catch output (do NOT print!)
	@curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);	// prevent waiting forever to get a result
	@curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);	// prevent waiting forever to get a result
	@curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.0) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.89 Safari/537.1");	// pretend we're a regular browser
	@curl_setopt($ch, CURLOPT_FOLLOWLOCATION,true);
	@curl_setopt($ch, CURLOPT_MAXREDIRS, $maxredirs);	// fairly random number, but could prevent unwanted endless redirects with followlocation=true

	@curl_exec($ch);
	if(@curl_errno($ch)){	// should be 0
		@curl_close($ch);

	//cclog( "\n" . 'curl_errno');
		return false;
	}

	$code = @curl_getinfo($ch, CURLINFO_HTTP_CODE); // note: php.net documentation shows this returns a string, but really it returns an int
	//cclog( "\n" . '$code: ' . $code);
	@curl_close($ch);
	if ($code != 200) {
	//cclog( "\n" . '$code != 200');
		return false;
	}

	//cclog( "\n" . '$arturl is good');
	//cclog( "\n" . 'isValidUrl() ended');
	return true;
}

function cardfuncs_makeLinearText($options, &$frame) {
	//cclog( "\n\n" . 'cardfuncs_makeLinearText() called');
	$defaults = [
		'font' => '../fonts/kelvinch-bold.ttf'
		,'fontcjk' => '../fonts/sourcehan-regular.otf'
		,'fontsize' => 20.0
		,'textx' => 34
		,'texty' => 48
		,'wmax' => 375
		,'angle' => 0.0
		,'text' => ''
		,'textcolor' => '#000'
		,'shadowcolor' => FALSE
		,'textalign' => imagick::ALIGN_LEFT
		,'textdecoration' => imagick::DECORATION_NO
		,'interlinespacing' => -5
	];
	$options = array_merge($defaults, $options);
	// set variables within local scope
	foreach($options as $key => $option) {
		// this is to make sure that only intended arguments are passed
		if(isset($defaults[$key])) {
			${$key} = $option;
		}
	}
/*
echo $text;
echo '<br>';
//*/
	//$frame->draw->setFont($font);
	$frame->draw->setFont($fontcjk);
	$frame->draw->setFontSize($fontsize);
	$frame->draw->setTextDecoration($textdecoration);
	$oldinterlinespacing = $frame->draw->getTextInterlineSpacing();
	$frame->draw->setTextInterlineSpacing($interlinespacing);

	$clear = new ImagickPixel('transparent');
	$timg = new Imagick();
	$timg->newImage($wmax * 2, $fontsize * 2, $clear);

	$frame->draw->setTextAlignment($textalign);
	$tx = 0;
	$ty = $fontsize;
	if ($textalign == imagick::ALIGN_CENTER) {
		$tx = $wmax;
	}
	elseif ($textalign == imagick::ALIGN_RIGHT) {
		$tx = $wmax * 2;
	}

	if ($shadowcolor) {
		$frame->draw->setFillColor($shadowcolor);
		$timg->annotateImage($frame->draw, $tx+1, $ty+1, 0, $text);
	}
	$frame->draw->setFillColor($textcolor);
	$timg->annotateImage($frame->draw, $tx, $ty, 0, $text);
	$timg->trimImage(0.0);
	$timg->setImagePage(0, 0, 0, 0);

	//$qfm = $timg->queryFontMetrics($frame->draw, $text);

	// squeeze text to within max width
	if ($timg->getImageWidth() > $wmax) {
		$timg->thumbnailImage( $wmax, $timg->getImageHeight());
	}

	// rotate angled text
	if ($angle!=0.0) {
		$timg->rotateImage($clear, $angle);
	}

	// handle aligned text
	if ($textalign == imagick::ALIGN_CENTER) {
		if ($angle==90.0 || $angle==270.0) {
			$textx -= $timg->getImageWidth()/2;
		}
		else {
			// $angle == 0.0
			$textx -= $timg->getImageWidth()/2;
		}
		$texty -= $timg->getImageHeight()/2;
	}
	elseif ($textalign == imagick::ALIGN_RIGHT) {
		if ($angle==90.0 || $angle==270.0) {
			$textx -= $timg->getImageWidth()/2;
		}
		else {
			// $angle == 0.0
			$textx -= $timg->getImageWidth();
			$texty -= $timg->getImageHeight()/2;
		}
	}
	else {
		// this means $textalign == imagick::ALIGN_LEFT
		if ($angle==270.0 ) {
			$texty -= $timg->getImageHeight();
		}
		elseif ($angle==90.0) {
			// do nothing
		}
		elseif ($angle==180.0) {
			$textx -= $timg->getImageWidth();
			$texty -= $timg->getImageHeight();
		}
		else {
			// $angle == 0.0
			$texty -= $timg->getImageHeight()/2;
		}
	}

	$frame->im->compositeImage($timg, imagick::COMPOSITE_DEFAULT, $textx, $texty);

	$frame->draw->setTextInterlineSpacing($oldinterlinespacing);
}

function cardfuncs_artExtras($art, $arturl) {
	// read URL fragment after double hashes ## for extra instructions, like crop, rotate
	if (strpos($arturl, '##') !== FALSE) {
		$MAX_INST = 5; // max instructions to execute, to limit CPU usage
		$artextra = explode('##', $arturl, $MAX_INST+2);
		$background = new ImagickPixel('transparent');

		$art = clone $art;
		$art->setImageBackgroundColor($background);

		/*/ debug
		cclog("\n");
		cclog(print_r($artextra, true));
		//*/

		// $artextra[0] = art URL
		// $artextra[1]+ = art extra instructions

		for ($i=1; $i<count($artextra) && $i<=$MAX_INST; $i++) {
			$inst = explode(',', $artextra[$i], 255);
			// $inst[0] = verb
			// $inst[1]+ = parameters

			//*/ debug
			cclog("\n");
			cclog(print_r($inst, true));
			//*/

			$verb = strtolower(trim($inst[0]));

			if ($verb == 'crop') {
				// crop, x, y, w, h
				$art->cropImage($inst[3], $inst[4], $inst[1], $inst[2]);
				$art->setImagePage(0, 0, 0, 0);
			}
			else if ($verb == 'rotate') {
				// rotate, degrees
				$art->rotateImage($background, $inst[1]);
				$art->setImagePage(0, 0, 0, 0);
			}
			else if ($verb == 'alpha') {
				// alpha, value (between 0.0 and 1.0)
				$art->evaluateImage(Imagick::EVALUATE_MULTIPLY, $inst[1], Imagick::CHANNEL_ALPHA);
				$art->setImagePage(0, 0, 0, 0);
			}
			else if ($verb == 'flip') {
				// flip (around x-axis)
				$art->flipImage();
				$art->setImagePage(0, 0, 0, 0);
			}
			else if ($verb == 'flop') {
				// flop (around y-axis)
				$art->flopImage();
				$art->setImagePage(0, 0, 0, 0);
			}
		}
	}
	return $art;
}

function cardfuncs_makeArtImage($options, &$frame) {
	//cclog( "\n\n" . 'cardfuncs_makeArtImage() called');
	$defaults = [
		'artx' => 92
		,'arty' => 92
		,'artwidth' => 566
		,'artheight' => 416
		,'mask' => false
		,'confirm' => true
	];
	$options = array_merge($defaults, $options);
	// set variables within local scope
	foreach($options as $key => $option) {
		// this is to make sure that only intended arguments are passed
		if(isset($defaults[$key])) {
			${$key} = $option;
		}
	}

	$arturl = $frame->q['arturl'];
	$im = $frame->im;

	if (isset($arturl) && $arturl!="") {
		// add http://
		if (0==preg_match("/^http(s)?:\/\//", $arturl)) {
			$arturl = "http://$arturl";
		}
		if (!isValidUrl($arturl)) {
			return false;
		}
		try {
			$art = new Imagick($arturl);
			if ($art) {
				$art = cardfuncs_artExtras($art, $arturl);

				$art->thumbnailImage($artwidth, $artheight);
				if ($mask) {
					$white = new ImagickPixel('white');
					$magenta = new ImagickPixel('magenta');
					$mask = new Imagick($mask);
					$mask->transparentPaintImage($white, 0.0, 0, false);
					$art->compositeImage($mask, imagick::COMPOSITE_OVER, 0, 0);
					$art->transparentPaintImage($magenta, 0.0, 0, false);
				}

				if ($confirm) {
					$im->compositeImage($art, imagick::COMPOSITE_OVER, $artx, $arty);
				}
				else {
					return $art;
				}

				/*
				// get number of sub-images to see if animation
				$numimages = $art->getNumberImages() - 1;
				//cclog( "\n" . '$numimages: ' . $numimages);

				if ($numimages) {
					// animate
					$images = $art->coalesceImages();
					foreach ($images as $image) {
						$image->thumbnailImage($artwidth, $artheight);
						$im->compositeImage($image, imagick::COMPOSITE_OVER, $artx, $arty);
						//$im->setImageDispose(1);
						$im->addImage($im->getImage());
					}
					$im->removeImage();
					//$im = $im->deconstructImages();
				}
				else {
					$art->thumbnailImage($artwidth, $artheight);
					$im->compositeImage($art, imagick::COMPOSITE_OVER, $artx, $arty);
				}
				//*/

			}

			//cclog( "\n" . '$im images: ' . $im->getNumberImages());

		}
		catch (ImagickException $e) {
				cclog( "\n" . '$art error : ' . $e);
		}
	}

	//cclog( "\n" . 'cardfuncs_makeArtImage() ended');
}

function cardfuncs_makeWatermark($options, &$frame) {
	//cclog( "\n\n" . 'cardfuncs_makeWatermark() called');

	$defaults = [
		'wmx' => 310
		,'wmy' => 142
		,'wmw' => 187
		,'wmh' => 399
	];
	$options = array_merge($defaults, $options);
	// set variables within local scope
	foreach($options as $key => $option) {
		// this is to make sure that only intended arguments are passed
		if(isset($defaults[$key])) {
			${$key} = $option;
		}
	}

	$im = $frame->im;
	$wmurl = $frame->q['wmurl'];

	if (isset($wmurl) && $wmurl != "") {
		// add http://
		if (0==preg_match("/^http(s)?:\/\//", $wmurl)) {
			$wmurl = "http://$wmurl";
		}
		if (isValidUrl($wmurl)) {
			try {
				//cclog( "\n" . '$wmurl: ' . $wmurl);
				$wm = new Imagick($wmurl);
				if ($wm) {
					$wm = cardfuncs_artExtras($wm, $wmurl);
					$wm->thumbnailImage($wmw, $wmh, true);
					$im->compositeImage($wm, imagick::COMPOSITE_OVER, $wmx - $wm->getImageWidth()/2, $wmy - $wm->getImageHeight()/2);
					//cclog( "\n" . '$wmurl success');
				}
			}
			catch (ImagickException $e) {
					cclog( "\n" . '$wm error : ' . $e);
			}
		}
	}
}

function cardfuncs_makeSetIcon($options, &$frame) {
	//cclog( "\n\n" . 'cardfuncs_makeSetIcon() called');

	$defaults = [
		'w' => 21
		,'h' => 21
		,'x' => 320
		,'y' => 297
		,'angle' => false
	];
	$options = array_merge($defaults, $options);
	// set variables within local scope
	foreach($options as $key => $option) {
		// this is to make sure that only intended arguments are passed
		if(isset($defaults[$key])) {
			${$key} = $option;
		}
	}

	$im = $frame->im;
	$seticonurl = $frame->q['seticonurl'];
	$rarity = $frame->q['rarity'];
	$seticondefault = isset($frame->seticon) ? $frame->seticon : null;

	// rarity and set icon
	if (isset($seticonurl) && $seticonurl != "") {
		// add http://
		if (0==preg_match("/^http(s)?:\/\//", $seticonurl)) {
			$seticonurl = "http://$seticonurl";
		}
		if (isValidUrl($seticonurl)) {
			$seticon = new Imagick($seticonurl);
		}
	}
	if (!isset($seticon) && isset($seticondefault)) {
		$seticon = new Imagick($seticondefault);
	}
	try {
		if (isset($seticon)) {
			/*
			$raritycolors = array();
			$raritycolors[1] = 'rgb( 0, 0, 0 )'; // black #000000
			$raritycolors[2] = 'rgb(  208,  208, 208 )'; // silver #c0c0c0
			$raritycolors[3] = 'rgb( 196,  170, 93 )'; // #c4aa5d
			$raritycolors[4] = 'rgb( 198, 94, 41 )'; // #c65e29
			$raritycolors[5] = 'rgb( 0,  0, 255 )'; // blue #0000ff
			//*/

			$seticon = cardfuncs_artExtras($seticon, $seticonurl);

			// target color that will be replaced by rarity colors
			$masktarget = '#ff00ff'; // magenta

			// must clone to allow transparentPaintImage and OpaquePaintImage
			$mask = clone $seticon;
			$icon = clone $mask;
			$mask->OpaquePaintImage( $masktarget, '#000000', 0, true );
			$mask->OpaquePaintImage( $masktarget, '#ffffff', 0, false );
			$mask->transparentPaintImage('#000000', 0.0, 0.0, false);
			$mask->thumbnailImage($w, $h);

			// apply rarity texture onto mask's shape
			$underlay = new Imagick("$frame->dirframe/rarity_$rarity.png");
			$underlay->thumbnailImage($w, $h);
			$underlay->compositeImage($mask, imagick::COMPOSITE_COPYOPACITY, 0, 0);
			$underlay->transparentPaintImage('#000000', 0.0, 0, false);

			// merge rarity color onto icon
			$icon->transparentPaintImage($masktarget, 0.0, 0.0, false);
			$icon->thumbnailImage($w, $h);
			$icon->compositeImage($underlay, imagick::COMPOSITE_OVER, 0, 0);

			// merge icon onto card image
			if ($angle) {
				$icon->rotateImage($frame->clear, $angle);
				if ($angle == 180) {
					$x -= $w;
					$y -= $h;
				}
			}
			$im->compositeImage($icon, imagick::COMPOSITE_OVER, $x, $y);
		}
	}
	catch (ImagickException $e) {
		cclog( "\n" . '$icon error : ' . $e);
	}
}

function cardfuncs_makeArtistCreator($options, &$frame) {
	//cclog( "\n\n" . 'cardfuncs_makeArtistCreator() called');
	$defaults = [
		'font' => "../fonts/kelvinch-bold.ttf"
		,'fontsize' => 12.0
		,'artistx' => 32
		,'artisty' => 500
		,'creatorx' => 32
		,'creatory' => 518
		,'artistalign' => Imagick::ALIGN_LEFT
		,'creatoralign' => Imagick::ALIGN_LEFT
		,'cbcolor1' => "#fff" // main color
		,'cbcolor2' => "#000" // outline color
		,'artistpre' => "Illus. by "
		,'creatorpre' => "Created by "
		,'interlinespacing' => 0
	];
	$options = array_merge($defaults, $options);
	// set variables within local scope
	foreach($options as $key => $option) {
		// this is to make sure that only intended arguments are passed
		if(isset($defaults[$key])) {
			${$key} = $option;
		}
	}

	$artist = $artistpre . $frame->q['artist'];
	$creator = $creatorpre . $frame->q['creator'];

	$draw = $frame->draw;
	$draw->setFont($font);
	$draw->setFontSize($fontsize);
	$draw->setTextAlignment($align);
	$oldinterlinespacing = $draw->getTextInterlineSpacing();
	$draw->setTextInterlineSpacing($interlinespacing);

	$im = $frame->im;

	// outline first
	$draw->setFillColor($cbcolor2);
	$im->annotateImage($draw, $artistx+1, $artisty+1, 0, $artist);
	$im->annotateImage($draw, $creatorx+1, $creatory+1, 0, $creator);

	// main color
	$draw->setFillColor($cbcolor1);
	$im->annotateImage($draw, $artistx, $artisty, 0, $artist);
	$im->annotateImage($draw, $creatorx, $creatory, 0, $creator);

	$draw->setTextInterlineSpacing($oldinterlinespacing);
}

function cardfuncs_makeManaCost($options, &$frame) {
	//cclog( "\n\n" . 'cardfuncs_makeManaCost() called');

	global $HYBRIDMANA;

	$defaults = [
		'font' => "../fonts/ubuntumono-regular.ttf"
		,'fontsize' => 18.0
		,'fontcolor' => "#000"
		,'manacostx' => 343
		,'manacosty' => 33
		,'manaiconwidth' => 18
		,'manaiconheight' => 18
		,'width' => 375
		,'height' => 523
		,'vertical' => false
		,'manaalign' => ''
		,'manapos' => false
		,'angle' => 0
	];
	$options = array_merge($defaults, $options);
	// set variables within local scope
	foreach($options as $key => $option) {
		// this is to make sure that only intended arguments are passed
		if(isset($defaults[$key])) {
			${$key} = $option;
		}
	}

	$im = $frame->im;
	$clear = $frame->clear;
	$draw = $frame->draw;
	$dirmana = $frame->dirmana;
	$mcircle = $frame->mcircle;
	$manacost = preg_replace('/(([0-9wubrgpc]\/[0-9wubrgpc])|[0-9]|[a-z])/', '$1,', $frame->q['manacost']);
	$manacost = explode(',', $manacost);
	array_pop($manacost); //removes last extra empty item
	$mcbox = new Imagick();
	$mcbox->newImage($width, $height, $clear);
	// different for vertically
	$mcboxw = 0;
	$mcboxh = 0;
	if ($vertical) {
		//$manacost = array_reverse($manacost);
		$dx = 0;
		$dy = $manaiconheight;
	}
	else {
		$dx = $manaiconwidth;
		$dy = 0;
	}
	// if manapos present
	if ($manapos) {
		$manaposi = 0;
		$manaposct = count($manapos);
	}

	$draw->setFont($font);
	$draw->setFillColor($fontcolor);
	$draw->setTextAlignment(imagick::ALIGN_CENTER);

	foreach($manacost as $value) {
		$value = trim($value);
		$manafound = false;
		//echo "<br>" . $value;
		if (preg_match('/^[a-z]$/', $value)) {
			// is valid color
		//echo "_$value";
			$file = "$dirmana/mana_$value.png";
			if (file_exists($file)) {
				$manafound = true;
				$mc = new Imagick( $file);
				$mc->thumbnailImage($manaiconwidth, $manaiconheight);
			}
		}
		elseif ( preg_match('/^([0-9wubrgpc]\/[0-9wubrgpc])$/', $value)) {
		// is hybrid mana
		//echo "_$value";
			if (preg_match('/^([wubrgpc]\/[wubrgpc])$/', $value, $matches)) {
				// c/d or d/c
				if (isset($HYBRIDMANA[ $matches[0] ])) {
					$file = "$dirmana/mana_" . $HYBRIDMANA[ $matches[0] ] . ".png";
					if (file_exists($file)) {
						$manafound = true;
						$mc = new Imagick($file);
						$mc->thumbnailImage($manaiconwidth, $manaiconheight);
					}
				}
			}
			elseif (preg_match('/^([0-9wubrg]\/[0-9wubrg])$/', $value, $matches)) {
				// 2/c or c/2
				$halfcolor = preg_replace('/^.*([wubrg]).*$/', "$1", $value);
				$halfvalue = preg_replace('/^.*([0-9]).*$/', "$1", $value);
				$mc = new Imagick();
				$mc->newImage( $manaiconwidth, $manaiconheight, $clear);
				$mchalf = new Imagick( "$dirmana/mana_n$halfcolor.png");
				if ($mchalf) {
					$manafound = true;
					$mchalf->thumbnailImage($manaiconwidth, $manaiconheight);
					$mc->compositeImage($mchalf, imagick::COMPOSITE_OVER, 0, 0);
				}
				$draw->setFontSize($fontsize * 0.6);
				$mc->annotateImage($draw, $manaiconwidth * 0.33, $manaiconheight * 0.5, 0, $halfvalue);
			}
		}
		if (!$manafound) {
			// any other text
			//echo "_$value";
			$value = strtoupper($value);
			$mc = clone $mcircle;
			$mc->thumbnailImage($manaiconwidth, $manaiconheight);
			$draw->setFontSize($fontsize);
			$mc->annotateImage($draw, $manaiconwidth * 0.5, $manaiconheight * 0.8, 0, $value);
		}

		// copy mana symbol to $mcbox
		if (isset($mc)) {
			if ($manapos) {
				$im->compositeImage($mc, imagick::COMPOSITE_OVER, $manapos[$manaposi][0], $manapos[$manaposi][1]);
			}
			else {
				$mcbox->compositeImage($mc, imagick::COMPOSITE_OVER, $mcboxw, $mcboxh);
				$mcboxw += $dx;
				$mcboxh += $dy;
			}
		}
		unset($mc);

		if ($manapos) {
			$manaposi++;
			if ($manaposi >= $manaposct) {
				return;
			}
		}

	}
	if ($manapos) {
		return;
	}

	// copy $mcbox to image

	$mcbox->trimImage(0);
	$mcbox->setImagePage(0, 0, 0, 0);
	if ($manaalign == 'CENTER') {
		$manacostx -= $mcbox->width / 2;
		$manacosty -= $mcbox->height / 2;
	}
	elseif ($vertical) {
		$manacostx = $manacostx;
		$manacosty = $manacosty;
	}
	else {
		// default align right
		$manacostx -= $mcboxw;
		$manacosty = $manacosty;
	}
	if ($angle == 180) {
		$mcbox->rotateImage($clear, 180.0);
		$manacostx -= $mcbox->getImageWidth();
		$manacosty -= $mcbox->getImageHeight();
	}
	$im->compositeImage($mcbox, imagick::COMPOSITE_OVER, $manacostx, $manacosty);

	return $mcbox->getImageGeometry();
}

function saveFinalImage($im) {
	global $SAVEFILEPNG, $SAVEFILEGIF;
	if (is_object($im) && $im->getNumberImages() > 1) {
		$savefilename = $SAVEFILEGIF;
	}
	else {
		$savefilename = $SAVEFILEPNG;
	}
	unlink($savefilename);
	if (is_object($im)){
		// is Imagick
		$im->setImageDepth(8);
		$im->writeImage($savefilename);
	}
	else {
		// is GD
		imagepng($im, $savefilename, 8);
	}
}

function sendFinalImage($im) {
	/* Output the image*/
	$im->setFirstIterator();
	$width = $im->getImageWidth();
	$height = $im->getImageHeight();

	// see if animation
	$numimages = $im->getNumberImages() - 1;
	if ($numimages && FALSE) { // not active, can't make animation
		//cclog( "\n" . 'is GIF');
		header("Content-Type: image/gif");
		// copy all images

		$out = new Imagick();
		$out->newImage($width, $height, 'black', 'gif');

		/*
		$out = $im->coalesceImages();
		//*/

		//*
		$images = $im->coalesceImages();
		foreach ($images as $frame) {
			$frame->thumbnailImage($width, $height);
			$out->compositeImage($frame, imagick::COMPOSITE_OVERLAY, 0, 0);
			$out->setImageDispose(1);
			$out->addImage($out->getImage());
		}
		$out->removeImage();
		//*/

		/*
		for ($i=0; $i <= $numimages; $i++) {
			$out->addImage($im->getImage());
			$out->setImageDispose(1);
			$im->nextImage();
		//cclog( "\n" . $out->getImage());
		}
		//*/

		//*
		$out = $out->deconstructImages();
		//cclog( "\n" . $out);
		//*/

		return $out;
		//return $im->deconstructImages();
	}
	else {
		header("Content-Type: image/png");
		$out = new Imagick();
		$out->newImage($width, $height, 'black', 'png');
		$out->compositeImage($im, imagick::COMPOSITE_OVER, 0, 0);

		//cclog( "\n" . 'is PNG');
	}

	return $out;
}

// sanitize URLs in query to prevent, e.g. recursive images
// infinite looping will use up server processes and hang website
function sanitizeUrl(&$url) {
	$unsafeURLs = [
		"ieants.cc/imgd/ymtc" => "",
		"localhost/imgd/ymtc" => "",
	];
	if (isset($url)) {
		//cclog("\n\n:Input URL: " . $url);
		$url = strtr($url, $unsafeURLs);
		//cclog("\n:Safer URL: " . $url);
	}
}

function initQueryVar(&$q, $key, $value) {
	if (!isset($q[$key])) {
		$q[$key] = $value;
	}
	$q[$key] = rawurldecode(stripslashes($q[$key]));
}

function sanitizeQuery(&$q) {
	// replace certain strings, e.g. unsafe URLS, cardname

	// init query vars
	$vars = [
		'cardname' => '',
		'creator' => ' ',
		'manacost' => '',
		'powertoughness' => '',
		'extra' => '',
		'rulestext' => '',
		'flavortext' => '',
		'rarity' => 1,
		'seticonurl' => '',
		'wmurl' => '',
		'arturl' => '',
		'artist' => '',
		'color' => 'c',
		'border' => '',
		'frame' => 'classicshift',
		'symbol' => '',
		'genre' => 'magic',
		'cardtype' => '00000000000000',
		'supertype' => '00000000',
		'subtype' => '',
	];
	foreach ($vars as $key => $value) {
		initQueryVar($q, $key, $value);
	}

	// create cardback
	$q['cardback'] = $q['color'] . $q['border'] . "card.jpg";

	// sanitize URLS
	$urllist = ['arturl', 'seticonurl', 'wmurl'];
	foreach ($urllist as $url) {
		if ($q[$url] != "") {
			sanitizeUrl($q[$url]);
			//cclog("\n:$url: " . $q[$url]);
		}
	}

	// replace names from ~	in rules, extra text
	$q['extra'] = replaceName($q['cardname'], $q['extra']);
	$q['rulestext'] = replaceName($q['cardname'], $q['rulestext']);

	// replace brackets in mana cost
	$q['manacost'] = preg_replace('/[{}\()[\],]+/', '', strtolower($q['manacost']));

	// replace strings in rules flavor, extra texts
	$texts = ['rulestext', 'flavortext', 'extra'];
	foreach ($texts as $t) {
		$q[$t] = trim($q[$t]);
		$q[$t] = preg_replace('/(\*)/', '•', $q[$t]);			// asterisk => bullet
		$q[$t] = preg_replace('/(\-\-)/', '—', $q[$t]);			// double dash => emdash
		$q[$t] = preg_replace('/([\r\n]+)/', "\n", $q[$t]);	// collapse consecutive newlines
	}

}
?>