<?php

// planeshifted frames

class CardFrame_Planeshift {

	////// CONSTANTS //////
	const WIDTH = 375;
	const HEIGHT = 523;

	////// PROPERTIES //////

	public $dirframe, $dirmana, $dirfont; // directories to resources
	public $q; // array holding query variables
	public $cardnamefont, $typefont, $manafont, $ptfont, $textfont, $italicfont;
	public $im, $mcircle, $textdivider; // images
	public $black, $white, $clear; // ImagickPixel constants
	public $typetext; // final type line

	////// METHODS //////

	function __construct($qarray) {
		// data from query
		$this->q = $qarray;

		$this->typetext = cardimage_gettypeline($this->q['supertype'], $this->q['cardtype'], $this->q['subtype'], $this->q['genre'], "— ");

		if (preg_match('/artifact/i', $this->typetext) && preg_match('/^c/', $this->q['cardback'])) {
			$this->q['cardback'] = "acard.jpg";
		}

		$this->q['rulestext'] = $this->q['extra'] . "\n". $this->q['rulestext'];

		// file resources
		$this->dirframe = "frames/frame_" . $this->q['frame'];
		$this->textdivider = new Imagick( "$this->dirframe/horiz-divider.png" );
		$this->dirmana = "symbols/" . ((empty($this->q['symbol'])) ? "modern" : $this->q['symbol']);
		$this->mcircle = new Imagick( "$this->dirmana/mana_circle.png");
		$this->seticon = "$this->dirframe/seticon.png";
		$this->dirfont = $_SERVER['DOCUMENT_ROOT'] . "/fonts";
		$this->cardnamefont = "$this->dirfont/kelvinch-bold.ttf";
		$this->typefont = "$this->dirfont/kelvinch-bold.ttf";
		$this->manafont = "$this->dirfont/ubuntumono-regular.ttf";
		$this->ptfont = "$this->dirfont/crimson-regular.ttf";
		$this->boldfont = "$this->dirfont/crimson-bold.ttf";
		$this->textfont = "$this->dirfont/crimson-regular.ttf";
		$this->italicfont = "$this->dirfont/crimson-italic.ttf";
		$this->bolditalicfont = "$this->dirfont/crimson-bolditalic.ttf";
		$this->dyratio = 1.0;

		// drawing resources
		$this->black = new ImagickPixel("black");
		$this->white = new ImagickPixel("white");
		$this->clear = new ImagickPixel("transparent");
		$this->draw = new ImagickDraw();
		$this->manadraw = new ImagickDraw();
		$this->im = new Imagick();
		$this->im->newImage($this::WIDTH, $this::HEIGHT, $this->white, "png");
	}

	function drawBackground() {
		// background base

        $cardback = "$this->dirframe/" . $this->q['cardback'];
		if (file_exists($cardback)) {
			$bg = new Imagick($cardback);
            $this->im->compositeImage($bg, imagick::COMPOSITE_OVER, 0, 0);
        }
	}

	function drawBlend() {
		// blending for multicolor and hybrid

		$this->blendoptions = cardfuncs_getblendoptions($this->q['manacost'], $this->q['rulestext']);
		$options = [
			'width' => $this::WIDTH
			,'height' => $this::HEIGHT
			,'black' => $this->black
			,'blendstartrate' => 0.4
			,'blendendrate' => 0.6
			,'contrast' => 20
			,'midtone' => 0.166
		];
		if ($this->q['color']=='q') {
			$options['blendframemask'] = "blend_frame_mask.png";
		}
		$this->im = cardfuncs_renderblend($options, $this);
	}

	function drawArt() {
		// art

		$options = [
			'artx' => 32
			,'arty' => 62
			,'artwidth' => 311
			,'artheight' => 228
		];

		cardfuncs_makeArtImage($options, $this);
	}

	function drawArtistCreator() {
		// artist and creator

		$options = [
			'font' => $this->typefont
			,'fontsize' => 13.0
			,'textx' => 60
			,'texty' => 491
			,'wmax' => 208
			,'text' => $this->q['artist']
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
		];

		cardfuncs_makeLinearText($options, $this);

		$options = [
			'font' => $this->typefont
			,'fontsize' => 13.0
			,'textx' => 60
			,'texty' => 514
			,'wmax' => 208
			,'text' => "Created by " . $this->q['creator']
			,'textcolor' => $this->white
			,'shadowcolor' => $this->black
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawWatermark() {
		// watermark

		$options = [
			'wmx' => 187
			,'wmy' => 399
			,'wmw' => 310
			,'wmh' => 142
		];

		cardfuncs_makeWatermark($options, $this);
	}

	function drawManaCost() {
		// write mana cost, simplified, align right

		$options = [
			'font' => $this->manafont
			,'fontsize' => 18.0
			,'fontcolor' => $this->black
			,'manacostx' => 343
			,'manacosty' => 33
			,'manaiconwidth' => 18
			,'manaiconheight' => 18
			,'width' => 375
			,'height' => 18
		];

		$this->mcbox = cardfuncs_makeManaCost($options, $this);
	}

	function drawCardName() {
		// write card name
		$x = 36;
		try {
			if (isset($this->typeicon)) {
				$x += $this->typeicon->getimagewidth() + 5;
			}
		}
		catch (Exception $e) {
			cclog( "\n" . '$icon error : ' . $e);
		}

		$options = [
			'font' => $this->cardnamefont
			,'fontsize' => 22.0
			,'textx' => $x
			,'texty' => 42
			,'wmax' => 339 - $this->mcbox['width'] - $x
			,'text' => $this->q['cardname']
			,'textcolor' => $this->black
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawType() {
		// write card type

		$options = [
			'font' => $this->typefont
			,'fontsize' => 16.0
			,'textx' => 36
			,'texty' => 308
			,'wmax' => 283
			,'text' => $this->typetext
			,'textcolor' => $this->black
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawPT() {
		// write power toughness

		global $ENUM_CREATURE;

		$ptimgx = 271;
		$ptimgy = 461;

		// draw PT frame
		if (substr($this->q['cardtype'], $ENUM_CREATURE, 1)=='1' || ($this->q['powertoughness'] != "")) {
			$ptfile = preg_replace('/^(.*)card.jpg/', '/$1pt.jpg', $this->q['cardback']);
            $ptfile = "$this->dirframe/$ptfile";
            if (file_exists($ptfile)) {
                $ptimage = new Imagick($ptfile);
                $this->im->compositeImage($ptimage, imagick::COMPOSITE_OVER, $ptimgx, $ptimgy);
            }
		}

		// write power / toughness, centered
		$options = [
			'font' => $this->ptfont
			,'fontsize' => 22.0
			,'textx' => 314
			,'texty' => 479
			,'wmax' => 57
			,'text' => $this->q['powertoughness']
			,'textcolor' => $this->black
			,'textalign' => imagick::ALIGN_CENTER
		];

		cardfuncs_makeLinearText($options, $this);
	}

	function drawRulesText() {
		// write rules and flavor texts

		cardfuncs_conjoined_cardframe_op($this->q, ['rulestext', 'flavortext'], ["cardfuncs_replaceMemtext", "cardfuncs_replaceCardTraits"]);

		$options = [
			'textfont' => $this->textfont
			,'boldfont' => $this->boldfont
			,'italicfont' => $this->italicfont
			,'bolditalicfont' => $this->bolditalicfont
			,'fontsize' => 26.0
			,'minfontsize' => 6.0
			,'textletterx' => 0
			,'textlettery' => 0
			,'textboxx' => 31
			,'textboxy' => 330
			,'textboxwidth' => 307
			,'textboxheight' => 135
			,'rulestext' => $this->q['rulestext']
			,'flavortext' => $this->q['flavortext']
         ,'dyratio' => 1.0
		];

		cardfuncs_makeRulesbox($options, $this);
	}

	function drawSetIcon() {
		// draw set icon, with rarity colors filled

		$options = [
			'w' => 21
			,'h' => 21
			,'x' => 344
			,'y' => 297
		];

		cardfuncs_makeSetIcon($options, $this);
	}

	function drawTypeIcon() {
		// write card type icon
		$options = [
			'x' => 32
			,'y' => 32
			,'w' => 20
			,'h' => 20
		];

		$this->typeicon = cardfuncs_makeTypeIcon($options, $this);
	}

	function drawOverlay() {
        $options = [
            'ovx' => 17
            ,'ovy' => 17
            ,'ovw' => 340
            ,'ovh' => 488
        ];

		cardfuncs_makeOverlay($options, $this);
	}

	function createFinalImage() {
		// start creation of image
		$this->drawBackground();
		$this->drawBlend();
		$this->drawArt();
		$this->drawWatermark();
		$this->drawSetIcon();
		$this->drawManaCost();
		$this->drawTypeIcon();
		$this->drawCardName();
		$this->drawType();
		$this->drawPT();
		$this->drawRulesText();
		$this->drawArtistCreator();
		$this->drawOverlay();

		/* Output the image*/
		return $this->im;
	}
} // end class def

$cardframe = new CardFrame_Planeshift($qarray);
?>
