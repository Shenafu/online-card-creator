<?php
$finalHeight = 523;
?>

<!doctype html>
<html class="shenafu" id="cardcreatorlistrandom">
<head>
	<title>Random Cards @ Shena'Fu's Card Creator</title>
	<meta  charset="utf-8" />
	<link rel="icon" type="image/jpg" href="/magic/favicon.ico"/>
<style type="text/css">
.pagenav {max-height: 4em; max-width: 100%; overflow: auto;}
.cardimage {height: <?php echo $finalHeight;?>px; }
.cardimage img {max-height: <?php echo $finalHeight;?>px; max-width: 100%; }
</style>
</head>
<body>
<div id="BodyBox">
<div id="MainBox">
<a href="/magic/ccrand"><h1> Random Cards @ Shena'Fu's Card Creator</h1></a>
<?php
include_once "cclog.php";

$perpage = isset($_GET['perpage']) && $_GET['perpage'] != '' ? $_GET['perpage'] : 12;
$perpage = min($perpage, 18);

include_once "cccreds.php"; // holds $sqlhost, $sqluser, $sqlpass, $sqldb, $sqltbl
$mysqli = new mysqli($sqlhost, $sqluser, $sqlpass, $sqldb);
if ($mysqli->connect_errno) {
	printf("Connect failed: %s\n", $mysqli->connect_error);
	exit();
}
//echo 'Connected successfully';

$table = $sqltbl;
$pagecreator = '';
if (isset($_GET['creator']) && $_GET['creator'] != '') {
	$pagecreator = $_GET['creator'];
	$query = "call get_count_by_creator('$pagecreator');";
}
else {
	$query = "call get_count();";
}
$totalcards = $mysqli->query($query)->fetch_array()[0];

echo "<p>Searching ";
if ($pagecreator != '') {
	echo "Cards by: $pagecreator";
} else {
	echo "All cards";
}
echo "<br>Total Cards: $totalcards</p>";
echo "<p>Showing $perpage random cards:</p>";

// show the cards
echo "<br>\n<div id=\"cardlist\">\n\n";

// get list of random cards
$query = ($pagecreator == '')
	 ? "call get_random($perpage);"
	 : "call get_random_by_creator($perpage, '$pagecreator');";
$mysqli->next_result(); // must clear before calling sto-pro
$mysqli->multi_query($query);

if ($result = $mysqli->store_result()) {
	while ($row = $result->fetch_assoc()) {

		$cardname = stripslashes($row['cardname']);
		$creator = stripslashes($row['creator']);
		if ($creator == '') {
			$creator = ' ';
		}

		// fix characters for mod_rewrite
		$URLcardname = rawurlencode(rawurlencode($cardname));
		$URLcreator = rawurlencode(rawurlencode($creator));
		$imagename = "$URLcreator/$URLcardname";
		$target = ''==$cardname ? '_blank' : $cardname;
		echo "<span class=\"cardimage\"><a href=\"/magic/cc/$URLcreator/$URLcardname\" target=\"$target\"><img src=\"/magic/ccimg/$imagename.png?h=$finalHeight\" alt=\"$cardname by $creator\"/></a></span>\n\n";
	}
}

echo "</div>";

?>

</div>
</div>
</body>
</html>